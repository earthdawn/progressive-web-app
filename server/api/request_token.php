<?php
namespace EarthdawnPwa;

require_once __DIR__ . '/Authentication.php';


// Allow CORS preflight
if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    http_response_code(204);
    return;
}


if($_SERVER['REQUEST_METHOD'] !== 'POST') {
    http_response_code(400);
    return;
}

$email = filter_var($_POST["email"], FILTER_VALIDATE_EMAIL);
if($email === false) {
    http_response_code(400);
    return;
}

if((new Authentication())->createAndSendToken($email)) {
    http_response_code(204);
} else {
    http_response_code(429);
}