<?php
namespace EarthdawnPwa;

require_once __DIR__ . '/Authentication.php';


// Allow CORS preflight
if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    http_response_code(204);
    return;
}


$userId = (new Authentication())->verifyAndReturnUserId();
if($userId === false) {
    http_response_code(401);
    return;
}
http_response_code(204);
return;