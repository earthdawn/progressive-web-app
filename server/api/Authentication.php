<?php
namespace EarthdawnPwa;

require_once __DIR__ . '/Config.php';
require_once __DIR__ . '/Database.php';

use DateInterval;
use DateTime;
use PDO;


class Authentication {

    public function verifyAndReturnUserId(): mixed {
		if(!isset($_SERVER['HTTP_AUTHORIZATION'])) {
			return false;
		}
        $key = explode(":", $_SERVER['HTTP_AUTHORIZATION'], 2);
        $email = base64_decode($key[0]);
        $token = base64_decode($key[1]);

        $db = Database::connect(Config::getProgressiveWebAppDb());
        $stmt = $db->prepare("SELECT id FROM users WHERE email = :email AND token = :token AND expiration > NOW()");
        $stmt->bindValue(':email', $email, PDO::PARAM_STR);
        $stmt->bindValue(':token', $token, PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetchColumn();
        $db = null;

        return $data;
	}

    public function createAndSendToken(string $email): bool {
        $db = Database::connect(Config::getProgressiveWebAppDb());
        $stmt = $db->prepare("SELECT id, token, expiration, last_token_request FROM users WHERE email = :email");
        $stmt->bindValue(':email', $email, PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);

        if($data !== false) {
            if($data['last_token_request']) {
                $last = new DateTime($data['last_token_request']);
                $oneMinuteAgo = (new DateTime())->sub(new DateInterval('PT1M'));
                if($last >= $oneMinuteAgo) {
                    return false;
                }
            }
            if($data['token']) {
                $expiration = new DateTime($data['expiration']);
                $nearFuture = (new DateTime())->add(new DateInterval('PT10M'));
                if($expiration >= $nearFuture) {
                    $this->sendEmail($email, $data['token']);
                    return true;
                }
            }
            $token = $this->createToken();
            $expiration = (new DateTime())->add(new DateInterval('P6M'));
            $stmt = $db->prepare("UPDATE users SET token = :token, expiration = :expiration, last_token_request = NOW() WHERE id = :user");
            $stmt->bindValue(':user', (int)$data['id'], PDO::PARAM_INT);
            $stmt->bindValue(':token', $token, PDO::PARAM_STR);
            $stmt->bindValue(':expiration', $expiration->format("c"), PDO::PARAM_STR);
            $stmt->execute();
            $this->sendEmail($email, $token);
        } else {
            $token = $this->createToken();
            $expiration = (new DateTime())->add(new DateInterval('P6M'));
            $stmt = $db->prepare("INSERT INTO users SET email = :email, token = :token, expiration = :expiration, last_token_request = NOW()");
            $stmt->bindValue(':email', $email, PDO::PARAM_STR);
            $stmt->bindValue(':token', $token, PDO::PARAM_STR);
            $stmt->bindValue(':expiration', $expiration->format("c"), PDO::PARAM_STR);
            $stmt->execute();
            $this->sendEmail($email, $token);
        }
        return true;
    }

    private function sendEmail(string $email, string $token) {
        $link = "https://earthdawn.behindthemirrors.de/pwa/#{$token}";
        $subject = "Earthdawn App Token";
        $message = "Hi!\r\nYou have requested a new authentication token for the Earthdawn Character app.\r\nPlease copy the following code and paste it into the Token field of the Settings section in the app:\r\n\r\n{$token}\r\n\r\nIf you have installed the app on your mobile device, you might also be able to just click the following link to transfer the token. If your app does not open but you see a warning page, then please copy and paste the token manually.\r\n{$link}\r\n\r\nCheers!\r\n  Kaspi\r\n";
        mail($email, $subject, $message, ['From' => 'earthdawn-pwa@behindthemirrors.de']);
    }

    private function createToken(): string {
        $bytes = random_bytes(20 / 4 * 3);
        return base64_encode($bytes);
    }
}
