<?php
namespace EarthdawnPwa;

require_once __DIR__ . '/Config.php';
require_once __DIR__ . '/Database.php';

use PDO;


header("Content-Type: application/javascript; charset=utf-8");
echo "Earthdawn.data = {";

function replaceLineBreaks(string $text): string {
    return str_replace(["\n", "\r", "\""], ['\n', '', '\"'], $text);
}


$db = Database::connect(Config::getEarthdawnDb());

$stmt = $db->prepare("SELECT id, name, attribute, action, strain, karma, description FROM ed_talents_v3 ORDER BY id");
$stmt->execute();
$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

echo "talents: [";
$index = 0;
foreach ($data as $talent) {
    while($index < $talent['id']) {
        echo ",";
        ++$index;
    }
    echo "new TalentData(";
    echo $talent['id'];
    echo ",";
    echo "\"" . $talent['name'] . "\"";
    echo ",";
    echo ($talent['attribute'] == null ? "null" : $talent['attribute']);
    echo ",";
    echo ($talent['action'] ? "true" : "false");
    echo ",";
    echo $talent['strain'];
    echo ",";
    echo ($talent['karma'] ? "true" : "false");
    echo ",";
    echo "\"" . replaceLineBreaks($talent['description']) . "\"";
    echo ")";
}

echo "]";
// End of talents.


$stmt = $db->prepare("SELECT id, name, attribute, action, strain, type, description FROM ed_skills_v3 ORDER BY id");
$stmt->execute();
$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

echo ",\n\nskills: [";
$index = 0;
foreach ($data as $skill) {
    while($index < $skill['id']) {
        echo ",";
        ++$index;
    }
    echo "new SkillData(";
    echo $skill['id'];
    echo ",";
    echo "\"" . $skill['name'] . "\"";
    echo ",";
    echo $skill['type'];
    echo ",";
    echo $skill['attribute'];
    echo ",";
    echo ($skill['action'] ? "true" : "false");
    echo ",";
    echo $skill['strain'];
    echo ",";
    echo "\"" . replaceLineBreaks($skill['description']) . "\"";
    echo ")";
}
echo "]";
// End of skills


$stmt = $db->prepare("SELECT id, name, circle, threads, weaving_diff, reattunement_diff, casting_diff, `range`, duration, effect, illusion, description FROM ed_spells_v3 ORDER BY id");
$stmt->execute();
$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

echo ",\n\nspells: [";
$index = 0;
foreach ($data as $spell) {
    while($index < $spell['id']) {
        echo ",";
        ++$index;
    }
    echo "new SpellData(";
    echo $spell['id'];
    echo ",";
    echo "\"" . $spell['name'] . "\"";
    echo ",";
    echo $spell['circle'];
    echo ",";
    echo "\"" . $spell['casting_diff'] . "\"";
    echo ",";
    echo $spell['threads'];
    echo ",";
    echo $spell['weaving_diff'];
    echo ",";
    echo $spell['reattunement_diff'];
    echo ",";
    echo "\"" . $spell['range'] . "\"";
    echo ",";
    echo "\"" . $spell['duration'] . "\"";
    echo ",";
    echo "\"" . $spell['effect'] . "\"";
    echo ",";
    echo "\"" . replaceLineBreaks($spell['description']) . "\"";
    echo ",";
    echo ($spell['illusion'] ? "true" : "false");
    echo ")";
}
echo "]";
// End of spells


// Abilities

$stmt = $db->prepare("SELECT id, name, type, bonus, affected_trait, description FROM ed_abilities_v3 ORDER BY id");
$stmt->execute();
$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

echo ",\n\nabilities: [";
$index = 0;
foreach ($data as $ability) {
    while($index < $ability['id']) {
        echo ",";
        ++$index;
    }
    echo "new AbilityData(";
    echo $ability['id'];
    echo ",";
    echo "\"" . replaceLineBreaks($ability['name']) . "\",";
    echo "\"" . replaceLineBreaks($ability['description']) . "\",";
    echo $ability['type'];
    $hasBonus = false;
    if((int)$ability['bonus'] != 0) {
        echo ",";
        echo $ability['bonus'];
        $hasBonus = true;
    }
    if((int)$ability['affected_trait'] != 0) {
        if(!$hasBonus) {
            echo ",null";
        }
        echo ",";
        echo $ability['affected_trait'];
    }
    echo ")";
}
echo "]";
// End of abilities



// disciplines
echo ",\n\ndisciplines: [";

$stmt = $db->prepare("SELECT d.id, d.name, du.unconsciousness, du.death FROM ed_disciplines AS d JOIN ed_durability AS du ON d.id = du.discipline_id ORDER BY d.id");
$stmt->execute();
$disciplines = $stmt->fetchAll(PDO::FETCH_ASSOC);


foreach($disciplines as $discipline) {
    echo ",new DisciplineData(";
    echo $discipline['id'] . ",\"" . $discipline['name'] . "\",";
    echo "[" . $discipline['unconsciousness'] . "," . $discipline['death'] . "],";

    // core talents
    $stmt = $db->prepare("SELECT talent_id, circle FROM ed_core_talents_v3 WHERE discipline_id = :disc ORDER BY circle");
    $stmt->bindValue(":disc", $discipline['id'], PDO::PARAM_INT);
    $stmt->execute();
    $cores = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $coreTalents = array();
    $circle = 0;
    foreach($cores as $core) {
        if($circle != $core['circle']) {
            $circle = $core['circle'];
            $coreTalents[$circle] = array();
        }
        $id = $core['talent_id'];
        $coreTalents[$circle][] = $id;
    }

    echo "[";
    $index = 0;
    foreach($coreTalents as $circle => $talents) {
        while($index < $circle) {
            ++$index;
            echo ",";
        }
        if(count($talents) > 1) {
            echo "[";
            echo implode(",", $talents);
            echo "]";
        } else {
            echo $talents[0];
        }
    }
    echo "],";

    // talent options
    $stmt = $db->prepare("SELECT talent_id, status_group_id FROM ed_talent_options_v3 WHERE discipline_id = :disc ORDER BY status_group_id");
    $stmt->bindValue(":disc", $discipline['id'], PDO::PARAM_INT);
    $stmt->execute();
    $options = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $talentOptions = array();
    $status = 0;
    foreach($options as $option) {
        if($status != $option['status_group_id']) {
            $status = $option['status_group_id'];
            $talentOptions[$status] = array();
        }
        $talentOptions[$status][] = $option['talent_id'];
    }
    echo "[";
    foreach($talentOptions as $status => $talents) {
        if(count($talents) > 1) {
            echo ",[";
            echo implode(",", $talents);
            echo "]";
        } else {
            echo "," . $talents[0];
        }
    }
    echo "]";

    // abilities
    $stmt = $db->prepare("SELECT ability_id, circle FROM ed_discipline_abilities_v3 WHERE discipline_id = :disc ORDER BY circle");
    $stmt->bindValue(":disc", $discipline['id'], PDO::PARAM_INT);
    $stmt->execute();
    $abilities = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo ",{";
    $circle = 0;
    foreach($abilities as $ability) {
        if($circle < (int)$ability['circle']) {
            if($circle > 0) {
                echo "],";
            }
            $circle = (int) $ability['circle'];
            echo "\"{$circle}\":[";
        } else {
            echo ",";
        }
        echo $ability['ability_id'];
    }
    if($circle > 0) {
        echo "]";
    }
    echo "}";

    // racial restrictions
    $stmt = $db->prepare("SELECT r.id FROM ed_races AS r LEFT JOIN ed_racial_restrictions AS x ON r.id = x.race_id AND x.discipline_id = :disc WHERE x.race_id IS NULL");
    $stmt->bindValue(":disc", $discipline['id'], PDO::PARAM_INT);
    $stmt->execute();
    $races = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
    if(count($races) > 2) {     // ignore racial restrictions for all core disciplines that are not special to one specific race (two in case of Elf and Blood Elf)
        echo ",null";
    } else {
        echo ",[";
        echo implode(",", $races);
        echo "]";
    }

    // spells
    $stmt = $db->prepare("SELECT sp.id, sp.circle FROM ed_discipline_spells_v3 AS ds JOIN ed_spells_v3 AS sp ON ds.spell_id = sp.id WHERE ds.discipline_id = :disc ORDER BY sp.circle, sp.id");
    $stmt->bindValue(":disc", $discipline['id'], PDO::PARAM_INT);
    $stmt->execute();
    $spells = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if(count($spells) > 0) {
        $spellsByCircle = array();
        $circle = 0;
        foreach($spells as $spell) {
            if($circle != $spell['circle']) {
                $circle = $spell['circle'];
                $spellsByCircle[$circle] = array();
            }
            $spellsByCircle[$circle][] = $spell['id'];
        }

        echo ",[";
        $index = 0;
        foreach($spellsByCircle as $circle => $spells) {
            while($index < $circle) {
                ++$index;
                echo ",";
            }
            if(count($spells) > 1) {
                echo "[";
                echo implode(",", $spells);
                echo "]";
            } else {
                echo $spells[0];
            }
        }
        echo "]";
    }  
    // end of spells

    echo ")";   // end of single discipline
}

echo "]";   // end of all disciplines

echo "};";
$db = null;

