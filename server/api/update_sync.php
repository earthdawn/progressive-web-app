<?php
namespace EarthdawnPwa;

require_once __DIR__ . '/Authentication.php';
require_once __DIR__ . '/Synchronizer.php';


// Allow CORS preflight
if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    http_response_code(204);
    return;
}


$userId = (new Authentication())->verifyAndReturnUserId();
if($userId === false) {
    http_response_code(401);
    return;
}

if($_SERVER['REQUEST_METHOD'] !== 'POST') {
    http_response_code(400);
    return;
}

$requestBody = file_get_contents('php://input');
try {
    (new Synchronizer())->updateCharacters($requestBody, $userId);
}
catch(\Throwable $e) {
    error_log($e);
    http_response_code(500);
    return;
}

http_response_code(204);
