<?php
namespace EarthdawnPwa;

use PDO;


abstract class Database {

	public static function connect(array $config) {
		$connectionString = 'mysql:host=' . $config['host'] . ';port=' . $config['port'] . ';dbname=' . $config['database'] . ';charset=utf8mb4';
		try {
			$handle = new PDO($connectionString, $config['user'], $config['password'], array(
				PDO::ATTR_PERSISTENT => $config['persistent'],
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
			));
			return $handle;
		} catch (\PDOException $e) {
			error_log($e);
			throw new \RuntimeException("Unable to connect to database: " . $e->getMessage());
		}
	}

}
