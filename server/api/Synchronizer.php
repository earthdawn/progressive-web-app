<?php
namespace EarthdawnPwa;

require_once __DIR__ . '/Config.php';
require_once __DIR__ . '/Database.php';


use PDO;



class Synchronizer {

    private $db;

    public function __construct() {
		$this->db = Database::connect(Config::getProgressiveWebAppDb());
	}

    public function __destruct() {
        $this->db = null;
    }

    public function checkCharacterVersions($postData, int $userId): string {
        $postData = json_decode($postData, true);

        $request = [];      // map indexed by character id
        $data = [];         // array of cloud records
        if(isset($postData["check-all"])) {
            $request = $postData["check-all"];
            $data = $this->loadAllRecords($userId);
        } else {
            $request = $postData["check"];
            $id = array_key_first($request);
            $data = $this->loadSingleRecord($id, $userId);
        }

        $result = [];
        foreach($data as $cloud) {
            $id = $cloud['id'];
            $local = isset($request[$id]) ? $request[$id] : null;
            if(!$local) {
                $result[$id] = ["action" => "create", "changed" => $cloud['uploaded'], "character" => $cloud['data']];     // character exists only in cloud -> tell local to download it and insert
            } else {
                unset($request[$id]);
                $result[$id] = $this->compareLocalAndCloud($local, $cloud);
                if($result[$id]["action"] === "delete") {
                    $this->deleteCharacter($userId, $id);
                }
            }
        }
        foreach($request as $id => $local) {
            if(!isset($local['synced']) || !$local['synced']) {     // new local character that was never uploaded
                $result[$id] = ["action" => "upload"];
            } else if($local['changed'] > $local['synced']) {    // character has changed locally since the last sync but was deleted in the cloud -> conflict without any cloud details
                $result[$id] = ["action" => "conflict"];
            } else {    // character had been synced in the past but does not exist in cloud anymore and there was no local change since then -> someone else has deleted it, therefore should be deleted also locally
                $result[$id] = ["action" => "delete"];
            }
        }

        return $this->toJson($result);
    }


    public function updateCharacters($postData, int $userId) {
        $postData = json_decode($postData, true);
        $request = $postData["update"];      // object indexed by character id

        foreach($request as $id => $local) {
            $stmt = $this->db->prepare("INSERT INTO characters SET user_id = :user, id = :id, uploaded = :uploaded, data = :data ON DUPLICATE KEY UPDATE uploaded = :uploaded, data = :data");
            $stmt->bindValue(':user', $userId, PDO::PARAM_INT);
            $stmt->bindValue(':id', $id, PDO::PARAM_STR);
            $stmt->bindValue(':uploaded', $local['changed'], PDO::PARAM_INT);
            $stmt->bindValue(':data', json_encode($local['character']), PDO::PARAM_STR);
            if(!$stmt->execute()) {
                throw new \RuntimeException("Unable to update character {$id}:" .  print_r($stmt->errorInfo(), true));
            }
        }
    }

    // build the JSON output ourselves because json_encode() would double-encode our 'character' data which is already a stringyfied JSON object
    private function toJson(array $result): string {
        $output = [];
        foreach($result as $id => $details) {
            $obj = "\"{$id}\":{";
            $obj .= "\"action\":\"{$details['action']}\"";
            if(isset($details['changed'])) {
                $obj .= ",\"changed\":" . $details['changed'];
            }
            if(isset($details['character'])) {
                $obj .= ",\"character\":" . $details['character'];
            }
            $obj .= "}";
            $output[] = $obj;
        }
        return "{\"state\":{" . implode(",", $output) . "}}";
    }

    private function loadAllRecords(int $userId): array {
        $stmt = $this->db->prepare("SELECT id, uploaded, data FROM characters WHERE user_id = :user");
        $stmt->bindValue(':user', $userId, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    private function loadSingleRecord(string $id, int $userId): array {
        $stmt = $this->db->prepare("SELECT id, uploaded, data FROM characters WHERE user_id = :user AND id = :id");
        $stmt->bindValue(':user', $userId, PDO::PARAM_INT);
        $stmt->bindValue(':id', $id, PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    private function compareLocalAndCloud(array $local, array $cloud): array {
        $uploaded = $cloud['uploaded'];
        $data = $cloud['data'];

        if(!isset($local['synced']) || !$local['synced']) {     
            return ["action" => "conflict", "changed" => $uploaded, "character" => $data];   // exists both in cloud and local but was never uploaded: must be a conflict in ids -> tell local to fix it
        }
        if($local['synced'] < $uploaded) {       // cloud version is newer than last sync from local
            if($local['synced'] >= $local['changed']) {     // local version was not changed since last sync, so we can just update it to the latest cloud version
                return ["action" => "update", "changed" => $uploaded, "character" => $data];
            }
            // local version has been changed/deleted (is newer) since last sync and thus conflicts with the also changed cloud version.
            return ["action" => "conflict", "changed" => $uploaded, "character" => $data];
        } 
        // cloud version was not changed since last sync but maybe local was
        if(isset($local['deleted']) && $local['deleted']) {     // local version was deleted, so we delete the cloud too and tell local that we were successful
            return ["action" => "delete"];
        }
        if($local['changed'] != $uploaded) {      // local version is different (has to be newer, otherwise it would have been downloaded and synced the last time): -> tell local to upload its version
           return ["action" => "upload", "changed" => $uploaded];
        }
        return ["action" => "keep"];      // local version is same as cloud version, no change required.
    }


    private function deleteCharacter(int $userId, string $characterId) {
        $stmt = $this->db->prepare("DELETE FROM characters WHERE user_id = :user AND id = :id");
        $stmt->bindValue(':user', $userId, PDO::PARAM_INT);
        $stmt->bindValue(':id', $characterId, PDO::PARAM_STR);
        $stmt->execute();
    }
}