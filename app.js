'use strict';

/**  
 * Status level that is used to group talents together. The status level is bound to circles (see {@link Earthdawn.getStatusLevelForCircle}).
 * Higher status levels have higher legend point costs when learning/improving talents.
 * Uses ED3 levels. ED4 does not know the INITIATE level but starts Novice already at circle 1.
 * @readonly
 * @enum {number}
*/
const StatusLevel = Object.freeze({
    INITIATE: 1,
    NOVICE: 2,
    JOURNEYMAN: 3,
    WARDEN: 4,
    MASTER: 5
});


/**
 * Rules and costs for improving a character.
 */
class Advancement {
    /** @type {number[]} - 1-based array of legend point (LP) costs. Index relates to rank of e.g. {@link Talent}s or the like. Earthdawn uses the same sequence but with an offset also for other costs like {@link Attribute} improvement. */
    #legendPointCosts;
    /** LP cost (legendPointCosts) offset for increasing attributes */
    #attributeCostOffset = 4;
    /** How many times can a single attribute be improved */
    #maxAttributeImprovements = 5;
    /** Offset into the legendPointCosts table for skills */
    #skillCostOffset = 1;
    /** @type {number[]} - LP cost (legendPointCosts) offset for talents by status level (1-based) */
    #talentCostOffset = [, 0, 0, 1, 2, 3];
    /** Array of offsets for additional disciplines (index 0 = second disc, 1 = third, 2 = fourth or more). Apply offset to talent costs additionally to status level offets but max out total at 3. */
    #additionalDisciplineCostOffset = [1, 2, 3];
    /** @type {number[][]} - LP cost (legendPointCosts) offset; array of arrays: outer indexed by new discipline (cap at 3) (index 0 = second, etc.), inner indexed (1-based) by circle achieved in previous (lowest) disciplines (1-5, cap at 5). */
    #additionalDisciplineStartingTalentCosts = [[,5,4,3,2,1], [,6,5,4,3,2], [,7,6,5,4,3]];

    constructor() {
        this.#legendPointCosts = Advancement.#calculateFibonacciLegendPointCosts(20);
    }
    /**
     * Gets the legend points (LP) cost for buying the given rank in a {@link Talent}.
     * @param {number} rank - New talent rank to buy.
     * @param {StatusLevel} [level=StatusLevel.INITIATE] - Status level of the talent. Talents of higher level cost more to buy/improve. Status level of a talent is defined by the discipline that provides it.
     * @param {number} [disciplinePosition=1] - What number of discipline (of a multi-discipline character) is this (first discipline = 1, second = 2...)?
     * @returns {number} - LP cost
     */
    getTalentLegendPointCost(rank , level = StatusLevel.INITIATE, disciplinePosition = 1) {
        let disciplineOffset = disciplinePosition === 1 ? 0 : this.#additionalDisciplineCostOffset[Math.min(disciplinePosition, 4) - 2];
        return this.#legendPointCosts[rank + this.#talentCostOffset[level] + disciplineOffset];
    }
    /**
     * Gets the legend points (LP) cost for buying the given rank in a {@link Skill}. In ED3 skills all use the same cost offset, while in ED4 there are different status levels also for skills.
     * @param {number} rank - New skill rank to buy.
     * @returns {number} - LP cost
     */
    getSkillLegendPointCost(rank) {
        return this.#legendPointCosts[rank + this.#skillCostOffset];
    }
    /**
     * Gets the legend points (LP) cost for buying a new {@link Spell}.
     * We use the ED4 rules here (an ED3 optional rule) that the spell cost is equal to the cost of a novice level talent. ED1 had no LP costs for new spells and ED2 used a linear cost of circle * 100.
     * @param {number} spellCircle - Circle of the spell to buy.
     * @returns {number} - LP cost
     */
    getSpellLegendPointCost(spellCircle) {
        return this.#legendPointCosts[spellCircle + this.#talentCostOffset[StatusLevel.NOVICE]];
    }
    /**
     * Gets the legend points (LP) cost for increasing an {@link Attribute} value.
     * @param {number} improvement - Rank of the attribute improvement to buy.
     * @returns {number} - LP cost
     */
    getAttributeLegendPointCost(improvement) {
        if(improvement > this.#maxAttributeImprovements) {
            throw new Error(`At most ${this.#maxAttributeImprovements} attribute improvements are allowed per attribute but tried to raise by ${improvement}`);
        }
        return this.#legendPointCosts[improvement + this.#attributeCostOffset];
    }
    /**
     * Get the legend points (LP) costs for buying the first rank in {@link Talent}s of a new discipline for a multi-discipline character. This method must not be called for the first discipline of a character.
     * @param {number} newDisciplinePosition - What number of new discipline is this (second=2, third=3, etc.)? The costs do not increase after the 4th discipline.
     * @param {number} lowestAchievedCircle - Lowest achieved circle (1-based) of all existing disciplines of this character (i.e. where discipline position is < newDisciplinePosition). The cost decreases with higher circles but does not change after 5th.
     * @returns {number} - LP cost for buying a single talent at rank 1.
     */
    getMultiDisciplineFirstRankTalentCost(newDisciplinePosition, lowestAchievedCircle) {
        if(newDisciplinePosition <= 1) {
            throw new Error(`New discipline must be at least the second of a multi-discipline character but was ${newDisciplinePosition}`);
        }
        lowestAchievedCircle = Math.min(lowestAchievedCircle, 5);   // cost does not decrease anymore after 5th circle
        newDisciplinePosition = Math.min(newDisciplinePosition, 4); // cost does not increase anymore after 4th discipline.
        let offset = this.#additionalDisciplineStartingTalentCosts[newDisciplinePosition - 2][lowestAchievedCircle];
        return this.#legendPointCosts[1 + offset];
    }
    getAttributeImprovementMaximum() {
        return this.#maxAttributeImprovements;
    }
    getMaximumSkillRank() {
        return 10;
    }
    getMaximumCircle() {
        return 15;
    }
    /**
     * Get total number of talents the character has to have learned in order to advance to the given circle.
     * @param {number} circle - Next circle the character wants to advance to.
     * @returns {number} - Minimum number of learned talents in the current discipline.
     */
    getMinimumNumberOfTalentsToAdvanceTo(circle) {
        return circle + 3;
    }
    /**
     * Get the lowest rank all talents have to have that are required for a circle advancement.
     * @param {number} circle - Next circle the character wants to advance to.
     * @returns {number} - Minimum talent rank
     * @see Advancement#getMinimumNumberOfTalentsToAdvanceTo
     */
    getMinimumTalentRankToAdvanceTo(circle) {
        return circle;
    }
    /**
     * @returns {number[]} - Returns the number of talents (core + talent options) any discipline may learn per circle (array is 1-based).
     */
    getTalentCountPerCircle() {
        const result = [];
        result[1] = 7;      // we moved Thread Weaving as an additional Talent for all discipline to circle 1, therefore the count is one higher here than normally for ED3.
        for(let circle = 2; circle <= this.getMaximumCircle(); ++circle) {
            result[circle] = 2;     // every circle after the first provides one discipline (core) talent and one talent option.
        }
        return result;
    }
    /**
     * Calculates legend point costs which are based on the Fibonacci sequence multiplied by 100.
     * @param {number} maxRank - Maximum rank to calculate 
     * @returns {number[]} - A 1-based array of legend point costs with maxRank entries. Array index 0 always has the value 0.
     */
    static #calculateFibonacciLegendPointCosts(maxRank) {
        let result = [0], 
            prev = 0, curr = 1;
        while(result.length <= maxRank) {
            curr = prev + (prev = curr);
            result.push(curr * 100);
        }
        return result;
    }
}


/**
 * @readonly
 * @enum {number}
 */
const ActionType = Object.freeze({
    GENERAL: 0,     // everything hat does not fall in one of the other categories
    ATTACK: 1,
    DEFENSIVE: 2,   // things like Avoid Blow or Parry
    DAMAGE: 3,
    INITIATIVE: 4,
    KNOCKDOWN: 5    // knockdown test
});

/**
 * Helper class to apply additional attributes to {@link TalentData} and {@link SkillData} which are not stored there.
 */
class Actions {
    #defensive = ["Avoid Blow", "Cast Net", "Defense", "Parry", "Riposte", "Spirit Dodge", "Wheeling Defense"];
    #damage = {
        "close": ["Crushing Blow", "Dive Attack", "Down Strike", "Surprise Strike"],
        "unarmed": ["Body Blade", "Body Control", "Claw Shape", "Venom"],
        "melee": ["Charge", "Shield Charge"],
        "ranged": ["Flame Arrow"]
    };
    #attack = {
        "close": ["Momentum Attack", "Multi-Strike", "Pin", "Second Attack", "Spirit Strike", "Swing Attack", "Whirlwind"],
        "unarmed": ["Frenzy", "Swift Kick", "Unarmed Combat"],
        "melee": ["Blood Guilt Weapon", "Double-Charge", "Melee Weapons", "Multi-Charge", "Second Weapon", "Wheeling Attack"],
        "ranged": ["Bank Shot", "Multi-Shot", "Quick Shot", "Second Shot", "True Shot"],
        "missile": ["Missile Weapons"],
        "throwing": ["Blade Juggle", "Throwing Weapons"],
        "all": ["Disarm", "Ethereal Weapon"]
    };
    /**
     * @param {string} name - Name of a {@link Talent} or {@link Skill} to identify the action.
     * @returns {boolean} - Returns true if the given action counts as defensive. This is mainly interesting for {@link StepModDialog}.
     */
    isDefensive(name) {
        return this.#defensive.includes(name);
    }
    /**
     * @param {string} name - Name of a {@link Talent} or {@link Skill} to identify the action.
     * @returns {boolean} - Returns true if the given action replaces the normal strength test to prevent getting knocked down.
     */
    preventsKnockDown(name) {
        return name === "Wound Balance" || name === "Sure Mount";
    }
    /**
     * @param {string} name - Name of a {@link Talent} or {@link Skill} to identify the action.
     * @returns {?WeaponType[]} - If the given action can cause or improve damage tests then an array with all weapon types it is compatible with is returned, else null.
     */
    improvesDamage(name) {
        if(this.#damage.close.includes(name)) {
            return [WeaponType.UNARMED, WeaponType.MELEE];
        }
        if(this.#damage.unarmed.includes(name)) {
            return [WeaponType.UNARMED];
        }
        if(this.#damage.melee.includes(name)) {
            return [WeaponType.MELEE];
        }
        if(this.#damage.ranged.includes(name)) {
            return [WeaponType.MISSILE, WeaponType.THROWING];
        }
        return null;
    }
    /**
     * @param {string} name - Name of a {@link Talent} or {@link Skill} to identify the action.
     * @returns {?WeaponType[]} - If the given action can be used to make an attack then an array with all weapon types it is compatible with is returned, else null.
     */
    makesAttack(name) {
        if(this.#attack.close.includes(name)) {
            return [WeaponType.UNARMED, WeaponType.MELEE];
        }
        if(this.#attack.unarmed.includes(name)) {
            return [WeaponType.UNARMED];
        }
        if(this.#attack.melee.includes(name)) {
            return [WeaponType.MELEE];
        }
        if(this.#attack.ranged.includes(name)) {
            return [WeaponType.MISSILE, WeaponType.THROWING];
        }
        if(this.#attack.missile.includes(name)) {
            return [WeaponType.MISSILE];
        }
        if(this.#attack.throwing.includes(name)) {
            return [WeaponType.THROWING];
        }
        if(this.#attack.all.includes(name)) {
            return [WeaponType.MELEE, WeaponType.UNARMED, WeaponType.MISSILE, WeaponType.THROWING];
        }
        return null;
    }
    /**
     * @returns {SkillData[]} - Returns the all skills that allow default use (on rank 0) even if the character has not learned that skill. Returns mainly skills that have an action type different from General or require strain.
     */
    getDefaultSkills() {
        return [10, 29, 68, 70, 74, 83, 104, 105, 108, 112, 128].map(id => Earthdawn.data.skills[id]);
    }
}

/**
 * @namespace
 */
const Earthdawn = {
    rules: {
        advancement: new Advancement(),
        /**
         * Get the status level into which the given circle belongs.
         * @param {number} circle - Circle for which to lookup the status level 
         * @returns {StatusLevel} - Returns the status level value for the given circle.
         */
        getStatusLevelForCircle(circle) {
            if(circle === 1) {
                return StatusLevel.INITIATE;
            }
            return Math.ceil(circle / 4) + 1;
        },
        /**
         * Get the lower circle boundary for the given status level.
         * @param {StatusLevel} statusLevel - Status level value
         * @returns {number} - Returns the lowest circle that belongs to the given status level.
         */
        getLowestCircleForStatusLevel(statusLevel /* StatusLevel */) {
            if(statusLevel <= StatusLevel.NOVICE) {
                return statusLevel;     // = 1 (INITIATE), 2 (NOVICE)
            }
            return (statusLevel - 1) * 4 - 3;   // = 5 (JOURNEYMAN), 9 (WARDEN), 13 (MASTER)
        },
        /**
         * Get dice to roll for the given step number.
         * @param {number} step - Step number
         * @returns {string} - Returns the dice to roll as a string, e.g. "D10+D6"
         */
        getDiceForStep(step) {
            const maxStep = Earthdawn.data.stepDice.length - 1;
            return Earthdawn.data.stepDice[Math.min(Math.max(1, step), maxStep)];
        },
        actions: new Actions()
    },
    /**
     * @typedef {object} GameData
     * @property {TalentData[]} talents - Base information about all talents in the game. Array is indexed by {@link TalentData#getId}.
     * @property {SkillData[]} skills - Base information about all skills in the game. Array is indexed by {@link SkillData#getId}.
     * @property {SpellData[]} spells - Base information about all spells in the game. Array is indexed by {@link SpellData#getId}.
     * @property {AbilityData[]} abilities - Base information about all racial and discipline abilities in the game. Array is indexed by {@link AbilityData#getId}.
     * @property {DisciplineData[]} disciplines - Base information about all disciplines in the game. Array is indexed by {@link DisciplineData#getId}.
     * @property {ArmorData[]} armors - Base information about all armor in the game. Array is indexed by {@link ArmorData#getId}.
     * @property {WeaponData[]} weapons - Base information about all weapons in the game. Array is indexed by {@link WeaponData#getId}. Contains both melee and ranged weapons.
     * @property {RaceData[]} races - Base information about all races/species in the game. Array is indexed by {@link RaceData#getId}.
     * @property {string[]} stepDice - Array indexed by step number that contains the corresponding dice (as string) for the step.
     * @property {number[]} defenses - Defense values. The array is indexed by the corresponding {@link Attribute#getTotalValue} (Dexterity for Physical, Perception for Spell, Charisma for Social).
     * @property {{full: number[], combat: number[]}} movement - Movement ranges for full and combat movement. The arrays are indexed by {@link Attribute#getTotalValue} for Dexterity plus racial modifier.
     * @property {number[]} carry - Carrying capacity values. The array is indexed by {@link Attribute#getTotalValue} for Strength.
     * @property {number[]} deathRating - Death rating. The array is indexed by {@link Attribute#getTotalValue} for Toughness.
     * @property {number[]} unconsciousnessRating - Unconsciousness rating. The array is indexed by {@link Attribute#getTotalValue} for Toughness.
     * @property {number[]} woundThreshold - Wound threshold. The array is indexed by {@link Attribute#getTotalValue} for Toughness.
     * @property {number[]} recoveryTests - Number of recovery tests. The array is indexed by {@link Attribute#getTotalValue} for Toughness.
     * @property {number[]} mysticArmor - Mystic armor values. The array is indexed by {@link Attribute#getTotalValue} for Willpower.
     */
    /**
     * @type {?GameData} - Provides static data that makes up the Earthdawn rules. This object is filled by data.js file.
     */
    data: null
}


/**
 * @typedef {number} AttributeId - Unique id of an {@link Attribute}, 1-based.
 */
/**
 * Enum of ids that identify specific {@link Attribute}s.
 * @readonly
 */
const AttributeIds = (function(){
    /** @enum {AttributeId} */
    const obj = {
        DEXTERITY: 1,
        STRENGTH: 2,
        TOUGHNESS: 3,
        PERCEPTION: 4,
        WILLPOWER: 5,
        CHARISMA: 6
    };
    /** @type {Map.<AttributeId, string>} */
    const names = new Map();
    Object.entries(obj).forEach(([key, id]) => {
        const name = key.charAt(0) + key.toLowerCase().substring(1);
        names.set(id, name);
    });
    return Object.freeze(Object.assign(obj, {
        getName: /** @param {AttributeId} id */ (id) => {
                const name = names.get(id);
                if(!name) {
                    throw new Error(`Unknown attribute id ${id}. Must be one of the predefined values.`);
                }
                return name;
            },
        getIds: /** @returns {AttributeId[]} */ () => {
            return Array.from(names.keys());
        }
    }));
})();


/**
 * A single attribute belonging to a {@link Character}. An Earthdawn character has six different attributes that are defined by {@link AttributeId}.
 */
class Attribute {
    #id;
    /** @type {string} - Human-readable name */
    #name;
    #initialValue;      // racial base + points bought during creation
    #improvements;
    /** @type {?Character} - Character owning this attribute */
    #character;

    /**
     * @param {AttributeId} id - Id of this attribute. Must be uniquely identify one of the six expected attributes.
     * @param {number} value - Attribute value (not step) at the end of character creation. Immutable during play.
     * @param {number} [improvements=0] - Number of times the attribute value was improved. Limited by {@link Advancement#getAttributeImprovementMaximum}.
     */
    constructor(id, value, improvements = 0) {
        if(value > this.#maximumSupportedInitialValue) {
            throw new Error(`Attribute value out of bounds: ${value}`);
        }
        if(improvements > Earthdawn.rules.advancement.getAttributeImprovementMaximum()) {
            throw new Error(`Attribute improvement count out of bounds: ${improvements}`);
        }
        this.#id = id;
        this.#name = AttributeIds.getName(id);
        this.#initialValue = value;
        this.#improvements = improvements;
    }
    getId() {
        return this.#id;
    }
    getName() {
        return this.#name;
    }
    /**
     * Assign this attribute to a {@link Character}.
     * @param {Character} character - The character owning this attribute.
     */
    setCharacter(character) {
        this.#character = character;
    }
    /**
     * @returns {number} - Returns the attribute value as set at the end of character creation. This value is immutable during play.
     */
    getInitialValue() {
        return this.#initialValue;
    }
    /**
     * Increase initial attribute value by one. Must be called only by {@link CharacterCreator}.
     */
    increaseInitialValue() {
        if(this.#initialValue >= this.#maximumSupportedInitialValue) {
            throw new Error(`Cannot increase initial value above ${this.#maximumSupportedInitialValue}`);
        }
        this.#initialValue++;
    }
    /**
     * Decrease initial attribute value by one. Must be called only by {@link CharacterCreator}.
     */
    decreaseInitialValue() {
        if(this.#initialValue === 0) {
            throw new Error(`Cannot decrease initial value below zero`);
        }
        this.#initialValue--;
    }
    /**
     * @returns {number} - Returns how often this attribute was already improved with legend points.
     */
    getImprovements() {
        return this.#improvements;
    }
    /**
     * @returns {number} - Returns legend points cost of improving this attribute one more time.
     */
    getCostOfNextImprovement() {
        return Earthdawn.rules.advancement.getAttributeLegendPointCost(this.getImprovements() + 1);
    }
    /**
     * Checks if this attribute can be improved one more times without exceeding the allowed maximum. Does not check if there are improvement possibilities left, nor if the character has enough LP.
     * @returns {boolean} - Returns true if this attribute can be improved one more time, else false.
     */
    canImprove() {
        return this.#improvements < Earthdawn.rules.advancement.getAttributeImprovementMaximum()
    }
    /**
     * Increases the number of applied improvements by one.
     */
    addImprovement() {
        if(!this.canImprove()) {
            throw new Error(`Cannot add attribute improvement. Maximum of ${Earthdawn.rules.advancement.getAttributeImprovementMaximum()} reached.`);
        }
        this.#improvements++;
    }
    /**
     * @returns {boolean} - Returns true if this attribute has an improvement that can be removed.
     */
    canRemoveImprovement() {
        return this.#improvements > 0;
    }
    /**
     * Decreases the number of applied improvements by one.
     */
    removeImprovement() {
        if(!this.canRemoveImprovement()) {
            throw new Error(`Cannot remove attribute improvement. Value is already zero.`)
        }
        this.#improvements--;
    }
    /**
     * @returns {number} - Total attribute value. The sum of initial value, all improvements, and all bonuses due to abilities.
     */
    getTotalValue() {
        const bonus = this.#character.getDisciplineAbilities().reduce((accu, ability) => (ability.getType() === AbilityType.INCREASED_ATTRIBUTE_VALUE && ability.getAffectedTraitId() === this.getId()) ? accu + ability.getBonusValue() : accu, 0);
        return this.#initialValue + this.#improvements + bonus;
    }
    /**
     * @returns {number} - The step number for the current total value.
     */
    getStep() {
        return Math.ceil(this.getTotalValue() / 3.0) + 1;
    }
    /**
     * @returns {string} - The dice to roll for this attribute's step.
     */
    getDice() {
        return Earthdawn.rules.getDiceForStep(this.getStep());
    }
    get #maximumSupportedInitialValue() {
        return Earthdawn.data.defenses.length - Earthdawn.rules.advancement.getAttributeImprovementMaximum();   // check any of the look-up tables that are indexed by attribute value to see what the supported maximum is
    }
    /**
     * @returns {[number, number]} - Saves this attribute as an array for serialization.
     */
    save() {
        return [this.#initialValue, this.#improvements];
    }
    /**
     * Deserializes an attribute from a saved state.
     * @param {object} state - Saved character state. Do not change this object during deserialization!
     * @param {number} version - Serialization version used during saving.
     * @param {Character} character - Owning character.
     * @param {AttributeId} id - Id of this attribute.
     * @returns {Attribute} - The deserialized attribute instance.
     */
    static load(state, version, character, id) {
        const attribute = new this(id, state[0], state[1]);
        attribute.setCharacter(character);
        return attribute;
    }
}

/**
 * Base class for immutable equipment game data.
 */
class EquipmentData {
    #id;
    #name;
    #cost;
    /**
     * @param {number} id - Unique id for this type of equipment. See {@link Earthdawn.data}
     * @param {string} name - Name of this equipment 
     * @param {number} cost - Cost in silver pieces
     */
    constructor(id, name, cost) {
        this.#id = id;
        this.#name = name;
        this.#cost = cost;
    }
    getId() {
        return this.#id;
    }
    getName() {
        return this.#name;
    }
    /**
     * @returns {number} - Returns cost in silver.
     */
    getCost() {
        return this.#cost;
    }
    /**
     * Sorts two instances by localized name.
     * @param {EquipmentData} a 
     * @param {EquipmentData} b 
     * @returns {number} Returns 0 if a and b are equal, 1 if a is greater than b, -1 if a is less than b.
     */
    static compare(a, b) {
        return a.getName().localeCompare(b.getName());
    }
}

/**
 * Immutable game data for armor.
 * 
 * @extends EquipmentData
 */
class ArmorData extends EquipmentData {
    #physical;
    #mystical;
    #iniPenalty;
    #isLiving;
    #implantDamage;
    /**
     * @param {number} id - Unique id of this armor
     * @param {string} name - Armor name 
     * @param {number} cost - Cost in silver to buy this armor
     * @param {number} physical - Physical armor rating
     * @param {number} mystical - Mystic armor rating
     * @param {number} [penalty=0] - Dexterity step penalty for initiative tests for wearing this armor 
     * @param {boolean} [living=false] - Flag if this armor counts as a living armor. Some species can only wear living armor. 
     * @param {number} [implantDamage=0] - If this is a living armor that is implanted into the wearer's body, then this is the permanent blood damage taken by this procedure. 
     */
    constructor(id, name, cost, physical, mystical, penalty = 0, living = false, implantDamage = 0) {
        super(id, name, cost);
        if(implantDamage > 0 && !living) {
            throw new Error(`Implanted armor has to be marked as living`);
        }
        this.#physical = physical;
        this.#mystical = mystical;
        this.#iniPenalty = penalty;
        this.#isLiving = living;
        this.#implantDamage = implantDamage;
    }
    getPhysicalRating() {
        return this.#physical;
    }
    getMysticalRating() {
        return this.#mystical;
    }
    /**
     * @returns {number} - The penalty to the Dexterity step for initiative tests
     */
    getInitiativePenalty() {
        return this.#iniPenalty;
    }
    /**
     * @returns {boolean} - True if this is a living armor like e.g. Fernweave.
     */
    isLivingArmor() {
        return this.#isLiving;
    }
    /**
     * @returns {boolean} - True if this is an implanted armor that causes permanent blood damage, for example Blood Pebbles.
     */
    usesBloodMagic() {
        return this.#implantDamage !== 0;
    }
    /**
     * @returns {number} - The permanent blood damage of implanted living armor.
     */
    getImplantDamage() {
        return this.#implantDamage;
    }
}

/**
 * @readonly
 * @enum {number} - Usage/damage type of weapons. Keep this order in sync with {@link KarmaUse}. Used also by {@link Equipment} as array index.
 */
const WeaponType = Object.freeze({
    MELEE: 0,
    UNARMED: 1,
    THROWING: 2,
    MISSILE: 3
});

/**
 * Immutable game data of melee weapons or base class for ranged weapons.
 * @extends EquipmentData
 */
class WeaponData extends EquipmentData {
    #damage;
    #size;
    #minStrength;
    #minDexterity;
    /** @type {?number} - If this weapon can entangle the target, this is the test difficulty number to beat for freeing oneself, else null. */
    #entanglingDifficulty;
    /** @type {WeaponType} - Type of this weapon */
    #type;
    /**
     * @param {number} id - Unique id of this armor
     * @param {string} name - Armor name 
     * @param {number} cost - Cost in silver pieces to buy this weapon
     * @param {number} damage - Damage step of this weapon. Is normally add to the wearer's strength attribute step.
     * @param {number} size - Size of the weapon
     * @param {number} minStrength - Minimum strength attribute value (not step) required to wield this weapon without penalties. 
     * @param {number} [minDexterity=0] - Minimum dexterity attribute value (not step) required to wield this weapon without penalties. 
     * @param {WeaponType} [type=WeaponType.MELEE] - Type category of this weapon
     * @param {?number} [entanglingDiff=null] - If this weapon can entangle the target, this is the test difficulty number to beat for freeing oneself, else null.
     */
    constructor(id, name, cost, damage, size, minStrength, minDexterity = 0, type = WeaponType.MELEE, entanglingDiff = null) {
        super(id, name, cost);
        this.#damage = damage;
        this.#size = size;
        this.#minStrength = minStrength;
        this.#minDexterity = minDexterity;
        this.#type = type;
        this.#entanglingDifficulty = entanglingDiff;
    }
    getDamageStep() {
        return this.#damage;
    }
    getSize() {
        return this.#size;
    }
    getMinimumStrength() {
        return this.#minStrength;
    }
    getMinimumDexterity() {
        return this.#minDexterity;
    }
    /**
     * @returns {boolean} - Returns true if this weapon can be used to entangle a target. In this case {@link WeaponData#getEntanglingDifficulty} will return the test difficulty to break free.
     */
    canEntangle() {
        return this.#entanglingDifficulty != null;
    }
    /**
     * @returns {number} - Returns the test difficulty number to break from being entangled by this weapon. Throws if {@link WeaponData#canEntangle} returns false.
     */
    getEntanglingDifficulty() {
        if(!this.canEntangle()) {
            throw new Error("This is not an entangling weapon");
        }
        return this.#entanglingDifficulty;
    }
    /**
     * @returns {WeaponType} - Type of this weapon
     */
    getType() {
        return this.#type;
    }
    /**
     * Checks if a character of the given race can carry this weapon and how many hands they would need.
     * @param {RaceData} race - The character's race.
     * @returns {number} - The number of hands needed to use this weapon (1 = one-handed, 2 = two-handed), or zero if the character cannot use this weapon (too small or too large).
     */
    handsNeeded(race) {
        if(this.getSize() < race.getUsableWeaponSizes().onehanded.min || this.getSize() > race.getUsableWeaponSizes().twohanded.max) {
            return 0;
        }
        return this.getSize() <= race.getUsableWeaponSizes().onehanded.max ? 1 : 2;
    }
}

/**
 * Immutable game data for ranged weapons (throwing or missile weapons)
 * @extends WeaponData
 */
class RangedWeaponData extends WeaponData {
    /** @type {{short:{min:number,max:number}, long:{min:number,max:number}}} - Object describing the minimum and maximum values of the short and long range distances in yards. */
    #range;
    /**
     * @param {number} id - Unique id of this armor
     * @param {string} name - Armor name 
     * @param {number} cost - Cost in silver pieces to buy this weapon
     * @param {number} damage - Damage step of this weapon. Is normally add to the wearer's strength attribute step.
     * @param {number} size - Size of the weapon
     * @param {number} shortRange - Maximum distance in yards that still counts a short range
     * @param {number} longRange - Maximum distance in yards of long range
     * @param {number} minStrength - Minimum strength attribute value (not step) required to wield this weapon without penalties. 
     * @param {number} [minDexterity=0] - Minimum dexterity attribute value (not step) required to wield this weapon without penalties. 
     * @param {WeaponType} [type=WeaponType.MISSILE] - Type category of this weapon
     * @param {?number} [entanglingDiff=null] - If this weapon can entangle the target, this is the test difficulty number to beat for freeing oneself, else null.
     */
    constructor(id, name, cost, damage, size, shortRange, longRange, minStrength, minDexterity = 0, type = WeaponType.MISSILE, entanglingDiff = null) {
        super(id, name, cost, damage, size, minStrength, minDexterity, type, entanglingDiff);
        this.#range = {
            short: {
                min: 2,
                max: shortRange
            },
            long: {
                min: shortRange + 1,
                max: longRange
            }
        }
    }
    getRange() {
        return this.#range;
    }
}


/**
 * An equipped piece of armor.
 */
class EquippedArmor {
    #data;
    /**
     * @param {ArmorData} data - The armor to equip.
     */
    constructor(data) {
        this.#data = data;
    }
    getData() {
        return this.#data;
    }
    /**
     * @returns {object} - Saved data for serialization
     */
    save() {
        return {id: this.getData().getId()};
    }
    /**
     * Deserializes armor from a saved state.
     * @param {object} state - Saved character state. Do not change this object during deserialization!
     * @param {number} version - Serialization version used during saving.
     * @param {Character} character - Owning character.
     * @returns {EquippedArmor} - The deserialized equipped armor instance.
     */
    static load(state, version, character) {
        return new this(Earthdawn.data.armors[state.id]);
    }
}

/**
 * An equipped weapon. Can be any melee, missile or throwing weapon.
 */
class EquippedWeapon {
    #data;
    #damageBonus;
    /** @type {number} - Total step penalty (negative) on all attack tests made with the weapon due to not satisfying min strength and min dexterity requirements */
    #attackPenalty;
    /** @type {boolean} - True if the owning {@link Character} can carry this weapon one-handed, false if two-handed. */
    #oneHanded;
    /**
     * @param {(WeaponData|RangedWeaponData)} data - The weapon to equip
     * @param {number} [damageBonus=0] - Bonus to the damage step of the base weapon, e.g. from talents like Forge Weapon.
     */
    constructor(data, damageBonus = 0) {
        this.#data = data;
        this.#damageBonus = damageBonus;
        this.#attackPenalty = 0;
        this.#oneHanded = true;
    }
    getData() {
        return this.#data;
    }
    isRangedWeapon() {
        return this.getData().getType() >= WeaponType.THROWING;
    }
    getDamageBonus() {
        return this.#damageBonus;
    }
    /**
     * Sets the additional damage bonus for this weapon.
     * @param {number} bonus - Step bonus to apply to the base weapon's damage 
     */
    setDamageBonus(bonus) {
        if(this.getData().getDamageStep() + bonus < 0) {
            throw new Error(`Negative bonus damage must not decrease total damage of the weapon below zero`);
        }
        this.#damageBonus = bonus;
    }
    /**
     * @returns {number} - The damage step of this weapon as a sum of the weapon's base step plus any damage bonuses applied to this weapon. Does not include the {@link Character}'s strength step yet!
     */
    getTotalDamageStep() {
        return this.getData().getDamageStep() + this.getDamageBonus();
    }
    /**
     * @returns {number} - A negative number if the character suffers any penalty to attack tests with this weapon, else zero.
     */
    getAttackStepPenalty() {
        return this.#attackPenalty;
    }
    canCarryOneHanded() {
        return this.#oneHanded;
    }
    /**
     * Calculates derived values based on the owning character
     * @param {Character} character - Owning character
     */
    calculateFrom(character) {
        this.#attackPenalty = Math.min(0, character.getAttribute(AttributeIds.STRENGTH).getTotalValue() - this.getData().getMinimumStrength()) + Math.min(0, character.getAttribute(AttributeIds.DEXTERITY).getTotalValue() - this.getData().getMinimumDexterity());
        this.#oneHanded = this.getData().handsNeeded(character.getRace()) === 1;
    }
    /**
     * @returns {object} - Saved data for serialization
     */
    save() {
        return {id: this.getData().getId(), db: this.#damageBonus};
    }
    /**
     * Deserializes weapon from a saved state.
     * @param {object} state - Saved character state. Do not change this object during deserialization!
     * @param {number} version - Serialization version used during saving.
     * @param {Character} character - Owning character.
     * @returns {EquippedWeapon} - The deserialized equipped weapon instance.
     */
    static load(state, version, character) {
        return new this(Earthdawn.data.weapons[state.id], state.db);
    }
    /**
     * Sorts two instances by localized name.
     * @param {EquippedWeapon} a 
     * @param {EquippedWeapon} b 
     * @returns {number} Returns 0 if a and b are equal, 1 if a is greater than b, -1 if a is less than b.
     */
    static compare(a, b) {
        return EquipmentData.compare(a.getData(), b.getData());
    }
}

/**
 * Collection of equipment owned by a {@link Character}.
 */
class Equipment {
    #character;
    /** @type {?EquippedArmor} - According to the rules only one piece of armor can be equipped at anytime. */
    #armor;
    /** @type {EquippedWeapon[][]} - All weapons this character carries. The outer array is indexed by {@link WeaponType}, the inner arrays are sorted. */
    #weapons;
    /**
     * @param {Character} character - The character owning all the equipment
     */
    constructor(character) {
        this.#character = character;
        this.#armor = null;
        this.#weapons = [];
    }
    /**
     * @returns {?EquippedArmor} - The armor the character is wearing, or null if none is worn.
     */
    getArmor() {
        return this.#armor;
    }
    /**
     * Equip a piece of armor. If the character already wears armor at the moment, it is replaced by this method.
     * Recalculates the game characteristics of the owning {@link Character} because it changes the {@link ArmorRating} and (for living armor) the permanent damage of {@link Health}.
     * @param {ArmorData} armorData - The game data for the armor to wear.
     */
    equipArmor(armorData) {
        if(!armorData) {
            throw new Error(`ArmorData is missing`);
        }
        this.#armor = new EquippedArmor(armorData);
        this.#character.calculateCharacteristics();
    }
    /**
     * Unequips any worn armor. Can be safely called if none is worn at the moment.
     * Recalculates the game characteristics of the owning {@link Character}.
     */
    unequipArmor() {
        const hadArmor = this.#armor != null;
        this.#armor = null;
        if(hadArmor) {
            this.#character.calculateCharacteristics();
        }
    }
    /**
     * @returns {EquippedWeapon[]} - All equipped melee weapons of this character. Do not modify the returned array! 
     */
    getMeleeWeapons() {
        return this.getWeapons(WeaponType.MELEE);
    }
    /**
     * @returns {EquippedWeapon[]} - All equipped missile weapons of this character. Do not modify the returned array! 
     */
    getMissileWeapons() {
        return this.getWeapons(WeaponType.MISSILE);
    }
    /**
     * @returns {EquippedWeapon[]} - All equipped throwing weapons of this character. Do not modify the returned array! 
     */
    getThrowingWeapons() {
        return this.getWeapons(WeaponType.THROWING);
    }
    /**
     * @param {WeaponType} type - The type of weapons to filter for
     * @returns {EquippedWeapon[]} - All equipped weapons of the given type. Do not modify the returned array! 
     */
    getWeapons(type) {
        return this.#weapons[type] ?? [];
    }
    /**
     * @param {WeaponType} type - The type of weapon to check for
     * @returns {boolean} - Returns true if the character can spend Karma to boost damage tests with weapons of the given type.
     */
    canUseKarmaForDamageTestsWithWeaponsOf(type) {
        const affectedTrait = KarmaUse.MELEE_DAMAGE + type;     // assumes that WeaponType has the same order as KarmaUse for damage with a 0-based offset.
        return this.#character.getAllEffectiveAbilities().some(ability => ability.getType() === AbilityType.KARMA_USE && ability.getAffectedTraitId() === affectedTrait);
    }
    /**
     * Equip any type of new weapon. Automatically adds it to the right collection and sorts it. There is no limit to the number of weapons a character can carry (apart from encumberance).
     * This method will calculated all derived values of the newly equipped weapon.
     * @param {WeaponData} weaponData - Game data of the weapon to equip.
     * @returns {EquippedWeapon} - Returns the equipped weapon.
     */
    equipWeapon(weaponData) {
        const equipped = new EquippedWeapon(weaponData);
        equipped.calculateFrom(this.#character);
        let collection = this.#weapons[weaponData.getType()];
        if(collection == null) {
            this.#weapons[weaponData.getType()] = collection = [];
        }
        collection.push(equipped);
        collection.sort(EquippedWeapon.compare);
        return equipped;
    }
    /**
     * @param {EquippedWeapon} weapon - The weapon to unequip.
     */
    unequipWeapon(weapon) {
        const collection = this.#weapons[weapon.getData().getType()] ?? [];
        const index = collection.indexOf(weapon);
        if(index === -1) {
            throw new Error(`No equipped weapon '${weapon.getData().getName()}' found.`);
        }
        collection.splice(index, 1);
    }
    /**
     * Calculates derived values based on the owning character's abilities and traits.
     * @param {Ability[]} effectiveAbilities - See {@link Character#getAllEffectiveAbilities}.
     */
    calculateFrom(effectiveAbilities) {
        this.#weapons.flat().forEach(weapon => weapon.calculateFrom(this.#character));
    }
    /**
     * @returns {object} - Returns save data for serialization
     */
    save() {
        const state = {};
        if(this.getArmor()) {
            state.ar = this.getArmor().save();
        }
        state.wp = this.#weapons.flat().map(weapon => weapon.save());
        return state;
    }
    /**
     * Deserializes the equipment from a saved state.
     * @param {object} state - Saved character state. Do not change this object during deserialization!
     * @param {number} version - Serialization version used during saving.
     * @param {Character} character - Owning character.
     * @returns {Equipment} - The deserialized equipment instance.
     */
    static load(state, version, character) {
        const result = new this(character);
        if(state.ar != null) {
            result.#armor = EquippedArmor.load(state.ar, version, character);
        }
        if(state.wp) {
            const weapons = /** @type {object[]} */ (state.wp).map(st => EquippedWeapon.load(st, version, character));
            Object.keys(WeaponType).forEach(key => {
                const type = WeaponType[key];
                result.#weapons[type] = weapons.filter(weapon => weapon.getData().getType() === type);
                result.#weapons[type].sort(EquippedWeapon.compare);
            });
        }
        return result;
    }
}

/**
 * @typedef {number} DefenseId - Unique id of a specific {@link DefenseType}.
 */
/**
 * @typedef {object} DefenseType
 * @property {DefenseId} id - Unique id identifying the type of defense.
 * @property {string} name - Human-readable name of the defense.
 * @property {AttributeId} attributeId - Id of the attribute on which this defense is based.
 * @readonly
 */
/**
 * Constants for {@link Defense}s.
 * @readonly
 */
const DefenseType = Object.freeze({
    /** @type {DefenseType} */
    PHYSICAL: {
        id: 1,
        name: "Physical Defense",
        attributeId: AttributeIds.DEXTERITY
    },     
    /** @type {DefenseType} */
    SPELL: {
        id: 2,
        name: "Spell Defense",
        attributeId: AttributeIds.PERCEPTION
    },
    /** @type {DefenseType} */
    SOCIAL: {
        id: 3,
        name: "Social Defense",
        attributeId: AttributeIds.CHARISMA
    }
});

/**
 * A single defense trait owned by a {@link Character}.
 */
class Defense {
    #type;
    /**
     * Total defense value based on attribute value + discipline abilities + racial abilities
     * @type {number}
     */
    #totalValue;
    /**
     * @param {DefenseType} type 
     */
    constructor(type) {
        this.#type = type;
        this.#totalValue = 0;
    }
    getId() {
        return this.#type.id;
    }
    getName() {
        return this.#type.name;
    }
    getTotal() {
        if(this.#totalValue === 0) {
            throw new Error(`Unable to get total value of ${this.#type.name}: not yet calculated`);
        }
        return this.#totalValue;
    }
    /**
     * Calculates the final value of this trait. This method should be called only by the {@link Character} class.
     * @param {Character} character - Owner of this trait 
     * @param {Ability[]} abilities - Unordered list of effective abilities owned by the character.
     * @returns {Defense} - Returns this
     */
    calculateFrom(character, abilities) {
        const abilityBonus = abilities.reduce((accu, ability) => {
                if(ability.getType() === AbilityType.DEFENSE && ability.getAffectedTraitId() === this.#type.id) {
                    accu += ability.getBonusValue();
                }
                return accu;
            }, 0);
        const attributeValue = character.getAttribute(this.#type.attributeId).getTotalValue();
        this.#totalValue = Earthdawn.data.defenses[attributeValue] + abilityBonus;
        return this;
    }
}

/**
 * The raw initiative trait of a {@link Character}.
 */
class Initiative {
    /** @type {number} - Initiative step based on attribute, abilities and equipment. Does not include bonuses from talents */
    #totalStep;
    /** @type {boolean} - Can the adept use Karma for any initiative tests */
    #canUseKarma;

    constructor() {
        this.#totalStep = 0;
        this.#canUseKarma = false;
    }
    getStep() {
        if(this.#totalStep === 0) {
            throw new Error(`Initiative is not yet calculated`);
        }
        return this.#totalStep;
    }
    getDice() {
        return Earthdawn.rules.getDiceForStep(this.getStep());
    }
    canUseKarma() {
        return this.#canUseKarma;
    }
    /**
     * Calculates the final value of this trait. This method should be called only by the {@link Character} class.
     * @param {Character} character - Owner of this trait 
     * @param {Ability[]} abilities - Unordered list of effective abilities owned by the character.
     * @returns {Initiative} - Returns this
     */
    calculateFrom(character, abilities) {
        const abilityBonus = abilities.reduce((accu, ability) => {
                if(ability.getType() === AbilityType.INITIATIVE) {
                    accu += ability.getBonusValue();
                }
                if(ability.getType() === AbilityType.KARMA_USE && ability.getAffectedTraitId() === KarmaUse.INITIATIVE) {
                    this.#canUseKarma = true;
                }
                return accu;
            }, 0);
        const attributeStep = character.getAttribute(AttributeIds.DEXTERITY).getStep();
        this.#totalStep = attributeStep + abilityBonus;
        const armor = character.getEquipment().getArmor();
        if(armor) {
            this.#totalStep -= armor.getData().getInitiativePenalty();
        }
        return this;
    }
}

/**
 * Helper class to identify specific talents by name. Depends on the data provided in {@link Earthdawn.data.talents}.
 */
class KnownTalent {
    /** @type {?Map.<string, TalentData>} - Map for looking up talent data by name */
    static #talentsByName = null;

    #name;
    /**
     * @param {string} name - Name of the talent as used in {@link TalentData#getName}.
     */
    constructor(name) {
        this.#name = name;
    }
    getId() {
        return this.getTalentData().getId();
    }
    getTalentData() {
        return KnownTalent.#getData(this.#name);
    }
    /**
     * @param {string} name - Name of a talent as used in {@link TalentData#getName}.
     * @returns {TalentData} - Game data of the talent. Throws if no match found.
     */
    static #getData(name) {
        if(!this.#talentsByName) {
            const map = new Map();
            Earthdawn.data.talents.forEach(data => {
                map.set(data.getName(), data);
            });
            this.#talentsByName = map;
        }
        const data = this.#talentsByName.get(name);
        if(!data) {
            throw new Error(`No talent data found for name ${name}`);
        }
        return data;
    }
    /**
     * @param {TalentId} talentId - Id of a talent.
     * @returns {boolean} - Returns true if this is any kind of spell matrix because these are the only talents that can be learned multiple times within a single discipline and between multi-disciplines (as often as available per discipline)
     */
    static isAnySpellMatrix(talentId) {
        return talentId === KnownTalent.SPELL_MATRIX.getId() || talentId === KnownTalent.ARMORED_MATRIX.getId() || talentId === KnownTalent.ENHANCED_MATRIX.getId() || talentId === KnownTalent.SHARED_MATRIX.getId() || talentId === KnownTalent.SPELL_FETISH.getId() || talentId === KnownTalent.ARMORED_FETISH.getId() || talentId === KnownTalent.ENHANCED_FETISH.getId() || talentId === KnownTalent.SHARED_FETISH.getId();
    }
    static KARMA_RITUAL = new KnownTalent('Karma Ritual');
    static DURABILITY = new KnownTalent('Durability');
    static VERSATILITY = new KnownTalent('Versatility');
    static SPELLCASTING = new KnownTalent('Spellcasting');
    static SPELL_MATRIX = new KnownTalent('Spell Matrix');
    static ENHANCED_MATRIX = new KnownTalent('Enhanced Matrix');
    static ARMORED_MATRIX = new KnownTalent('Armored Matrix');
    static SHARED_MATRIX = new KnownTalent('Shared Matrix');
    static SPELL_FETISH = new KnownTalent('Spell Fetish');
    static ENHANCED_FETISH = new KnownTalent('Enhanced Fetish');
    static ARMORED_FETISH = new KnownTalent('Armored Fetish');
    static SHARED_FETISH = new KnownTalent('Shared Fetish');
    static WILLFORCE = new KnownTalent('Willforce');
    static TIGER_SPRING = new KnownTalent('Tiger Spring');
    static AIR_DANCE = new KnownTalent('Air Dance');
    static COBRA_STRIKE = new KnownTalent('Cobra Strike');
    static FIREBLOOD = new KnownTalent('Fireblood');
    static MIND_BLADE = new KnownTalent('Mind Blade');
    static WIND_BOW = new KnownTalent('Wind Bow');
}

/**
 * @typedef {number} TalentId - Unique id of a {@link TalentData}.
 */
/**
 * Immutable game data for talents.
 */
class TalentData {
    #id;
    #name;
    #description;
    #attributeId;
    #needsAction;
    #strain;
    #requiresKarma;
    /**
     * @param {TalentId} id - Unique id of this talent.
     * @param {string} name - Unique name of this talent. 
     * @param {?AttributeId} [attribute=null] - Id of the {@link Attribute} on which this talent is based. May be null if this is a passive talent that is never rolled, like e.g. Spell Matrix. 
     * @param {boolean} [action=false] - Flag if this talent requires an action (standard or sustained) when used in combat.
     * @param {number} [strain=0] - Strain damage caused by using this talent. 
     * @param {boolean} [karma=false] - Flag if usage of this talent requires the expenditure of a {@link Karma} point. Karma usage may be overridden by other rules.
     * @param {?string} [description=null] - Optional description of the talent for display in the UI.
     */
    constructor(id, name, attribute = null, action = false, strain = 0, karma = false, description = null) {
        this.#id = id;
        this.#name = name;
        this.#attributeId = attribute;
        this.#needsAction = action;
        this.#strain = strain;
        this.#requiresKarma = karma;
        this.#description = description;
    }
    getId() {
        return this.#id;
    }
    getName() {
        return this.#name;
    }
    getDescription() {
        return this.#description;
    }
    /** 
     * @returns {boolean} - True if this talent can be rolled and thus has a step number. Only talents that are based on attributes have steps.
     */
    hasStep() {
        return this.#attributeId !== null;
    }
    /**
     * @returns {?AttributeId} - Id of the attribute on which this talent is based. Can be null for talents that are never rolled.
     */
    getAttributeId() {
        return this.#attributeId;
    }
    needsAction() {
        return this.#needsAction;
    }
    getStrain() {
        return this.#strain;
    }
    requiresKarma() {
        return this.#requiresKarma;
    }
    /**
     * @returns {?WeaponType} - Returns a weapon type if this talent counts as weapon, else null. It's rank can be added to the character's strength attribute for damage test and it can be used in combination we other talents that improve damage tests.
     */
    countsAsWeapon() {
        if(this.getId() === KnownTalent.MIND_BLADE.getId()) {
            return WeaponType.MELEE;
        }
        if(this.getId() === KnownTalent.WIND_BOW.getId()) {
            return WeaponType.MISSILE
        }
        return null;
    }
    /**
     * @returns {ActionType} - What type of action this talent provides;
     */
    getActionType() {
        if(this.getId() === KnownTalent.AIR_DANCE.getId() || this.getId() === KnownTalent.COBRA_STRIKE.getId()) {
            return ActionType.INITIATIVE;
        }
        if(Earthdawn.rules.actions.preventsKnockDown(this.getName())) {
            return ActionType.KNOCKDOWN;
        }
        if(Earthdawn.rules.actions.makesAttack(this.getName()) != null) {
            return ActionType.ATTACK;
        }
        if(Earthdawn.rules.actions.isDefensive(this.getName())) {
            return ActionType.DEFENSIVE;
        }
        if(Earthdawn.rules.actions.improvesDamage(this.getName()) != null) {
            return ActionType.DAMAGE;
        }
        return ActionType.GENERAL;
    }
    /**
     * @param {WeaponType} weaponType - The type of weapon to check for
     * @returns {boolean} - Returns true if this is a talent for making attacks and it can be used together with the given type of weapon, else false.
     */
    canAttackWith(weaponType) {
        return Earthdawn.rules.actions.makesAttack(this.getName())?.includes(weaponType) ?? false;
    }
    /**
     * @param {WeaponType} weaponType - The type of weapon to check for
     * @returns {boolean} - Returns true if this is a talent that improves/replaces damage tests and it can be used together with the given type of weapon, else false.
     */
    improvesDamage(weaponType) {
        return Earthdawn.rules.actions.improvesDamage(this.getName())?.includes(weaponType) ?? false;
    }
    /**
     * Sorts two instances by localized name and then id.
     * @param {TalentData} a 
     * @param {TalentData} b 
     * @returns {number} Returns 0 if a and b are equal, 1 if a is greater than b, -1 if a is less than b.
     */
    static compare(a, b) {
        let result = a.getName().localeCompare(b.getName());
        if(result === 0) {
            result = a.getId() > b.getId() ? 1 : (a.getId() < b.getId() ? -1 : 0);
        }
        return result;
    }
}

/**
 * Defines the source how a {@link Character} has gained access to a {@link Talent}.
 * @readonly
 * @enum {number}
 */
const TalentSource = Object.freeze({
    /** @constant - Core talent of a discipline */
    CORE: 0,
    /** @constant - Learned as a talent option from a discipline */
    OPTION: 1,
    /** @constant - Talent of a foreign discipline learned by a human via Versatility, or all talents acquired by the Journeyman discipline. Note: the Versatility talent itself has source == Ability. */
    VERSATILITY: 2,
    /** @constant - Talent provided for free by a racial or discipline ability */
    ABILITY: 3
});

/**
 * Enum used as return value when calculating if the rank of a learned {@link Talent} can be changed.
 * @readonly
 * @enum {number}
 */
const TalentRankChange = Object.freeze({
    /** @constant - Ok. Rank can be changed in either direction */
    POSSIBLE: 0,
    /** @constant - No, decrease not possible. Either the absolute minimum rank was already reached, or all ranks bought with legend points were already removed but free ranks must not be decreased, or the minimum of free ranks was reached for mandatory talents like e.g. KarmaRitual. */
    MIN_REACHED: 1,
    /** @constant - No, increase not possible. Absolute maximum reached, or maximum of free ranks reached during character creation. */
    MAX_REACHED: 2,
    /** @constant - No, decrease not possible. Lowering this talent's rank would violate previous circle advancement rules (meaning that the discipline circle has to be decreased first). */
    CIRCLE_ADVANCEMENT: 3,
    /** @constant - No, decrease not possible. Versatility talent rank cannot be decreased because number of foreign talents would exceed total count supported by its ranks. */
    VERSATILITY_USED: 4,
    /** @constant - No, decrease not possible. The character knows spells that can only be cast with this Thread Weaving talent. The spells have to be forgotten first. */
    SPELLS_LEARNED: 5
});

/**
 * Mutable instance of a learned talent owned by a {@link Character}.
 * Talents normally belong to a {@link Discipline}. For easier management and code unification, talents granted by abilities are treated as if they belonged to the first discipline a character has learned.
 */
class Talent {
    #data;
    /** @type {number} - Ranks bought was legend points. Does not include free ranks from character creation. */
    #rank;
    /** @type {number} - Free ranks assigned during character creation. */
    #freeRanks;
    #boughtOnCircle;
    #source;
    #discipline;
    #character;
    /** @type {(null|{fromPosition: number, fromStatus: number, ranks: number})} - If a talent learned as an option in a previous discipline becomes available as a core talent in a new discipline, then the talent is moved to the new discipline (and source changes from TalentOption to Core and boughtOnCircle changes to new circle). In this case we keep track here from which discipline (fromPosition) and at which status level (fromStatus) it came and how many bought ranks it already had. Because if the player then decreases the ranks, we have to refund less LP than if it was was normally learned for this new discipline (because later disciplines have higher talent costs). In this case we also decrease the migrated rank accordingly. */
    #migrated = null;

    /**
     * @param {TalentData} data - The basic game data of this talent.
     * @param {number} circle - Circle (1-based) on which this talent was learned by this character. Core talents have a fixed circle when they become available through a discipline. Talent options and forgein talents have variable slots/ranges when they become available.
     * @param {TalentSource} source - The source that provided access to this talent at the time of learning it. A character may later have access to other sources for the same talent, but that does not change this one.
     * @param {Discipline} discipline - Discipline to which this talent belongs. Foreign talents and talents acquired through abilities are added to the first discipline a character possesses.
     * @param {Character} character - The owning character. 
     */
    constructor(data, circle, source, discipline, character) {
        this.#data = data;
        this.#boughtOnCircle = circle;
        this.#source = source;
        this.#discipline = discipline;
        this.#character = character;
        this.#rank = 0;
        this.#freeRanks = 0;
    }
    getId() {
        return this.#data.getId();
    }
    getData() {
        return this.#data;
    }
    /**
     * @returns {number} - Returns the circle on which this talent was acquired. For core talents this matches the circle from {@link DisciplineData#getCoreTalents}. For talent options this is the option slot that was used (i.e. the circle on which it was learned), meaning that even if the discipline has already a higher circle the talent could still be bought on a lower circle if there was an open unused slot left.
     */
    getBoughtOnCircle() {
        return this.#boughtOnCircle;
    }
    /**
     * @returns {boolean} - True if this talent was migrated from one discipline to another. Decreasing this talent's rank may refund a different number of legend points than talents learned from ground up in the new discipline.
     */
    wasMigrated() {
        return this.#migrated != null;
    }
    /**
     * Migrates a talent already learned as an option in previous (= lower-position) discipline to a new (higher-position) discipline where it is available as a core talent.
     * @param {Discipline} newDiscipline - The new discipline to which this talent should be migrated. It will take new ownership of this talent. Afterwards {@link Talent#wasMigrated} will return true.
     * 
     * @todo Implement actual logic!
     */
    migrateTalentOptionToCoreTalent(newDiscipline) {
        if(this.#source !== TalentSource.OPTION) {
            throw new Error(`Only talent options can be migrated to core talent`);
        }
        // TODO: check that new discipline has a matching core talent and on which circle. Remember circle.
        // TODO: remove talent from this.#discipline and assign new discipline instead.
        this.#migrated = { fromPosition: this.#discipline.getPosition(), fromStatus: Earthdawn.rules.getStatusLevelForCircle(this.getBoughtOnCircle()), ranks: this.#rank};
        // TODO: set this.#boughtOnCircle to circle at which core talent becomes available in new discipline
        this.#source = TalentSource.CORE;
    }
    /**
     * Checks if the character has enough legend points to adopt this talent into the new discipline.
     * A talent can be adopted only if it was acquired as a foreign talent via Versatility and later becomes available as a core or talent option in a newer discipline. This method does not check any of these prerequisites!
     * @param {Discipline} newDiscipline - New discipline that provides this talent as a core or talent option.
     * @param {number} newTalentCircle - Circle on which this talent becomes available in the new discipline and into which it will be adopted.
     * @returns {boolean} - True if the character has enough unspent legend points to pay for the adoption.
     */
    canAdoptForeignTalentTo(newDiscipline, newTalentCircle) {
        return this.#character.getLegendPoints().getUnspent() >= this.calculateCostToAdoptForeignTalentTo(newDiscipline, newTalentCircle);
    }
    /**
     * Adopts a foreign talent learned via Versatility into a new discipline (one with a higher position) as a normal talent, thus freeing up one use of Versatility.
     * Adoption requires to pay the difference in legend point costs for re-acquiring the talent as a core or talent option of the new discipline. Disciplines learned later normally have higher talent buy costs
     * but adoption may also refund legend points.
     * @param {Discipline} newDiscipline - New discipline that provides this talent as a core or talent option. Will become the new owner of this talent.
     * @param {number} newTalentCircle - Circle on which this talent becomes available in the new discipline and into which it will be adopted. This counts as the new circle on which the talent was learned after the adoption.
     * @param {TalentSource} newSource - New source of the talent. Must be either {@link TalentSource.CORE} or {@link TalentSource.OPTION}.
     */
    adoptTo(newDiscipline, newTalentCircle, newSource) {
        if(!this.canAdoptForeignTalentTo(newDiscipline, newTalentCircle)) {
            throw new Error(`${this.getData().getName()} cannot be adopted into new discipline. Not enough legend points`);
        }
        const cost = this.calculateCostToAdoptForeignTalentTo(newDiscipline, newTalentCircle);
        const newTalent = newDiscipline.addTalent(this.getData(), newSource, newTalentCircle);
        newTalent.#rank = this.#rank;
        newTalent.#freeRanks = this.#freeRanks;
        if(cost < 0) {
            this.#character.getLegendPoints().refundPoints(-cost);
        } else {
            this.#character.getLegendPoints().spendPoints(cost);
        }
        this.#discipline.removeTalent(this);
        this.#discipline = null;
        this.#character = null;
    }
    /**
     * Calculate the costs to adopt this foreign talent (learned via Versatility) as a normal talent of a new discipline. Cost may be negative or zero if, for example, the talent was originally bought on a much higher circle
     * than it will be re-learned in the new discipline.
     * @param {Discipline} newDiscipline - New discipline that provides this talent as a core or talent option.
     * @param {number} newTalentCircle - Circle on which this talent becomes available in the new discipline and into which it will be adopted.
     * @returns {number} - Returns the cost (positive) or refund (negative) for adopting this talent into the new discipline on the given circle.
     */
    calculateCostToAdoptForeignTalentTo(newDiscipline, newTalentCircle) {
        if(this.getSource() !== TalentSource.VERSATILITY) {
            throw new Error(`${this.getData().getName()} was not learned via Versatility and therefore cannot be adopted into another discipline`);
        }
        const boughtOnStatus = Earthdawn.rules.getStatusLevelForCircle(this.#boughtOnCircle);
        const newStatusLevel = Earthdawn.rules.getStatusLevelForCircle(newTalentCircle);
        let costViaVersatility = 0;
        let costAsInNewDiscipline = 0;
        for(let boughtRank = this.getFreeRanks() + 1; boughtRank <= this.getTotalRank(); ++boughtRank) {
            costViaVersatility += Earthdawn.rules.advancement.getTalentLegendPointCost(boughtRank, boughtOnStatus, this.#discipline.getPosition());
            costAsInNewDiscipline += Earthdawn.rules.advancement.getTalentLegendPointCost(boughtRank, newStatusLevel, newDiscipline.getPosition());
        }
        return costAsInNewDiscipline - costViaVersatility;
    }
    /**
     * @returns {TalentSource} - The source used to learn this talent. Uses {@link TalentSource#VERSATILITY} for both talents acquired through Versatility and for all talents learned as part of the Journeyman discipline.
     */
    getSource() {
        return this.#source;
    }
    /**
     * @returns {number} - The total achieved rank of this talent. This is the sum of free ranks assigned during character creation and ranks bought later via legend points.
     */
    getTotalRank() {
        return this.#freeRanks + this.#rank;
    }
    /**
     * Quick-checks if the total rank of this talent may be decreased by one when editing the character. Call this method every time before decreasing the total ranks of a talent to make sure it is fairly safe.
     * This method does not check if circle advancement rules are violated because that check is expensive. This extra check is only done internally when calling {@link Talent#decreaseRank}.
     * @param {boolean} lpRanksOnly - If true than only ranks bought with legend points can be removed but not free ranks assigned during character creation.
     * @returns {TalentRankChange} - Returns a value that defines if a decrease in rank is allowed or if it violates any restrictions. Even if {@link TalentRankChange.POSSIBLE} is returned, the decrease may still fail when circle advancement rules are violated.
     */
    mayDecreaseTotalRank(lpRanksOnly = true) {
        if(!this.#canDecreaseRank()) {
            if(lpRanksOnly || !this.#canDecreaseFreeRanks()) {
                return TalentRankChange.MIN_REACHED;
            }
        }
        if(this.is(KnownTalent.VERSATILITY)) {
            if(this.#discipline.getOpenVersatilitySlots() === 0) {
                return TalentRankChange.VERSATILITY_USED;
            } else {
                return TalentRankChange.POSSIBLE;
            }
        }
        // If trying to remove this talent: Check if this is a ThreadWeaving talent and the character possesses any spells that need this talent because they cannot be cast using any other ThreadWeaving that the character may also know. Spells can be acquired only when knowing the right ThreadWeaving talent.
        if(this.getTotalRank() === 1 && this.getData().getName().startsWith('Thread Weaving')) {      // Cannot use KnownTalents here because there are many ThreadWeaving talents, one for each discipline.
            const otherThreadWeavings = new Map();
            const needsThis = this.#character.getSpells().some(spell => {
                const talents = spell.getThreadWeavingTalentsThatCanLearnThisSpell();
                if(talents.some(talentData => talentData.getId() === this.getId())) {
                    // This talent is one of the ThreadWeaving talents that is used by one of the learned spells.
                    if(talents.length === 1) {  // there is no alternative talent for this spell
                        return true;
                    }
                    talents.forEach(talentData => {
                        if(talentData.getId() !== this.getId()) {
                            otherThreadWeavings.set(talentData.getId(), true);      // this map logic assumes that a spell is at most available to two disciplines. If a spell has more than 1 alternative then this logic does not work correctly.
                        }
                    });
                }
                return false;
            });
            if(needsThis) {
                return TalentRankChange.SPELLS_LEARNED;
            }
            // there might be alternatives to this talent. Check if the character possesses them.
            const allThreadWeaving = this.#character.getAccessToSpells(lpRanksOnly);       // exclude Versatility during character creation (when lpRanksOnly == false)
            for(const talentId of otherThreadWeavings.keys()) {
                if(!allThreadWeaving.get(talentId)) {
                    return TalentRankChange.SPELLS_LEARNED;     // we had need for another thread weaving talent but this character has no ranks in it.
                }
            }
        }
        // normally we would have to call #checkAdvancementViolations() here too, but we do this other check only when trying to save the Character because it is expensive! Meaning: Possible means maybe-possible.
        return TalentRankChange.POSSIBLE;
    }
    /**
     * Throws an error if decreasing this talent would violate any previous circle advancement rules, meaning the character could not have advanced to this point (including learning new disciplines) without the current talent rank.
     * This is an expensive operation.
     * @throws {Error}
     */
    #checkAdvancementViolations() {
        if(this.getSource() === TalentSource.VERSATILITY && !this.#discipline.isJourneyman()) {   // assumes that foreign talents learned via Versatility are never used for circle advancement (except for Journeyman), even if a later new discipline owns that talent as core or option.
            return;
        }
        // Check here if decreasing this talent by one rank would affect/violate previous discipline circle advancements (i.e. having a certain number of talents of certain ranks).
        // For the discipline on which it was bought we can shortcut the check if the rank is not higher than the circle it was acquired on. We cannot use that shortcut for other disciplines because we do not know the order in which they were advanced. 
        if(this.getTotalRank() <= this.#discipline.getCircle() && this.getTotalRank() > this.getBoughtOnCircle() && !this.#discipline.canAdvanceToCircle(this.getTotalRank(), this)) {
            throw new Error(`This talent is required to satisfy the advancement rules for circle ${this.getTotalRank()} of the ${this.#discipline.getData().getName()} discipline.`);
        }
        this.#character.getDisciplines().filter(discipline => discipline !== this.#discipline).forEach(discipline => {
            if(this.getTotalRank() <= discipline.getCircle() && !discipline.canAdvanceToCircle(this.getTotalRank(), this)) {
                throw new Error(`This talent is required to satisfy the advancement rules for circle ${this.getTotalRank()} of the ${discipline.getData().getName()} discipline.`);
            }
        });
    }
    /**
     * @returns {boolean} - Returns true if this talent should be removed from the character when its total rank is decreased to zero, which is true for all talents except the ones gained by racial or discipline abilities.
     */
    #removeWhenTotalRankZero() {
        return this.getSource() !== TalentSource.ABILITY;
    }
    /**
     * @returns {boolean} - Returns true if the total rank of this talent has not yet reached the allowed maximum.
     */
    #canIncreaseRankRaw() {
        return this.getTotalRank() < Earthdawn.rules.advancement.getMaximumCircle();
    }
    /**
     * @returns {boolean} - Returns true if the rank of this talent can be increased.
     */
    canIncreaseRank() {
        return this.#canIncreaseRankRaw() && this.#character.getLegendPoints().getUnspent() >= this.#getCostForNextRank();
    }
    /**
     * @returns {boolean} - Returns true if the talent has any bought ranks left.
     */
    #canDecreaseRank() {
        return this.#rank > 0;
    }
    /**
     * Buying an increase of one rank with legend points. Recalculates all derived characteristics.
     */
    increaseRank() {
        if(!this.canIncreaseRank()) {
            throw new Error(`Cannot increase rank any further`);
        }
        this.#character.getLegendPoints().spendPoints(this.#getCostForNextRank());
        this.#rank += 1;
        this.#refreshCharacter();
    }
    /**
     * Decreases the rank of this talent by one and refunds the legend points spent for it. Cannot decrease free ranks, only bought ranks.
     * Call {@link Talent#mayDecreaseTotalRank(true)} before calling this method to do a prelimiary check if the decrease it possible. This method may still
     * also throw an exception if the decrease would violate the circle advancement rules.
     * @throws {Error}
     */
    decreaseRank() {
        if(this.mayDecreaseTotalRank() !== TalentRankChange.POSSIBLE) {
            throw new Error(`Cannot decrease rank any further`);
        }
        this.#checkAdvancementViolations();
        this.#decreaseUnsafe();
    }
    /**
     * Decreases the bought ranks by one, refunds the legend points and recalculates derived statistics. Removes the talent from the character if necessary.
     * This method does not check if decreasing the talent is violating any rules! It can handle adopted talents.
     */
    #decreaseUnsafe() {
        if(this.#rank === 0) {
            throw new Error(`Cannot decrease talent ${this.getData().getName()} with rank 0 any further`);
        }
        this.#rank -= 1;
        let cost = this.#getCostForNextRank();
        if(this.#migrated && this.#migrated.ranks > this.#rank) {
            cost = Earthdawn.rules.advancement.getTalentLegendPointCost(this.getTotalRank() + 1, Earthdawn.rules.getStatusLevelForCircle(this.#migrated.fromStatus), this.#migrated.fromPosition);
            this.#migrated.ranks -= 1;
            if(this.#migrated.ranks === 0) {
                this.#migrated = null;
            }
        }
        this.#character.getLegendPoints().refundPoints(cost);
        if(this.getTotalRank() === 0 && this.#removeWhenTotalRankZero()) {
            this.#discipline.removeTalent(this);
        }
        this.#refreshCharacter();
    }
    /**
     * Removes this talent from its discipline. Must be called only by {@link Discipline} when forgetting a whole discipline (with position > 1 of a multi-discipline character).
     * Can be called only for talents bought on first circle of an additional discipline. All others have to be decreased and removed previously via {@link Talent#decreaseRank}.
     */
    forgetTalent() {    // must only be called from Discipline when forgetting an additional discipline. Assumes that someone else has already run all the necessary checks.
        if(this.getBoughtOnCircle() != 1 || this.getFreeRanks() > 0 || this.#discipline.getPosition() === 1 || this.#discipline.getCircle() > 1) {
            throw new Error(`Cannot forget this talents. This method must not be called to decrease or remove talents except when forgetting additional disciplines!`);
        }
        while(this.#rank > 0) {
            this.#decreaseUnsafe();
        }
    }
    /**
     * @returns {number} - Returns the legend point cost for increasing this talent's rank by one.
     */
    #getCostForNextRank() {
        return Earthdawn.rules.advancement.getTalentLegendPointCost(this.getTotalRank() + 1, Earthdawn.rules.getStatusLevelForCircle(this.getBoughtOnCircle()), this.#discipline.getPosition());
    }
    /**
     * @returns {number} - The number of free ranks assigned during character creation. Do not use this value later, use {@link Talent#getTotalRank} instead.
     */
    getFreeRanks() {
        return this.#freeRanks;
    }
    /**
     * This method should be called only by {@link CharacterCreator}.
     * @returns {boolean} - Returns true if the free ranks of this talent can be increased by one.
     */
    canIncreaseFreeRanks() {
        return this.#freeRanks < 3 && this.#canIncreaseRankRaw();
    }
    /**
     * This method should be called only by {@link CharacterCreator}.
     * @returns {boolean} - Returns true if the free ranks of this talent can be decreased by one.
     */
    #canDecreaseFreeRanks() {
        const minimum = this.is(KnownTalent.KARMA_RITUAL) ? 1 : 0;   // KarmaRitual is a required talent that the first discipline has to own at least on rank 1 (being a free rank during character creation). This is not according to ED3 rules.
        return this.#freeRanks > minimum;
    }
    /**
     * Increase free ranks by one. This method should be called only by {@link CharacterCreator}.
     */
    increaseFreeRanks() {
        if(!this.canIncreaseFreeRanks()) {
            throw new Error(`Cannot increase free ranks any further`);
        }
        this.#freeRanks += 1;
    }
    /**
     * Decrease free ranks by one. This method should be called only by {@link CharacterCreator}.
     * Call {@link Talent#mayDecreaseTotalRank(false)} before calling this method to ensure that decreasing does not violate any rules (like removing Thread Weaving will having spells that require it).
     */
    decreaseFreeRanks() {
        if(!this.#canDecreaseFreeRanks()) {
            throw new Error(`Cannot decrease free ranks any further`);
        }
        this.#freeRanks -= 1;
        if(this.getTotalRank() === 0 && this.#removeWhenTotalRankZero()) {
            this.#discipline.removeTalent(this);
        }
    }
    /**
     * @returns {KarmaRequirement} - Checks if the usage of this talent requires or allows the use of Karma. Returns "required" (always needs Karma), "optional" (may use Karma), "no" (not allowed or not possible to use Karma).
     */
    getKarmaRequirement() {
        if(!this.hasStep()) {
            return KarmaRequirement.NO;
        }
        if(this.#source === TalentSource.CORE) {
            return KarmaRequirement.OPTIONAL;      // core talents are always discipline talents and as such allow free use of Karma even for talents that normally require Karma
        }
        if(this.#data.requiresKarma()) {
            return KarmaRequirement.REQUIRED;
        }
        /*  This is not official: normally KarmaUse is for Attribute-only tests (or optionally also for Skill use) but never for Talent checks
        if(this.#character.getDisciplineAbilities().some(ability => ability.getType() === AbilityType.KARMA_USE && ability.getAffectedTraitId() === this.#data.getAttributeId())) {
            return KarmaRequirement.OPTIONAL;
        }
        */
        return KarmaRequirement.NO;
    }
    /**
     * @returns {boolean} - Returns true if this talent has a step number and can be rolled. Some passive talents like e.g. Spell Matrix don't have steps.
     */
    hasStep() {
        return this.#data.hasStep();
    }
    /**
     * @returns {number} - The total step of this talent including the corresponding attribute step. This is the step number to roll.
     */
    getStep() {
        const attribute = this.getAttribute();
        return this.getTotalRank() + attribute.getStep();
    }
    getDice() {
        return Earthdawn.rules.getDiceForStep(this.getStep());
    }
    /**
     * @returns {Attribute} - Returns the associated attribute of this talent. Throws if this talent does not have a step (and thus no attribute). @see {@link Talent#hasStep}.
     */
    getAttribute() {
        if(!this.hasStep()) {
            throw new Error(`This talent does not have an associated attribute`);
        }
        return this.#character.getAttribute(this.#data.getAttributeId());
    }
    /**
     * Check if this talent is the given known talent.
     * @param {KnownTalent} knownTalent - The known talent constant to check for.
     * @returns {boolean} - Return true if this talent represents the given known talent, else false.
     */
    is(knownTalent) {
        return this.#data.getId() === knownTalent.getId();
    }
    /**
     * Recalculates the character's characteristics that are dependent on this talent's rank.
     */
    #refreshCharacter() {
        if(this.is(KnownTalent.KARMA_RITUAL) || this.is(KnownTalent.DURABILITY)) {
            this.#character.calculateCharacteristics();
        }
    }
    /**
     * @returns {object} - Returns the saved state of this talent for serialization.
     */
    save() {
        const state = {
            id: this.#data.getId(),
            cl: this.#boughtOnCircle,
            so: this.#source,
            rk: this.#rank,
            fr: this.#freeRanks
        };
        if(this.#migrated) {
            state.mg = {
                fp: this.#migrated.fromPosition,
                fs: this.#migrated.fromStatus,
                rk: this.#migrated.ranks
            };
        }
        return state;
    }
    /**
     * Deserializes a talent from a saved state.
     * @param {object} state - Saved character state. Do not change this object during deserialization!
     * @param {number} version - Serialization version used during saving.
     * @param {Character} character - Owning character.
     * @param {Discipline} discipline - The discipline to which this talent belongs.
     * @returns {Talent} - The deserialized talent instance.
     */
    static load(state, version, character, discipline) {
        const talent = new this(Earthdawn.data.talents[state.id], state.cl, state.so, discipline, character);
        talent.#rank = state.rk;
        talent.#freeRanks = state.fr;
        if(state.mg) {
            talent.#migrated = {
                fromPosition: state.mg.fp,
                fromStatus: state.mg.fs,
                ranks: state.mg.rk
            };
        }
        return talent;
    }
    /**
     * Sorts two instances by their base data. @see {@link TalentData.compare}.
     * @param {Talent} a 
     * @param {Talent} b 
     * @returns {number} Returns 0 if a and b are equal, 1 if a is greater than b, -1 if a is less than b.
     */
    static compare(a, b) {
        return TalentData.compare(a.getData(), b.getData());
    }
}


/**
 * Type category of a {@link SkillData}.
 * Mainly of interest during character creation where different numbers of free ranks may be assigned to different skill types. Or for some ability bonuses.
 * @readonly
 * @enum {number}
 */
const SkillType = Object.freeze({
    GENERAL: 1,
    KNOWLEDGE: 2,
    ARTISAN: 3,
    LANGUAGE: 4
});

/**
 * Immutable game data describing a {@link Skill}.
 */
class SkillData {
    #id;
    #name;
    #description;
    #type;
    #attributeId;
    #needsAction;
    #strain;
    /**
     * @param {number} id - Unique id of this skill 
     * @param {string} name - Unique name of this skill
     * @param {SkillType} type - Category to which this skill belongs 
     * @param {AttributeId} attributeId - Id of the attribute on which this skill's step is based.
     * @param {boolean} [action=false] - Flag if using this skill requires an action or not 
     * @param {number} [strain=0] - Strain damage caused by usage of this skill 
     * @param {?string} [description=null] - Optional description of this skill for the UI 
     */
    constructor(id, name, type, attributeId, action = false, strain = 0, description = null) {
        this.#id = id;
        this.#name = name;
        this.#type = type;
        this.#attributeId = attributeId;
        this.#needsAction = action;
        this.#strain = strain;
        this.#description = description;
    }
    getId() {
        return this.#id;
    }
    getName() {
        return this.#name;
    }
    getDescription() {
        return this.#description;
    }
    /**
     * @returns {SkillType} - To which category this skill belongs. Mainly of interest during character creation.
     */
    getType() {
        return this.#type;
    }
    /**
     * @returns {AttributeId} - Id of the attribute on which this skill's step is based. All skills have an attribute, there are no passive skills.
     */
    getAttributeId() {
        return this.#attributeId;
    }
    needsAction() {
        return this.#needsAction;
    }
    getStrain() {
        return this.#strain;
    }
    /**
     * @returns {ActionType} - What type of action this talent provides;
     */
    getActionType() {
        if(Earthdawn.rules.actions.preventsKnockDown(this.getName())) {
            return ActionType.KNOCKDOWN;
        }
        if(Earthdawn.rules.actions.makesAttack(this.getName()) != null) {
            return ActionType.ATTACK;
        }
        if(Earthdawn.rules.actions.isDefensive(this.getName())) {
            return ActionType.DEFENSIVE;
        }
        if(Earthdawn.rules.actions.improvesDamage(this.getName()) != null) {
            return ActionType.DAMAGE;
        }
        return ActionType.GENERAL;
    }
    /**
     * @param {WeaponType} weaponType - The type of weapon to check for
     * @returns {boolean} - Returns true if this is a skill for making attacks and it can be used together with the given type of weapon, else false.
     */
    canAttackWith(weaponType) {
        return Earthdawn.rules.actions.makesAttack(this.getName())?.includes(weaponType) ?? false;
    }
    /**
     * @param {WeaponType} weaponType - The type of weapon to check for
     * @returns {boolean} - Returns true if this is a skill that improves/replaces damage tests and it can be used together with the given type of weapon, else false.
     */
    improvesDamage(weaponType) {
        return Earthdawn.rules.actions.improvesDamage(this.getName())?.includes(weaponType) ?? false;
    }
    /**
     * Sorts two instances by their locale name and then id.
     * @param {SkillData} a 
     * @param {SkillData} b 
     * @returns {number} Returns 0 if a and b are equal, 1 if a is greater than b, -1 if a is less than b.
     */
    static compare(a, b) {
        let result = a.getName().localeCompare(b.getName());
        if(result === 0) {
            result = a.getId() > b.getId() ? 1 : (a.getId() < b.getId() ? -1 : 0);
        }
        return result;
    }
}

/**
 * Mutable instance of a learned skill owned by a {@link Character}.
 */
class Skill {
    #data;
    #character;
    /** @type {number} - Ranks bought with legend points. Does not include free ranks. */
    #rank;
    /** @type {number} - Free ranks assigned during character creation */
    #freeRanks;
    /**
     * @param {SkillData} data - Basic game data of this skill
     * @param {Character} character - Owning character
     */
    constructor(data, character) {
        this.#data = data;
        this.#character = character;
        this.#rank = 0;
        this.#freeRanks = 0;
    }
    getId() {
        return this.#data.getId();
    }
    getData() {
        return this.#data;
    }
    /**
     * @returns {SkillType} - Type category of this skill
     */
    getType() {
        return this.#data.getType();
    }
    /**
     * @returns {number} - Total rank of this skill to roll on. This is the sum of free ranks and bought ranks.
     */
    getTotalRank() {
        return this.#freeRanks + this.#rank;
    }
    /**
     * @returns {boolean} - Returns true if the total rank of this skill has not yet reach the possible maximum.
     */
    #canIncreaseRankRaw() {
        return this.getTotalRank() < Earthdawn.rules.advancement.getMaximumSkillRank();
    }
    /**
     * @returns {boolean} - Returns true if the rank of this skill can be increased with legend points.
     */
    canIncreaseRank() {
        return this.#canIncreaseRankRaw() && this.#character.getLegendPoints().getUnspent() >= this.#getCostOfNextRank();
    }
    /**
     * @returns {boolean} - Returns true if the total rank of this skill can be decreased. This applies only to ranks bought with legend points. Free ranks cannot be removed.
     */
    canDecreaseRank() {
        return this.#rank > 0;
    }
    /**
     * Increase the rank of this skill by one by spending legend points.
     */
    increaseRank() {
        if(!this.canIncreaseRank()) {
            throw new Error(`Cannot increase rank any further`);
        }
        this.#character.getLegendPoints().spendPoints(this.#getCostOfNextRank());
        this.#rank += 1;
    }
    /**
     * Decrease the total rank of this skill by one. Can only remove ranks bought with legend points, not free ranks.
     */
    decreaseRank() {
        if(!this.canDecreaseRank()) {
            throw new Error(`Cannot decrease rank any further`);
        }
        this.#rank -= 1;
        this.#character.getLegendPoints().refundPoints(this.#getCostOfNextRank());
        if(this.getTotalRank() === 0) {
            this.#character.removeSkill(this);
        }
    }
    /**
     * @returns {number} - Returns the legend point costs of increasing this skill by one rank.
     */
    #getCostOfNextRank() {
        return Earthdawn.rules.advancement.getSkillLegendPointCost(this.getTotalRank() + 1);
    }
    /**
     * @returns {number} - Returns the free ranks assigned during character creation. In play use {@link Skill#getTotalRank} instead.
     */
    getFreeRanks() {
        return this.#freeRanks;
    }
    canIncreaseFreeRanks() {
        return this.#freeRanks < 3 && this.#canIncreaseRankRaw();
    }
    canDecreaseFreeRanks() {
        return this.#freeRanks > 0;
    }
    increaseFreeRanks() {
        if(!this.canIncreaseFreeRanks()) {
            throw new Error(`Cannot increase free ranks any further`);
        }
        this.#freeRanks += 1;
    }
    /**
     * Decreases the free ranks of this skill by one. If this brings the total rank down to zero (mainly during character creation) then the skill is automatically removed from the {@link Character}.
     */
    decreaseFreeRanks() {
        if(!this.canDecreaseFreeRanks()) {
            throw new Error(`Cannot decrease free ranks any further`);
        }
        this.#freeRanks -= 1;
        if(this.getTotalRank() === 0) {
            this.#character.removeSkill(this);
        }
    }
    getStep() {
        const attribute = this.getAttribute();
        return this.getTotalRank() + attribute.getStep();
    }
    getDice() {
        return Earthdawn.rules.getDiceForStep(this.getStep());
    }
    /**
     * @returns {Attribute} - Returns the attribute associated with this skill.
     */
    getAttribute() {
        return this.#character.getAttribute(this.#data.getAttributeId());
    }
    /**
     * @returns {boolean} - True if it is allowed to use Karma to boost rolls of this skill. Optional karma use for skills is provided through abilities.
     */
    canUseKarma() {
        return this.#character.getDisciplineAbilities().some(ability => ability.getType() === AbilityType.KARMA_USE && ability.getAffectedTraitId() === this.#data.getAttributeId());
    }
    /**
     * @returns {object} - Saved skill state for serialization.
     */
    save() {
        return {
            id: this.#data.getId(),
            rk: this.#rank,
            fr: this.#freeRanks
        };
    }
    /**
     * Deserializes a skill from a saved state.
     * @param {object} state - Saved character state. Do not change this object during deserialization!
     * @param {number} version - Serialization version used during saving.
     * @param {Character} character - Owning character.
     * @returns {Skill} - The deserialized skill instance.
     */
    static load(state, version, character) {
        const skill = new this(Earthdawn.data.skills[state.id], character);
        skill.#rank = state.rk;
        skill.#freeRanks = state.fr;
        return skill;
    }
    /**
     * Sorts two instances by their base data. @see {@link SkillData.compare}.
     * @param {Skill} a 
     * @param {Skill} b 
     * @returns {number} Returns 0 if a and b are equal, 1 if a is greater than b, -1 if a is less than b.
     */
    static compare(a, b) {
        return SkillData.compare(a.getData(), b.getData());
    }
}


/**
 * @typedef {number} SpellId - Unique id of a {@link SpellData}.
 */
/**
 * Immutable game data of a {@link Spell}.
 */
class SpellData {
    #id;
    #name;
    #circle;
    #description;
    #castingDifficulty;
    #threads;
    #weavingDifficulty;
    #reattunementDifficulty;
    #range;
    #duration;
    #effect;
    #isIllusion;
    /**
     * @param {SpellId} id - Unique id of this spell 
     * @param {string} name - Unique name of this spell
     * @param {number} circle - The circle on which this spell becomes available to spellcaster disciplines. In ED3 a spell shared betweem multiple disciplines is using the same circle for all of them.
     * @param {string} castingDiff - Either the string representation of a fixed difficulty number (e.g. "13") or a description who's spell defense is used (e.g. "Self" or "Target's Spell Defense").
     * @param {number} threads - Number of threads this spell requires. Can be 0 if this spell does not need threads.
     * @param {?number} weavingDiff - Weaving difficulty of the spell's threads. Can be null if the spell does not have any threads.
     * @param {number} reattune - Reattunement difficulty of this spell. This is the target number for placing the spell into a matrix.
     * @param {string} range - Description of the spell's range. Something like "Touch", "Self", "100 yards" or "Rank + 5 yards".
     * @param {string} duration - Description of the spell's duration. Something like "1 round" or "Rank + 4 minutes".
     * @param {string} effect - Short description of the spell's effect. Could be either the roll to make for effect like "WIL + 4" or something else like "+2 Recovery Tests".
     * @param {?string} [description=null] - Long description of the spell and its effect. Describes what the spell does and may give more information what the effect means.
     * @param {boolean} [illusion=false] - Flag to signal if this spell is an illusion. Illusions can be disbelieved and are normally not able to cause deadly damage. 
     */
    constructor(id, name, circle, castingDiff, threads, weavingDiff, reattune, range, duration, effect, description = null, illusion = false) {
        this.#id = id;
        this.#name = name;
        this.#circle = circle;
        this.#castingDifficulty = castingDiff;
        this.#threads = threads;
        this.#weavingDifficulty = weavingDiff || null;
        this.#reattunementDifficulty = reattune;
        this.#range = range;
        this.#duration = duration;
        this.#effect = effect;
        this.#description = description;
        this.#isIllusion = illusion;
    }
    getId() {
        return this.#id;
    }
    getName() {
        return this.#name;
    }
    getCircle() {
        return this.#circle;
    }
    getCastingDifficulty() {
        return this.#castingDifficulty;
    }
    getThreads() {
        return this.#threads;
    }
    /**
     * @returns {?number} - The spell's weaving difficulty if the spell requires any threads, or null.
     */
    getWeavingDifficulty() {
        return this.#weavingDifficulty;
    }
    getReattunementDifficulty() {
        return this.#reattunementDifficulty;
    }
    getRange() {
        return this.#range;
    }
    getDuration() {
        return this.#duration;
    }
    getEffect() {
        return this.#effect;
    }
    isIllusion() {
        return this.#isIllusion;
    }
    getDescription() {
        return this.#description;
    }
    /**
     * Sorts two instances by their circle and then localized name.
     * @param {SpellData} a 
     * @param {SpellData} b 
     * @returns {number} Returns 0 if a and b are equal, 1 if a is greater than b, -1 if a is less than b.
     */
    static compare(a, b) {
        let result = a.getCircle() > b.getCircle() ? 1 : (a.getCircle() < b.getCircle() ? -1 : 0);
        if(result === 0) {
            result = a.getName().localeCompare(b.getName());
        }
        return result;
    }
}

/**
 * Constant for marking if a spell was acquired for free or paid for with legend points.
 * @readonly
 * @enum {number}
 */
const FreeSpell = Object.freeze({
    /** @constant - Spell was paid with legend points */
    NO: 0,
    /** @constant - Spell was granted for free at character creation */
    CREATION: 1,
    /** @constant - Spell was granted for free due to circle advancement */
    ADVANCEMENT: 2
});

/**
 * Instance of a spell owned by a {@link Character}.
 * 
 * Even so spells are normally made available through specific disciplines, we store the spells directly on the character because several disciplines can have
 * access to the same spell or a character could have learned it via Versatility (by learning the Spellcasting and ThreadWeaving), and spell costs are indepenent
 * of disciplines.
 */
class Spell {
    #data;
    #character;
    /** @type {FreeSpell} - Information if this spell was bought with legend points or not */
    #gotForFree;
    /** @type {TalentData[]} - Array of Thread Weaving TalentData with which this spell can be learned/cast. */
    #availableTo;
    /**
     * @param {SpellData} data - Basic game data of this spell
     * @param {Character} character - Owning character
     * @param {FreeSpell} [forFree=FreeSpell.NO] - Marker if this spell was acquired for free or paid for with legend points 
     */
    constructor(data, character, forFree = FreeSpell.NO) {
        this.#data = data;
        this.#character = character;
        this.#gotForFree = forFree;
        this.#availableTo = this.#calculateAvailability();
    }
    getId() {
        return this.#data.getId();
    }
    getData() {
        return this.#data;
    }
    /** 
     * @returns {FreeSpell} - Constant defining how this spell was acquired.
     */
    acquiredForFree() {
        return this.#gotForFree;
    }
    /**
     * Unlearns a spell. Refunds any legend points and removes it from the character. Must not be used during character creation (see {@link Character#removeSpell} instead).
     */
    unlearn() {
        if(this.acquiredForFree() === FreeSpell.CREATION) {
            throw new Error(`Spell was acquired during character creation and cannot be unlearned`);        // otherwise we would have to calculate the free spell circle differently in Character
        }
        if(this.acquiredForFree() === FreeSpell.NO) {
            this.#character.getLegendPoints().refundPoints(Earthdawn.rules.advancement.getSpellLegendPointCost(this.getData().getCircle()));
        }
        this.#character.removeSpell(this);
    }
    /**
     * @returns {TalentData[]} - An array containing the talent data for all Thread Weaving talents that can be used to learn this spell. 
     */
    getThreadWeavingTalentsThatCanLearnThisSpell() {
        return this.#availableTo;
    }
    getThreadWeavingSpecialityNames() {     // array of discipline specific Thread Weaving talent names (e.g. Airweaving or Wizardry) for all talents that have access to this spell.
        return this.getThreadWeavingTalentsThatCanLearnThisSpell().map(talentData => {
            return talentData.getName().match(/Thread Weaving\s*\((.+)\)/)[1];  // extract the discipline specific name given in parenthesis
        });
    }
    getSensingDifficulty() {   // the difficulty to sense that this is an illusion spell and therefore disbelief it.
        if(!this.#data.isIllusion()) {
            throw new Error(`Only illusion spells have a sensing difficulty`);
        }
        return this.#data.getCircle() + 15;
    }
    /**
     * @returns {TalentData[]} - Array of talent data that contains the ThreadWeaving talents of all disciplines that are able to learn/cast this spell. ED3 rules require that the character knows the Thread Weaving talent to learn a spell from the matching discipline's spell list, even if the actual spell itself does not need any threads woven to cast it.
     */
    #calculateAvailability() {
        const result = [];
        Earthdawn.data.disciplines.forEach(discipline => {
            if(discipline.hasSpells() && discipline.getSpells(this.#data.getCircle()).some(spellId => spellId === this.#data.getId())) {
                const talentId = discipline.getThreadWeavingIdForSpellcasting();
                result.push(Earthdawn.data.talents[talentId]);
            }
        });
        return result;
    }
    /**
     * @returns {object} - Saved spell state for serialization.
     */
    save() {
        return {id: this.#data.getId(), fc: this.#gotForFree};
    }
     /**
     * Deserializes a spell from a saved state.
     * @param {object} state - Saved character state. Do not change this object during deserialization!
     * @param {number} version - Serialization version used during saving.
     * @param {Character} character - Owning character.
     * @returns {Spell} - The deserialized spell instance.
     */
    static load(state, version, character) {
        return new this(Earthdawn.data.spells[state.id], character, state.fc);
    }
    /**
     * Sorts two instances by their {@link SpellData}.
     * @param {Spell} a 
     * @param {Spell} b 
     * @returns {number} Returns 0 if a and b are equal, 1 if a is greater than b, -1 if a is less than b.
     */
    static compare(a, b) {
        return SpellData.compare(a.getData(), b.getData());
    }
}

/**
 * Defines what kind of bonus an {@link Ability} provides to a character. Abilities can also be negative and thus represent a malus.
 * @readonly
 * @enum {number}
 */
const AbilityType = Object.freeze({
    /** @constant - The ability has either mainly a roleplay/fluff effect or is too complicated to be automated by the app. These abilities have no further implemenation, the app only displays the description. */
    ROLEPLAY: 1,
    /** @constant - The ability enables the usage of Karma to boost certain rolls. See {@link KarmaUse} for further definition what character trait/action can benefit from Karma through this ability. */
    KARMA_USE: 2,
    /** @constant - This ability provides a free, inherent talent to the character. The character has access to such talents even if their rank is 0. */
    FREE_TALENT: 3,
    /** @constant - The ability provides a bonus to a specific {@link Defense} trait. The specific defense is defined via {@link DefenseType}. */
    DEFENSE: 4,
    /** @constant - The ability increases the characters {@link Initiative} step. This bonus also applies to talents that replace initiative, like e.g. Tiger Spring. */
    INITIATIVE: 5,
    /** @constant - The ability provides a bonus to the physical armor rating of the character. This normally counts as natural armor. */
    PHYSICAL_ARMOR: 6,
    /** @constant - The ability provides a bonus to the mystic armor rating of the character. This normally counts as natural armor. */
    MYSTICAL_ARMOR: 7,
    /** @constant - The ability increases the wound threshold of the character. */
    WOUND_THRESHOLD: 8,
    /** @constant - The ability provides additional recovery tests or removes existing. If a character's recovery tests would fall below 1, then instead the period it needs to regain all tests increases (e.g. 1 test every 2 days). */
    RECOVERY_TESTS: 9,
    /** @constant - The ability increases the character's pain resistance. Pain resistance decreases penalties caused by wounds. */
    PAIN_RESISTANCE: 10,
    /** @constant - The ability increases the {@link Karma} step of the character. That means the player may use better bonus dice when spending Karma for a test. */
    KARMA_STEP: 11,
    /** @constant - The ability increases the value (not step) of a specific {@link Attribute}. The affected attribute is specified through its {@link AttributeId}. This bonus in addition to the racial start values. */
    INCREASED_ATTRIBUTE_VALUE: 12,
    /** @constant - The ability provides a bonus to both the character's unconsciousness and death rating. */
    DURABILITY: 13,
    /** @constant - The ability is a choice-ability from the Journeyman discipline. When gaining this ability, the player must select one characteristic bonus of their choosing from {@link AbilityType.INITIATIVE}, {@link AbilityType.RECOVERY_TESTS} or {@link AbilityType.DURABILITY}. */
    JOURNEYMAN_SELECTION: 14
});

/**
 * For abilities of type {@link AbilityType.KARMA_USE} this enum is used to define which trait can now be boosted with Karma. If the Karma usage applies to an attribute, then {@link AttributeId} is used to note the affected trait instead of this enum.
 * Ideally keep the damage constants in sync with the order of {@link WeaponType}.
 * @readonly
 * @enum {number}
 */
const KarmaUse = Object.freeze({
    // for Attributes use value from AttributeId (value 1-6) enum
    /** @constant - Allows usage of Karma for damage rolls of melee weapon attacks. */
    MELEE_DAMAGE: 10,
    /** @constant - Allows usage of Karma for damage rolls of unarmed combat attacks. */
    UNARMED_DAMAGE: 11,
    /** @constant - Allows usage of Karma for damage rolls of thrown weapon attacks. */
    THROWN_DAMAGE: 12,
    /** @constant - Allows usage of Karma for damage rolls of missile weapon attacks. */
    MISSILE_DAMAGE: 13,
    /** @constant - Allows usage of Karma for spell effect rolls. */
    SPELL_EFFECT: 14,
    /** @constant - Allows usage of Karma for initiative tests. */
    INITIATIVE: 18,
    /** @constant - Allows usage of Karma for recovery tests. */
    RECOVERY_TEST: 20
});


/**
 * Immutable game data of a racial or discipline ability. As most abilities provide fixed bonuses, the data object is often used directly by the app code. 
 * @see {@link Ability}.
 */
class AbilityData {
    #id;
    #name;
    #description;
    /** @type {AbilityType} */
    #type;
    /** @type {?number} */
    #bonusValue;
    /** @type {(KarmaUse|DefenseId|AttributeId|TalentId|null)} - To what character trait the ability bonus applies. Either the id of {@link DefenseType} for {@link AbilityType.DEFENSE}, or an attribute id for {@link AbilityType.INCREASED_ATTRIBUTE_VALUE} or {@link AbilityType.KARMA_USE}, or a karma use constant for {@link AbilityType.KARMA_USE}, or a talent id for {@link AbilityType.FREE_TALENT}, or null if this ability does not affect another trait. */
    #affectedTrait;
    /**
     * @param {number} id - Unique id of this ability 
     * @param {string} name - Unique name of this ability 
     * @param {string} description - Human-readable description of this ability
     * @param {AbilityType} type - Type of bonus this ability provides.
     * @param {?number} [bonusValue=null] - The bonus value/amount provided by this ability. Some abilities (e.g. KARMA_USE or FREE_TALENT) do not provide a numeric bonus but just enable new features/actions. May also be negative (e.g. Blood Elf racial abilities).
     * @param {(KarmaUse|DefenseId|AttributeId|TalentId|null)} [affectedTrait=null] - If the ability type specifies a bonus to a specific character trait, this parameter defines the concrete id/type of the affected trait.
     */
    constructor(id, name, description, type, bonusValue = null, affectedTrait = null) {
        this.#id = id;
        this.#name = name;
        this.#description = description;
        this.#type = type;
        this.#bonusValue = bonusValue;
        this.#affectedTrait = affectedTrait;
        if((type === AbilityType.KARMA_USE || type === AbilityType.DEFENSE || type === AbilityType.FREE_TALENT || type === AbilityType.INCREASED_ATTRIBUTE_VALUE) && !affectedTrait) {
            throw new Error(`Ability of type ${type} must have affectedTrait id`);
        }
        if(type >= AbilityType.DEFENSE && type <= AbilityType.DURABILITY && !bonusValue) {
            throw new Error(`Ability of type ${type} must have bonusValue`);
        }
    }
    getId() {
        return this.#id;
    }
    getName() {
        return this.#name;
    }
    getDescription() {
        return this.#description;
    }
    /**
     * @returns {AbilityType} - The type of this ability. Defines what this ability provides.
     */
    getType() {
        return this.#type;
    }
    /**
     * @returns {?number} - The value of the bonus (positive or negative) this ability provides. May be null if the ability is an enabler.
     */
    getBonusValue() {
        return this.#bonusValue;
    }
    /**
     * @returns {(KarmaUse|DefenseId|AttributeId|TalentId|null)} - The id of the affected trait or null if this ability does not affect a specific trait. What kind of id this represents depends on the {@link AbilityData#getType}.
     */
    getAffectedTraitId() {
        return this.#affectedTrait;
    }
    /**
     * @returns {boolean} - Returns true if this ability's bonus is not commulative or if the ability does not provide any bonus. If a character possesses several abilities of this type (e.g. from multiple disciplines), only the highest {@link AbilityData#getBonusValue} of all these abilities applies but not their sum.
     */
    highestCountsForMultiDisciplines() {
        return this.getType() === AbilityType.KARMA_USE || (this.getType() !== AbilityType.DURABILITY && this.getBonusValue() != null);
    }
    /**
     * @returns {boolean} - Returns true if this ability requires the player to make a choice (select a specific effect in the form of another ability or a specific affected trait).
     */
    requiresChoice() {
        return this.getType() === AbilityType.JOURNEYMAN_SELECTION || this.#isSelectableDefense() || this.#isSelectableKarmaUse();
    }
    /**
     * @returns {AbilityData} - Returns this
     */
    getBaseData() {
        return this;
    }
    /**
     * @returns {boolean} - True if this ability provides Karma use but the affected trait is not predefined but can be chosen by the player when acquiring this ability.
     */
    #isSelectableKarmaUse() {
        return this.getType() === AbilityType.KARMA_USE && this.getAffectedTraitId() === -1;
    }
    /**
     * @returns {boolean} - True if this ability provides a bonus to a defense rating but the affected defense type is not predefined but can be chosen by the player when acquiring this ability.
     */
    #isSelectableDefense() {
        return this.getType() === AbilityType.DEFENSE && this.getAffectedTraitId() === -1;
    }
}

/**
 * An ability owned by a {@link Character}.
 * Can be either a fixed ability or a choice-ability that requires the player to make a selection when gaining this ability. This class is immutable, making a choice requires the creation of a new instance.
 * An instance of this class behaves either like its fixed or choice ability is is based on, when no selection was made, or like the selected option.
 * @extends {AbilityData}
 */
class Ability extends AbilityData {
    /** @type {?AbilityData} - If this is an ability that requires a choice (see for example the Journeyman discipline), then this property contains the original base AbilityData for which another ability had to be selected, else it is null (because the super class will already contain all info). */
    #basedOn = null;
    /**
     * @param {AbilityData} baseData - For fixed abilities this is just the normal game data. For choice-abilities this is the ability that provides the choice (defines the selection options), see {@link AbilityType.JOURNEYMAN_SELECTION}.
     * @param {?AbilityData} [selectedData=null] - Null if this is a fixed ability or no selection was made for a choice ability yet. If baseData is a choice ability and an option was selected, this is the selected option.
     */
    constructor(baseData, selectedData = null) {
        const eff = selectedData ?? baseData;
        super(eff.getId(), eff.getName(), eff.getDescription(), eff.getType(), eff.getBonusValue(), eff.getAffectedTraitId());  // This instance behaves either like the selected option, or if no choice was made yet (or is not required) like the base.
        if(selectedData) {
            this.#basedOn = baseData;
        }
    }
    /**
     * @returns {boolean} - Returns true if this instance represents a successfully made choice. In this case {@link Ability#getBaseData} returns the original choice ability.
     */
    wasSelected() {
        return this.#basedOn != null;
    }
    /**
     * @override
     * @returns {boolean} - Returns true if this ability is either a choice ability that still needs to be fulfilled or an already selected option of a choice ability. Returns false if this is a fixed ability that does not allow/require a choice.
     */
    requiresChoice() {
        return this.wasSelected() || super.requiresChoice();
    }
    /**
     * @override
     * @returns {AbilityData} - Returns either the original choice ability if {@link Ability#wasSelected} returns true, or this if this is a fixed ability or no selection was made yet.
     */
    getBaseData() {
        return this.#basedOn ?? this;
    }
}

/**
 * @typedef {number} RaceId - Unique id of a {@link RaceData}.
 */
/**
 * Immutable game data describing a race/species.
 */
class RaceData {
    #id;
    #name;
    /** @type {number[]} - 1-based array with six entries, one for each {@link AttributeId}. This is the racial modifier that is applied to the initial attribute base value (of 10) during character creation. */
    #attributeMods;
    #karmaMod;
    /** @type {number[]} - Array of AbilityData#getId that defines what abilities this race provides. */
    #abilitiesRaw;
    /** @type {?Ability[]} - lazy-loaded array of racial abilities. Null if not loaded yet. */
    #abilities;
    /**
     * @typedef {object} MovementMod
     * @property {number} land
     * @property {number} [air]
     */
    /**
     * @type {MovementMod}
     */
    #movementMod;
    /** @type {{onehanded:{min:number,max:number}, twohanded:{min:number,max:number}}} - Object giving the minimum and maximum size values of weapons this race can comfortably handle with one or two hands. */
    #usableWeaponSizes;
    /**
     * @param {RaceId} id - Unique id of this race 
     * @param {string} name - Name of this race 
     * @param {number[]} attributes - Array of racial modifiers to be applied to the initial {@link Attribute} value during character creation. Array can be 0 or 1 based but must contain exactly six entries.
     * @param {number} karmaMod - Base modifier for karma. The karma mod defines how many karma points a character can possess multiplied by their Karma Ritual rank.
     * @param {number[]} [usableWeaponSizes=[1,3,6]] - An array of three values: the minimum and maximum size value for one-handed weapons, and the maximum size for two-handed weapons that this race can handle.
     * @param {(number|{land: number, air: number})} [movement=0] - A single number for races that have land movement only, or an object {land:number, air:number} if the race possess flight too.
     * @param {?number[]} [abilities=null] - Array of {@link AbilityData#getId}, or null if the race does not have any racial abilities.
     */
    constructor(id, name, attributes, karmaMod, usableWeaponSizes = [1,3,6], movement = 0, abilities = null) {
        if(attributes.length === AttributeIds.CHARISMA) { // array is zero-based
            attributes.unshift(null);   // shift indices by one to allow access via AttributeId enum
            delete attributes[0];
        } else if(attributes.length < AttributeIds.CHARISMA +1) {    // array should be 1-based and contain exactly 6 entries
            throw new Error(`Attributes modifier array has unexpected length: ${attributes}`);
        }
        if(typeof movement === "number") {
            this.#movementMod = {
                land: movement
            };
        } else {
            if(!movement.hasOwnProperty(MovementType.LAND)) {
                throw new Error("Movement is not a valid object: " + JSON.stringify(movement));
            }
            this.#movementMod = movement;
        }
        if(usableWeaponSizes.length !== 3) {
            throw new Error(`Usable weapon sizes has to contain exactly three entries but was ${usableWeaponSizes}`);
        }
        this.#id = id;
        this.#name = name;
        this.#attributeMods = attributes;
        this.#karmaMod = karmaMod;
        this.#abilitiesRaw = (abilities ?? []);
        this.#abilities = null;
        this.#usableWeaponSizes = {
            onehanded: {
                min: usableWeaponSizes[0],
                max: usableWeaponSizes[1]
            },
            twohanded: {
                min: usableWeaponSizes[1] + 1,
                max: usableWeaponSizes[2]
            }
        }
    }
    getId() {
        return this.#id;
    }
    getName() {
        return this.#name;
    }
    /**
     * Gets racial base value for the given {@link Attribute}. This is the starting value during character creation before assigning additional attribute points.
     * @param {AttributeId} id - Id of an attribute.
     * @returns {number} - Initital attribute value during character creation.
     */
    getAttributeBase(id) {
        return 10 + this.#attributeMods[id];
    }
    getKarmaMod() {
        return this.#karmaMod;
    }
    getKarmaStep() {
        return 4;   // since 3rd Edition all races use the same Karma step of 4 (D6). This can be modified at higher circles by discipline abilities, see Karma class.
    }
    getMovementMod() {
        return this.#movementMod;
    }
    /**
     * @returns {{onehanded:{min:number,max:number}, twohanded:{min:number,max:number}}} - Object giving the minimum and maximum size values of weapons this race can comfortably handle with one or two hands.
     */
    getUsableWeaponSizes() {
        return this.#usableWeaponSizes;
    }
    /**
     * Lazy-loads all abilities from their corresponding ids.
     * @returns {Ability[]} - All racial abilities of this species.
     */
    getAbilities() {
        if(!this.#abilities) {
            this.#abilities = this.#abilitiesRaw.map(id => new Ability(Earthdawn.data.abilities[id]));
        }
        return this.#abilities;
    }
    hasATail() {
        return this.getName() === "T'Skrang";
    }
}

/**
 * Immutable game data of a {@link Discipline}.
 */
class DisciplineData {
    #id;
    #name;
    #coreTalents;
    #talentsOptions;
    /** @type {{unconsciousness: number, death: number}} - Modifier for the character's unconsciousness and death ratings. These mods are added to the final ratings for each rank in the Durability talent. */
    #durability;
    /** @type {Object.<string, number[]>} - Raw mapping of circles to ability ids.*/
    #abilitiesRaw;
    /** @type {Map.<number, AbilityData[]>} - Mapping of circles to array of {@link AbilityData} that belong to these circles. */
    #abilities;
    /** @type {(RaceId[]|null)} - The ids of {@link RaceData} by which this discipline can be learned. If null, this discipline is open to all species.  */
    #limitedToRaces;
    /** @type {((SpellId[]|SpellId|undefined)[]|null)} - Ids of all {@link SpellData} available per circle. May be null if this discipline is not a spellcaster. */
    #spells;
    /**
     * @param {number} id - Unique id of the discipline
     * @param {string} name - Unique name of the discipline
     * @param {number[]} durability - Durability modifiers of the discipline. This is an array containing two numbers: [0] contains the unconsciousness rating modifier, [1] contains the death rating modifier.
     * @param {(TalentId[]|TalentId|undefined)[]} coreTalents - Ids of the core (discipline) talents grouped by circle. The outer array is 1-based and indexed by circle. It can contain undefined if a circle does not have any core talents, or a single {@link TalentId} if it has only one talent, or an array (0-based) of unordered {@link TalentId}s of all core talents available to this discipline on that circle.
     * @param {(TalentId[]|TalentId|undefined)[]} talentOptions - Ids of talent options grouped by status level. The outer array is 1-based and indexed by {@link StatusLevel}. It can contain undefined if a status level does not have any talent options, or a single {@link TalentId} if there is only one option, or an array (0-based) of unordered {@link TalentId}s of all talent options available to this discipline on that status level.
     * @param {Object.<string,number[]>} [abilities={}] - The discipline abilities. The object's keys are circle numbers (given as string due to object literal limitations), the object's values are arrays of {@link AbilityData} ids that belong to the corresponding circle.
     * @param {(RaceId[]|null)} [limitedToRaces=null] - Racial limitations. Null if this discipline is open to any race, else an array of {@link RaceData} ids that define which species may learn this discipline.
     * @param {((SpellId[]|SpellId|undefined)[]|null)} [spells=null] - Ids of spells grouped by circles. The outer array is 1-based and indexed by circle. It can contain undefined if there are no spells on that circle, or a single {@link SpellData} id if there is only one spell, or an array (0-based) of unordered spell ids of all the spells that can be learned on that circle. This parameter may be null if this discipline cannot cast any spells.
     */
    constructor(id, name, durability, coreTalents, talentOptions, abilities = {}, limitedToRaces = null, spells = null) {
        this.#id = id;
        this.#name = name;
        this.#coreTalents = coreTalents;
        this.#talentsOptions = talentOptions;
        this.#durability = { unconsciousness: durability[0], death: durability[1] };
        this.#abilitiesRaw = abilities;
        this.#abilities = null;
        this.#limitedToRaces = limitedToRaces;
        this.#spells = spells;
    }
    getId() {
        return this.#id;
    }
    getName() {
        return this.#name;
    }
    /**
     * @param {RaceData} race - A specific {@link RaceData}
     * @returns {boolean} - Returns true if this discipline can be learned by the given race, else false.
     */
    isAvailableFor(race) {
        return this.#limitedToRaces?.includes(race.getId()) ?? true;
    }
    /**
     * @returns {{unconsciousness: number, death: number}} - Object with modifiers to the character's unconsciousness and death ratings. Add these for each rank in the Durability talent. Returns a fresh copy.
     */
    getDurability() {
        return { ...this.#durability};
    }
    /**
     * @param {number} circle - Circle for which we want to get the core talents.
     * @returns {TalentId[]} - Returns an array with the ids of all {@link TalentData} that are core (discipline) talents of the given circle.
     */
    getCoreTalents(circle) {
        let talentIds = this.#coreTalents[circle];
        if(talentIds == null) {
            return [];
        }
        return Array.isArray(talentIds) ? talentIds : [talentIds];
    }
    /**
     * @param {StatusLevel} statusLevel - Status level for which to retrieve the talent options.
     * @returns {TalentId[]} - Returns an array with the ids of all {@link TalentData} that are talent options for the given status level.
     */
    getTalentOptions(statusLevel) {
        let talentIds = this.#talentsOptions[statusLevel];
        if(talentIds == null) {
            return [];
        }
        return Array.isArray(talentIds) ? talentIds : [talentIds];
    }
    /**
     * @returns {boolean} - Returns true if this discipline can learn/cast any spells.
     */
    hasSpells() {
        return this.#spells != null;
    }
    /**
     * @param {number} circle - Circle for which to retrieve available spells
     * @returns {SpellId[]} - Returns an array with ids of all {@link SpellData} that are available on the given circle. Returns an empty array also for non-spellcaster disciplines (see {@link DisciplineData#hasSpells}).
     */
    getSpells(circle) {
        if(this.#spells == null || this.#spells[circle] == null) {
            return [];
        }
        let spellIds = this.#spells[circle];
        return Array.isArray(spellIds) ? spellIds : [spellIds];
    }
    /**
     * @returns {TalentId} - Returns the id of the Thread Weaving talent if this discipline is a spellcaster (see {@link DisciplineData#hasSpells}).
     * @throws {Error} - Throws if this method is called on non-spellcaster disciplines.
     */
    getThreadWeavingIdForSpellcasting() {
        if(!this.hasSpells()) {
            throw new Error(`This discipline does not have a spellcasting related ThreadWeaving talent`);
        }
        return this.getCoreTalents(1).find(talentId => {    // we assume the talent is available as a first circle core talent.
            const talentData = Earthdawn.data.talents[talentId];
            return talentData.getName().startsWith("Thread Weaving");
        });
    }
    /**
     * @returns {Map.<number, AbilityData[]>} - Returns a Map of all discipline abilities. The map key is the circle on which an ability becomes available, the value is an array of the {@link AbilityData} of this circle.
     */
    getAllAbilities() {
        if(!this.#abilities) {
            this.#abilities = new Map();
            Object.entries(this.#abilitiesRaw).forEach(([key, ids]) => {    // convert object to Map and change value from array of ids to array of AbilityData
                const data = ids.map(id => Earthdawn.data.abilities[id]);   
                this.#abilities.set(Number(key), data);     // cast type of key from string representation of a number to a number
            });
        }
        return this.#abilities;
    }
}

/**
 * A mutable instance of a discipline learned by a {@link Character}.
 * The discipline controls and manages all learned {@link Talent}s of a character because the discipline defines when and how a talent is acquired. For simplicities sake foreign talents learned via Versatility are always added to the learned
 * talents of the first discipline (position = 1) because that discipline also contains the racial discipline Versatility.
 * {@link Spell}s are also provided by a discipline but are maintained on the {@link Character} directly.
 * {@link Skill}s are also directly managed on the character.
 */
class Discipline {
    #data;
    #character;
    /** @type {number} - Current circle in this discipline. Every discipline starts on circle 1. Maximum is {@link Earthdawn.rules.getMaximumCircle} */
    #circle;
    /** @type {number} - Is this the first (1), second (2), third (3), etc. discpline of a character? Important for calculating costs. Only the first discipline provides the KarmaRitual talent and defines when attributes can be improved. Durability is taken from the first. Talents may be shared between disciplines for circle advancement. Talent Options may be converted later to core talents of a new discpline. */
    #position;
    /** @type {Talent[][]} - All learned talents grouped by circle. The outer array is indexed by circle (1-based), the inner arrays (0-based) contain all unordered {@link Talent}s learned. Learned talents normally have a minimum rank of 1, except for some free talents gained through abilities that can have rank 0. */
    #talents;
    /** @type {Map.<number, Ability[]>} - All discipline abilities accessible because they became available on the current or a lower circle. Map key is the circle, map value an array of {@link Ability}. */
    #abilities;
    /**
     * @param {DisciplineData} data - The basic game data of this discipline.
     * @param {Character} character - Owning character
     * @param {number} [position=1] - Learning position of this discipline. The very first discipline a character possesses has position 1. If the adept learns a second discipline it would have position 2, the 3rd one 3 and so on.
     */
    constructor(data, character, position = 1) {
        this.#data = data;
        this.#character = character;
        this.#position = position;
        this.#circle = 1;
        this.#talents = [];
        this.#abilities = new Map();
    }
    getId() {
        return this.#data.getId();
    }
    getData() {
        return this.#data;
    }
    /** 
     * @returns {number} - Returns the current circle of this learned discipline. Minimum 1.
     */
    getCircle() {
        return this.#circle;
    }
    /**
     * @returns {number} - Returns the 1-based position that describes when this discipline was learned (i.e. as first, second, third...).
     */
    getPosition() {
        return this.#position;
    }
    /**
     * @param {number} talentCircle - Circle on which the new talent should be learned. Defines the legend point costs.
     * @returns {boolean} - Returns true if the circle is within the allowed range (less or equal to the current discipline circle) and the character has enough unspent legend points to cover the costs.
     */
    canAffordNewTalent(talentCircle) {
        if(talentCircle > this.getCircle()) {
            return false;
        }
        const fakeTalent = new Talent(null, talentCircle, null, this, this.#character);     // does not add this temp talent to the character
        return fakeTalent.canIncreaseRank();    // let the talent calculate the costs.
    }
    /**
     * Learns a new talent on rank 1 by paying its legend point costs. Adds it to this discipline.
     * Reduce a talent's rank to zero via {@link Talent#decreaseRank} to unlearn and remove a talent again.
     * @param {TalentData} talentData - The game data of the new talent that should be learned.
     * @param {TalentSource} source - How this talent became available to the character when it was learned.
     * @param {number} circle - The circle on which the talent is learned. Core talents of a discipline have fixed slots (except when learned from a different source like e.g. Versatility) while talent options can be learned at any of the free slots after it became accessible.
     * @returns {Talent} - Returns the newly learned talent after it was added to this discipline.
     */
    learnNewTalent(talentData, source, circle) {
        const talent = this.addTalent(talentData, source, circle);
        talent.increaseRank();
        return talent;
    }
    /**
     * Creates a new {@link Talent} from the given information and adds it to this discipline. Does not set any ranks on the talent. The talent could be a talent not native to this discipline (like from racial ability or versatility). Assumes that someone else has already checked that this talent can be safely added.
     * Must be called only during character creation by {@link Character}! Use {@link Discipline#learnNewTalent} to learn a talent during play.
     * @param {TalentData} talentData - The game data of the new talent that should be learned.
     * @param {TalentSource} source - How this talent became available to the character when it was learned.
     * @param {number} circle - The circle on which the talent is learned.
     * @returns {Talent} - Returns the newly created talent after it was added to this discipline.
     */
    addTalent(talentData, source, circle) {
        if(circle > this.getCircle() || circle < 1) {
            throw new Error(`Invalid circle ${circle} given`);
        }
        const talent = new Talent(talentData, circle, source, this, this.#character);
        if(talent.getSource() === TalentSource.CORE) {
            if(!this.#data.getCoreTalents(circle).includes(talent.getId())) {
                throw new Error(`Talent ${talent.getId()} is not a core talent of discipline ${this.#data.getName()}`);
            }
            // no further check that this talent can still be added (is available and not a duplicate). Has to be done beforehand somewhere else.
        } else if(talent.getSource() === TalentSource.OPTION) {
            let found = false;
            for(let level = StatusLevel.INITIATE; level <= Earthdawn.rules.getStatusLevelForCircle(circle); ++level) {
                if(this.#data.getTalentOptions(level).includes(talent.getId())) {
                    found = true;
                }
            }
            if(!found) {
                throw new Error(`Talent ${talent.getId()} is not a talent option of discipline ${this.#data.getName()}`);
            }
            // no further check that this talent can still be added (is available and there is a slot free for it). Has to be done beforehand somewhere else.
        } else if(talent.getSource() === TalentSource.VERSATILITY && this.getPosition() !== 1) {
            throw new Error(`Foreign talents learned via Versatility can be added only to the first discipline that also owns the Versatility talent`);
        }
        let talents = this.#talents[circle];
        if(talents == null) {
            this.#talents[circle] = talents = [];
        }
        talents.push(talent);
        return talent;
    }
    /**
     * Removes a talent from the list of learned talents. Must be called only by the {@link Talent} class itself.
     * Assumes that someone else checked already that the talent can be safely removed. Use {@link Talent#decreaseRank} to unlearn a talent.
     * @param {Talent} talent - The talent instance that should be removed from this discipline.
     */
    removeTalent(talent) {
        if(!(talent instanceof Talent)) {
            throw new Error(`Wrong argument type: ${talent} is not a Talent`);
        }
        const talents = this.#talents[talent.getBoughtOnCircle()] ?? [];
        const location = talents.indexOf(talent);
        if(location === -1) {
            throw new Error(`Talent ${talent.getId()} not found in list of learned talents`);
        }
        talents.splice(location, 1);
    }
    /**
     * Get all {@link Talent}s learned by this discipline so far. The returned arrays are live references, do not modify them!
     * @returns {Talent[][]} - Returns an array of arrays of talents: the outer array is indexed by circle (1-based), the inner arrays contain the unsorted learned talents (may be undefined or empty if no talents were learned on a circle).
     */
    getLearnedTalents() {
        return this.#talents;
    }
    /**
     * Get all {@link Talent}s learned by this discipline on a specific circle. The returned array is a live references, do not modify it!
     * @param {number} circle - Circle to limit the result to the talents learned on this specific circle.
     * @returns {Talent[]} - Returns an unsorted array of talents learned on that circle (may be empty).
     */
    getLearnedTalentsOf(circle) {
        return this.getLearnedTalents()[circle] ?? [];
    }
    /**
     * @returns {Talent[]} - Returns an array of {@link Talent}s of all spell matrices/fetishes learned in this discipline (including by Versatility).
     */
    getSpellMatrices() {
        const result = [];
        this.getLearnedTalents().forEach(byCircle => {
            byCircle.forEach(talent => {
                if(KnownTalent.isAnySpellMatrix(talent.getId())) {
                    result.push(talent);
                }
            });
        });
        return result;
    }
    /**
     * Get the number of unused versatility slots. That means the number of additional foreign talents that can still be supported by that discipline's Versatility talent.
     * @returns {number} - Returns the number of ranks in the Versatility talent minus count of already learned foreign talents. If the discipline does not have the Versatility talent at all, -1 is returned (not 0).
     */
    getOpenVersatilitySlots() {
        let totalRanks = 0;
        let hasVersatility = false;
        this.getLearnedTalents().forEach(byCircle => {
            byCircle.forEach(talent => {
                if(talent.is(KnownTalent.VERSATILITY)) {
                    hasVersatility = true;
                    totalRanks += talent.getTotalRank();
                } else if(talent.getSource() === TalentSource.VERSATILITY) {
                    totalRanks--;
                }
            });
        });
        return hasVersatility ? totalRanks : -1;
    }
    /**
     * Get all core talents of this discipline that have not been learned yet. Because core talents occupy fixed slots in a discipline, that represents all these open slots.
     * This method also checks the learned talents of other disciplines (of a multi-discipline character) to see if a talent was learned in another discipline already and thus is not available anymore.
     * @param {number} [onlyThisCircle=0] - If omitted or 0 is given, then all available core talents grouped by circle up to the current circle are returned. If a specific circle (> 0) is given, than only the not yet learned core talents on that circle are returned (but still as array of arrays).
     * @returns {TalentData[][]} - Returns an array (index = circle) of arrays of TalentData of all not yet learned core talents of this discipline (optionally restricted to given circle) up to the current circle.
     */
    getAvailableCoreTalents(onlyThisCircle = 0) {
        if(onlyThisCircle < 0 || onlyThisCircle > Earthdawn.rules.advancement.getMaximumCircle()) {
            throw new Error(`Invalid circle ${onlyThisCircle} given`);
        }
        const allLearned = this.collectAllLearnedTalentsAcrossDisciplinesAndCountOwnUsedSlots(this.#character.getDisciplines(), TalentSource.CORE);
        const result = [];
        const start = Math.max(1, onlyThisCircle);
        const end = onlyThisCircle === 0 ? this.#circle : onlyThisCircle;
        for(let circle = start; circle <= end; ++circle) {
            const byCircle = result[circle] = [];
            this.#data.getCoreTalents(circle).forEach(talentId => {
                // TODO: could theoretically always omit KarmaRitual and Durability if this is not the first discipline (unclear how Durability should work in multi-discipline setup), but we just allow the player to buy it once in whatever discipline (unlikely that they first buy it in a second, third etc. discipline).
                const entry = allLearned.get(talentId);
                if(!entry) {
                    byCircle.push(Earthdawn.data.talents[talentId]);
                } else if(entry.count) {
                    if(entry.count[circle] > 0) {
                        entry.count[circle]--;
                    } else {
                        byCircle.push(Earthdawn.data.talents[talentId]);
                    }
                }
            });
        }
        return result;
    }
    /**
     * Get all talent options of this discipline that have not been learned yet.
     * This method also checks the learned talents of other disciplines (of a multi-discipline character) to see if a talent was learned in another discipline already and thus is not available anymore.
     * @param {number} upperCircleLimit - If omitted or 0 is given, then all available talents grouped by {@link StatusLevel} up to the level matching the current circle are returned. If a specific circle (> 0) is given, than only the not yet learned core talents on that circle's status level are returned (but still as array of arrays).
     * @returns {TalentData[][]} - Returns an array (index = {@link StatusLevel}) of arrays of TalentData of all not yet learned talent options of this discipline (optionally restricted to given circle) up to the current circle's status level.
     */
    getAvailableTalentOptions(upperCircleLimit = 0) {
        if(upperCircleLimit < 0 || upperCircleLimit > Earthdawn.rules.advancement.getMaximumCircle()) {
            throw new Error(`Invalid upper circle limit ${upperCircleLimit} given`);
        }
        const allLearned = this.collectAllLearnedTalentsAcrossDisciplinesAndCountOwnUsedSlots(this.#character.getDisciplines(), TalentSource.OPTION);
        const result = [];
        const start = StatusLevel.INITIATE;
        const end = Earthdawn.rules.getStatusLevelForCircle(upperCircleLimit ? upperCircleLimit : this.#circle);
        for(let statusLevel = start; statusLevel <= end; ++statusLevel) {
            const byLevel = result[statusLevel] = [];
            this.#data.getTalentOptions(statusLevel).forEach(talentId => {
                const entry = allLearned.get(talentId);
                if(!entry) {
                    byLevel.push(Earthdawn.data.talents[talentId]);
                } else if(entry.count) {
                    if(entry.count[statusLevel] > 0) {
                        entry.count[statusLevel]--;
                    } else {
                        byLevel.push(Earthdawn.data.talents[talentId]);
                    }
                }
            });
        }
        return result;
    }
    /**
     * @returns {number[]} - Returns the number of unused talent options (slots) per circle (=array index). Assumes that one talent option slot per circle is normally available.
     */
    getTalentOptionSlots() {
        let result = Array(this.#circle).fill(1);
        result.unshift(null);
        for(let circle = 1; circle <= this.#circle; ++circle) {
            const talents = this.#talents[circle];
            if(talents != null && talents.some(talent => talent.getSource() === TalentSource.OPTION)) {
                result[circle] = 0;
            }
        }
        return result;
    }
    /**
     * Gets a list of all foreign talents that can still be learned via Versatility and the circle on which they can be learned. This is the lowest circle on which the talent becomes available to any discipline.
     * According to the rules a character cannot learn core talents from their own disciplines via Versatility (but talent options are ok), but if the character later acquires a new discipline it may contain a talent that they had already learned via Versatility before.
     * This method can be called only on the first discipline (of a multi-discipline character) because Versatility is part of the first discipline.
     * @param {boolean} affordableWithLpOnly - If set, only talents are returned for which the character has enough unspent legend points left to pay the costs.
     * @param {?number} [upToCircle=null] - Limit the returned talents to ones that are available to this circle or lower. If this is null then the character's highest achieved circle in any of their disciplines is used as the upper circle limit of the returned talents.
     * @param {boolean} [includeSpellMatricesOnlyOnce=true] - If true then each type of spell matrix talent is included only once on the lowest available circle in the result, else all possible occurrences are included (could be even more than once per circle).
     * @returns {{data: TalentData, circle: number}[]} - Returns an unsorted array of objects. The data propery specifies the {@link TalentData}, the circle tells on what circle the character could learn the talent.
     */
    getAvailableVersatilityOptions(affordableWithLpOnly = true, upToCircle = null, includeSpellMatricesOnlyOnce = true) {
        if(this.getPosition() !== 1) {
            throw new Error(`Versatility can only be used on the first discipline`);
        }
        if(upToCircle != null && upToCircle < 1 || upToCircle > Earthdawn.rules.advancement.getMaximumCircle()) {
            throw new Error(`Given circle limit ${upToCircle} is not within allowed range`);
        }
        if(!this.isJourneyman() && !this.getLearnedTalentsOf(1).some(talent => talent.is(KnownTalent.VERSATILITY))) {
            throw new Error(`No Versatility talent found on this discipline`);
        }
        const unspentlp = affordableWithLpOnly ? this.#character.getLegendPoints().getUnspent() : 0;
        const highestAchievedCircle = upToCircle != null ? upToCircle : this.#character.getDisciplines().reduce((accu, discipline) => Math.max(accu, discipline.getCircle()), 1);
        /** @type {number[][]} - Outer array indexed by {@link TalentId}, inner arrays indexed by circle with values being the count how often that talent is available on that circle. All arrays are sparce. */
        const allTalentsWithinRange = [];
        // collect all possible talents from all disciplines (count how often because of spell matrices).
        Earthdawn.data.disciplines.forEach(discipline => {
            if(!discipline.isAvailableFor(this.#character.getRace())) {
                return;
            }
            for(let circle = 1; circle <= highestAchievedCircle; ++circle) {
                discipline.getCoreTalents(circle).forEach(talentId => {
                    let info = allTalentsWithinRange[talentId];
                    if(info == null) {
                        allTalentsWithinRange[talentId] = info = [];
                    }
                    info[circle] = (info[circle] ?? 0) + 1;
                });
            }
            for(let status = StatusLevel.INITIATE; status <= Earthdawn.rules.getStatusLevelForCircle(highestAchievedCircle); ++status) {
                discipline.getTalentOptions(status).forEach(talentId => {
                    const circle = Earthdawn.rules.getLowestCircleForStatusLevel(status);
                    let info = allTalentsWithinRange[talentId];
                    if(info == null) {
                        allTalentsWithinRange[talentId] = info = [];
                    }
                    info[circle] = (info[circle] ?? 0) + 1;
                });
            }
        });
        delete allTalentsWithinRange[KnownTalent.VERSATILITY.getId()];  // can't learn Versatility via Versatility.
        /** 
         * @function 
         * @param {TalentId} talentId - Id of the talent
         * @param {number} circle - Circle on which it was learned or becomes available.
         */
        const remover = (talentId, circle) => {
            const info = allTalentsWithinRange[talentId];
            if(info) {
                if(KnownTalent.isAnySpellMatrix(talentId)) {
                    if(info[circle]) {
                        info[circle] -= 1;
                    }
                } else {
                    delete allTalentsWithinRange[talentId];
                }
            }
        };
        // Remove all core talents that are or will become available in one of the owned disciplines
        this.#character.getDisciplines().forEach(discipline => {
            for(let circle = 1; circle <= Earthdawn.rules.advancement.getMaximumCircle(); ++circle) {
                discipline.#data.getCoreTalents(circle).forEach(id => remover(id, circle));
            }
        });
        // Remove all talents which have been already learned. That may include talents which have later become available through new disciplines and that were therefore already removed in the previous step.
        this.getLearnedTalents().forEach(byCircle => {
            byCircle.forEach(talent => {
                if(talent.getSource() !== TalentSource.CORE) {  // skip core talents because we have already removed them in the previous step to prevent double removing of spell matrices.
                    remover(talent.getId(), talent.getBoughtOnCircle());
                }
            });
        });
        // Now map all remaining entries into a flat array of {data: TalentData, circle: number};
        const result = [];
        allTalentsWithinRange.forEach((info, id) => {
            if(!includeSpellMatricesOnlyOnce && KnownTalent.isAnySpellMatrix(id)) {  // add all occurrences because spell matrices can be learned multiple times
                info.forEach((count, circle) => {
                    if(!affordableWithLpOnly || Earthdawn.rules.advancement.getTalentLegendPointCost(1, Earthdawn.rules.getStatusLevelForCircle(circle), this.getPosition()) <= unspentlp) {
                        const data = Earthdawn.data.talents[id];
                        while(count-- > 0) {
                            result.push({data: data, circle: circle});
                        }
                    }
                });
            } else {    // add only one occurrence of the lowest possible circle because normal talents can be learned only once
                const circle = info.findIndex(count => count > 0);
                if(circle !== -1 && (!affordableWithLpOnly || Earthdawn.rules.advancement.getTalentLegendPointCost(1, Earthdawn.rules.getStatusLevelForCircle(circle), this.getPosition()) <= unspentlp)) {
                    result.push({data: Earthdawn.data.talents[id], circle: circle});
                }
            }
        });
        return result;
    }
    /**
     * @typedef {object} UsedSlotsInfo
     * @property {(Talent|null)} talent - The actual leaned talent. May be null for spell matrices/fetishes where we report an aggregate count instead.
     * @property {Discipline} discipline - The discipline that owns the learned talent. For spell matrices/fetishes this is always the current discipline.
     * @property {(undefined|number[])} count - Undefined if the talent can be learned only once, else (i.e. spell matrices) an array indexed by circle (core talents) or status level (talent options) that contains the count how often the talent is available on that circle/level.
     */
    /**
     * Depending on the given source parameter checks either the used core talent or talent option slots of this discipline. It cross-references all learned disciplines and checks if (and in case of spell matrices/fetisches also how often) the
     * talent was already learned. The resulting map can be used to calculate the number of unused slots for the index talents.
     * @param {Discipline[]} disciplines - All disciplines the character owns
     * @param {TalentSource} source - What talent source (slots) should be counted for this discipline only. This parameter has no effect on the other disciplines.
     * @returns {Map.<TalentId, UsedSlotsInfo>} - Returns a map indexed by talent id that contains an info object (see {@link UsedSlotsInfo}).
     */
    collectAllLearnedTalentsAcrossDisciplinesAndCountOwnUsedSlots(disciplines, source) {   // returns a Map of all learned talents (across all disciplines) indexed by talent id. Value is an object {talent: Talent, discipline: Discipline that has learned the talent, count: an optional array for spell matrices }. For spell matrices count is an array (indexed by circle (for core) or status level (for options)) where the value is a number count of how often this talent was already learned. The source parameter defines what kind of talent slots we are interested in for the current discipline (does not matter for other disciplines), basically only important for counting spell matrices.
        const allLearned = new Map();   // collects all talents learned across all disciplines. Assumes that each talent can be learned at most once (regardless from which source) except for spell matrices.
        disciplines.forEach(discipline => {
            discipline.getLearnedTalents().forEach(byCircle => {
                for(const talent of byCircle) {
                    if(KnownTalent.isAnySpellMatrix(talent.getId())) {     // all spell matrices can be learned multiple times between all disciplines, therefore count only the slots for this discipline and ignore from others.
                        // FIXME: This whole spell matrix counting logic does not take into account that a character could have learned these talents also by Versatility. If a character multi-disciplines in all spellcaster disciplines and is also a human with versatility, then we would somehow have to figure out how many spell matrix slots were already taken as foreign talent via versatility. But this whole scenario is quite unlikely, so we don't give a shit now.
                        // TODO: In order to fix the counting for versatility too, we would have to:
                        // - total count how often a spell matrix talent (per type) was learned also by other disciplines (any source), or via versatility, or by this discipline (opposite source). This could be done here in the loop (using a local variable).
                        // - in the end check how many spell caster disciplines there are and how many spell matix slots per type (core and options) they provide in total (possible count).
                        // - compare the total count with the possible count. Decrease the number of open slots in this discipline accordingly (by increasing the entry count artificially) to make up for any versatility uses, so that we never allow the character to learn more spell matrices as a human than would be totally available.
                        if(discipline === this && talent.getSource() === source) {
                            let entry = allLearned.get(talent.getId());
                            if(!entry) {
                                entry = {talent: null, discipline: discipline, count: []};        // BEWARE: we set the talent to null here because this is an aggregated value and represents more than one talent instance.
                                allLearned.set(talent.getId(), entry);
                            }
                            const groupIndex = source === TalentSource.OPTION ? Earthdawn.rules.getStatusLevelForCircle(talent.getBoughtOnCircle()) : talent.getBoughtOnCircle();
                            entry.count[groupIndex] = (entry.count[groupIndex] ?? 0) + 1;
                        }
                    } else {
                        allLearned.set(talent.getId(), {talent: talent, discipline: discipline});
                    }
                }
            });
        });
        return allLearned;
    }
    /**
     * Checks if the circle of this discipline can advance to the given circle. It uses the ED2 advancement rules that require a certain number of talents on a certain rank dependant on the circle to acquire.
     * This method can be used to either check if this discipline can advance to the next circle (in which case the second parameter must be omitted), or to check if decreasing an existing talent's rank would
     * violate the rules for a previous circle advancement. In the latter case the first parameter would denote the current or a lower circle and the second parameter the talent that should be decreased.
     * 
     * @param {number} newCircle - The circle this discipline should be advanced to. Can also be a lower circle than the current one to check if decreasing talent ranks is possible instead of violating advancement rules.
     * @param {(Talent|null)} [checkForTalent=null] - If given check only whether this talent would make a different if its rank was one lower than it currently is. Omit to check for normal advancement. This is used only to check if a talent can be decreased without violating previous circle advancements.
     * @returns {boolean} - Returns true if circle advancement in this discipline is possible given all learned talents.
     */
    canAdvanceToCircle(newCircle, checkForTalent = null) {
        // This code checks advancement by looking for a certain number of talents per circle with a minimum rank equal to the next circle. One of these talents has to come from the current (= newCircle -1) circle.
        // This is the standard advancement rule in ED1 and ED2 but an optional rule in later versions. ED3 and ED4 by default just require all core talents to be learned.
        if(newCircle === 1 && this.getPosition() === 1) {
            return true;
        }
        if(newCircle > Earthdawn.rules.advancement.getMaximumCircle()) {
            return false;
        }
        const relevantTalentIds = new Map();    // all the core talents of this discipline up to the circle below the new one. The character has to have a certain number of these talents on a certain rank to qualify for advancement. Talent options only count if they were actually learned already. Available options learned by other disciplines do not count. Vice versa however, the talent options of other disciplines count against the core talent requirements (relevant talents map).
        const fromTheLastCircle = new Map();    // all talents that count as belonging to the previous circle (= newCircle -1, or corresponding status level)
        for(let circle = 1; circle < newCircle; ++circle) {
            this.#data.getCoreTalents(circle).forEach(id => {
                relevantTalentIds.set(id, (relevantTalentIds.get(id) ?? 0) +1);
                if(circle === newCircle -1) {
                    fromTheLastCircle.set(id, true);
                }
            });
        }
        // If this is an additional discipline (i.e. second, third...) and we check for its first circle, then count only core talents and always require all of them. That is the rule for acquiring a new discipline.
        const minimumNumberOfTalents = (newCircle === 1 && this.getPosition() > 1) ? relevantTalentIds.size : Earthdawn.rules.advancement.getMinimumNumberOfTalentsToAdvanceTo(newCircle);
        const skipOwnOptions = (newCircle === 1 && this.getPosition() > 1) ? true : false;
        // count learned talents to check if we satisfy the requirements
        let hasTalentFromCurrentCircle = false;
        let countOfMatchingTalents = 0;
        const minimumRank = Earthdawn.rules.advancement.getMinimumTalentRankToAdvanceTo(newCircle);
        const isJourneyman = this.isJourneyman();
        for(let circle = newCircle -1; circle >= 1; --circle) {
            this.getLearnedTalentsOf(circle).forEach(talent => {
                if(talent.getSource() !== TalentSource.VERSATILITY || !talent.is(KnownTalent.VERSATILITY) || isJourneyman) {      // the character could have a talent from an ability (like AstralSight) but would only be able to count it against the required talents if it is also a possible core talent or talent option of this discipline.
                    const rank = (talent === checkForTalent) ? talent.getTotalRank() -1 : talent.getTotalRank();
                    if(rank >= minimumRank) {
                        if(!skipOwnOptions && talent.getSource() === TalentSource.OPTION) {
                            countOfMatchingTalents += 1;
                            if(talent.getBoughtOnCircle() === newCircle -1) {
                                hasTalentFromCurrentCircle = true;
                            }
                        } else if(isJourneyman) {       // for a Journeyman all talents count, there are no core talents or talent options
                            countOfMatchingTalents += 1;
                            if(circle === newCircle -1) {
                                hasTalentFromCurrentCircle = true;
                            }
                        } else if(relevantTalentIds.get(talent.getId()) > 0) {
                            countOfMatchingTalents += 1;
                            relevantTalentIds.set(talent.getId(), relevantTalentIds.get(talent.getId()) - 1);   // spell matrices can be learned multiple times (also across disciplines). but they count only that many times for advancement as they are available to this discipline
                            if(fromTheLastCircle.has(talent.getId())) {
                                hasTalentFromCurrentCircle = true;
                            }
                        }
                    }
                }
            });
        }
        if(hasTalentFromCurrentCircle && countOfMatchingTalents >= minimumNumberOfTalents) {
            return true;
        }
        // if the learned talents from this discipline were not enough to satisfy the advancement requirement, check the other disciplines too.
        const otherDisciplines = this.#character.getDisciplines().filter(discipline => discipline !== this);
        otherDisciplines.forEach(discipline => {
            discipline.getLearnedTalents().forEach((byCircle) => {    // for other disciplines check all circles because a matching talent could be from a higher circle than newCircle
                byCircle.forEach(talent => {
                    const rank = (talent === checkForTalent) ? talent.getTotalRank() -1 : talent.getTotalRank();
                    if(rank >= minimumRank && relevantTalentIds.get(talent.getId()) > 0) {
                        countOfMatchingTalents += 1;
                        relevantTalentIds.set(talent.getId(), relevantTalentIds.get(talent.getId()) - 1);
                        if(fromTheLastCircle.has(talent.getId())) {
                            hasTalentFromCurrentCircle = true;
                        }
                    }
                });
            });
        });
        return hasTalentFromCurrentCircle && countOfMatchingTalents >= minimumNumberOfTalents;
    }
    /**
     * Increase the circle of this discipline by one and acquire all new discipline abilities for this new circle.
     * Recalculates all characteristics.
     */
    advanceToNextCircle() {     // learn and increase circle in this discipline
        if(!this.canAdvanceToCircle(this.getCircle() + 1)) {
            throw new Error(`Cannot advance to circle ${this.getCircle() + 1} in this discipline`);
        }
        this.#circle += 1;
        if(this.getData().getAllAbilities().has(this.getCircle())) {
            this.#abilities.set(this.getCircle(), this.getData().getAllAbilities().get(this.getCircle()).map(data => new Ability(data)));
        }
        this.#character.evictCache("disciplineAbilities");
        this.#character.calculateCharacteristics();
    }
    /**
     * @returns {boolean} - Returns true if it is possible to decrease the circle of this discipline by one. To completely forget a discipline (remove it from the character) it has to be the last one learned (highest position).
     */
    canForgetCircle() {
        if(this.getPosition() === 1 && this.getCircle() === 1) {    // cannot remove the first and only discipline
            return false;
        }
        if(this.#character.getAvailableAttributeImprovements() <= 0) {      // gain one attribute improvement per circle advancement. If they are all used up then we cannot decrease the circle
            return false;
        }
        if(this.getData().hasSpells() && this.#character.getFreeSpellCircles() < this.getCircle() * 2) {
            return false;
        }
        if(this.getCircle() === 1) {
            if(this.getPosition() < this.#character.getDisciplines().length) {  // we can forget an additional discipline only if it is the last one acquired, in all other cases we would have to adjust all following disciplines which would be a nightmare
                return false;
            }
        } else if(this.getLearnedTalentsOf(this.getCircle()).length > 0) {   // may not have any talents from the circle that we want to remove. Exception: forgetting the first circle of a new discipline (see above).
            return false;
        }
        return true;
    }
    /**
     * Decrease the circle in this discipline by one. Only possible if there are no attribute improvements that rely on this circle and no talents learned from this circle (and no free spells). See also {@link Discipline#canForgetCircle}.
     */
    forgetCircle() {
        if(!this.canForgetCircle()) {
            throw new Error(`Cannot reduce circle of this discipline to ${this.getCircle() - 1}`);
        }
        this.#abilities.delete(this.getCircle());
        this.#character.evictCache("disciplineAbilities");
        this.#character.calculateCharacteristics();
        if(this.#circle === 1) {    // can happen only for additional disciplines. Only first circle talents should be remaining at this point.
            this.getLearnedTalentsOf(1).concat().forEach(talent => talent.forgetTalent());    // does this on a concat copy because talents remove themselves when they reach rank zero.
            this.#character.removeDiscipline(this);
            this.#character = null;
        } else {
            this.#circle -= 1;
        }
    }
    /**
     * @returns {boolean} - Returns true if this is the Journeyman discipline. Journeyman works vastly different than most other disciplines and it can never be used for multi-discipline characters.
     */
    isJourneyman() {
        return this.#data.getName() === 'Journeyman';
    }
    /**
     * @returns {Ability[]} - Returns a flat array of all abilities acquired so far by this discipline.
     */
    getAbilities() {
        const result = [];
        for(const [key, byCircle] of this.getAbilitiesByCircle()) {
            result.push(...byCircle);
        }
        return result;
    }
    /**
     * @returns {Map.<number, Ability[]>} - Returns a Map keyed by circle that contains all corresponding acquired discipline abilities. Only up to the current circle in this discipline.
     */
    getAbilitiesByCircle() {
        return this.#abilities;
    }
    /**
     * Updates an existing Ability in place by replacing it with a new entry. Should be used only for {@link Ability} that require a choice in order to change the selected choice.
     * @param {Ability} oldAbility - The currently owned possessed ability
     * @param {Ability} replacement - The new ability that should replace the old one.
     */
    updateAbility(oldAbility, replacement) {    // 
        if(!(oldAbility instanceof Ability) || !(replacement instanceof Ability)) {
            throw new Error(`Only inputs of type Ability allowed`);
        }
        for(const [key, byCircle] of this.getAbilitiesByCircle()) {
            const found = byCircle.indexOf(oldAbility);
            if(found !== -1) {
                byCircle.splice(found, 1, replacement);
                this.#character.evictCache("disciplineAbilities");
                this.#character.calculateCharacteristics();
                return;
            }
        }
        throw new Error(`No matching ability ${oldAbility.getId()} found in discipline`);
    }
    /**
     * @returns {{unconsciousness: number, death: number}} - If this discipline contains the learned Durability talent, returns the non-zero bonuses for the two health ratings. Else the returned bonuses are zero.
     */
    getHealthBonus() {
        for (const byCircle of this.#talents) {
            if(!byCircle) {
                continue;
            }
            const durability = byCircle.find(talent => talent.is(KnownTalent.DURABILITY));
            if(durability) {
                const result = this.#data.getDurability();
                const improvement = this.getAbilities().find(ability => ability.getType() === AbilityType.DURABILITY);
                if(improvement) {
                    result.unconsciousness += improvement.getBonusValue();
                    result.death += improvement.getBonusValue();
                }
                result.unconsciousness *= durability.getTotalRank();
                result.death *= durability.getTotalRank();
                return result;
            }
        }
        return { unconsciousness: 0, death: 0 };
    }
    /**
     * Saves the selected options of choice-abilities to the character save state for serialization. @see {@link Ability}
     * @param {object} state - The save state that will be modified.
     */
    #saveAbilitiesTo(state) {
        let saved = null;
        for(const [key, value] of this.getAbilitiesByCircle()) {
            value.forEach(ability => {
                if(ability.wasSelected()) {
                    if(!saved) {
                        saved = {};
                    }
                    let byCircle = saved[key.toString()];
                    if(!byCircle) {
                        saved[key.toString()] = byCircle = {};
                    }
                    byCircle[ability.getBaseData().getId().toString()] = ability.getId();
                }
            });
        }
        if(saved) {
            state.ab = saved;
        }
    }
    /**
     * Loads all abilities of this discipline. Restores the information what options were selected for choice abilities. @see {@link Ability}
     * @param {object} state - Saved state. Do not modify!
     * @param {number} version - Serialization version used during saving
     */
    #loadAbilities(state, version) {
        const savedAbilities = state.ab ?? {};
        for(const [key, value] of this.getData().getAllAbilities()) {
            if(key <= this.getCircle()) {
                const savedByCircle = savedAbilities[key.toString()] ?? {};
                this.#abilities.set(key, value.map(data => {
                    const selection = savedByCircle[data.getId().toString()];
                    if(!data.requiresChoice() || !selection) {
                        return new Ability(data);
                    }
                    return new Ability(data, Earthdawn.data.abilities[selection]);
                }));
            }
        }
    }
    /**
     * @returns {object} - Returns the saved state of this discipline for serialization.
     */
    save() {
        const state = {
            id: this.#data.getId(),
            po: this.#position,
            cl: this.#circle
        };
        state.ta = this.#talents.map(byCircle => {
            return byCircle.map(talent => talent.save());
        });
        state.ta.shift();  // remove non-existant circle 0 because IndexDb will replace its value with null otherwise
        this.#saveAbilitiesTo(state);
        return state;
    }
    /**
     * Deserializes a discipline from a saved state.
     * @param {object} state - Saved character state. Do not change this object during deserialization!
     * @param {number} version - Serialization version used during saving.
     * @param {Character} character - Owning character.
     * @returns {Discipline} - The deserialized discipline instance.
     */
    static load(state, version, character) {
        const discipline = new this(Earthdawn.data.disciplines[state.id], character, state.po);
        discipline.#circle = state.cl;
        discipline.#talents = state.ta.map(byCircle => {
            return byCircle.map(taState => Talent.load(taState, version, character, discipline));
        });
        discipline.#talents.unshift(null);  // insert dummy at zero to adjust all indices back to circle (starting with 1)
        delete discipline.#talents[0];  // remove added null value from index 0 because we do not want to see this in loops
        discipline.#loadAbilities(state, version);
        return discipline;
    }
    // NOTE: For how talents work for Multi-Discipline characters, see Earthdawn 3rd page 242: check during each circle advancement in a discipline if the talents of other disciplines need to be moved/adjusted.
    // A talent option will automatically become a core talent of a new discipine as soon as it becomes available in the new discipline. This frees up the old option slot. From then on the talent is learned using the LP costs of the new discipline, 
    // but for all existing ranks the old LP cost apply. If the player then reduces the rank, we have to refund the old (less) costs but keep track that the rank was now reduced and relearning this rank costs more (new costs), which must also adjust the
    // internal counter for how many ranks were still learned by the old discipline.
    // If a discipline advances to a new circle (also when acquiring a new discipline) and that circle includes a core talent that was already learned as a core talent by another discipline, but the cost to learn is cheaper for this advanced discipline
    // then move the talent from the other discipline to this one and refund any legend points. If the cost to learn is equal, keep it on the original discipline.
}

/**
 * All health related characteristics of a {@link Character}.
 */
class Health {
    /** @type {number} */
    #unconsciousnessRating;
    /** @type {number} */
    #deathRating;
    /** @type {number} */
    #woundThreshold;
    /** @type {number} */
    #recoveryStep;
    /** @type {number} - Number of recovery tests this character has available per refresh period, minimum 1. */
    #recoveryTests;
    /** @type {number} - Length of the recovery test refresh period, minimum 1. For most characters this is 1 day, but for each point the number of recovery tests would fall below 1 this period goes up by 1. */
    #recoveryDays;
    /** @type {number} - Each point of pain resistance negates on point of wound penalties. For most character this is 0. Mainly gained through abilites. */
    #painResistance;
    /** 
     * @typedef {object} CurrentHealth
     * @property {number} damage - Current (temporary) damage this character has, minimum 0, maximum = death rating - permanent damage. This does not include permanent damage.
     * @property {number} permanentDamage - Permanent damage. This is mainly gained through blood damage. It does not heal normally. Will be calculated on-the-fly, not stored in saved state.
     * @property {number} wounds - Current number of wounds, minimum 0.
     * @property {?number} recoveryTests - Number of recovery tests the character has left to use, minimum 0. Maximum may be higher than the calculated {@link Health#recoveryTests} rating if the character gained additional temporary tests (for example, through talent Fire Heal).
     */
    /** @type {CurrentHealth} */
    #current;

    constructor() {
        this.#unconsciousnessRating = 0;
        this.#deathRating = 0;
        this.#woundThreshold = 0;
        this.#recoveryStep = 0;
        this.#recoveryTests = 0;
        this.#recoveryDays = 1;
        this.#painResistance = 0;
        this.#current = {
            damage: 0,
            permanentDamage: 0,
            wounds: 0,
            recoveryTests: undefined
        }
    }
    getUnconsciousnessRating() {
        return this.#unconsciousnessRating;
    }
    getDeathRating() {
        return this.#deathRating;
    }
    getWoundThreshold() {
        return this.#woundThreshold;
    }
    getRecoveryStep() {
        return this.#recoveryStep;
    }
    getRecoveryDice() {
        return Earthdawn.rules.getDiceForStep(this.getRecoveryStep());
    }
    /**
     * @returns {number} - The maximum of recovery tests to which the character refreshes during a recovery period when resting.
     */
    getRecoveryTestsPerRefreshPeriod() {
        return this.#recoveryTests;
    }
    /**
     * @returns {number} - The length of a recovery period in days, minimum 1.
     */
    getDaysForRecoveryTestFresh() {
        return this.#recoveryDays;
    }
    /**
     * After {@link Health#getDaysForRecoveryTestFresh} days have passed and some rest, the character's current recovery tests are reset to {@link Health#getRecoveryTestsPerRefreshPeriod} if not already higher.
     */
    refreshRecoveryTests() {
        if(this.hasSpentRecoveryTests()) {
            this.setAvailableRecoveryTestsTo(this.getRecoveryTestsPerRefreshPeriod());
        }
    }
    /**
     * @returns {boolean} - Returns true if the character has fewer recovery tests than they normally gain through a refresh. If a character somehow had more current tests and spent some but has still not fallen under their refresh rate, this method returns false.
     */
    hasSpentRecoveryTests() {
        return this.getAvailableRecoveryTests() < this.getRecoveryTestsPerRefreshPeriod();
    }
    /**
     * @returns {number} - Number of recovery tests left for use.
     */
    getAvailableRecoveryTests() {
        return this.#current.recoveryTests;
    }
    /**
     * Uses one recovery test if available.
     */
    useRecoveryTest() {
        if(this.getAvailableRecoveryTests() > 0) {
            this.setAvailableRecoveryTestsTo(this.getAvailableRecoveryTests() - 1);
        }
    }
    /**
     * Sets the number of currently available recovery tests.
     * @param {number} count - New number of available recovery tests, minimum 0. May be higher than normally refreshed if more are gained through other means, like for example the talent Fire Heal.
     */
    setAvailableRecoveryTestsTo(count) {
        if(count < 0) {
            throw new Error(`Cannot set number of recovery tests to ${count}`);
        }
        this.#current.recoveryTests = count;
    }
    /**
     * @returns {number} - The total damage the character has. This is the sum of temporary and permanent damage.
     */
    getTotalDamage() {
        return this.#current.damage + this.#current.permanentDamage;
    }
    /**
     * @returns {number} - The current temporary damage. This is the damage normally gained through attacks, it can be healed through recovery tests.
     */
    getTemporaryDamage() {
        return this.#current.damage;
    }
    /**
     * @returns {number} - The current permanent damage that cannot be healed normally, like blood damage from blood charms or oaths.
     */
    getPermanentDamage() {
        return this.#current.permanentDamage;
    }
    /**
     * Take new damage by increasing the current temporary damage of the character. The resulting total can never be greater than the character's death rating.
     * @param {number} damage - The new damage by which the current temporary damage is increased.
     * @param {boolean} [canCauseWounds=true] - If true, the character automatically gains one wound if the new damage is equal or greater than the wound threshold. 
     * @param {boolean} [mentalOnly=false] - If true, this damage it treated as mental damage (like e.g. from illusion spells). It is capped by the character's unconsciousness rating, a character can never die from this damage but can get wounds.
     */
    increaseTemporaryDamage(damage, canCauseWounds = true, mentalOnly = false) {
        if(damage < 0) {
            throw new Error(`Additional temporary damage must not be less than zero but was ${damage}`);
        }
        if(canCauseWounds && damage >= this.getWoundThreshold()) {
            this.increaseWounds();
        }
        if(mentalOnly && this.getTotalDamage() + damage > this.getUnconsciousnessRating()) {    // mental damage like from illusionary spells cannot cause more damage than reaching unconsciousness
            damage = Math.min(damage, Math.max(0, this.getUnconsciousnessRating() - this.getTotalDamage()));
        }
        this.setTemporaryDamage(this.getTemporaryDamage() + damage);
    }
    /**
     * Heal temporary damage.
     * @param {number} healing - The number of temporary damage to heal. Automatically capped at the current damage.
     */
    reduceTemporaryDamage(healing) {
        if(healing < 0) {
            throw new Error(`Healing applied to temporary damage must not be less than zero but was ${healing}`);
        }
        if(healing > this.getTemporaryDamage()) {
            healing = this.getTemporaryDamage();
        }
        this.setTemporaryDamage(this.getTemporaryDamage() - healing);
    }
    /**
     * Sets the current temporay damage.
     * @param {number} damage - The current temporary damage value to set. The resulting total cannot exceed the character's death rating.
     */
    setTemporaryDamage(damage) {
        if(damage < 0) {
            throw new Error(`Temporary damage must not be less than zero but was ${damage}`);
        }
        this.#current.damage = damage;
        if(this.getTotalDamage() > this.getDeathRating()) {
            this.#current.damage = this.getDeathRating() - this.getPermanentDamage();
        }
    }
    /**
     * Sets the current permanent damage.
     * @param {number} damage - The current permanent damage value to set.
     */
    #setPermanentDamage(damage) {
        if(damage < 0) {
            throw new Error(`Permanent damage must not be less than zero but was ${damage}`);
        }
        if(damage > this.getDeathRating()) {
            damage = this.getDeathRating();
        }
        this.#current.permanentDamage = damage;
        this.setTemporaryDamage(this.#current.damage);  // recalculate damage
    }
    getWounds() {
        return this.#current.wounds;
    }
    /**
     * Increase the current wounds by one.
     */
    increaseWounds() {
        this.setWounds(this.getWounds() + 1);
    }
    /**
     * Decrease the current wounds by one if above 0.
     */
    decreaseWounds() {
        if(this.getWounds() > 0) {
            this.setWounds(this.getWounds() - 1);
        }
    }
    /**
     * Sets the current wounds to the given value.
     * @param {number} count - The current wounds to set.
     */
    setWounds(count) {
        if(count < 0) {
            throw new Error(`Wounds must not be less than zero but were ${count}`);
        }
        this.#current.wounds = count;
    }
    /**
     * @returns {number} - Returns the step penalty (negative) for the current number of wounds. Normally one step penalty per wound after the first, but pain resistance decreases the penalty.
     */
    getStepPenaltyForWounds() {
        return Math.min(0, this.#painResistance + 1 - this.#current.wounds);
    }
    /**
     * Calculates the final health ratings. This method should be called only by the {@link Character} class.
     * @param {Character} character - Owner of this trait 
     * @param {Ability[]} abilities - Unordered list of effective abilities owned by the character.
     * @returns {Health} - Returns this
     */
    calculateFrom(character, abilities) {
        const toughness = character.getAttribute(AttributeIds.TOUGHNESS);
        this.#unconsciousnessRating = Earthdawn.data.unconsciousnessRating[toughness.getTotalValue()];
        this.#deathRating = Earthdawn.data.deathRating[toughness.getTotalValue()];
        character.getDisciplines().forEach(discipline => {
            const bonus = discipline.getHealthBonus();
            this.#unconsciousnessRating += bonus.unconsciousness;
            this.#deathRating += bonus.death;
        });

        this.#woundThreshold = Earthdawn.data.woundThreshold[toughness.getTotalValue()];
        this.#recoveryTests = Earthdawn.data.recoveryTests[toughness.getTotalValue()];      // may return floats > 0 but < 1 (e.g. 0.5 means 1 test per 2 days)
        this.#recoveryDays = 1;
        if(this.#recoveryTests < 1) {   // if less than one test per day -> normalize to one
            this.#recoveryDays = Math.round(1 / this.#recoveryTests);
            this.#recoveryTests = 1;
        }
        this.#recoveryStep = toughness.getStep();
        this.#painResistance = 0;
        abilities.forEach(ability =>  {
            switch(ability.getType()) {
                case AbilityType.PAIN_RESISTANCE:
                    this.#painResistance = Math.max(0, this.#painResistance + ability.getBonusValue());
                    break;
                case AbilityType.RECOVERY_TESTS:
                    this.#recoveryTests += ability.getBonusValue();   // only integers (positive or negative), never floats
                    if(this.#recoveryTests < 1) {       // convert tests less than one to more days instead.
                        this.#recoveryDays += 1 - this.#recoveryTests;
                        this.#recoveryTests = 1;
                    } else if(this.#recoveryDays > 1) { // check if we gained more tests and can decrease days now up to minimum of one.
                        const toConvert = Math.min(this.#recoveryTests - 1, this.#recoveryDays - 1);
                        this.#recoveryTests -= toConvert;
                        this.#recoveryDays -= toConvert;
                    }
                    break;
                case AbilityType.WOUND_THRESHOLD:
                    this.#woundThreshold = Math.max(0, this.#woundThreshold + ability.getBonusValue());
                    break;
            }
        });
        if(this.#current.recoveryTests == null) {   // initialize current recovery tests to refresh maximum if not defined yet (i.e. character was not loaded from a saved state)
            this.#current.recoveryTests = this.#recoveryTests;
        }
        const armor = character.getEquipment().getArmor();
        if(armor && armor.getData().usesBloodMagic()) {
            this.#setPermanentDamage(this.getPermanentDamage() + armor.getData().getImplantDamage());
        }
        return this;
    }
    /**
     * @returns {object} - Returns the saved state of the current health for serialization.
     */
    save() {
        return {
            dm: this.#current.damage,
            wd: this.#current.wounds,
            rt: this.#current.recoveryTests
        };
    }
    /**
     * Deserializes current health values from a saved state.
     * @param {object} state - Saved character state. Do not change this object during deserialization!
     * @param {number} version - Serialization version used during saving.
     * @param {Character} character - Owning character.
     * @returns {Health} - The deserialized health instance.
     */
    static load(state, version, character) {
        const health = new this();
        const current = health.#current;
        current.damage = state.dm;
        current.wounds = state.wd;
        current.recoveryTests = state.rt;
        return health;
    }
}

/**
 * @readonly
 * @enum {number}
 */
const VariableConditionState = Object.freeze({
    NO: 0,
    PARTIAL: 1,
    FULL: 2
})

class Condition {
    /** @type {string} - Display name of the condition */
    #name;
    /** @type {number} - Step penalty for actions */
    #actionPenalty;
    /** @type {number[]} - Array of defense modifiers indexed by {@link DefenseId} */
    #defenseModifiers;
    /** @type {ActionType[]} - List of actions to which this condition's action penalty applies. */
    #appliesTo;
    /**
     * @param {string} name - Display name
     * @param {number} actionPenalty - Step penalty (negative) for all actions
     * @param {number} [physDef=0] - Modifier for physical defense
     * @param {number} [spellDef=0] - Modifier for spell defense
     * @param {number} [socDef=0] - Modifier for social defense
     * @param {...ActionType} actions - List of actions to which this condition's step penalty applies. If empty it applies to all.
     */
    constructor(name, actionPenalty, physDef = 0, spellDef = 0, socDef = 0, ...actions) {
        this.#name = name;
        this.#actionPenalty = actionPenalty;
        this.#defenseModifiers = [];
        this.#defenseModifiers[DefenseType.PHYSICAL.id] = physDef;
        this.#defenseModifiers[DefenseType.SPELL.id] = spellDef;
        this.#defenseModifiers[DefenseType.SOCIAL.id] = socDef;
        this.#appliesTo = actions;
    }
    getName() {
        return this.#name;
    }
    /**
     * @param {ActionType} [actionType=ActionType.GENERAL] - What kind of action is the character performing. Penalties might not apply to all type of actions
     * @returns {number} - Returns the step penalty (negative) for all actions due to conditions.
     */
    getActionPenalty(actionType = ActionType.GENERAL) {
        if(this.isInEffect() && this.#appliesTo.includes(actionType)) {
            return this.#actionPenalty;
        }
        return 0;
    }
    /**
     * @returns {number[]} - An array with defense modifiers (positive or negative) based on current condition. The array is indexed by {@link DefenseId}. Do not modify the returned array!
     */
    getDefenseModification() {
        if(this.isInEffect()) {
            return this.#defenseModifiers;
        }
        const empty = [];
        empty[DefenseType.PHYSICAL.id] = 0;
        empty[DefenseType.SPELL.id] = 0;
        empty[DefenseType.SOCIAL.id] = 0;
        return empty;
    }
    /**
     * @param {ActionType} actionType - An action type to check for
     * @returns {boolean} Return true if this condition has an effect on the given action type
     */
    appliesTo(actionType) {
        return this.#appliesTo.includes(actionType);
    }
    /**
     * @abstract
     * @returns {boolean} - True if the condition applies/is enabled.
     */
    isInEffect() {
        throw new Error("Not implemented");
    }
    /**
     * @abstract
     * Removes this condition/disables it.
     */
    clear() {
        throw new Error("Not implemented");
    }
}

class BooleanCondition extends Condition {
    /** @type {boolean} - Is this condition in effect? */
    #state;
    /**
     * @param {string} name - Display name
     * @param {number} actionPenalty - Step penalty for all actions
     * @param {number} [physDef=0] - Modifier for physical defense
     * @param {number} [spellDef=0] - Modifier for spell defense
     * @param {number} [socDef=0] - Modifier for social defense
     * @param {...ActionType} actions - List of actions to which this condition's step penalty applies.
     */
    constructor(name, actionPenalty, physDef = 0, spellDef = 0, socDef = 0, ...actions) {
        super(name, actionPenalty, physDef, spellDef, socDef, ...actions);
        this.#state = false;
    }
    /**
     * @override
     */
    isInEffect() {
        return this.getState();
    }
    /**
     * @override
     */
    clear() {
        this.setState(false);
    }
    /**
     * @returns {boolean}
     */
    getState() {
        return this.#state;
    }
    /**
     * @param {boolean} state 
     */
    setState(state) {
        this.#state = state;
    }
}

class VariableCondition extends Condition {
    /** @type {VariableConditionState} - Is this condition in effect? */
    #state;
    /** @type {number} - Multiplier applied to the penalty/modifiers if the state is FULL */
    #multiplier;
    /**
     * @param {string} name - Display name
     * @param {number} actionPenalty - Step penalty for all actions
     * @param {number} [physDef=0] - Modifier for physical defense
     * @param {number} [spellDef=0] - Modifier for spell defense
     * @param {number} [socDef=0] - Modifier for social defense
     * @param {...ActionType} actions - List of actions to which this condition's step penalty applies.
     */
    constructor(name, actionPenalty, physDef = 0, spellDef = 0, socDef = 0, ...actions) {
        super(name, actionPenalty, physDef, spellDef, socDef, ...actions);
        this.#state = VariableConditionState.NO;
        this.#multiplier = 2;
    }
    getName() {
        const prefix = ["", "Partial ", "Full "][this.getState()];
        return prefix + this.getBaseName();
    }
    getBaseName() {
        return super.getName();
    }
    /**
     * @override
     * @param {ActionType} [actionType=ActionType.GENERAL] - What kind of action is the character performing. Penalties might not apply to all type of actions
     * @returns {number} - Returns the step penalty (negative) for all actions due to conditions.
     */
    getActionPenalty(actionType = ActionType.GENERAL) {
        const result = super.getActionPenalty(actionType);
        return this.getState() === VariableConditionState.FULL ? result * this.#multiplier : result;
    }
    /**
     * @returns {number[]} - An array with defense modifiers (positive or negative) based on current condition. The array is indexed by {@link DefenseId}. Do not modify the returned array!
     */
    getDefenseModification() {
        if(this.getState() === VariableConditionState.FULL) {
            return super.getDefenseModification().map(mod => mod * this.#multiplier);
        }
        return super.getDefenseModification();
    }
    /**
     * @override
     */
    isInEffect() {
        return this.getState() !== VariableConditionState.NO;
    }
    /**
     * @override
     */
    clear() {
        this.setState(VariableConditionState.NO);
    }
    /**
     * @returns {VariableConditionState}
     */
    getState() {
        return this.#state;
    }
    /**
     * @param {VariableConditionState} state 
     */
    setState(state) {
        this.#state = state;
    }
}

/**
 * Keeps track of the active conditions affecting a {@link Character}.
 * Besides the ongoing conditions managed by this class, also wound penalties (see {@link Health}) and weapon attack
 * penalties (see {@link EquippedWeapon}) can modify test steps. These are not included in any calculated values.
 * 
 * The conditions here are continuing conditions that apply to a whole round or multiple actions. One-time effects like, for example, the attack penalty for long range shots
 * or the attack bonus for aggressive attacks are not covered by this class.
 */
class Conditions {
    #character;
    /** @type {BooleanCondition} - Used the combat option Aggressive Attack at least once this round? - Aggressive Attack (+3 steps one close combat attack, +3 damage steps, -3 physical und spell defense this round, 1 strain)  */
    #hasAttackedAggressively;
    /** @type {BooleanCondition} - Used the combat option Defensive Stance this round? - Defensive Stance (-3 steps all actions this round except for defensive (e.g. Avoid Blow) and knockdown tests, +3 physical and spell defense this round) */
    #usedDefensiveStance;
    /** @type {BooleanCondition} - Used an extra tail attack this round (T'Skrang only)? - Tail Attack (-2 steps to all actions this round, second attack with tail for T'Skrank only) */
    #usedTailAttack;
    /** @type {BooleanCondition} - Split their movement this round? - Split Movement (-2 steps all actions, -2 physical and spell defense, 1 strain) */
    #usedSplitMovement;
    /** @type {BooleanCondition} - Has either reserved their action or changed it to something different than original declared? - Changed/Reserved Action (+2 difficulty for all tests this round) */
    #hasChangedOrReservedAction;
    /** @type {BooleanCondition} - Was knocked down and is currently prone? - Knocked Down (-3 steps all actions, -3 defenses) */
    #knockedDown;
    /** @type {BooleanCondition} - Is stunned? - Stunned (only defensive actions, counts a harried) */
    #stunned;
    /** @type {BooleanCondition} - Is harried by too many opponents? - Harried (-2 steps all actions, -2 defenses) */
    #harried;
    /** @type {BooleanCondition} - Was the character blindsided? - Blindsided (-2 physical and spell defense) */
    #blindsided;
    /** @type {BooleanCondition} - Was the character surprised? - Surprised (no actions possible, -3 physical and spell defense) */
    #surprised;
    /** @type {VariableCondition} - Does the character have cover? - Cover (partial +2, full +4 physical and spell defense) */
    #inCover;
    /** @type {VariableCondition} - Is it dark or is the character blinded? - Darkness (partial: -2 steps all actions, full -4) */
    #inDarkness;
    /**
     * @param {Character} character - Owning character 
     */
    constructor(character) {
        this.#character = character;
        this.#hasAttackedAggressively = new BooleanCondition("Used Aggressive Attack", 0, -3, -3);
        this.#usedDefensiveStance = new BooleanCondition("In Defensive Stance", -3, 3, 3, 0, ActionType.GENERAL, ActionType.ATTACK);
        this.#usedTailAttack = new BooleanCondition("Used Tail Attack", -2, 0, 0, 0, ActionType.GENERAL, ActionType.ATTACK, ActionType.DEFENSIVE);
        this.#usedSplitMovement = new BooleanCondition("Split Movement", -2, -2, -2, 0, ActionType.GENERAL, ActionType.ATTACK, ActionType.DEFENSIVE);
        this.#hasChangedOrReservedAction = new BooleanCondition("Changed/Reserved Action (+2 Diff)", 0, 0, 0, 0, ActionType.ATTACK, ActionType.DEFENSIVE, ActionType.GENERAL);
        this.#knockedDown = new BooleanCondition("Knocked Down", -3, -3, -3, -3, ActionType.GENERAL, ActionType.ATTACK, ActionType.DEFENSIVE, ActionType.DAMAGE);
        this.#stunned = new BooleanCondition("Stunned", -2, -2, -2, -2, ActionType.DEFENSIVE, ActionType.ATTACK, ActionType.DEFENSIVE, ActionType.INITIATIVE, ActionType.KNOCKDOWN);
        this.#harried = new BooleanCondition("Harried", -2, -2, -2, -2, ActionType.GENERAL, ActionType.ATTACK, ActionType.DEFENSIVE);
        this.#blindsided = new BooleanCondition("Blindsided", 0, -2, -2);
        this.#surprised = new BooleanCondition("Surprised", 0, -3, -3);
        this.#inCover = new VariableCondition("Cover", 0, 2, 2);
        this.#inDarkness = new VariableCondition("Darkness/Blinded", -2, 0, 0, 0, ActionType.GENERAL, ActionType.ATTACK, ActionType.DEFENSIVE);
    }
    getAggressiveAttack() {
        return this.#hasAttackedAggressively;
    }
    getDefensiveStance() {
        return this.#usedDefensiveStance;
    }
    getTailAttack() {
        return this.#usedTailAttack;
    }
    getSplitMovement() {
        return this.#usedSplitMovement;
    }
    getChangedOrReservedAction() {
        return this.#hasChangedOrReservedAction;
    }
    getKnockedDown() {
        return this.#knockedDown;
    }
    getStunned() {
        return this.#stunned;
    }
    getHarried() {
        return this.#harried;
    }
    getBlindsided() {
        return this.#blindsided;
    }
    getSurprised() {
        return this.#surprised;
    }
    getCover() {
        return this.#inCover;
    }
    getDarknessOrBlinded() {
        return this.#inDarkness;
    }
    /**
     * @param {ActionType} [actionType=ActionType.GENERAL] - What kind of action is the character performing. Penalties might not apply to all type of actions
     * @returns {number} - Returns the step penalty (negative) for all actions due to conditions. Does not include wound penalties.
     */
    getActionPenalty(actionType = ActionType.GENERAL) {
        let penalty = 0;
        penalty += this.#hasAttackedAggressively.getActionPenalty(actionType);
        penalty += this.#usedDefensiveStance.getActionPenalty(actionType);
        penalty += this.#usedTailAttack.getActionPenalty(actionType);
        penalty += this.#usedSplitMovement.getActionPenalty(actionType);
        penalty += this.#hasChangedOrReservedAction.getActionPenalty(actionType);
        penalty += this.#knockedDown.getActionPenalty(actionType);
        penalty += this.#stunned.getActionPenalty(actionType);
        penalty += this.#harried.getActionPenalty(actionType);
        penalty += this.#blindsided.getActionPenalty(actionType);
        penalty += this.#inCover.getActionPenalty(actionType);
        penalty += this.#inDarkness.getActionPenalty(actionType);
        return penalty;
    }
    /**
     * @returns {number[]} - An array with defense modifiers (positive or negative) based on current conditions. The array is indexed by {@link DefenseId}.
     */
    getDefenseModification() {
        let result = [];
        result[DefenseType.PHYSICAL.id] = 0;
        result[DefenseType.SPELL.id] = 0;
        result[DefenseType.SOCIAL.id] = 0;
        const sum = /** @param {Condition} cond */ (cond) => {
            const mods = cond.getDefenseModification();
            result = result.map((val, i) => val + mods[i]);
        };
        sum(this.#hasAttackedAggressively);
        sum(this.#usedDefensiveStance);
        sum(this.#usedTailAttack);
        sum(this.#usedSplitMovement);
        sum(this.#hasChangedOrReservedAction);
        sum(this.#knockedDown);
        sum(this.#stunned);
        sum(this.#harried);
        sum(this.#blindsided);
        sum(this.#inCover);
        sum(this.#inDarkness);
        return result;
    }
    /**
     * Resets all conditions to off/default.
     */
    clearAllConditions() {
        this.resetRoundConditions();
        this.#knockedDown.clear();
        this.#stunned.clear();
        this.#harried.clear();
        this.#inCover.clear();
        this.#inDarkness.clear();
    }
    /**
     * Resets all conditions to off that normally last only for the current combat round.
     * This does not include harried, stunned or knocked down because these conditions may last for several rounds.
     */
    resetRoundConditions() {
        this.#hasAttackedAggressively.clear();
        this.#usedDefensiveStance.clear();
        this.#usedTailAttack.clear();
        this.#usedSplitMovement.clear();
        this.#hasChangedOrReservedAction.clear();
        this.#blindsided.clear();
        this.#surprised.clear();
    }
    /**
     * @returns {object} - Saved state of this trait for serialization.
     */
    save() {
        const state = {};
        /**
         * Saves a property if it has a value different from its default.
         * @param {string} key - Save state key for this property
         * @param {(BooleanCondition|VariableCondition)} condition - Current value of the property
         * @param {(boolean|VariableConditionState)} def - Default value of this property 
         */
        const sv = (key, condition, def = false) => {
            const value = condition.getState();
            if(value !== def) {
                state[key] = value;
            }
        };
        sv('aa', this.#hasAttackedAggressively);
        sv('ds', this.#usedDefensiveStance);
        sv('ta', this.#usedTailAttack);
        sv('sm', this.#usedSplitMovement);
        sv('ra', this.#hasChangedOrReservedAction);
        sv('kd', this.#knockedDown);
        sv('st', this.#stunned);
        sv('hr', this.#harried);
        sv('bs', this.#blindsided);
        sv('sp', this.#surprised);
        sv('cv', this.#inCover, VariableConditionState.NO);
        sv('dk', this.#inDarkness, VariableConditionState.NO);
        return state;
    }
    /**
     * Deserializes the Conditions trait from a saved state.
     * @param {object} state - Saved character state. Do not change this object during deserialization!
     * @param {number} version - Serialization version used during saving.
     * @param {Character} character - Owning character.
     * @returns {Conditions} - The deserialized conditions instance.
     */
    static load(state, version, character) {
        const ld = (key, condition) => {
            const value = state[key];
            if(value != undefined) {
                condition.setState(value);
            }
        };
        const cond = new this(character);
        ld('aa', cond.#hasAttackedAggressively);
        ld('ds', cond.#usedDefensiveStance);
        ld('ta', cond.#usedTailAttack);
        ld('sm', cond.#usedSplitMovement);
        ld('ra', cond.#hasChangedOrReservedAction);
        ld('kd', cond.#knockedDown);
        ld('st', cond.#stunned);
        ld('hr', cond.#harried);
        ld('bs', cond.#blindsided);
        ld('sp', cond.#surprised);
        ld('cv', cond.#inCover);
        ld('dk', cond.#inDarkness);
        return cond;
    }
}


/**
 * @readonly
 * @enum {string}
 */
const MovementType = {
    LAND: "land",
    AIR: "air"
}

/**
 * Represents a single kind of movement possessed by a {@link Character}.
 */
class Movement {
    #type;
    #racialModifier;
    /** @type {number} - How many yards the character can move during combat while still taking another action */
    #combat;
    /** @type {number} - How many yards the character can move when not doing anything else */
    #full;

    /**
     * @param {MovementType} type - The type of this movement
     * @param {number} mod - Racial modifier as a bonus to the movement rate for this type of movement. May also be negative.
     */
    constructor(type, mod) {
        this.#type = type;
        this.#racialModifier = mod;
        this.#combat = 0;
        this.#full = 0;
    }
    /**
     * @returns {MovementType} - The type of this movement.
     */
    getType() {
        return this.#type;
    }
    /**
     * @returns {number} - How many yards the character can move during combat while still taking another action
     */
    getCombat() {
        return this.#combat;
    }
    /**
     * @returns {number} - How many yards the character can move when not doing anything else
     */
    getFull() {
        return this.#full;
    }
    /**
     * Calculates the movement ranges based on the character's Dexterity attribute and racial modifier.
     * @param {Character} character - The owning character
     */
    calculateFrom(character) {
        const dex = character.getAttribute(AttributeIds.DEXTERITY).getTotalValue();
        const adjusted = Math.max(3, dex + this.#racialModifier);
        this.#combat = Earthdawn.data.movement.combat[adjusted];
        this.#full = Earthdawn.data.movement.full[adjusted];
    }
}

/**
 * Carrying and lifting capacity of a {@link Character}.
 */
class Encumberance {
    /** @type {number} - Carrying capacity in lbs */
    #carry;
    /** @type {number} - Lifting capacity in lbs */
    #lift;

    constructor() {
        this.#carry = 0;
        this.#lift = 0;
    }
    getCarry() {
        return this.#carry;
    }
    getLift() {
        return this.#lift;
    }
    /**
     * Calculates this characteristic based on the character's traits
     * @param {Character} character 
     */
    calculateFrom(character) {
        this.#carry = Earthdawn.data.carry[character.getAttribute(AttributeIds.STRENGTH).getTotalValue()];
        this.#lift = this.#carry * 2;
    }
}

/**
 * The current armor ratings of a {@link Character}. Based on both natural armor and worn equipment.
 */
class ArmorRating {
    /** @type {number} - Physical armor rating. Combination of racial natrual armor and worn armor */
    #physical;
    /** @type {number} - Mystic armor rating. Combination of base rating due to Willpower, worn armor and ability bonuses */
    #mystic;

    constructor() {
        this.#physical = 0;
        this.#mystic = 0;
    }
    getPhysical() {
        return this.#physical;
    }
    getMystic() {
        return this.#mystic;
    }
    /**
     * Calculates the current armor ratings of the character.
     * @param {Character} character - Owning character.
     * @param {Ability[]} abilities - Effective abilities of the character.
     * @returns {ArmorRating} - Returns this.
     */
    calculateFrom(character, abilities) {
        const willpower = character.getAttribute(AttributeIds.WILLPOWER).getTotalValue();
        this.#physical = 0;
        this.#mystic = Earthdawn.data.mysticArmor[willpower];
        abilities.forEach(ability => {
            switch(ability.getType()) {
                case AbilityType.PHYSICAL_ARMOR:
                    this.#physical += ability.getBonusValue();
                    break;
                case AbilityType.MYSTICAL_ARMOR:
                    this.#mystic += ability.getBonusValue();
                    break;
            }
        });
        const armor = character.getEquipment().getArmor();
        if(armor) {
            this.#physical += armor.getData().getPhysicalRating();
            this.#mystic += armor.getData().getMysticalRating();
        }
        return this;
    }
}

/**
 * @readonly
 * @enum {string}
 */
const KarmaRequirement = Object.freeze({
    NO: "no",
    OPTIONAL: "optional",
    REQUIRED: "required"
});

/**
 * Details of a {@link Character}'s Karma rating and current points.
 */
class Karma {
    /** @type {number} - From leftover attribute buying points during character creation. This plus base karma mod from race plus all abilities that modify karma = total karma mod. */
    #additionalKarmaMod;
    /** @type {number} - Maximum number of Karma points the character may possess. Calculated from karma mod * ranks in Karma Ritual talent. */
    #totalMaximum;
    /** @type {number} - Step number of the Karma die/dice. */
    #karmaStep;
    /** @type {number} - Currently available Karma points. */
    #availableKarma;
    /**
     * @param {number} leftOverAttributeBuyingPoints - Number of attribute buying points that were left over from character creation, minimum 0. These are added to the racial karma modifier and increase the maximum Karma available to the character.
     * @param {number} [available=0] - Current available Karma points.
     */
    constructor(leftOverAttributeBuyingPoints, available = 0) {
        if(leftOverAttributeBuyingPoints < 0 || leftOverAttributeBuyingPoints > 2) {
            throw new Error(`Left over attribute buying points converted to karma mod must be between 0 and 2 but was ${leftOverAttributeBuyingPoints}`);
        }
        this.#additionalKarmaMod = leftOverAttributeBuyingPoints;
        this.#totalMaximum = 0;
        this.#karmaStep = 0;
        this.#availableKarma = available;
    }
    getPossibleMaximum() {
        return this.#totalMaximum;
    }
    getStep() {
        return this.#karmaStep;
    }
    getDice() {
        return Earthdawn.rules.getDiceForStep(this.getStep());
    }
    getAvailablePoints() {
        return this.#availableKarma;
    }
    /**
     * Sets the currently available Karma points.
     * @param {number} karma - New value to set. Must be between 0 and {@link Karma#getPossibleMaximum}.
     */
    setAvailablePoints(karma) {
        if(karma < 0) {
            throw new Error(`Karma cannot be set to negative value but was ${karma}`);
        }
        if(karma > this.getPossibleMaximum()) {
            throw new Error(`Karma cannot be set to value ${karma} greater than possible maximum of ${this.getPossibleMaximum()}`);
        }
        this.#availableKarma = karma;
    }
    /**
     * Decrease current Karma points by one if any are available.
     */
    spendOnePoint() {
        if(this.getAvailablePoints() > 0) {
            this.setAvailablePoints(this.getAvailablePoints() - 1);
        }
    }
    /**
     * Calculates the Karma characteristics.
     * @param {Character} character - Owning character.
     * @param {Ability[]} abilities - Effective abilities of this character.
     * @returns {Karma} - Returns this.
     */
    calculateFrom(character, abilities) {
        const karmaMod = character.getRace().getKarmaMod() + this.#additionalKarmaMod;
        const firstDiscipline = character.getDisciplines()[0];  // assumes that Karma Ritual is always added to the first discipline.
        if(firstDiscipline.getPosition() !== 1) {
            throw new Error(`Unexpected discpline order! Discpline with index 0 should have Position 1 but had ${firstDiscipline.getPosition()}`);
        }
        const circle = 1;
        const karmaRitual = firstDiscipline.getLearnedTalentsOf(circle).find(talent => talent.is(KnownTalent.KARMA_RITUAL));
        this.#totalMaximum = karmaMod * karmaRitual.getTotalRank();
        this.#karmaStep = character.getRace().getKarmaStep();
        abilities.forEach(ability => {
            if(ability.getType() === AbilityType.KARMA_STEP) {
                this.#karmaStep += ability.getBonusValue();
            }
        });
        return this;
    }
    /**
     * @returns {object} - Saved state of this trait for serialization.
     */
    save() {
        return {
            km: this.#additionalKarmaMod,
            av: this.#availableKarma
        };
    }
    /**
     * Deserializes the Karma trait from a saved state.
     * @param {object} state - Saved character state. Do not change this object during deserialization!
     * @param {number} version - Serialization version used during saving.
     * @param {Character} character - Owning character.
     * @returns {Karma} - The deserialized karma instance.
     */
    static load(state, version, character) {
        return new this(state.km, state.av);
    }
}

/**
 * Information about a {@link Character}'s legend points account.
 */
class LegendPoints {
    #total;
    #unspent;
    /**
     * @param {number} total - Total number of legend points (spent + unspent) the character possesses.
     * @param {number} unspent - Unspent legend points that can be used to buy new things.
     */
    constructor(total = 0, unspent = 0) {
        this.#total = total;
        this.#unspent = unspent;
    }
    /**
     * @returns {number} - The sum of spent and unspent legend points.
     */
    getTotal() {
        return this.#total;
    }
    getUnspent() {
        return this.#unspent;
    }
    getSpent() {
        return this.getTotal() - this.getUnspent();
    }
    /**
     * Increases the amount of unspent (and total) legend points.
     * @param {number} lp - Additional legend points to add to unspent.
     */
    increasePoints(lp) {
        if(lp < 0) {
            throw new Error(`Cannot increase legend points by negative value`);
        }
        this.#unspent += lp;
        this.#total += lp;
    }
    /**
     * Decreases the amount of unspent (and total) legend points. Use {@link LegendPoints#spendPoints} to just spend already owned unspent points.
     * @param {number} lp - Number of legend points to subtract from the unspent amount.
     */
    decreasePoints(lp) {
        if(lp < 0) {
            throw new Error(`Cannot decrease legend points by negative value`);
        }
        if(lp > this.getUnspent()) {
            throw new Error(`Cannot decrease legend points by ${lp} because it exceeds unspent points of ${this.getUnspent()}`);
        }
        this.#unspent -= lp;
        this.#total -= lp;
    }
    /**
     * Sets the amount of unspent legend points to the given value and adjusts the total accordingly.
     * @param {number} lp - New value for the unspent legend point amount.
     */
    setUnspentAndAdjustTotal(lp) {
        if(lp < 0) {
            throw new Error(`Cannot set unspent legend points to negative value`);
        }
        const diff = lp - this.#unspent;
        if(diff < 0) {
            this.decreasePoints(-diff);
        } else {
            this.increasePoints(diff);
        }
    }
    /**
     * Spends some of the unspent legend points (buy something with legend points). Does not change the total points the character has.
     * @param {number} lp - The number of unspent points to spend.
     */
    spendPoints(lp) {
        if(lp < 0) {
            throw new Error(`Cannot spend negative legend points`);
        }
        if(lp > this.getUnspent()) {
            throw new Error(`Cannot spend ${lp} legend points because it exceeds unspent points of ${this.getUnspent()}`);
        }
        this.#unspent -= lp;
    }
    /**
     * Refunds some of the spent legend points, increasing the amount of unspent points without affecting the total.
     * @param {number} lp - Number of spent legend points to refund (unspent them).
     */
    refundPoints(lp) {
        if(lp < 0) {
            throw new Error(`Cannot refund negative legend points`);
        }
        if(lp > this.getSpent()) {
            throw new Error(`Cannot refund ${lp} legend points because it exceeds spent points of ${this.getSpent()}`);
        }
        this.#unspent += lp;
    }
    /**
     * @returns {Array} - Saved state of this object for serialization.
     */
    save() {
        return [this.#total, this.#unspent];
    }
    /**
     * Deserializes the legend points trait from a saved state.
     * @param {object} state - Saved character state. Do not change this object during deserialization!
     * @param {number} version - Serialization version used during saving.
     * @param {Character} character - Owning character.
     * @returns {LegendPoints} - The deserialized legend points instance.
     */
    static load(state, version, character) {
        return new this(state[0], state[1]);
    }
}

/**
 * @typedef {string} CharacterId - Unique id of a {@link Character}, e.g. a UUID. Used as the primery key for storage {see {@link Persistence}}.
 */
/**
 * An Earthdawn adept character.
 * Each adapt belongs to a species ({@link RaceData}), has a set of six {@link Attribute}s, has at least one {@link Discipline} that provides {@link Talent}s, possesses any number of {@link Skill}s
 * and optional {@link Spell}s (gained from spellcaster disciplines but maintained on the character). Also a couple of derived characteristics and additional possessions like equipment.
 */
class Character {
    /** @type {CharacterId} - Unique id of the character, e.g. UUID */
    #id;
    /** @type {string} - Non-empty name of the character */
    #name;
    /** @type {RaceData} - Race of the character */
    #race;
    /** @type {Attribute[]} - Array (1-based) of attributes indexed by {@link AttributeId}. The six standard attributes of Earthdawn. */
    #attributes;
    /** @type {Discipline[]} - All the learned disciplines of this character (0-based unsorted array). Contains at least the first discipline but may have more. The array index here is equal to {@link Discipline#getPosition} - 1. */
    #disciplines;
    /** @type {Skill[]} - All learned skills (0-base unsorted array) */
    #skills;
    /** @type {Spell[]} - All learned spells across all disciplines (0-based array sorted by circle, name, id ascending) */
    #spells;
    /** @type {Defense[]} - Array of defenses indexed by {@link DefenseId} (1-based). The three standard defenses of Earthdawn. */
    #defenses;
    /** @type {ArmorRating} - Armor rating characteristics */
    #armorRating;
    /** @type {Initiative} - Initiative characteristics */
    #initiative;
    /** @type {Health} - Health characteristics */
    #health;
    /** @type {Conditions} - Conditions affecting the character */
    #conditions;
    /** @type {Movement[]} - Array (0-based) of Movement that the character has. Contains at least {@link MovementType.LAND} */
    #movement;
    /** @type {Encumberance} - Encumberance characteristic */
    #encumberance;
    /** @type {Karma} - Karma characteristic */
    #karma;
    /** @type {LegendPoints} - Legend points characteristic */
    #legendPoints;
    /** @type {Equipment} - Equipment characteristic */
    #equipment;
    /**
     * @typedef {object} CharacteristicCache
     * @property {?Ability[]} [disciplineAbilities] - Cached array of the character's effective discipline abilities.
     */
    /** @type {CharacteristicCache} - An object with optional properties to cache locally within the character because on-the-fly calculation is expensive. */
    #cache;

    /**
     * @param {CharacterId} id - Unique id of this character (e.g. UUID).
     * @param {string} name - Non-empty name of this character.
     * @param {RaceData} race - Race of this character. Immutable after creation.
     * @param {Attribute[]} attributes - An array of all six attributes indexed by {@link AttributeId} (1-based). The {@link Attribute}s get assigned to this character. May be an empty array during deserialization (see {@link Character.load}).
     */
    constructor(id, name, race, attributes) {
        if(attributes.length > 0 && !AttributeIds.getIds().every(attId => attributes[attId] instanceof Attribute)) {
            throw new Error(`Attributes array must contain exactly one entry `)
        }
        this.#id = id;
        this.setName(name);
        this.#race = race;
        this.#attributes = attributes;
        this.#attributes.forEach(attribute => attribute.setCharacter(this));
        this.#disciplines = [];
        this.#skills = [];
        this.#spells = [];
        this.#cache = {};
        this.#createCharacteristics();
    }
    /**
     * @returns {CharacterId}
     */
    getId() {
        return this.#id;
    }
    /**
     * Sets a new unique id for this character.
     * @param {CharacterId} id - New unique id (e.g. UUID)
     */
    setId(id) {
        if(!id) {
            throw new Error(`Id is missing`);
        }
        this.#id = id;
    }
    getName() {
        return this.#name;
    }
    /**
     * @param {string} name - Non-empty name for this character
     */
    setName(name) {
        if(name == null) {
            throw new Error(`Character name must not be missing`);
        }
        name = name.trim();
        if(name.length === 0) {
            throw new Error(`Character name must not be empty`);
        }
        this.#name = name;
    }
    getRace() {
        return this.#race;
    }
    /**
     * @param {AttributeId} id - The id of the attribute to retrieve
     * @returns {Attribute} - Returns the attribute for the given id.
     */
    getAttribute(id) {
        return this.#attributes[id];
    }
    /**
     * @returns {Attribute[]} - All attributes of this character indexed by {@link AttributeId} (1-based).
     */
    getAttributes() {
        return this.#attributes;
    }
    /**
     * @returns {number} - Count how many attribute improvements can be acquired by this character. Each new circle in any discipline allows one improvement.
     */
    getAvailableAttributeImprovements() {
        const totalCircleAdvancements = this.getDisciplines().reduce((accu, discipline) => {
            return accu + Math.max(0, discipline.getCircle() - (discipline.getPosition() === 1 ? 1 : 0));   // each new circle in any discipline allows to improve one attribute. The very first circle of the first discipline does not count, of course.
        }, 0);
        return this.#attributes.reduce((accu, attribute) => accu - attribute.getImprovements(), totalCircleAdvancements);
    }
    /**
     * Increase the value (not step) of the given attribute by one point. Needs to be allowed to and able to do the improvement.
     * @param {Attribute} attribute - The attribute to improve by one point.
     */
    improveAttribute(attribute) {
        const lp = this.getLegendPoints();
        if(!attribute.canImprove() || lp.getUnspent() < attribute.getCostOfNextImprovement() || this.getAvailableAttributeImprovements() <= 0) {
            throw new Error(`Cannot improve attribute ${attribute.getName()}`);
        }
        lp.spendPoints(attribute.getCostOfNextImprovement());
        attribute.addImprovement();
        this.calculateCharacteristics();
    }
    /**
     * Removes one improvement from the given attribute and therefore decreases its value (not step) by one point. Needs to have an improvement.
     * @param {Attribute} attribute - The attribute to decrease by one point.
     */
    degradeAttribute(attribute /* Attribute */) {
        if(!attribute.canRemoveImprovement()) {
            throw new Error(`Cannot degrade attribute ${attribute.getName()}`);
        }
        attribute.removeImprovement();
        this.getLegendPoints().refundPoints(attribute.getCostOfNextImprovement());
        this.calculateCharacteristics();
    }
    /**
     * @param {boolean} [includeDefaults=false] - Set this to true in order to also get unlearned default skills in the result.
     * @returns {Skill[]} - Returns all skills of this character. If includeDefaults is true then this will also include default skills on rank 0 that the character does not possess otherwise as skill or talent.
     */
    getSkills(includeDefaults = false) {
        if(!includeDefaults) {
            return this.#skills;
        }
        const allTalents = this.getDisciplines().flatMap(discipline => discipline.getLearnedTalents().flat());
        const result = this.#skills.concat();   // shallow-copy.
        const defaults = Earthdawn.rules.actions.getDefaultSkills();
        for(let i = defaults.length - 1; i >= 0; --i) {
            const defSkill = defaults[i];
            if(!this.#skills.some(skill => skill.getId() === defSkill.getId()) && !allTalents.some(talent => talent.getData().getName() === defSkill.getName())) {
                result.push(new Skill(defSkill, this));
                defaults.splice(i, 1);
            }
        }
        return result;
    }
    /**
     * Get all skills that the character does not know yet.
     * @param {?SkillType} [type=null] - Optional type to limit the returned skills to. 
     * @returns {SkillData[]} - Returns all skills that this character does not yet possess as an unsorted array. If a skill type is given, only skills of that type are returned.
     */
    getAvailableSkills(type = null) {
        const learned = new Map();
        this.getSkills().forEach(skill => learned.set(skill.getId(), true));
        const result = [];
        Earthdawn.data.skills.forEach(skillData => {
            if((type === null || skillData.getType() === type) && !learned.has(skillData.getId())) {
                result.push(skillData);
            }
        });
        return result;
    }
    /**
     * @returns {boolean} - Returns true if the character has enough unspent legend points to buy a new skill at rank 1.
     */
    canAffordNewSkill() {    
        return this.getLegendPoints().getUnspent() >= Earthdawn.rules.advancement.getSkillLegendPointCost(1);   // assumes that all skills cost the same to buy on rank 1
    }
    /**
     * Buys a new skill with legend points at rank 1 and adds it to this character. If you want to unlearn a skill, decrease its rank to 0.
     * @param {SkillData} skillData - The base game data of the new skill to learn.
     * @returns {Skill} - Returns the new skill added to this character
     */
    learnNewSkill(skillData) {
        if(!this.canAffordNewSkill()) {
            throw new Error(`Character does not have enough unspent legend points to learn new skill ${skillData.getName()}`);
        }
        const skill = this.addSkill(skillData);
        skill.increaseRank();
        return skill;
    }
    /**
     * Adds a new skill with rank 0 to this character without paying for it. This method should be called only by {@link CharacterCreator} or other methods of this class.
     * @param {SkillData} skillData -  The base game data of the new skill to add
     * @returns {Skill} - The newly added skill
     */
    addSkill(skillData) {      
        if(this.#skills.some(skill => skill.getId() === skillData.getId())) {
            throw new Error(`The character already knows the skill ${skillData.getId()}`);
        }
        const skill = new Skill(skillData, this);
        this.#skills.push(skill);
        return skill;
    }
    /**
     * Removes a skill from this character without refunding any points for it. This method should be called only by {@link CharacterCreator} or other methods of this class. If you want to unlearn a skill, use {@link Skill#decreaseRank} instead.
     * @param {Skill} skill - The skill to remove.
     */
    removeSkill(skill) { 
        const index = this.#skills.indexOf(skill);
        if(index === -1) {
            throw new Error(`Skill ${skill.getId()} not found in the list of learned skills`);
        }
        this.#skills.splice(index, 1);
    }
    /**
     * Looks for a specific learned talent by id. Searches all known disciplines of this character.
     * @param {TalentId} id - The id of the talent to search for.
     * @param {?number} [lookInCircle=null] - Restrict the search to the given circle only. If this is omitted, all circles will be searched through. 
     * @returns {?Talent} - Returns the found talent or null.
     */
    findTalent(id, lookInCircle = null) {
        let result = null;
        this.getDisciplines().some(discipline => {
            let talents = lookInCircle ? discipline.getLearnedTalentsOf(lookInCircle) : discipline.getLearnedTalents().flat();
            return talents.some(talent => {
                if(talent.getId() === id) {
                    result = talent;
                    return true;
                }
                return false;
            });
        });
        return result;
    }
    /**
     * Get all Thread Weaving talents of spellcaster disciplines this character possesses. Check for null values in the map!
     * @param {boolean} [includeAccessByVersatility=true] - If this is set to true, then also talents learned by Versatility are included, else not.
     * @returns {Map.<TalentId, ?Talent>} - A map containing the ids of all Thread Weaving talents that belong to spellcaster disciplines as keys. The values are either the matching {@link Talent}s if the character knows them or null if not.
     */
    getAccessToSpells(includeAccessByVersatility = true) {   // returns a Map with key = thread weaving talent id and value = ThreadWeaving talent that this character possesses for all spell caster disciplines
        const threadWeavings = new Map();
        Earthdawn.data.disciplines.forEach(disciplineData => {  // find all ThreadWeaving talent ids for spellcasters so that we can later identify these talents also when learned by Versatility.
            if(disciplineData.hasSpells()) {
                threadWeavings.set(disciplineData.getThreadWeavingIdForSpellcasting(), null);
            }
        });
        this.getDisciplines().forEach(discipline => {
            const circle = 1;       // all spellcasters learn thread weaving on first circle (includes access via Versatility)
            discipline.getLearnedTalentsOf(circle).forEach(talent => {      // the first discipline may contain more than one spellcaster thread weaving talent if some were learned by Versatility. Can even have such talent if the discipline itself is not a spellcaster.
                if(threadWeavings.has(talent.getId()) && (includeAccessByVersatility || talent.getSource() !== TalentSource.VERSATILITY)) {
                    threadWeavings.set(talent.getId(), talent);
                }
            });
        });
        return threadWeavings;
    }
    /**
     * During advancement to a new circle in a spellcaster discipline, the character can acquire a certain number of spells for free from their teacher. The spells can come from any circle not greater than the current discipline circle.
     * The amount of free spells is dependent on their circle. A spell needs points equal to its circle. The character gains free points equal to the new discipline circle times two. This is an ED2 rule, not ED3.
     * @returns {number} - Number of free spell circles (points) the character can still get for free. A spell needs such points in the amount of its circle to acquire for free (e.g. a 3rd circle spell needs points worth of 3 spell circles to learn).
     */
    getFreeSpellCircles() {     // This is implementing the rule from ED2 (get two free spells per circle advancement). ED1 and ED3 did not charge for spells at all, ED4 charges for all spells (no free spells except during character creation).
        const totalGranted = this.getDisciplines().reduce((accu, discipline) => {
            if(discipline.getData().hasSpells()) {      // during the advancement to a new circle each spellcaster discipline provides free spells in the worth of two spells from the new circle (i.e. a circle 3 spell is worth 3 points). Does not apply to spellcasting learned via Versatility, only to real disciplines.
                let circle = discipline.getPosition() === 1 ? 2 : 1;    // the first discipline did provide free spells during character creation: we do not count these here and thus skip the first circle.
                while(circle <= discipline.getCircle()) {
                    accu += 2 * circle++;
                }
            }
            return accu;
        }, 0);
        const spent = this.getSpells().reduce((accu, spell) => (spell.acquiredForFree() > 1) ? accu + spell.getData().getCircle() : accu, 0);     // do not count spells gotten for free during character creation
        return totalGranted - spent;
    }
    /**
     * Get spells that the character is still able to learn (does not know yet). Only spells are included for which the character has the required Thread Weaving talent.
     * @param {?number} [upToCircle=null] - If given the returned data is only containing spells up to this circle, else all spells are returned.
     * @param {boolean} [includeAccessByVersatility=true] - If true also Thread Weaving talents learned by Versatility are taken into account when checking access to spells, else only learned spellcaster disciplines are used.
     * @returns {SpellData[][]} - Returns an array indexed by circle (1-based) that contains arrays of game data for all spells that the character can still learn on that circle.
     */
    getAvailableSpells(upToCircle = null, includeAccessByVersatility = true) {
        const threadWeaving = this.getAccessToSpells(includeAccessByVersatility);
        const knownSpells = new Map();      // collect for easier lookup
        this.getSpells().forEach(spell => {
            knownSpells.set(spell.getId(), true);
        });
        const maxCircle = upToCircle ?? Earthdawn.rules.advancement.getMaximumCircle();
        const result = [];
        const collect = (disciplineData) => {
            for(let circle = 1; circle <= maxCircle; ++circle) {
                let byCircle = result[circle];
                if(!byCircle) {
                    result[circle] = byCircle = [];
                }
                disciplineData.getSpells(circle).forEach(spellId => {
                    if(!knownSpells.has(spellId)) {
                        byCircle.push(Earthdawn.data.spells[spellId]);
                        knownSpells.set(spellId, false);    // remember this new spell so that we do not add it twice (if more than one discipine provides access to it)
                    }
                });
            }
        };
        Earthdawn.data.disciplines.forEach(disciplineData => {
            if(disciplineData.hasSpells()) {
                const threadWeavingId = disciplineData.getThreadWeavingIdForSpellcasting();
                if(threadWeaving.get(threadWeavingId)) {
                    collect(disciplineData);
                }
            }
        });
        return result;
    }
    /**
     * @returns {Spell[]} - Returns a sorted array of learned spells.
     */
    getSpells() {
        return this.#spells;
    }
    /**
     * Learns a new spell and adds it to this character. If there are enough free spell circles, gets the spell for free. Otherwise pays for it with legend points.
     * This method does not check if this character may actually learn any spells (use {@link Character#getAvailableSpells}).
     * @param {SpellData} spellData - Game data of the new spell to learn.
     * @returns {Spell} - The newly learned spell.
     */
    learnNewSpell(spellData) {
        /** @type {FreeSpell} */
        let forFree = FreeSpell.ADVANCEMENT;
        if(this.getFreeSpellCircles() < spellData.getCircle()) {
            const cost = Earthdawn.rules.advancement.getSpellLegendPointCost(spellData.getCircle());
            if(cost > this.getLegendPoints().getUnspent()) {
                throw new Error(`Character does not have enough unspent legend points to learn spell ${spellData.getName()}`);
            }
            this.getLegendPoints().spendPoints(cost);
            forFree = FreeSpell.NO;
        }
        return this.addSpell(spellData, forFree);
    }
    /**
     * Adds a new spell to this character without paying any legend points for it. Do not call this directly, intended for {@link CharacterCreator} or internal methods.
     * @param {SpellData} spellData - Game data of the new spell to learn.
     * @param {FreeSpell} forFree - A constant defining if this spell counts as learned with legend points, or for free due to character creation or circle advancement. Necessary to be able to refund points later when trying to unlearn this spell.
     * @returns {Spell} - Returns the newly added spell.
     */
    addSpell(spellData, forFree = FreeSpell.NO) {
        if(this.#spells.some(spell => spell.getId() === spellData.getId())) {
            throw new Error(`The character already knows the spell ${spellData.getId()}`);
        }
        const spell = new Spell(spellData, this, forFree);
        this.#spells.push(spell);
        this.#spells.sort(Spell.compare);
        return spell;
    }
    /**
     * Removes a spell from this character without refunding any points. This method should only be called by the {@link CharacterCreator} or other methods of this class. To unlearn a spell during play use {@link Spell#unlearn};
     * @param {Spell} spell - The spell to remove from this character
     */
    removeSpell(spell) {
        const index = this.#spells.indexOf(spell);
        if(index === -1) {
            throw new Error(`Spell ${spell.getId()} not found in list of learned spells`);
        }
        this.#spells.splice(index, 1);
    }
    /**
     * @returns {Talent[]} - Returns an array of all learned spell matrix/fetish talents of this character across all their disciplines.
     */
    getSpellMatrices() {
        return this.getDisciplines().flatMap(discipline => discipline.getSpellMatrices());
    }
    /**
     * Gets a single defense characteristic.
     * @param {DefenseType} type - The type of the defense
     * @returns {Defense} - A single defense matching the given type.
     */
    getDefense(type) {  
        return this.#defenses[type.id];
    }
    getInitiative() { 
        return this.#initiative;
    }
    /**
     * @returns {Movement[]} - Unsorted array of movements. Contains at least an entry for land movement but may contain further types if the character's race supports it.
     */
    getMovement() {
        return this.#movement;
    }
    getEncumberance() { 
        return this.#encumberance;
    }
    getHealth() { 
        return this.#health;
    }
    getConditions() {
        return this.#conditions;
    }
    getArmorRating() { 
        return this.#armorRating;
    }
    getKarma() { 
        return this.#karma;
    }
    getLegendPoints() {
        return this.#legendPoints;
    }
    getEquipment() {
        return this.#equipment;
    }
    /**
     * @returns {number} - Returns the highest circle of all learned disciplines.
     */
    getHighestAchievedCircle() {
        return this.getDisciplines().reduce((highest, discipline) => Math.max(discipline.getCircle(), highest), 1);
    }
    /**
     * @returns {Discipline[]} - An array of all learned disciplines (0-based). The array is ordered by the discipline position (-1), i.e. first discipline has index 0, second index 1, etc.
     */
    getDisciplines() {
        return this.#disciplines;
    }
    /**
     * Adds a new discipline to this character. Does not pay any costs. Do not call this to create the first discipline of a character, use {@link Character#setupFirstDiscipline} instead.
     * @param {DisciplineData} disciplineData - The game data of the new discipline to add.
     * @returns {Discipline} - The newly added discipline.
     */
    addDiscipline(disciplineData) { 
        const currentCount = this.getDisciplines().length;
        if(currentCount === 0) {
            throw new Error(`First discipline cannot be added with this method. Use setupFirstDiscipline() instead!`);
        }
        const discipline = new Discipline(disciplineData, this, currentCount + 1);
        this.#disciplines.push(discipline);
        return discipline;
    }
    /**
     * Removes a discipline from this character. Do not call this method directly, use {@link Discipline#forgetCircle} instead.
     * @param {Discipline} discipline - The discipline to remove. Must be the last learned one.
     */
    removeDiscipline(discipline) {  
        const index = this.#disciplines.length - 1;     // we only allow to remove the last learned discipline of a character, see Discipline#canForgetCircle.
        if(this.#disciplines[index] !== discipline) {
            throw new Error(`The given discipline ${discipline.getData().getName()} is not the last discipline of this character`);
        }
        this.#disciplines.splice(index, 1);
    }
    /**
     * Set up the first discipline of a character. Automatically adds Karma Ritual as a free talent at rank 1 (house rule). Adds all free talents through racial abilities to this discipline.
     * Call this only during character creation!
     * @param {DisciplineData} disciplineData - The game data of the first discipline.
     */
    setupFirstDiscipline(disciplineData) {
        if(this.#disciplines.length > 0) {
            throw new Error(`Only the first discipline can be added by this method`);
        }
        const discipline = new Discipline(disciplineData, this);
        const karmaRitual = discipline.addTalent(KnownTalent.KARMA_RITUAL.getTalentData(), TalentSource.CORE, 1);   // KarmaRitual is a required core talent for all disciplines. It is needed to calculate the available Karma (see Karma class)
        karmaRitual.increaseFreeRanks();
        for(const ability of this.#race.getAbilities()) {
            if(ability.getType() === AbilityType.FREE_TALENT && (!discipline.isJourneyman() || ability.getAffectedTraitId() !== KnownTalent.VERSATILITY.getId())) {     // the Journeyman does not get Versatility like other humans
                discipline.addTalent(Earthdawn.data.talents[ability.getAffectedTraitId()], TalentSource.ABILITY, 1);
            }
        }
        this.#disciplines.push(discipline);
        this.calculateCharacteristics();
    }
    /**
     * Finalizes this character during character creation, calculates all derived characteristics. Call this once at the end of character creation to set the final values and complete the character!
     * @param {string} id - Unique id of this character
     * @param {string} name - Non-empty name of this character 
     * @param {number} leftOverAttributeBuyingPoints - The number of attribute buying points left at the end of character creation. These are converted into an additional bonus to the karma modifier.
     * @returns {Character} - Returns this
     */
    finalizeCreation(id, name, leftOverAttributeBuyingPoints) {
        this.setId(id);
        this.setName(name);
        this.#karma = new Karma(leftOverAttributeBuyingPoints);
        this.calculateCharacteristics();
        this.getKarma().setAvailablePoints(this.getKarma().getPossibleMaximum());
        return this;
    }
    /**
     * Get all abilities from all learned disciplines (but not race) that can take effect.
     * If multiple disciplines provide the same abilities which provide a characteristic bonus or grant karma use, only the ones with higher total are taken into account.
     * @returns {Ability[]} - Returns an unordered array of all effective discipline abilities. This value is cached internally. Call {@link Character#evictCache('disciplineAbilities')} before to force re-calculation.
     */
    getDisciplineAbilities() {
        if(this.#cache.disciplineAbilities) {
            return this.#cache.disciplineAbilities;
        }        
        const result = [];
        const abilitiesByBonusType = new Map();
        this.#disciplines.forEach(discipline => {
            const byBonusType = new Map();
            discipline.getAbilities().forEach(ability =>  {
                if(ability.highestCountsForMultiDisciplines()) {
                    // collect all abilities that affect the same characteristic (like e.g. Defense) or provide a KarmaUse
                    let typeKey = ability.getType() + "#" + ability.getAffectedTraitId();
                    let abilities = byBonusType.get(typeKey);
                    if(abilities == null) {
                        abilities = [];
                        byBonusType.set(typeKey, abilities);
                    }
                    abilities.push(ability);
                } else {
                    result.push(ability);   // all other independent abilities (e.g. RolePlay or new talents) are just added to the result
                }
            });
            byBonusType.forEach((abilities, key) => {
                // check if another discipline has also provided bonuses for the same characteristic (or KarmaUse): if yes, take only the higher total into account.
                if(abilitiesByBonusType.has(key)) {
                    const existingTotal = abilitiesByBonusType.get(key).reduce((accu, ability) => accu + (ability.getBonusValue() ?? 1), 0);
                    const newTotal = abilities.reduce((accu, ability) => accu + (ability.getBonusValue() ?? 1), 0);
                    if(newTotal <= existingTotal) {
                        return;
                    }
                }
                abilitiesByBonusType.set(key, abilities);
            })
        });
        abilitiesByBonusType.forEach(abilities => result.push(...abilities));   // add the abilities that provided the highest total to the result.
        this.#cache.disciplineAbilities = result;
        return result;
    }
    /**
     * @returns {Ability[]} - Unordered array of all racial and discipline abilities that can take effect for this character. Excludes non-cummulative duplicates (see {@link Character#getDisciplineAbilities}).
     */
    getAllEffectiveAbilities() {
        return [...this.getRace().getAbilities(), ...this.getDisciplineAbilities()];
    }
    /**
     * (Re)calculates all derived characteristics that are dependent on attributes, talents etc.
     */
    calculateCharacteristics() {
        const effectiveAbilities = this.getAllEffectiveAbilities();
        this.#calculateDefenses(effectiveAbilities);
        this.#calculateInitiative(effectiveAbilities);
        this.#calculateHealth(effectiveAbilities);
        this.#calculateEquipment(effectiveAbilities);
        this.#calculateArmorRating(effectiveAbilities);
        this.#calculateKarma(effectiveAbilities);
        this.#calculateMovement();
        this.#calculateEncumberance();
    }
    /**
     * @param {Ability[]} effectiveAbilities - See {@link Character#getAllEffectiveAbilities}.
     */
    #calculateDefenses(effectiveAbilities) {
        this.#defenses.forEach(defense => defense.calculateFrom(this, effectiveAbilities));
    }
    /**
     * @param {Ability[]} effectiveAbilities - See {@link Character#getAllEffectiveAbilities}.
     */
    #calculateInitiative(effectiveAbilities) {
        this.#initiative.calculateFrom(this, effectiveAbilities);
    }
    /**
     * @param {Ability[]} effectiveAbilities - See {@link Character#getAllEffectiveAbilities}.
     */
    #calculateHealth(effectiveAbilities) {
        this.#health.calculateFrom(this, effectiveAbilities);
    }
    /**
     * @param {Ability[]} effectiveAbilities - See {@link Character#getAllEffectiveAbilities}.
     */
    #calculateArmorRating(effectiveAbilities) {
        this.#armorRating.calculateFrom(this, effectiveAbilities);
    }
    /**
     * @param {Ability[]} effectiveAbilities - See {@link Character#getAllEffectiveAbilities}.
     */
    #calculateKarma(effectiveAbilities) {
        this.#karma.calculateFrom(this, effectiveAbilities);
    }
    #calculateMovement() {
        this.#movement.forEach(move => move.calculateFrom(this));
    }
    #calculateEncumberance() {
        this.#encumberance.calculateFrom(this);
    }
    /**
     * @param {Ability[]} effectiveAbilities - See {@link Character#getAllEffectiveAbilities}.
     */
    #calculateEquipment(effectiveAbilities) {
        this.#equipment.calculateFrom(effectiveAbilities);
    }
    #createCharacteristics() {
        this.#defenses = [];
        Object.values(DefenseType).forEach(defense => {
            this.#defenses[defense.id] = new Defense(defense);
        });
        this.#initiative = new Initiative();
        this.#karma = new Karma(0);
        this.#legendPoints = new LegendPoints();
        this.#health = new Health();
        this.#conditions = new Conditions(this);
        this.#armorRating = new ArmorRating();
        this.#movement = Object.entries(this.#race.getMovementMod()).map(([key, mod]) => new Movement(key, mod));
        this.#encumberance = new Encumberance();
        this.#equipment = new Equipment(this);
    }
    /**
     * Evicts an entry from the internal cache.
     * @param {string} key - Name of the property of the cache object that should be removed.
     */
    evictCache(key) {
        delete this.#cache[key];
    }
    /**
     * @returns {object} - Saved character state for serialization
     */
    save() {
        if(!this.#id) {
            throw new Error(`Cannot save incomplete character`);
        }
        const state = {
            version: Character.#version,     // update this everytime something changes in the de/serialization logic and update the save/load methods to stay backward compatible
            id: this.#id,       // used by Persistence as primary key (and by CharacterRecord)
            name: this.#name,   // used by CharacterRecord
            rc: this.#race.getId(),
            sk: this.#skills.map(skill => skill.save()),
            di: this.#disciplines.map(discipline => discipline.save()),
            sp: this.#spells.map(spell => spell.save()),
            ka: this.#karma.save(),
            hl: this.#health.save(),
            cd: this.#conditions.save(),
            lp: this.#legendPoints.save(),
            eq: this.#equipment.save()
        };
        state.at = this.#attributes.map(attribute => attribute.save());     // keep index order
        state.at.shift();      // remove empty slot at index 0 because IndexDB would otherwise replace it with null
        return state;
    }
    /**
     * Loads a character from deserilaized saved state. Restores a Character from a state object and returns it as a Promise. Loads all sub-objects by passing along 
     * the version (and this Character) so that they might load their data too.
     * @param {object} state - The state loaded from a file or the database
     * @returns {Promise<Character>} - The loaded character as a promise
     */
    static async load(state) {    // DO NOT MODIFY STATE (read-only)!
        if(state.version !== Character.#version) {
            throw new Error(`Unsupported character version ${state.version} found during loading`);
        }
        if(!state.id || !state.name) {
            throw new Error(`Id or name of character missing`);
        }
        const character = new this(state.id, state.name, Earthdawn.data.races[state.rc], []);
        const attributeState = new Array(1).concat(state.at);   // add removed first slot to restore original length/order
        character.#attributes = attributeState.map((atState, id) => Attribute.load(atState, state.version, character, id));
        character.#skills = state.sk.map(skState => Skill.load(skState, state.version, character));
        character.#disciplines = state.di.map(diState => Discipline.load(diState, state.version, character));
        character.#spells = state.sp.map(spState => Spell.load(spState, state.version, character));
        character.#karma = Karma.load(state.ka, state.version, character);
        character.#health = Health.load(state.hl, state.version, character);
        character.#legendPoints = LegendPoints.load(state.lp, state.version, character);
        if(state.eq) {
            character.#equipment = Equipment.load(state.eq, state.version, character);
        }
        if(state.cd) {
            character.#conditions = Conditions.load(state.cd, state.version, character);
        }
        character.calculateCharacteristics();
        return character;
    }
    /**
     * @returns {number} - The serialization version. This is the maximum save state version from which this code can load a character. 
     */
    static get #version() {
        return 1;
    }
}

/**
 * Configuration values of the character creation logic.
 * @see {@link CharacterCreator}
 */
class GenerationConfig {
    /** @type {number} Version of this config. Needed to load and restore a saved character with the same values. Currently only version 1 is supported */
    #version;
    #attributePoints;
    #attributeCosts;
    #freeTalentRanks;
    #freeLanguageSkills;
    #freeKnowledgeSkills;
    #freeArtisanSkills;
    /**
     * @param {number} version - Version of this config. Should be changed when any other values change so that we can always reconstruct a loaded character (e.g. for refunding points).
     * @param {number} attributes - Number of attribute buying points during character creation
     * @param {number} talents - Number of free talent ranks to distribute during character creation
     * @param {number} languageSkill - Number of free language skill ranks to distribute during character creation
     * @param {number} knowledgeSkill - Number of free knowledge skill ranks to distribute during character creation
     * @param {number} artisanSkill - Number of free artisan skill ranks to distribute during character creation
     * @param {(attributes: Attribute[]) => number} generalSkill - A function that gets an array of {@link Attribute}s indexed by {@link AttributeId} and returns the number of free general skill ranks to distribute during character creation.
     * @param {(attributes: Attribute[]) => number} spellCircles - A function that gets an array of {@link Attribute}s indexed by {@link AttributeId} and returns the number of free spell circles to distribute during character creation (for spellcaster disciplines).
     * @param {number[]} attributeCosts - Array of length 11 that contains the attribute buying point costs for increasing or decreasing an initial attribute value by a given modifier. The index - 2 is the modifier (difference) to the base attribute value, i.e. index 0 = mod -2, 1 = mod -1, 2 = mod 0, etc.
     */
    constructor(version, attributes, talents, languageSkill, knowledgeSkill, artisanSkill, generalSkill, spellCircles, attributeCosts) {
        if(typeof generalSkill !== "function") {
            throw new Error(`generalSkill has to be a function but was a ${typeof generalSkill}`);
        }
        if(typeof spellCircles !== "function") {
            throw new Error(`spellCircles has to be a function but was a ${typeof spellCircles}`);
        }
        if(attributeCosts.length !== 11) {
            throw new Error(`attributesCosts has to be an array with 11 entries but was ${attributeCosts}`);
        }
        this.#version = version;
        this.#attributePoints = attributes;
        this.#freeTalentRanks = talents;
        this.#freeLanguageSkills = languageSkill;
        this.#freeKnowledgeSkills = knowledgeSkill;
        this.#freeArtisanSkills = artisanSkill;
        this.getFreeGeneralSkills = generalSkill;
        this.getFreeSpellCircles = spellCircles;
        this.#attributeCosts = attributeCosts;
    }
    getVersion() {
        return this.#version;
    }
    getAttributePoints() {
        return this.#attributePoints;
    }
    getFreeTalentRanks() {
        return this.#freeTalentRanks;
    }
    getFreeLanguageSkills() {
        return this.#freeLanguageSkills;
    }
    getFreeKnowledgeSkills() {
        return this.#freeKnowledgeSkills;
    }
    getFreeArtisanSkills() {
        return this.#freeArtisanSkills;
    }
    /**
     * Calculates how many free general skill ranks can be distributed during character creation. ED1/2 made this dependent on the Charisma attribute step while ED3/4 use a fixed value of 8.
     * @param {Attribute[]} attributes - Array of attributes indexed by {@link AttributeId}.
     * @returns {number} - Number of free general skill ranks to use. Can be based on the given attributes.
     */
    getFreeGeneralSkills(attributes) {
        throw new Error(`Has to be overridden`);
        // re-define in ctor
    }
    /**
     * Calculates how many free spell circles can be distributed during character creation. ED3 bases this on the Perception attribute step.
     * @param {Attribute[]} attributes - Array of attributes indexed by {@link AttributeId}.
     * @returns {number} - Number of free spell circles to use. Can be based on the given attributes.
     */
    getFreeSpellCircles(attributes) {
        throw new Error(`Has to be overridden`);
        // re-define in ctor
    }
    /** 
     * @param {number} modifier - A modifier to be applied to the initial attribute value (based on race) to get the final attribute value.
     * @returns {boolean} - True if the given modifier is in the allowed range between -2 and +8. That means the racial base value of an attribute can be at most decreased by 2 or increased by up to 8.
     */
    isSupportedAttributeModifier(modifier) {    // see this.getAttributeCostForModifier
        return modifier >= -2 && modifier <= 8;
    }
    /**
     * @param {number} modifier - A modifier to be applied to the initial attribute value (based on race) to get the final attribute value. Has to be a value between -2 and +8 (see {@link Generator#isSupportedAttributeModifier}).
     * @returns {number} - The number of attribute buying points to spend in order to apply the given modifier to an attribute value.
     */
    getAttributeCostForModifier(modifier) {
        if(!this.isSupportedAttributeModifier(modifier)) {
            throw new Error(`Attribute modifier can only range from -2 to 8 but was ${modifier}`);
        }
        return this.#attributeCosts[modifier + 2];  // returns the total cost for achieving this modifier (non-cummulative).
    }
    /**
     * Creates a generator for the given version number. At the moment only version 1 is supported.
     * 
     * The version 1 generator uses the creation values from ED3 with the following deviations:
     *  - the number of free talent ranks is one higher (normally 8) because we auto-assign the Karma Ritual talent on rank 1 to all characters but want to treat this as a free rank (this is a house rule partly based on ED4).
     *  - the number of free general skill ranks is dependent on the character's Charisma step. This was the standard rule of ED2. ED3/4 uses a fixed value of 8 instead.
     * @param {number} version - Version number of the generator to create.
     * @returns {GenerationConfig} - A generation config instance modelling the configuration values for character creation.
     */
    static create(version) {
        if(version !== 1) {
            throw new Error(`Unexpected GenerationConfig version: ${version}`);
        }
        return new this(1, 25, 9, 2, 2, 1, (attributes) => attributes[AttributeIds.CHARISMA].getStep(), (attributes) => attributes[AttributeIds.PERCEPTION].getStep(), [-2, -1, 0, 1, 2, 3, 5, 7, 9, 12, 15]);
    }
}

/**
 * Class that models the character creation rules based on the {@link GenerationConfig} values. Creates a new {@link Character} starting from the selection of a race.
 */
class CharacterCreator {
    /** @type {RaceData} Selected race for the new character. Cannot be changed after creation started. */
    #race;
    /** @type {?Discipline} - Selected first discipline of the new character or null if not selected yet */
    #discipline;
    /** @type {Attribute[]} - Array of the six standard Earthdawn attributes indexed by {@link AttributeId} */
    #attributes;
    /** @type {Skill[]} - Array of skills assigned to the character. */
    #skills;
    /** @type {?Character} - The new character to create. This is an incomplete character during the creation until finalized. Will be overwritten every time the discipline is changed. */
    #character;
    /** @type {GenerationConfig} - The creation parameters of the chosen ruleset. */
    #generator;     /* Generator used */
    /**
     * @param {RaceData} raceData - The race of the new character to create
     * @param {GenerationConfig} generator - The basic character creation parameters as defined by the rules.
     */
    constructor(raceData, generator) {
        this.#race = raceData;
        this.#generator = generator;
        this.#discipline = null;
        this.#attributes = CharacterCreator.#buildAttributes(this.#race);
        this.#character = null;
        this.#skills = [];
    }
    getRace() {
        return this.#race;
    }
    /** 
     * @typedef {object} AttributeCCInfo - Information about an {@link Attribute} during character creation.
     * @property {number} value - Current value of the attribute. This is the racial base value plus the currently applied modifier (see also {@link GenerationConfig#getAttributeCostForModifier}).
     * @property {number} step - Step number for the current value
     * @property {number} dice - Step dice
     * @property {(number|"min")} minus - Either the buying point cost difference of applying the next lower modifier to the attribute value, or the string "min" if the currently applied modifier is already the lowest possible one.
     * @property {(number|"max")} plus - Either the buying point cost difference of applying the next higher modifier to the attribute value, or the string "max" if the currently applied modifier is already the highest possible one.
     */
    /**
     * @returns {AttributeCCInfo[]} - Array of attribute information indexed by {@link AttributeId}.
     */
    getAttributeInfo() {    // returns AttributeId-indexed array 
        const result = [];
        this.#attributes.forEach(attribute => {
            let info = result[attribute.getId()] = {
                value: attribute.getInitialValue(),
                step: attribute.getStep(),
                dice: attribute.getDice(),
                minus: undefined,
                plus: undefined
            };

            const currentMod = attribute.getInitialValue() - this.#race.getAttributeBase(attribute.getId());
            const currentCost = this.#generator.getAttributeCostForModifier(currentMod);
            if(this.#generator.isSupportedAttributeModifier(currentMod - 1)) {
                info.minus = this.#generator.getAttributeCostForModifier(currentMod - 1) - currentCost;
            } else {
                info.minus = "min";
            }
            if(this.#generator.isSupportedAttributeModifier(currentMod + 1)) {
                info.plus = this.#generator.getAttributeCostForModifier(currentMod + 1) - currentCost;
            } else {
                info.plus = "max";
            }
        });
        return result;
    }
    /**
     * Increase the value of an attribute by one (applying the next higher modifier) or throws.
     * @param {AttributeId} id - The id of the {@link Attribute} to increase
     */
    increaseAttribute(id) {
        const currentMod = this.#attributes[id].getInitialValue() - this.#race.getAttributeBase(id);
        if(!this.#generator.isSupportedAttributeModifier(currentMod + 1)) {
            throw new Error(`Cannot increase attribute ${this.#attributes[id].getName()}. Maximum reached.`);
        }
        const cost = this.#generator.getAttributeCostForModifier(currentMod + 1) - this.#generator.getAttributeCostForModifier(currentMod);
        const available = this.getAvailableAttributePoints();
        if(cost > available) {
            throw new Error(`Not enough attribute buying points: ${available} left, ${cost} needed.`)
        }
        this.#attributes[id].increaseInitialValue();
    }
    /**
     * Decrease the value of an attribute by one (applying the next lower modifier) or throws.
     * @param {AttributeId} id - The id of the {@link Attribute} to decrease
     */
    decreaseAttribute(id /* AttributeId */) {
        const currentMod = this.#attributes[id].getInitialValue() - this.#race.getAttributeBase(id);
        if(!this.#generator.isSupportedAttributeModifier(currentMod - 1)) {
            throw new Error(`Cannot decrease attribute ${this.#attributes[id].getName()}. Minimum reached.`);
        }
        this.#attributes[id].decreaseInitialValue();
    }
    /**
     * Calculates all characteristics and return them as one combined object for rendering.
     * @returns
     */
    getCharacteristics() {
        this.#character.calculateCharacteristics();
        return {
            defenses: {
                physical: this.#character.getDefense(DefenseType.PHYSICAL).getTotal(),
                spell: this.#character.getDefense(DefenseType.SPELL).getTotal(),
                social: this.#character.getDefense(DefenseType.SOCIAL).getTotal()
            },
            armor: {
                physical: this.#character.getArmorRating().getPhysical(),
                mystic: this.#character.getArmorRating().getMystic()
            },
            initiative: {
                step: this.#character.getInitiative().getStep(),
                dice: this.#character.getInitiative().getDice()
            },
            health: {
                unconsciousness: this.#character.getHealth().getUnconsciousnessRating(),
                death: this.#character.getHealth().getDeathRating(),
                woundThreshold: this.#character.getHealth().getWoundThreshold(),
                recoveryDays: this.#character.getHealth().getDaysForRecoveryTestFresh() === 1 ? "day" : `${this.#character.getHealth().getDaysForRecoveryTestFresh()} days`,
                recoveryCount: this.#character.getHealth().getRecoveryTestsPerRefreshPeriod(),
                recoveryStep: this.#character.getHealth().getRecoveryStep(),
                recoveryDice: this.#character.getHealth().getRecoveryDice()
            },
            karma: {
                modifier: this.#race.getKarmaMod()     // we always show the racial karma mod here because it is not clear how many attribute buying points will remaing after creation.
            },
            movement: {
                combat: this.#character.getMovement().map(move => `${move.getCombat()} (${move.getType()})`).join(" / "),
                full: this.#character.getMovement().map(move => `${move.getFull()} (${move.getType()})`).join(" / ")
            },
            encumberance: {
                carry: this.#character.getEncumberance().getCarry(),
                lift: this.#character.getEncumberance().getLift()
            }
        };
    }
    /**
     * Sets the first discipline of the character. The code replaces the whole internal character to cleanup all talents and spells.
     * @param {DisciplineData} disciplineData - The game data of the new discipline
     * @returns {boolean} - Returns true if the discipline was changed, else false. Can be used to decide if the has to re-render the information.
     */
    setDiscipline(disciplineData) {
        if(this.#discipline != null && this.#discipline.getId() === disciplineData.getId()) {
            return false;
        }
        this.#character = new Character("_dummy", "_dummy", this.#race, this.#attributes);
        this.#character.setupFirstDiscipline(disciplineData);
        this.#discipline = this.#character.getDisciplines()[0];
        if(this.#skills.length > 0) {   // keep the existing skills of the old character and adopt them to the character instance.
            this.#skills.forEach(skill => {
                const reassigned = this.#character.addSkill(skill.getData());
                for(let rank = 1; rank <= skill.getFreeRanks(); ++rank) {
                    reassigned.increaseFreeRanks();
                }
            });
        } else {
            // First time we create/set skills. Every character starts with some free ranks Speak Language skill (that cannot be removed)
            let languageRanks = this.#generator.getFreeLanguageSkills();
            if(languageRanks > 0) {
                const languageSkill = Earthdawn.data.skills.find(skillData => skillData && skillData.getType() === SkillType.LANGUAGE);
                const speakLanguage = this.#character.addSkill(languageSkill);
                while(languageRanks-- > 0) {
                    speakLanguage.increaseFreeRanks();
                }
            }
        }
        this.#skills = this.#character.getSkills();
        return true;
    }
    /**
     * @typedef {object} TalentCCInfo - Information about a {@link Talent} for rendering
     * @property {Talent} talent - Reference of the talent
     * @property {TalentSource} source - Source from which the talent was learned
     * @property {string} name - Name of the talent
     * @property {number} rank - Free ranks of this talent
     * @property {boolean} action - Flag if usage of this talent requires an action
     * @property {number} strain - Number of strain damage the usage of this talent causes
     * @property {KarmaRequirement} karma - Enum value that defines if Karma use is possible or even required for this talent
     * @property {?string} [attributeName] - Name of the attribute on which this talent is based. May be missing if this talent is not based on an attribute.
     * @property {?number} [step] - Step of this talent if it has one.
     * @property {?string} [dice] - Step dice of this talent if it has a step.
     * @property {?() => void} [minus] - Function that can be called to decrease the free ranks of this talent by one if it is possible to do so, else undefined.
     * @property {?() => void} [plus] - Function that can be called to increase the free ranks of this talent by one if it is possible to do so, else undefined.
     */
    /**
     * Get information about all currently learned talents of first circle (because during character creation only first circle talents are available).
     * @returns {TalentCCInfo[]} - Unsorted array of talent information
     */
    getAllTalentInfo() {
        const circle = 1;
        const pointsAvailable = this.getAvailableTalentRanks();
        return this.#discipline.getLearnedTalentsOf(circle).map(talent => {
            const info = {
                talent: talent,
                source: talent.getSource(),
                name: talent.getData().getName(),
                rank: talent.getFreeRanks(),
                action: talent.getData().needsAction(),
                strain: talent.getData().getStrain(),
                karma: talent.getKarmaRequirement()
            };
            if(talent.hasStep()) {
                info.attributeName = talent.getAttribute().getName();
                info.step = talent.getStep();
                info.dice = talent.getDice();
            }
            if(talent.mayDecreaseTotalRank(false) === TalentRankChange.POSSIBLE) {
                info.minus = () => talent.decreaseFreeRanks();
            }
            if(pointsAvailable > 0 && talent.canIncreaseFreeRanks()) {
                info.plus = () => talent.increaseFreeRanks();
            }
            return info;
        });
    }
    getAvailableCoreTalents() {
        const circle = 1;
        return this.#discipline.getAvailableCoreTalents()[circle];
    }
    getAvailableTalentOptions() {
        const circle = 1;
        if(this.#discipline.getTalentOptionSlots()[circle] > 0) {
            return this.#discipline.getAvailableTalentOptions()[StatusLevel.INITIATE];
        }
        return [];
    }
    getAvailableVersatilityOptions() {
        return this.#discipline.getAvailableVersatilityOptions(false);
    }
    canLearnForeignTalents() {
        if(this.#discipline.isJourneyman()) {   // Journeyman basically uses something like Versatility to learn foreign talents but without actually having the Versatility talent.
            const circle = 1;
            const openSlots = Earthdawn.rules.advancement.getTalentCountPerCircle()[circle] - this.#discipline.getLearnedTalentsOf(circle).length - this.getAvailableCoreTalents().length;
            return {
                hasVersatility: true,
                canLearnMore: openSlots > 0,
                isJourneyman: true
            }
        }
        const openSlots = this.#discipline.getOpenVersatilitySlots();
        return {
            hasVersatility: openSlots !== -1,
            canLearnMore: openSlots > 0,
            isJourneyman: false
        };
    }
    /**
     * Learn a new talent from the available options at rank 1.
     * @param {TalentData} talentData - The game data of the new talent to learn
     * @param {TalentSource} source - The source how this talent was acquired
     * @returns {Talent} - The newly added talent
     */
    addNewTalentWithOneRank(talentData, source) {
        if(this.getAvailableTalentRanks() <= 0) {
            throw new Error(`Cannot learn talent ${talentData.getName()} because no free talent ranks left`);
        }
        const talent = this.#discipline.addTalent(talentData, source, 1);
        talent.increaseFreeRanks();
        return talent;
    }
    /**
     * @typedef {object} SkillCCInfo - Information about a {@link Skill} for rendering
     * @property {Skill} skill - Reference of the skill
     * @property {SkillType} type - Type of this skill
     * @property {string} name - Name of the skill
     * @property {number} rank - Free ranks of this skill
     * @property {boolean} action - Flag if usage of this skill requires an action
     * @property {number} strain - Number of strain damage the usage of this skill causes
     * @property {string} attributeName - Name of the attribute on which this skill is based.
     * @property {number} step - Step of this skill
     * @property {string} dice - Step dice of this skill
     * @property {?() => void} [minus] - Function that can be called to decrease the free ranks of this skill by one if it is possible to do so, else undefined.
     * @property {?() => void} [plus] - Function that can be called to increase the free ranks of this skill by one if it is possible to do so, else undefined.
     */
    /**
     * Get information about all currently learned skills
     * @returns {SkillCCInfo[]} - Unsorted array of skill information
     */
    getAllSkillInfo() {
        const pointsAvailable = this.getAvailableSkillRanks();
        return this.#skills.map(skill => {
            const info = {
                skill: skill,
                type: skill.getType(),
                name: skill.getData().getName(),
                rank: skill.getFreeRanks(),
                action: skill.getData().needsAction(),
                strain: skill.getData().getStrain(),
                attributeName: skill.getAttribute().getName(),
                step: skill.getStep(),
                dice: skill.getDice()
            };
            let enoughPoints = false;
            switch(skill.getType()) {
                case SkillType.ARTISAN:
                    enoughPoints = pointsAvailable.artisan > 0 || pointsAvailable.general > 0;
                    break;
                case SkillType.KNOWLEDGE:
                    enoughPoints = pointsAvailable.knowledge > 0 || pointsAvailable.general > 0;
                    break;
                case SkillType.GENERAL:
                case SkillType.LANGUAGE:    // assumes that we have already added the Speak Language skill with maximum available rank to the Character by default.
                    enoughPoints = pointsAvailable.general > 0;
                    break;
            }
            if(enoughPoints && skill.canIncreaseFreeRanks()) {
                info.plus = () => skill.increaseFreeRanks();
            }
            if(skill.canDecreaseFreeRanks() && (skill.getType() !== SkillType.LANGUAGE || skill.getFreeRanks() > this.#generator.getFreeLanguageSkills())) {       // Do not allow the removal of the Speak Language skill
                info.minus = () => skill.decreaseFreeRanks();
            }
            return info;
        });
    }
    /**
     * @param {SkillType} type - A specific skill type for which to retrieve skills
     * @returns {SkillData[]} - Returns an array of all skills of the given type that have not been learned yet
     */
    getAvailableSkills(type) {
        return this.#character.getAvailableSkills(type);
    }
    /**
     * Learns a new skill at rank 1. Does not check if there are enough free skill ranks left to acquire this skill.
     * @param {SkillData} skillData - Game data of the skill to learn
     * @returns {Skill} - The newly added skill
     */
    addNewSkillWithOneRank(skillData) {
        const skill = this.#character.addSkill(skillData);
        skill.increaseFreeRanks();
        return skill;
    }
    getAvailableAttributePoints() {
        return this.#generator.getAttributePoints() - this.#getSpentAttributePoints();
    }
    getAvailableTalentRanks() {
        return this.#generator.getFreeTalentRanks() - this.#getSpentTalentRanks();
    }
    getAvailableSkillRanks() {
        const spent = this.#getSpentSkillRanks();
        return {
            language: this.#generator.getFreeLanguageSkills() - spent.language,
            knowledge: this.#generator.getFreeKnowledgeSkills() - spent.knowledge,
            artisan: this.#generator.getFreeArtisanSkills() - spent.artisan,
            general: this.#generator.getFreeGeneralSkills(this.#attributes) - spent.general    // "general" may be negative if more free skill ranks used already (e.g. after Charisma attribute was decreased)
        };
    }
    /**
     * @returns {Spell[]} - Returns a sorted array of all spells the character has already learned.
     */
    getAllSpells() {
        return this.#character.getSpells();
    }
    /**
     * Removes an already learned spell again.
     * @param {Spell} spell - The spell to remove from the character. 
     */
    removeSpell(spell) {
        this.#character.removeSpell(spell);
    }
    /**
     * Learn a new spell. Does not check if there are enough free spell circles left to do so.
     * @param {SpellData} spellData - Game data of the new spell
     * @returns {Spell} - The newly added spell
     */
    addSpell(spellData) {
        return this.#character.addSpell(spellData, FreeSpell.CREATION);
    }
    /**
     * Get all a available (not yet learned) spells up to the given circle. Includes only spells accesible to the character's discipline. It is not possible to learn foreign spells during character creation, even
     * if the character possesses Versatility and has learned a foreign Thread Weaving and Spellcasting talent.
     * @param {number} upToCircle - Maximum circle the returned spells can have. Capped at 3 at character creation.
     * @returns {SpellData[][]} - Returns an array indexed by circle (1-based) of all the available spells on that circle.
     */
    getAvailableSpellsByCircle(upToCircle) {
        return this.#character.getAvailableSpells(Math.min(upToCircle, 3), false);
    }
    /**
     * @returns {number} - The amount of free spell circles left. Returns 0 if the character's discipline does not provide any spells.
     */
    getAvailableSpellCircles() {
        if(!this.canHaveSpells()) {
            return 0;
        }
        return this.#generator.getFreeSpellCircles(this.#attributes) - this.#getSpentSpellCircles();
    }
    /**
     * @returns {boolean} - Returns true if the character can learn spells. This is the case only if their discipline provides spells and they have already learned the the corresponding Thread Weaving talent. Versatility does not provide free spells during character creation.
     */
    canHaveSpells() {
        // If the character has learned Spellcasting/ThreadWeaving only via Versatility during Character Creation, we do not allow them to learn free spells.
        if(this.#discipline.getData().hasSpells()) {
            const threadWeavingId = this.#discipline.getData().getThreadWeavingIdForSpellcasting();
            return this.#discipline.getLearnedTalentsOf(1).some(talent => talent.getId() === threadWeavingId && talent.getTotalRank() > 0);
        }
        return false;
    }
    /**
     * Checks if the character is fully created by spending all available points. Call this before trying to {@link CharacterCreator#finalizeCharacter}.
     * @returns {{ok:boolean, issues:string[]}} - If ok is true, the character creation is complete and the character can be finalized, else the issues array contains a list of human-readable messages of steps still incomplete.
     */
    canComplete() {
        const info = {ok: false, issues: []};
        const remainingAttributePoints = this.getAvailableAttributePoints();
        if(remainingAttributePoints > 1) {      // it's always possible to spend 2 or more buying points to increase an attribute value but 1 might not be enough. All leftover buying points can be converted to Karma mod.
            info.issues.push(`Usable attribute buying points: ${remainingAttributePoints}`);
        }
        const remainingTalentRanks = this.getAvailableTalentRanks();
        if(remainingTalentRanks !== 0) {
            info.issues.push(`Unspent free talent ranks: ${remainingTalentRanks}`);
        }
        const remainingSkillRanks = this.getAvailableSkillRanks();
        if(remainingSkillRanks.knowledge > 0) {
            info.issues.push(`Unspent free knowledge skill ranks: ${remainingSkillRanks.knowledge}`);
        }
        if(remainingSkillRanks.artisan > 0) {
            info.issues.push(`Unspent free artisan skill ranks: ${remainingSkillRanks.artisan}`);
        }
        if(remainingSkillRanks.general > 0) {
            info.issues.push(`Unspent free general skill ranks: ${remainingSkillRanks.general}`);
        } else if(remainingSkillRanks.general < 0) {
            info.issues.push(`Too many general skill ranks spent: ${-remainingSkillRanks.general}`);
        }
        const remainingSpellPoints = this.getAvailableSpellCircles();
        if(remainingSpellPoints > 0) {
            info.issues.push(`Unspent free spell circles: ${remainingSpellPoints}`);
        } else if(remainingSpellPoints < 0) {
            info.issues.push(`Too many spell circles spent: ${-remainingSpellPoints}`);
        }
        info.ok = info.issues.length === 0;
        return info;
    }
    /**
     * Finalizes the character creation by setting the last values. Check {@link CharacterCreator#canComplete} first before calling this method to ensure that no creation rules were violated!
     * After this method was called, this instance of the CharacterCreator should be destroyed and cannot be used any further.
     * @param {string} id - Unique id (e.g. UUID) for this character. This is later used by the {@link Persistence} as primary key.
     * @param {string} name - Non-empty name of this character.
     * @returns {Character} - The completed new character with which play can begin.
     */
    finalizeCharacter(id, name) {
        this.#character.finalizeCreation(id, name, this.getAvailableAttributePoints());
        return this.#character;
    }
    #getSpentTalentRanks() {
        if(this.#discipline == null) {
            return 0;
        }
        return this.#discipline.getLearnedTalents().reduce((accumulator, byCircle) => byCircle.reduce((accu, talent) => accu + talent.getFreeRanks(), accumulator), 0);
    }
    #getSpentAttributePoints() {
        let spent = 0;
        this.#attributes.forEach(attribute => {
            const mod = attribute.getInitialValue() - this.#race.getAttributeBase(attribute.getId());
            spent += this.#generator.getAttributeCostForModifier(mod);
        });
        return spent;   // may be negative
    }
    #getSpentSkillRanks() {
        let language = 0,
            knowledge = 0,
            artisan = 0,
            general = 0;
        for(const skill of this.#skills) {
            switch(skill.getType()) {
                case SkillType.LANGUAGE:
                    language += skill.getFreeRanks();
                    break;
                case SkillType.ARTISAN:
                    artisan += skill.getFreeRanks();
                    break;
                case SkillType.KNOWLEDGE:
                    knowledge += skill.getFreeRanks();
                    break;
                default:
                    general += skill.getFreeRanks();
            }
        }
        if(language > this.#generator.getFreeLanguageSkills()) {
            const diff = language - this.#generator.getFreeLanguageSkills();
            language -= diff;
            general += diff;
        }
        if(knowledge > this.#generator.getFreeKnowledgeSkills()) {
            const diff = knowledge - this.#generator.getFreeKnowledgeSkills();
            knowledge -= diff;
            general += diff;
        }
        if(artisan > this.#generator.getFreeArtisanSkills()) {
            const diff = artisan - this.#generator.getFreeArtisanSkills();
            artisan -= diff;
            general += diff;
        }
        return {
            language: language,
            knowledge: knowledge,
            artisan: artisan,
            general: general
        };
    }
    #getSpentSpellCircles() {
        return this.#character.getSpells().reduce((accu, spell) => {
            return accu + spell.getData().getCircle();
        }, 0);
    }
    /**
     * @param {RaceData} race - The character's race
     * @returns {Attribute[]} - Array of attributes indexed by {@link AttributeId}. The attributes have the racial base value assigned as their initial value.
     */
    static #buildAttributes(race) {
        const attributes = [];
        AttributeIds.getIds().forEach(id => {
            attributes[id] = new Attribute(id, race.getAttributeBase(id));
        });
        return attributes;
    }
}

/**
 * A number input field with a down and up button on its side and a range slider beneath it for faster value changes.
 * Use it either in code as is or in HTML as <number-field value="0" min="0" max="100"></number-field>
 */
class NumberField extends HTMLElement {
    /** @type {HTMLInputElement} - The number input field child that holds the actual value */ 
    #numberField;
    /** @type {HTMLInputElement} - The range input field child that represents the slider */
    #rangeSlider;
    /** @type {number} - The minimum value allowed by this element */
    #min;
    /** @type {number} - The maximum value allowed by this element */
    #max;

    constructor() {
        super();
        this.replaceChildren();     // for some reason cloning this element (e.g. as part of another element) will also clone all its children into the newly constructed element. Therefore remove all (cloned) children here first.
        const wrapper = document.createElement('div');
        wrapper.className = 'value-field';
        const firstLine = document.createElement('div');
        wrapper.appendChild(firstLine);
        const downButton = document.createElement('button');
        downButton.className = 'icon down';
        downButton.type = 'button';
        firstLine.appendChild(downButton);
        this.#setupButtonAction(downButton, () => this.#decreaseBy(1));
        let icon = document.createElement('span');
        icon.className = 'fa-solid fa-caret-left';
        downButton.appendChild(icon);
        this.#numberField = document.createElement('input');
        this.#numberField.type = 'number';
        this.#numberField.className = 'value';
        this.#numberField.autocomplete = 'off';
        this.#setupNumberChecker(this.#numberField);
        firstLine.appendChild(this.#numberField);
        const upButton = document.createElement('button');
        upButton.className = 'icon up';
        upButton.type = 'button';
        firstLine.appendChild(upButton);
        this.#setupButtonAction(upButton, () => this.#increaseBy(1));
        icon = document.createElement('span');
        icon.className = 'fa-solid fa-caret-right';
        upButton.appendChild(icon);
        this.#rangeSlider = document.createElement('input');
        this.#rangeSlider.type = 'range';
        this.#rangeSlider.className = 'slider';
        this.#setupSlider(this.#rangeSlider);
        wrapper.appendChild(this.#rangeSlider);
        this.appendChild(wrapper);
        this.initialize(0, 100, 0);
    }
    /**
     * Defines the initial values of this element.
     * @param {number} min - The minimum inclusive boundary of the input.
     * @param {number} max - The maximum inclusive boundary of the input. Has to be greater than min.
     * @param {number} initialValue - The initial value of the input. Has to be between min and max.
     * @returns 
     */
    initialize(min, max, initialValue) {
        if(min >= max) {
            throw new Error(`Minimum must not be greater or equal to maximum`);
        }
        if(initialValue < min || initialValue > max) {
            throw new Error(`Initial value must be within min and max range`);
        }
        this.#setMin(min);
        this.#setMax(max);
        this.#setInitialValue(initialValue);
        return this;
    }
    /**
     * Lifecycle event called by the browser when this element is added to the DOM. Useful only when this element is used directly in HTML as tag via <number-field></number-field>
     * @callback
     */
    connectedCallback() {
        if(this.hasAttribute('min')) {
            this.#setMin(Number(this.getAttribute('min')));
        }
        if(this.hasAttribute('max')) {
            this.#setMax(Number(this.getAttribute('max')));
        }
        if(this.hasAttribute('value')) {
            this.#setInitialValue(Number(this.getAttribute('value')));
        }
    }
    /**
     * @param {number} minimum - Sets the minimum of the input. 
     */
    #setMin(minimum) {
        this.#min = minimum;
        this.#numberField.min = minimum + '';
        this.#rangeSlider.min = minimum + '';
    }
    /**
     * @param {number} maximum - Sets the maximum of the input
     */
    #setMax(maximum) {
        this.#max = maximum;
        this.#numberField.max = maximum + '';
        this.#rangeSlider.max = maximum + '';
    }
    /**
     * @param {number} value - The initial value of the input
     */
    #setInitialValue(value) {
        this.#numberField.value = value + '';
        this.#rangeSlider.value = value + '';
    }
    /**
     * @returns {number} - Returns the current value of the input or the minimum if not value was set yet.
     */
    getValue() {
        const value = this.#numberField.value;
        if(value === '') {
            return this.#min;
        }
        return Number(value);
    }
    /**
     * @param {number} value - The current value to set. Is automatically adjusted to fall between min and max.
     */
    setValue(value) {
        value = Math.max(this.#min, Math.min(value, this.#max));
        this.#setValueInternal(value);
        this.#updateSliderPosition();
    }
    /**
     * @param {number} diff - The difference by which the current value should be decreased.
     * @returns {boolean} - Returns true if the current value can be decreased further after this operation.
     */
    #decreaseBy(diff) {
        let value = this.getValue();
        value = Math.max(this.#min, value - diff);
        this.#setValueInternal(value);
        this.#updateSliderPosition();
        return value > this.#min;
    }
    /**
     * @param {number} diff - The difference by which the current value should be increased.
     * @returns {boolean} - Returns true if the current value can be increased further after this operation.
     */
    #increaseBy(diff) {
        let value = this.getValue();
        value = Math.min(value + diff, this.#max);
        this.#setValueInternal(value);
        this.#updateSliderPosition();
        return value < this.#max;
    }
    #updateSliderPosition() {
        this.#rangeSlider.value = this.getValue() + '';
    }
    /**
     * Adds event listeners to the button controls.
     * @param {HTMLButtonElement} button - One of the up/down buttons
     * @param {(() => boolean)} actionCallback - Callback function that returns true if the button can be clicked again, else false if the linked operation should not be executed anymore.
     */
    #setupButtonAction(button, actionCallback) {
        button.addEventListener('pointerdown', event => {
            if(!actionCallback()) {
                return false;
            }
            let timer = setTimeout(() => {
                clearTimeout(timer);
                timer = setInterval(() => {
                    if(!actionCallback()) {
                        clearInterval(timer);
                        timer = null;
                    }
                }, 70);
            }, 500);
            const aborter = new AbortController();
            const stopAction = () => {
                if(timer != null) {
                    clearTimeout(timer);
                    clearInterval(timer);
                    timer = null;
                }
                aborter.abort();
            };
            button.addEventListener('pointerleave', stopAction, {signal: aborter.signal});
            button.addEventListener('pointerup', stopAction, {signal: aborter.signal});
            return true;
        });
    }
    /**
     * Sets up input validation and automatically converts invalid input into numbers or the minimum if the input cannot be interpreted as a number.
     * @param {HTMLInputElement} input - The number input field
     */
    #setupNumberChecker(input) {
        input.pattern = '[1-9][0-9]*';
        input.addEventListener('input', event => {
            if(!input.checkValidity()) {
                let value = parseInt(input.value);
                if(isNaN(value)) {
                    value = this.#min;
                } else {
                    value = Math.max(this.#min, Math.min(value, this.#max));
                }
                this.#setValueInternal(value);
            } else {
                this.#fireChanged();
            }
            this.#updateSliderPosition();
        });
    }
    /**
     * Sets the event listener on the slider element
     * @param {HTMLInputElement} input - The range input element
     */
    #setupSlider(input) {
        input.addEventListener('input', event => {
            this.#setValueInternal(input.value);
        });
    }
    /**
     * @param {(number|string)} value - The value to set on the number field (without updating the slider)
     */
    #setValueInternal(value) {
        this.#numberField.value = value + '';
        this.#fireChanged();
    }
    /**
     * Fires a change event on this custom div element so that others can react to value changes.
     */
    async #fireChanged() {
        this.dispatchEvent(new Event("change"));
    }
}
/** Defines the derived class name of the customer NumberField for usage as an HTML tag: <number-field></number-field>. This declaration is mandatory for all custom elements.  */
window.customElements.define('number-field', NumberField);


/**
 * Base class for a container element that consists of multiple pages from which only one page is visible at a time. A single page fills the full visible width 
 * of the container while all other pages are invisible in the overflow area. The pages are horizontally aligned and can be scrolled left or right to switch 
 * between them seamlessly. Alternatively, one can jump directly to a specific page. The pages are indexed by their 1-based position in order from left to right.
 * 
 * Example:
 * <div class='page-container'>
 *   <div class='page'></div>
 *   <div class='page'></div>
 * </div>
 */
class PageScroller {
    /** @type {HTMLElement} container element holding all pages. It defines the visible area that can display one page while all others overflow into hiding. */
    pageContainer;
    /** @type {number} Number of pages in this pageContainer */
    pageCount;
    /**
     * @param {HTMLElement} container - The container element housing all pages. It should have the 'page-container' CSS class. Has to include one or more children that have the CSS class 'page'.
     * @param {Presenter} presenter - Window presenter
     */
    constructor(container, presenter) {
        this.pageContainer = container;
        this.pageCount = presenter.findElements(".page", container).length;
    }
    /**
     * Scroll one page to the left.
     */
    scrollLeft() {
        const current = this.pageContainer.scrollLeft;
        if(current === 0) {
            return;
        }
        this.pageContainer.scrollLeft = Math.max(0, current - this.calculateScrollStep());
    }
    /**
     * Scroll one page to the right.
     */
    scrollRight() {
        const stepWidth = this.calculateScrollStep();
        const maxScroll = this.pageContainer.scrollWidth - stepWidth;
        const current = this.pageContainer.scrollLeft;
        if(current === maxScroll) {
            return;
        }
        this.pageContainer.scrollLeft = Math.min(maxScroll, current + stepWidth);
    }
    /** Reset the scroll position back to the first page */
    reset() {
        this.jumpToPage(1);
    }
    /**
     * Jumps to (shows) the page at the given position by setting the scroll position within the container accordingly.
     * @param {number} page - 1-based position of the page to jump to. Note: this is not necessarily the value given in the 'data-page' attribute of a page element but its child position within the container children.
     */
    jumpToPage(page) {
        this.pageContainer.scrollLeft = this.calculateScrollStep() * (page - 1);
    }
    /**
     * This is an internal method to be called only by derived classes to adjust the private page count property.
     * @protected
     * @param {number} diff - Difference by which the page count of this scoller should be adjusted.
     */
    adjustPageCount(diff) {
        this.pageCount += diff;
    }
    /**
     * This is an internal method to be called only by derived classes to calculate how many pixels the contained, overflowing children have to be scrolled to switch between pages.
     * @protected
     * @returns {number} - Width in pixels of one page within this page container.
     */
    calculateScrollStep() {
        return this.pageContainer.scrollWidth / this.pageCount;
    }
    /**
     * This is an internal method to be called only by derived classes to calculate which page is currently visible.
     * @protected
     * @returns {number} - Returns the 1-based position of the page that currently visible based on the current scroll position within the container. If between two pages, the page that is more than 50% visible is returned.
     */
    calculateCurrentPage() {
        return Math.round(this.pageContainer.scrollLeft / this.calculateScrollStep()) + 1;
    }
}

/**
 * A navigation bar (navbar) that provides controls and a menu to manage the navigation for a paged display element (see {@link PageScroller}).
 * 
 * The navbar is rendered as a bar element that displays the currently displayed page's title highlighted in the middle (with optionally the neighboring page titles
 * besides it if there is enough screen space) and an outward pointing arrow button on either side of it. Clicking one of the arrow icons scrolls one page to
 * the left or right, if possible. Clicking the navbar itself opens a selection menu in a modal dialog that lists all page titles in the scrolling order with the
 * current page highlighted. Clicking on one of the page titles in the menu closes the dialog and jumps to that page.
 * 
 * The navbar element has to contain the two arrow buttons (button elements with a 'data-direction' attribute with value 'left' or 'right') and a menu element. The 
 * menu element has to contain one list item (li) element per page that contains the display title as inner text. Both the menu list items as well as the corresponding
 * page elements of the scroll container have to contain a matching 'data-page' attribute with a unique number value (ideally matching the page's 1-based position). The
 * list items must be in the same order as their corresponding pages.
 * Pages can be excluded (hidden) from the scroller, which will also hide their menu entry. These pages can later be included again. But both the page and menu item element
 * should exist already when the DynamicNavbarScroller is created.
 * 
 * Example:
 * <div class="navbar-s">
 *   <button data-direction="left">&lt;</button>
 *   <menu>
 *     <li data-page="1">Title of page 1</li>
 *     <li data-page="2">Title of page 2</li>
 *   </menu>
 *   <button data-direction="right">&gt;</button>
 * </div>
 * <div class='page-container'>
 *   <div class='page' data-page='1'></div>
 *   <div class='page' data-page='2'></div>
 * </div>
 */
class DynamicNavbarScroller extends PageScroller {
    /** @type {Presenter} - Window presenter */
    #presenter;
    /** @type {HTMLElement} - The menu child element of the navbar */
    #menu;
    /** @type {{left:HTMLButtonElement, right:HTMLButtonElement}} - An object containing references to both the left and right navigation buttons of the navbar */
    #buttons;
    /** @type {number} - The currently selected page. This is the 1-based position based on the total count of non-excluded pages. Not necessarily the same as the value of the data-page attribute of the select page. */
    #selectedPage;
    /** @type {AbortController} - An abort controller registered with all event listeners created by this class in order to remove these listeners when this instance is destroyed. */
    #aborter;
    /** 
     * @param {HTMLElement} pageContainer - The container element that contains the content pages through which the user can scroll either directly (e.g. by swiping) or via the navbar controls/menu. See {@link PageScroller}.
     * @param {HTMLElement} navbar - The element representing the navigation bar. This element has to contain the arrow buttons on the side as well as the menu with its list items (see class description).
     * @param {Presenter} presenter - Window presenter
     */
    constructor(pageContainer, navbar, presenter) {
        super(pageContainer, presenter);
        this.#presenter = presenter;
        this.#aborter = new AbortController();
        this.#selectedPage = -1;
        this.#setupEvents(navbar, presenter);
    }
    /**
     * Hides a page and its corresponding menu entry from the navigation and the scroll container. The page is identified via its data-page attribute value, not by its position.
     * @param {number} pageNumber - The value from the 'data-page' attribute (not necessarily the page position).
     */
    excludePage(pageNumber) {
        this.#togglePageAvailability(pageNumber, false);
    }
    /**
     * Unhides a formerly excluded page and its corresponding menu entry and makes it available in the navigation again. The page is identified via its data-page attribute value, not by its position.
     * @param {number} pageNumber - The value from the 'data-page' attribute (not necessarily the page position).
     */
    includePage(pageNumber) {
        this.#togglePageAvailability(pageNumber, true);
    }
    /**
     * Jumps to the first page and adjusts the navigation accordingly.
     * @override
     */
    reset() {
        super.reset();
        this.adjustMenuSelection();
    }
    /**
     * Updates the current selection and the rendered state of the navigation elements based on the current scroll position of the page container.
     * @protected
     * @param {boolean} [forced=false] - If true the render state is also re-caculated when the currently selection has not changed.
     */
    adjustMenuSelection(forced = false) {
        const position = this.calculateCurrentPage();
        if(position === this.#selectedPage && !forced) {
            return;
        }
        this.#selectedPage = position;
        const items = this.#menu.children;
        let visibleCount = 0;
        for(let i = 0; i < items.length; ++i) {
            const item = /** @type {HTMLElement} */ (items[i]);
            if(!item.hidden) {
                ++visibleCount;
            }
            item.classList.toggle("selected", visibleCount === position);
        }
        this.#adjustButtonStates();
    }
    /**
     * Call this when this instance is destroyed to clean up all held resources.
     */
    destroy() {     // de-registers all event listeners
        this.#aborter.abort();
        this.#aborter = null;
    }
    #adjustButtonStates() {
        this.#buttons.left.disabled = this.#selectedPage === 1;
        this.#buttons.right.disabled = this.#selectedPage === this.pageCount;
    }
    /**
     * Include or exclude a page and its menu entry.
     * @param {number} pageNumber - The value from the 'data-page' attribute (not necessarily the page position).
     * @param {boolean} include - Flag if the page should be included (true) or excluded (false)
     */
    #togglePageAvailability(pageNumber /* from data-page */, include /* boolean */) {
        const page = this.#presenter.getElement(`.page[data-page="${pageNumber}"]`, this.pageContainer);
        if(page.hidden === !include) {
            return;
        }
        page.hidden = !include;
        this.#presenter.getElement(`li[data-page="${pageNumber}"]`, this.#menu).hidden = !include;
        this.adjustPageCount(include ? 1 : -1);
        this.adjustMenuSelection(true);
    }
    /**
     * Listener for the navigation button click events.
     * @param {?Event} event - Button click event
     */
    #navigatePage(event) {
        const dir = /** @type {HTMLElement} */ (event.currentTarget).getAttribute("data-direction") === "left" ? "scrollLeft" : "scrollRight";
        this[dir]();   // triggers a scroll event that will adjust the menu and buttons
    }
    /**
     * Opens the modal dialog with the navigation menu when the navbar is clicked.
     */
    #showNavigationDialog() {
        const modal = this.#presenter.getModal();
        modal.addCssClass("modal-menu");
        const content = this.#menu.cloneNode(true);
        modal.addContent(content);
        this.#presenter.addObserverTo(content, "click", event => {
            let position = 0;
            for(const item of this.#presenter.findElements("li:not([hidden])", /** @type {Element} */ (event.currentTarget))) {
                ++position;
                if(item === event.target) {
                    modal.close().then(() => this.jumpToPage(position));
                    return;
                }
            }
        });
        modal.open();
    }
    /**
     * @param {HTMLElement} navbar - The navbar HTML element that contains the menu and arrow buttons.
     * @param {Presenter} presenter - Window presenter
     */
    #setupEvents(navbar, presenter) {
        this.#menu = presenter.getElement("menu", navbar);
        this.#buttons = {
            left: /** @type {HTMLButtonElement} */ (presenter.getElement("button[data-direction='left']", navbar)),
            right: /** @type {HTMLButtonElement} */ (presenter.getElement("button[data-direction='right']", navbar))
        };
        presenter.addObserverTo(this.#menu, "click", event => this.#showNavigationDialog(), {signal: this.#aborter.signal});
        presenter.addObserverTo(this.#buttons.left, "click", event => this.#navigatePage(event), {signal: this.#aborter.signal});
        presenter.addObserverTo(this.#buttons.right, "click", event => this.#navigatePage(event), {signal: this.#aborter.signal});
        presenter.addObserverTo(this.pageContainer, "scroll", event => this.adjustMenuSelection(), {passive: true, signal: this.#aborter.signal});
    }
}

/**
 * Dynamically adds expandable/collapsable areas as a new rows into a CSS grid when existing grid items (the expander toggles) are clicked.
 * The expander is created for a container element of type display:grid. The grid may contain any number of expander toggles that have to be registered via {@link GridExpandable#registerToggle}. Each toggle gets a unique content
 * id assigned as data attribute. When a toggle is clicked, a new visible content element (called a collapsable) is added to the grid. The retriever callback is called with the content id and should return the text content which
 * gets inserted into the collapsable area. When the toggle is clicked again, the collapsable area is removed from the grid and destroyed again. The toggle can be used any number of times. Only one collapsable area can be open
 * at a time. Opening a new area will automatically close all others.
 */
class GridExpandable {
    #presenter;
    #retriever;
    #grid;
    #offset;
    /** @type {AbortController} - Abort controller used to clear all event listeners created by this class. Called by destroy. */
    #aborter;
    /** @type {?{expander: Element, collapse: HTMLElement}} - Object that is null if the collapsable is closed (does not exist), else it contains a reference to the expander toggle and the open collapsable area. */
    #open;
    /**
     * @param {Presenter} presenter - Window presenter
     * @param {(id:(number|string)) => string} retriever - A callback that is called with an id that was extracted from the 'data-expander' attribute of the toggle element. Should return the content that should be displayed as innerText in the collapsable area: either a string or an HTML node.
     * @param {Element} grid - A container element that represents a CSS grid (display: grid). This grid must contain the expander element registered via {@link GridExpandable#registerToggle}. The collapsable area will be inserted into this grid.
     * @param {number} offset - Offset how many sibling HTML elements after the expander toggle the collapsable area should be inserted into the grid. Offset 0 means directly after the expander, 1 means to add it after the next sibling, etc.
     */
    constructor(presenter, retriever, grid, offset) {
        this.#presenter = presenter;
        this.#retriever = retriever;
        this.#grid = grid;
        this.#offset = offset;
        this.#aborter = null;
        this.#open = null;
        this.#startListening();
    }
    /**
     * Registers the element that acts as the toggle button for opening/closing the expandable/collapsable area. Inititally there is no collapsable area, it will be created every time the toggle opens it and destroyed when it is closed.
     * @param {HTMLElement} expanderElement - The grid item that acts as the expander toggle. Clicking this element will open and close the collapsable area.
     * @param {(number|string)} contentId - Id that uniquely identifies the content to be shown in the collapsable area. This id will be passed to the retriever callback to load the text content. This id is set as a 'data-expander' attribute by this method.
     */
    registerToggle(expanderElement, contentId) {
        expanderElement.setAttribute(this.#expanderAttribute, contentId + '');
    }
    destroy() {
        this.#aborter.abort();
        this.#aborter = null;
        this.#grid = null;
        this.#retriever = null;
        this.#presenter = null;
        this.#open = null;
    }
    /**
     * Registers a click event handler on the whole grid but then checks for each fired event if the click target was an expander toggle element.
     * Clicking anywhere in the grid will close a currently open collapsable. Clicking an expander toggle will open the corresponding collapsable area.
     */
    #startListening() {
        this.#aborter = new AbortController();
        this.#presenter.addObserverTo(this.#grid, "click", (event) => {
            const target = /** @type {HTMLElement} */ (event.target);
            if(this.#open) {
                const expander = this.#open.expander;
                this.#closeCollapsable();
                if(expander === target) {
                    return;     // this was already open, so just close it and be done.
                }
            }
            if(target.hasAttribute(this.#expanderAttribute)) {
                const id = target.getAttribute(this.#expanderAttribute);
                this.#createAndOpenCollapsable(target, id);
            }
        }, {signal: this.#aborter.signal});
    }
    /**
     * Creates a new collapsable area, inserts it as a sibling into the grid after the expander toggle element with {@link GridExpandable#offset} elements in-between, and fills it with the data returned by the {@link GridExpandable#retriever} callback.
     * @param {Element} expander - The toggle element that was clicked. Has to be a grid item.
     * @param {(number|string)} contentId - The content id that was extracted from the expander toggle's data-expander attribute.
     */
    #createAndOpenCollapsable(expander, contentId) {
        const collapsable = this.#presenter.createElement("div");
        collapsable.className = "collapsable";
        collapsable.setAttribute(this.#collapsableAttribute, contentId + '');
        const content = this.#retriever(contentId);
        if(typeof content === "string") {
            collapsable.innerText = content;
        } else {
            collapsable.append(content);
        }

        let skip = this.#offset;
        let afterThat = expander;
        while(skip-- > 0) {
            afterThat = afterThat.nextElementSibling;
        }
        afterThat.after(collapsable);
        expander.classList.add("expanded");
        this.#open = {
            expander: expander,
            collapse: collapsable
        }
    }
    #closeCollapsable() {
        if(this.#open) {
            this.#open.collapse.remove();
            this.#open.expander.classList.remove("expanded");
            this.#open = null;
        }
    }
    get #expanderAttribute() {
        return "data-expander";
    }
    get #collapsableAttribute() {
        return "data-collapse";
    }
}

/**
 * Base class for all section presenters. Each HTML section has one corresponding presenter assigned that takes care of rendering and user interactions.
 * @interface
 */
class AbstractSectionPresenter {
    /**
     * Called to make this presenter's section HTML element visible. The implementation should set the hidden property of the section to false.
     * @abstract
     * @param {...any} args - Optional arguments to be passed to the presenter when it become visible. Can be used to pass along data like e.g. a {@link Character} that should be rendered by this presenter.
     */
    show(...args) {
        throw new Error(`Method not implemented yet by class ${this.constructor.name}`);
    }
    /**
     * Called to hide this presenter's section HTML element. The implementation should set the hidden property of the section to true. Can also do further cleanup.
     * @abstract
     */
    hide() {
        throw new Error(`Method not implemented yet by class ${this.constructor.name}`);
    }
    /**
     * When the user uses the browser's back button or clicks any UI button that triggers {@link BackManager#leaveSection}, the BackManager will ask the current presenter if it is ok to leave the displayed section by calling this close method.
     * The presenter can either agree with the user's decision by returning true from this method. This will pop the latest history stack and automatically trigger a call to {@link AbstractSectionPresenter#hide}.
     * Or the presenter can prevent the user from leaving now by returning false from this method. This should be done, for example, if the user still has to finish an ongoing process or make a choice. The presenter may open a confirmation
     * dialog in such cases to ask the user for further feedback. If the presenter decides later that the user may leave its section now (e.g. when the user clicked Ok in the confirmation dialog), it can call the given leave() callback.
     * Note: a presenter must never call {@link BackManager#leaveSection} from within this method but use the provided leave callback instead.
     * @abstract
     * @param {LeaverFunction} leave - A leave function given by the {@link BackManager#stageChanged}. If the presenter returned false from this method, it can later call this leave function to tell the BackManager that is wants to exit its section now without getting another call to close.
     * @returns {boolean} - Return true if the {@link BackManager} is allowed to leave the currents section immediately. Return false if this presenter wants to prevent leaving its section now. It may then call the given leave() function later to signal the {@link BackManager} when it wants to exit. In both cases when the section is left, the BackManager will automatically call this presenter's {@link AbstractSectionPresenter#hide}.
     */
    close(leave) {
        return true;    // default allows immediate leaving of this presenter/section.
    }
    // static get sectionName(): each Presenter should implement such as static getter to return a unique name for it.
}

/**
 * Presenter for the character creation section. Uses {@link CharacterCreator} to build a new {@link Character} when done.
 * The user may leave this section without finishing creation, in which case all changes will be discarded. If the user completes the creation process, the newly created
 * character is stored in local {@link Persistence} and the presenters gives control back to the {@link OverviewPresenter}.
 * @extends {AbstractSectionPresenter}
 */
class CreationPresenter extends AbstractSectionPresenter {
    #presenter;
    #viewPresenter;
    #editPresenter;
    /** @type {HTMLElement} The HTML section element controlled by this presenter */
    #section;
    /** @type {DynamicNavbarScroller} - The navbar for this section */
    #navbar;
    /** @type {{input:HTMLInputElement, previousOk:boolean}} - The input field for entering the new character's name and a flag if the input value was non empty before */
    #name;
    /** @type {HTMLSelectElement} - The dropdown box for selecting the new character's race */
    #race;
    /** @type {HTMLElement} - Container element for rendering the character's racial abilities */
    #racialAbilities;
    /** @type {HTMLSelectElement} - The dropdown box for selecting the new character's discipline */
    #discipline;
    /** @type {HTMLElement} - HTML element for displaying the remaining attribute buying points as innerText */
    #attributePoints;
    /** @type {HTMLElement[]} - Array indexed by {@link AttributeId} of container elements, one for each attribute. Contains value and button children. */
    #attributes;
    /** @type {HTMLElement} - Container element for rendering the new character's characteristics */
    #characteristics;
    /** @type {{root:HTMLElement, core:HTMLElement, options:HTMLElement, versatility:HTMLElement, aborter:?AbortController}} - Object of container elements. Root is the top-level container for all talents, the other properties are sub-elements for the more specific parts. */
    #talents;
    /** @type {{root:HTMLElement, artisan:HTMLElement, knowledge:HTMLElement, general:HTMLElement, aborter:?AbortController}} - Object of container elements. Root is the top-level container for all skills, the other properties are sub-elements for more specific parts. */
    #skills;
    /** @type {{root:HTMLElement, aborter:?AbortController}} - Root is the page for spells, aborter an optional abort controller for cleaning up event listeners */
    #spells;
    /** @type {{button:HTMLButtonElement, leaving:boolean}} - Button is the finishing button to complete character creation. Leaving is a flag to signal that the user has finished the character creation and that leaving this section is possible without further confirmation. */
    #finish;
    /** @type {CharacterCreator} - The creator instance that handles the creation rules */
    #creator;
    /** @type {GenerationConfig} - The configuration values used for the character creator */
    #generator;
    /** @type {OverviewPresenter} - The overview presenter from which the user nagivated to this section. Used to get access to the character storage. */
    #overview;
    /**
     * @param {Presenter} presenter - Window presenter
     * @param {CharacterViewPresenter} viewPresenter - Presenter that provides reusable rendering functions to display certain game information like talent, skill or spell details.
     * @param {CharacterEditPresenter} editPresenter - Presenter that provides reusable rendering function for adding new talents, skills or spells to a character.
     */
    constructor(presenter, viewPresenter, editPresenter) {
        super();
        this.#creator = null;
        this.#overview = null;
        this.#generator = GenerationConfig.create(1);
        this.#presenter = presenter;
        this.#viewPresenter = viewPresenter;
        this.#editPresenter = editPresenter;
        this.#section = presenter.getElement("#creation");
        this.#navbar = this.#getAndSetupNavbar(presenter, this.#section);
        this.#name = {input: /** @type {HTMLInputElement} */ (presenter.getElement("#creation-character-name", this.#section)), previousOk: false};
        this.#race = this.#getAndFillRaceSelector(presenter, this.#section);
        this.#racialAbilities = presenter.getElement("#creation-racial-abilities", this.#section);
        this.#discipline = this.#getDisciplineSelector(presenter, this.#section);
        this.#attributePoints = presenter.getElement("#creation-attribute-points .points");
        this.#attributes = this.#getAttributes(presenter, this.#section);
        this.#characteristics = presenter.getElement("#creation-calculated-values", this.#section);
        this.#talents = this.#getAndSetupTalents(presenter, this.#section);
        this.#skills = this.#getAndSetupSkills(presenter, this.#section);
        this.#spells = { root: presenter.getElement(".page[data-page='5']", this.#section), aborter: null };
        this.#finish = this.#getAndSetupFinish(presenter, this.#section);

        this.#presenter.addObserverTo(this.#presenter.getElement(".header button.cancel", this.#section), "click", this.#presenter.getHistory().leaveSection);
        this.#presenter.addObserverTo(this.#name.input, "input", (event) => {
            const ok = this.#name.input.value.trim().length !== 0;
            if(this.#name.previousOk != ok) {
                this.#name.previousOk = ok;
                this.#updateFinishButton();
            }
        });
    }
    static get sectionName() {
        return "creation";
    }
    /**
     * @override
     */
    hide() {
        this.#section.hidden = true;
        this.#creator = null;
        this.#overview = null;
        this.#name.input.value = "";
        this.#name.previousOk = false;
        this.#race.selectedIndex = 0;
    }
    /**
     * @override
     * @param {OverviewPresenter} overview 
     */
    show(overview) {
        this.#overview = overview;
        this.#section.hidden = false;
        this.#navbar.reset();
        this.#selectRace();
    }
    /**
     * @override
     * @param {() => void} leave
     * @returns {boolean}
     */
    close(leave) {
        if(this.#finish.leaving) {
            this.#finish.leaving = false;
            return true;
        }
        this.#presenter.getModal().openConfirmation("Discard Character?", "Do you want to go back and discard your new character?", "Stay", "Discard").then(() => leave(), () => {});
        return false;
    }
    #canComplete() {
        const info = this.#creator.canComplete();
        if(!this.#name.previousOk) {
            info.ok = false;
            info.issues.push("Character name is missing");
        }
        return info;
    }
    #complete() {
        const completeInfo = this.#canComplete();
        if(!completeInfo.ok) {
            const modal = this.#presenter.getModal();
            modal.addTitle("Incomplete Character");
            let content = this.#presenter.createElement("p");
            content.textContent = "Please resolve the following issues before finishing character creation:";
            modal.addContent(content);
            content = this.#presenter.createElement("ul");
            completeInfo.issues.forEach(text => {
                const issue = this.#presenter.createElement("li");
                issue.textContent = text;
                content.append(issue);
            });
            modal.addContent(content);
            content = this.#presenter.createElement("button");
            content.textContent = "Back";
            this.#presenter.addObserverTo(content, "click", () => modal.close(), {once: true});
            modal.addContent(content);
            modal.open();
            return;
        }
        const character = this.#creator.finalizeCharacter(crypto.randomUUID(), this.#name.input.value);
        this.#finish.button.disabled = true;
        this.#overview.insertNewCharacter(character).then(() => {
            this.#finish.button.disabled = false;
            this.#finish.leaving = true;
            this.#presenter.getHistory().leaveSection();
        }, error => {
            this.#finish.button.disabled = false;
            this.#presenter.getNotifications().postError(error.message);
        });
    }
    #updateFinishButton() {
        this.#presenter.getElement("span:nth-child(2)", this.#finish.button).hidden = this.#canComplete().ok;   // hide the strike-through overlay
    }
    /**
     * @listens Event
     */
    #selectRace() {
        const raceId = Number(this.#race.selectedOptions[0].value);
        if(!this.#creator || this.#creator.getRace().getId() !== raceId) {
            this.#creator = new CharacterCreator(Earthdawn.data.races[raceId], this.#generator);
            this.#renderRacialAbilities();
            this.#fillDisciplines();    // selects and assigns a discipline. Has to happen before we render characteristics
            this.#renderAttributes();
            this.#renderCharacteristics();
            this.#renderSkills();
        }
    }
    /**
     * @listens Event
     */
    #selectDiscipline() {
        const diciplineId = Number(this.#discipline.selectedOptions[0].value);
        if(this.#creator.setDiscipline(Earthdawn.data.disciplines[diciplineId])) {
            this.#renderTalents();
            this.#updateFinishButton();
        }
    }
    /**
     * @listens Event
     * @param {Event} event 
     * @param {AttributeId} attributeId 
     */
    #changeAttribute(event, attributeId) {
        event.stopPropagation();
        let element =  /** @type {Element} */ (event.target);
        while(element.tagName.toUpperCase() !== "BUTTON") {
            element = element.parentElement;
        }
        switch(element.className) {
            case "plus":
                this.#creator.increaseAttribute(attributeId);
                break;
            case "minus":
                this.#creator.decreaseAttribute(attributeId);
                break;
        }
        this.#renderAttributes();
        this.#renderCharacteristics();
        this.#renderTalents();
        this.#renderSkills();
        this.#renderFreeSpellPoints();
        this.#updateFinishButton();
    }
    #renderRacialAbilities() {      // depends on race
        const entries = [];
        this.#creator.getRace().getAbilities().forEach(ability => {
            let term = this.#presenter.createElement("dt");
            term.textContent = ability.getName();
            entries.push(term);
            let definition = this.#presenter.createElement("dd");
            definition.textContent = ability.getDescription();
            entries.push(definition);
        });
        this.#racialAbilities.replaceChildren(...entries); 
    }
    #renderAttributes() {       // depends on race and attributes
        const remainingBuyingPoints = this.#creator.getAvailableAttributePoints();
        this.#attributePoints.textContent = remainingBuyingPoints + '';
        const attributeDetails = this.#creator.getAttributeInfo();
        this.#attributes.forEach((row, id) => {
            const details = attributeDetails[id];
            this.#presenter.getElement(".value", row).textContent = details.value + '';
            this.#presenter.getElement(".step", row).textContent = details.step + '';
            this.#presenter.getElement(".dice", row).textContent = details.dice + '';

            const minusPossible = details.minus !== "min";
            const minusButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.minus", row));
            minusButton.disabled = !minusPossible;
            this.#presenter.getElement(".cost", minusButton).innerHTML = minusPossible ? ("+" + -details.minus) : "&nbsp;";   // show number of buying points that will be refunded (positive) by clicking the button

            const plusPossible = details.plus !== "max" && details.plus <= remainingBuyingPoints;
            const plusButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.plus", row));
            plusButton.disabled = !plusPossible;
            this.#presenter.getElement(".cost", plusButton).innerHTML = plusPossible ? (-details.plus + '') : "&nbsp;";      // show number of buying points that will be spent (negative) by clicking the button
        });
    }
    #renderCharacteristics() {  // depends on race, discipline, attribute (and talent values but not during character creation)
        const info = this.#creator.getCharacteristics();
        Object.keys(info).forEach(block => {
            Object.entries(info[block]).forEach(([type, value]) => {
                this.#presenter.getElement(`.${block} [data-type=${type}]`, this.#characteristics).textContent = value;
            });
        });
    }
    /**
     * @param {TalentCCInfo} talentInfo - Details of a learned talent
     * @param {HTMLElement} template - Row template element with children to be cloned.
     * @param {HTMLElement} table - Container element for adding rows
     */
    #renderTalentRow(talentInfo, template, table) {
        const karmaCss = {
            required: "fa-circle-dot",
            optional: "fa-circle-check",
            no: "fa-circle"
        };
        const row = /** @type {HTMLElement} */ (template.cloneNode(true));
        const attribute = talentInfo.attributeName ? talentInfo.attributeName.substring(0, 3) : "-";
        const name = this.#presenter.getElement(".name", row);
        name.textContent = talentInfo.name;
        this.#presenter.addObserverTo(name, "click", () => this.#viewPresenter.showTalentDetails(talentInfo.talent));
        this.#presenter.getElement(".rank", row).textContent = talentInfo.rank + '';
        this.#presenter.getElement(".attribute", row).textContent = attribute;
        this.#presenter.getElement(".step", row).textContent = '' + (talentInfo.step ?? "-");
        this.#presenter.getElement(".dice", row).textContent = talentInfo.dice ?? "-";
        this.#presenter.getElement(".action > span", row).classList.add(talentInfo.action ? "fa-circle-dot" : "fa-circle");
        this.#presenter.getElement(".karma > span", row).classList.add(karmaCss[talentInfo.karma]);
        this.#presenter.getElement(".strain", row).textContent = talentInfo.strain + '';

        ["minus", "plus"].forEach(action => {
            const button = /** @type {HTMLButtonElement} */ (this.#presenter.getElement(`button.${action}`, row));
            const callback = talentInfo[action];
            if(callback) {
                this.#presenter.addObserverTo(button, "click", () => {
                    callback();
                    this.#renderTalents();
                    this.#updateFinishButton();
                    if(talentInfo.name.startsWith("Thread Weaving")) {
                        this.#updateSpellSection();
                    }
                }, {once: true});
            } else {
                button.disabled = true;
            }
        });
        table.append(...row.children);
    }
    #renderTalents() {      // depends on race, discipline and attributes
        // render remaining free ranks
        const availableRanks = this.#creator.getAvailableTalentRanks();
        this.#presenter.getElement("#creation-talent-points .points", this.#talents.root).textContent = availableRanks + '';
        // render all talents
        const template = this.#presenter.getElement("#creation-talent-row-template", this.#talents.root);
        const coreTable = this.#presenter.getElement(".table-body", this.#talents.core);
        coreTable.replaceChildren();    // empty table
        const optionsTable = this.#presenter.getElement(".table-body", this.#talents.options);
        optionsTable.replaceChildren();    // empty table
        const versatilityTable = this.#presenter.getElement(".table-body", this.#talents.versatility);
        versatilityTable.replaceChildren();    // empty table
        this.#creator.getAllTalentInfo().forEach(talentInfo => {
            switch(talentInfo.source) {
                case TalentSource.CORE:
                case TalentSource.ABILITY:
                    this.#renderTalentRow(talentInfo, template, coreTable);
                    break;
                case TalentSource.OPTION:
                    this.#renderTalentRow(talentInfo, template, optionsTable);
                    break;
                case TalentSource.VERSATILITY:
                    this.#renderTalentRow(talentInfo, template, versatilityTable);
                    break;
            }
        });
        // toggle Versatility section
        const versatilityInfo = this.#creator.canLearnForeignTalents();
        this.#talents.versatility.hidden = !versatilityInfo.hasVersatility;
        // toggle buttons and add event handlers
        if(this.#talents.aborter) {
            this.#talents.aborter.abort();  // remove all previous event handlers
        }
        this.#talents.aborter = new AbortController();
        const addCoreButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.add", this.#talents.core));
        addCoreButton.disabled = true;
        if(availableRanks > 0) {
            const availableCoreTalents = this.#creator.getAvailableCoreTalents();
            if(availableCoreTalents.length > 0) {
                addCoreButton.disabled = false;
                this.#presenter.addObserverTo(addCoreButton, "click", () => this.#editPresenter.showNewTalents(availableCoreTalents, "Add Core Talent", TalentSource.CORE, (talentData, source) => this.#addNewTalent(talentData, source)), {signal: this.#talents.aborter.signal});
            }
        }
        if(versatilityInfo.isJourneyman) {
            this.#talents.options.hidden = true;
        } else {
            this.#talents.options.hidden = false;
            const addTalentOptions = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.add", this.#talents.options));
            addTalentOptions.disabled = true;
            if(availableRanks > 0) {
                const availableTalentOptions = this.#creator.getAvailableTalentOptions();
                if(availableTalentOptions.length > 0) {
                    addTalentOptions.disabled = false;
                    this.#presenter.addObserverTo(addTalentOptions, "click", () => this.#editPresenter.showNewTalents(availableTalentOptions, "Add Talent Option", TalentSource.OPTION, (talentData, source) => this.#addNewTalent(talentData, source)), {signal: this.#talents.aborter.signal});
                }
            }
        }
        if(versatilityInfo.hasVersatility) {
            const addForeignTalent = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.add", this.#talents.versatility));
            addForeignTalent.disabled = true;
            if(availableRanks > 0 && versatilityInfo.canLearnMore) {
                const availableVersatilityOptions = this.#creator.getAvailableVersatilityOptions().map(info => info.data);
                addForeignTalent.disabled = false;
                this.#presenter.addObserverTo(addForeignTalent, "click", () => this.#editPresenter.showNewTalents(availableVersatilityOptions, "Add Foreign Talent", TalentSource.VERSATILITY, (talentData, source) => this.#addNewTalent(talentData, source)), {signal: this.#talents.aborter.signal});
            }
            // Note: we do not allow a human to learn spells for free via versatility during character creation. Else we would have to check and toggle the spell page here.
        }
        if(this.#creator.canHaveSpells()) {
            this.#showSpellPage();
            this.#renderSpells();
        } else {
            this.#hideSpellPage();
        }
    }
    /** 
     * @param {TalentData} talentData - Game data of the new talent to add
     * @param {TalentSource} source - How this talent was acquired
     */
    #addNewTalent(talentData, source) {
        this.#creator.addNewTalentWithOneRank(talentData, source);
        this.#renderTalents();
        this.#updateFinishButton();
        if(talentData.getName().startsWith("Thread Weaving")) {
            this.#updateSpellSection();
        }
    }
    /**
     * @param {SkillCCInfo} skillInfo - Details of a learned skill
     * @param {HTMLElement} template - Row template element with children to be cloned.
     * @param {HTMLElement} table - Container element for adding rows
     */
    #renderSkillRow(skillInfo, template, table) {
        const row = /** @type {HTMLElement} */ (template.cloneNode(true));
        const name = this.#presenter.getElement(".name", row);
        name.textContent = skillInfo.name;
        this.#presenter.addObserverTo(name, "click", () => this.#viewPresenter.showSkillDetails(skillInfo.skill));
        this.#presenter.getElement(".rank", row).textContent = skillInfo.rank + '';
        this.#presenter.getElement(".attribute", row).textContent = skillInfo.attributeName.substring(0, 3);
        this.#presenter.getElement(".step", row).textContent = skillInfo.step + '';
        this.#presenter.getElement(".dice", row).textContent = skillInfo.dice;
        this.#presenter.getElement(".action > span", row).classList.add(skillInfo.action ? "fa-circle-dot" : "fa-circle");
        this.#presenter.getElement(".strain", row).textContent = skillInfo.strain + '';

        ["minus", "plus"].forEach(action => {
            const button = /** @type {HTMLButtonElement} */ (this.#presenter.getElement(`button.${action}`, row));
            const callback = skillInfo[action];
            if(callback) {
                this.#presenter.addObserverTo(button, "click", () => {
                    callback();
                    this.#renderSkills();
                    this.#updateFinishButton();
                }, {once: true});
            } else {
                button.disabled = true;
            }
        });
        table.append(...row.children);
    }
    #renderSkills() {       // depends on attributes
        // render remaining free ranks
        const availableRanks = this.#creator.getAvailableSkillRanks();
        this.#presenter.getElement("#creation-general-skill-points .points", this.#skills.root).textContent = availableRanks.general + '';  // may be negative if attributes have changed
        this.#presenter.getElement("#creation-artisan-skill-points .points", this.#skills.artisan).textContent = availableRanks.artisan + '';
        this.#presenter.getElement("#creation-knowledge-skill-points .points", this.#skills.knowledge).textContent = availableRanks.knowledge + '';
        // render all skills
        const template = this.#presenter.getElement("#creation-skill-row-template", this.#skills.root);
        const outputTables = [];
        outputTables[SkillType.ARTISAN] = this.#presenter.getElement(".table-body", this.#skills.artisan);
        outputTables[SkillType.ARTISAN].replaceChildren();    // empty table
        outputTables[SkillType.KNOWLEDGE] = this.#presenter.getElement(".table-body", this.#skills.knowledge);
        outputTables[SkillType.KNOWLEDGE].replaceChildren();    // empty table
        outputTables[SkillType.GENERAL] = this.#presenter.getElement(".table-body", this.#skills.general);
        outputTables[SkillType.GENERAL].replaceChildren();    // empty table
        outputTables[SkillType.LANGUAGE] = outputTables[SkillType.GENERAL];
        this.#creator.getAllSkillInfo().forEach(skillInfo => {
            this.#renderSkillRow(skillInfo, template, outputTables[skillInfo.type]);
        });
        // toggle buttons and add event handlers
        if(this.#skills.aborter) {
            this.#skills.aborter.abort();  // remove all previous event handlers
        }
        this.#skills.aborter = new AbortController();
        ["Artisan", "Knowledge", "General"].forEach(typeName => {
            const addSkillButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.add", this.#skills[typeName.toLowerCase()]));
            addSkillButton.disabled = true;
            if(availableRanks[typeName.toLowerCase()] > 0 || availableRanks.general > 0) {
                const availableSkills = this.#creator.getAvailableSkills(SkillType[typeName.toUpperCase()]);
                if(availableSkills.length > 0) {
                    addSkillButton.disabled = false;
                    this.#presenter.addObserverTo(addSkillButton, "click", () => { 
                        this.#editPresenter.showNewSkills(availableSkills, (skillData) => this.#addNewSkill(skillData), `Add ${typeName} Skill`);
                    }, {signal: this.#skills.aborter.signal});
                }
            }
        });
    }
    /**
     * @param {SkillData} skillData - Game data of a new skill to add
     */
    #addNewSkill(skillData) {
        this.#creator.addNewSkillWithOneRank(skillData);
        this.#renderSkills();
        this.#updateFinishButton();
    }
    #updateSpellSection() {     // should be called every time the talents have changed (because Thread Weaving could have been added/removed)
        if(this.#creator.canHaveSpells()) {
            this.#showSpellPage();
            this.#renderSpells();
        } else {
            this.#hideSpellPage();
        }
    }
    #renderFreeSpellPoints() {      // depends on discipline and attributes
        const availablePoints = this.#creator.getAvailableSpellCircles();
        this.#presenter.getElement("#creation-spell-points .points", this.#spells.root).textContent = availablePoints + '';
        /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.add", this.#spells.root)).disabled = availablePoints <= 0;
        return availablePoints;
    }
    #renderSpells() {   // depends on discipline and attributes
        if(!this.#creator.canHaveSpells()) {
            return;
        }
        const remainingBuyingPoints = this.#renderFreeSpellPoints();
        // render all spells
        const template = this.#presenter.getElement("#creation-spell-row-template", this.#spells.root);
        const tableBody =  this.#presenter.getElement(".spells .table-body", this.#spells.root);
        tableBody.replaceChildren();
        this.#creator.getAllSpells().forEach(spell => {
            const row = /** @type {HTMLElement} */ (template.cloneNode(true));
            const name = this.#presenter.getElement(".name", row);
            name.textContent = spell.getData().getName();
            this.#presenter.addObserverTo(name, "click", () => {
                this.#viewPresenter.showSpellDetails(spell);
            });
            this.#presenter.getElement(".circle", row).textContent = spell.getData().getCircle() + '';
            this.#presenter.getElement(".threads", row).textContent = spell.getData().getThreads() + '';
            this.#presenter.addObserverTo(this.#presenter.getElement("button.remove", row), "click", () => {
                this.#creator.removeSpell(spell);
                this.#renderSpells();
                this.#updateFinishButton();
                if(this.#creator.getAllSpells().length === 0) {     // last spell removed, update talents (ThreadWeaving can now be removed too)
                    this.#renderTalents();
                }
            }, {once: true});
            tableBody.append(...row.children);
        });
        if(this.#spells.aborter) {
            this.#spells.aborter.abort();  // remove all previous event handlers
        }
        this.#spells.aborter = new AbortController();
        const addButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.add", this.#spells.root));
        if(remainingBuyingPoints > 0) {     // assumes that there are more spells available than anyone could buy with free slots at character creation
            addButton.disabled = false;
            this.#presenter.addObserverTo(addButton, "click", () => this.#editPresenter.showNewSpells(this.#creator.getAvailableSpellsByCircle(remainingBuyingPoints), (spellData) => this.#addNewSpell(spellData)), {signal: this.#spells.aborter.signal});
        } else {
            addButton.disabled = true;
        }
    }
    /**
     * @param {SpellData} spellData - Game data of the new spell to add
     */
    #addNewSpell(spellData) {
        const hadSpellsBefore = this.#creator.getAllSpells().length > 0;
        this.#creator.addSpell(spellData);
        this.#renderSpells();
        this.#updateFinishButton();
        if(!hadSpellsBefore) {
            this.#renderTalents();  // ThreadWeaving must not be removed anymore now
        }
    }
    #showSpellPage() {
        this.#navbar.includePage(5);
    }
    #hideSpellPage() {
        this.#navbar.excludePage(5);
    }
    #fillDisciplines() {
        let disciplines = Earthdawn.data.disciplines.filter(disc => disc.isAvailableFor(this.#creator.getRace())).sort((a, b) => a.getName() < b.getName() ? -1 : 1);
        disciplines.forEach((disc, index) => {
            let option = this.#discipline.options.item(index);
            if(!option) {
                option = /** @type {HTMLOptionElement} */ (this.#presenter.createElement("option"));
                this.#discipline.add(option);
            }
            option.text = disc.getName();
            option.value = disc.getId() + '';
        });
        for(let i = this.#discipline.options.length - 1; i >= disciplines.length; --i) {
            this.#discipline.remove(i);
        }
        this.#discipline.selectedIndex = 0;
        this.#selectDiscipline();
    }
    /**
     * @param {Presenter} presenter - Window presenter
     * @param {HTMLElement} section - Root element of this section
     * @returns {HTMLSelectElement} - The dropdown for race selection
     */
    #getAndFillRaceSelector(presenter, section) {
        const select = /** @type {HTMLSelectElement} */ (presenter.getElement("#creation-race", section));
        Earthdawn.data.races.forEach(race => {
            let option = /** @type {HTMLOptionElement} */ (presenter.createElement("option"));
            option.text = race.getName();
            option.value = race.getId() + '';
            select.add(option);
        });
        presenter.addObserverTo(select, "change", this.#selectRace.bind(this));
        return select;
    }
    /**
     * @param {Presenter} presenter - Window presenter
     * @param {HTMLElement} section - Root element of this section
     * @returns {HTMLSelectElement} - The dropdown for discipline selection
     */
    #getDisciplineSelector(presenter, section) {
        const select = /** @type {HTMLSelectElement} */ (presenter.getElement("#creation-discipline", section));
        presenter.addObserverTo(select, "change", this.#selectDiscipline.bind(this));
        return select;
    }
    /**
     * @param {Presenter} presenter - Window presenter
     * @param {HTMLElement} section - Root element of this section
     * @returns {HTMLElement[]} - Array of row elements that represent attributes. Indexex by {@link AttributeId}.
     */
    #getAttributes(presenter, section) {
        let result = [];
        presenter.findElements("#creation-attributes div[data-attribute]", section).forEach(el => {
            let attributeName = /** @type {HTMLElement} */ (el).getAttribute("data-attribute").toUpperCase();
            let attributeId = AttributeIds[attributeName];
            result[attributeId] = el;
            presenter.findElements("button", /** @type {HTMLElement} */ (el)).forEach(button => presenter.addObserverTo(button, "click", event => this.#changeAttribute(event, attributeId)))
        });
        return result;
    }
    /**
     * Gets references to all container elements used for rendering talents.
     * @param {Presenter} presenter - Window presenter
     * @param {HTMLElement} section - This section's root element
     * @returns {{root:HTMLElement, core:HTMLElement, options:HTMLElement, versatility:HTMLElement, aborter:?AbortController}} - Object of container elements related to talents.
     */
    #getAndSetupTalents(presenter, section) {
        const root = presenter.getElement("#creation-talents", section);
        return {
            root: root, 
            core: presenter.getElement(".talents .core", root), 
            options: presenter.getElement(".talents .options", root), 
            versatility: presenter.getElement(".talents .versatility", root),
            aborter: null
        };
    }
    /**
     * Gets references to all container elements used for rendering skills.
     * @param {Presenter} presenter - Window presenter
     * @param {HTMLElement} section - This section's root element.
     * @returns {{root:HTMLElement, artisan:HTMLElement, knowledge:HTMLElement, general:HTMLElement, aborter:?AbortController}}- Object of container elements related to skills.
     */
    #getAndSetupSkills(presenter, section) {
        const root = presenter.getElement("#creation-skills", section);
        return {
            root: root,
            artisan: presenter.getElement(".skills .artisan", root), 
            knowledge: presenter.getElement(".skills .knowledge", root), 
            general: presenter.getElement(".skills .general", root),
            aborter: null
        };
    }
    /**
     * @param {Presenter} presenter - Window presenter
     * @param {HTMLElement} section - This section's root element 
     * @returns {DynamicNavbarScroller} - The navbar for this section
     */
    #getAndSetupNavbar(presenter, section) {
        const pageContainer = presenter.getElement(".page-container", section);
        const navbarContainer = presenter.getElement(".navbar-s", section);
        return new DynamicNavbarScroller(pageContainer, navbarContainer, presenter);
    }
    /**
     * @param {Presenter} presenter - Window presenter
     * @param {HTMLElement} section - This section's root element
     * @returns {{button:HTMLButtonElement, leaving:boolean}} - Reference to the finish button and flag if the creation was already finished and this section can be left immediately.
     */
    #getAndSetupFinish(presenter, section) {
        const finish = {
            leaving: false,
            button: /** @type {HTMLButtonElement} */ (presenter.getElement(".header button.finish", section))
        };
        presenter.addObserverTo(finish.button, "click", () => this.#complete());
        return finish;
    }
}

/**
 * Presenter for editing a single {@link Discipline} of a {@link Character}.
 * @extends AbstractSectionPresenter
 */
class DisciplineEditPresenter extends AbstractSectionPresenter {
    #presenter;
    #viewPresenter;
    #editPresenter;
    /** @type {HTMLElement} - Root element of the UI section controlled by this presenter */
    #section;
    /** @type {DynamicNavbarScroller} - Navigation component of this section */
    #navbar;
    /** @type {{value:HTMLElement, minus:HTMLButtonElement, plus:HTMLButtonElement}} - References to HTML elements that display the discipline's circle and allow to modify it */
    #circle;
    /** @type {Discipline} - The currently edited discipline */
    #discipline;
    /** @type {Character} - The character owning the edited discipline */
    #character;
    /** @type {CharacterRecord} - The underlying record of the character */
    #record;
    /**
     * @param {Presenter} presenter - Window presenter 
     * @param {CharacterViewPresenter} viewPresenter - Presenter that provides reusable rendering functions to display certain game information like talent, skill or spell details.
     * @param {CharacterEditPresenter} editPresenter - Presenter that provides reusable rendering function for adding new talents, skills or spells to a character.
     */
    constructor(presenter, viewPresenter, editPresenter) {
        super();
        this.#presenter = presenter;
        this.#viewPresenter = viewPresenter;
        this.#editPresenter = editPresenter;
        this.#section = presenter.getElement("#edit-discipline");
        this.#navbar = this.#getAndSetupNavbar(presenter, this.#section);
        this.#circle = this.#getAndSetupCircle(presenter, this.#section);
        this.#presenter.addObserverTo(this.#presenter.getElement(".header button.cancel", this.#section), "click", this.#presenter.getHistory().leaveSection);
    }
    static get sectionName() {
        return "edit-discipline";
    }
    /**
     * @override
     */
    hide() {
        this.#section.hidden = true;
        this.#record = null;
        this.#character = null;
        this.#discipline = null;
    }
    /**
     * @override
     * @param {Discipline} discipline - The discipline to open for editing
     * @param {CharacterRecord} characterRecord - The character record owning the discipline
     */
    show(discipline, characterRecord) {
        this.#discipline = discipline;
        this.#record = characterRecord;
        characterRecord.getCharacter().then(character => {
            this.#character = character;
            this.#renderStatusBar();
            this.#renderCircle();
            this.#renderTalents();
            this.#renderAbilities();
            this.#section.hidden = false;
            this.#navbar.reset();
        });
    }
    #renderStatusBar() {
        this.#presenter.getElement(".header .name [data-type=text]", this.#section).textContent = this.#discipline.getData().getName();
        this.#presenter.getElement(".header .legend-points [data-type=available]", this.#section).textContent = this.#character.getLegendPoints().getUnspent() + '';
    }
    #renderCircle() {
        this.#circle.value.textContent = this.#discipline.getCircle() + '';
        this.#circle.minus.disabled = this.#discipline.getPosition() === 1 && this.#discipline.getCircle() === 1;
        this.#circle.plus.disabled = this.#discipline.getCircle() === Earthdawn.rules.advancement.getMaximumCircle();
    }
    #renderTalents() {
        if(this.#discipline.isJourneyman()) {
            this.#renderJourneymanTalents();
            return;
        }
        const table = this.#presenter.getElement(".talents .edit .table-body", this.#section);
        table.replaceChildren();
        const circleHeaderTemplate = this.#presenter.getElement(".talents .circle-header-template", this.#section);
        
        const allLearnedTalents = this.#discipline.collectAllLearnedTalentsAcrossDisciplinesAndCountOwnUsedSlots(this.#character.getDisciplines(), TalentSource.CORE);
        for(let circle = 1; circle <= Earthdawn.rules.advancement.getMaximumCircle(); ++circle) {
            const circleHeader = /** @type {HTMLElement} */ (circleHeaderTemplate.cloneNode(true));
            circleHeader.textContent = `Circle ${circle}`;
            circleHeader.hidden = false;
            table.append(circleHeader);

            // render core talents (learned and open slots) for this circle
            const learnedCore = this.#discipline.getLearnedTalentsOf(circle).filter(talent => talent.getSource() === TalentSource.CORE || talent.getSource() === TalentSource.ABILITY);
            this.#discipline.getData().getCoreTalents(circle)
                .map(talentId => Earthdawn.data.talents[talentId]).sort(TalentData.compare)
                .forEach(talentData => {
                    const hasLearned = learnedCore.findIndex(talent => talent.getId() === talentData.getId());
                    if(hasLearned !== -1) {  // this talent was learned by this discipline as a normal core talent of this circle
                        const talent = learnedCore[hasLearned];
                        learnedCore.splice(hasLearned, 1);
                        this.#renderLearnedTalent(talent, table);
                        return;
                    }
                    // talent was not learned by this discipline on this circle, so check if the character already possesses it:
                    const foundElsewhere = allLearnedTalents.get(talentData.getId());
                    if(foundElsewhere && (foundElsewhere.discipline !== this.#discipline || (foundElsewhere.talent && foundElsewhere.talent.getSource() === TalentSource.ABILITY))) {    
                        if(foundElsewhere.talent && foundElsewhere.talent.getSource() === TalentSource.VERSATILITY) {       // This talent was learned by versatility. Allow to migrate it.
                            this.#renderLearnedByVersatility(foundElsewhere.talent, circle, table);
                            return;
                        }
                        if(foundElsewhere.discipline !== this.#discipline || (foundElsewhere.talent && foundElsewhere.talent.getSource() === TalentSource.ABILITY)) {       // this slot is already taken up by another discipline or by an ability talent from a lower circle of this discipline.
                            this.#renderTalentOwnedByOther(foundElsewhere.talent, foundElsewhere.discipline, table);
                            return;
                        }
                        // this is most likely a spell matrix that can be learned multiple times, so we treat it as an open slot (fall through).
                    }
                    // this is an open core slot. Nobody has learned this talent yet.
                    this.#renderOpenCoreSlot(talentData, circle, table);
            });
            learnedCore.filter(talent => !talent.is(KnownTalent.VERSATILITY)).sort(Talent.compare).forEach(talent => {     // if we have talents left that were acquired through an ability: add them here
                this.#renderLearnedTalent(talent, table);
            });
            // render talent options or open slots for this circle
            const separator = /** @type {HTMLElement} */ (this.#presenter.getElement(".talents .option-slot-separator", this.#section).cloneNode(true));
            separator.hidden = false;
            table.append(separator);
            const learnedOption = this.#discipline.getLearnedTalentsOf(circle).find(talent => talent.getSource() === TalentSource.OPTION);    // assumes there is only one talent option slot per circle
            if(learnedOption) {
                this.#renderLearnedTalent(learnedOption, table);
            } else {
                this.#renderOpenTalentOptionSlot(circle, table);
            }
        }
    }
    /**
     * The Journeyman discipline has a few core talents (like Karma Ritual and Thread Weaving) but all other slots are empty and free to fill (we count them as learned by Versatility). 
     * The discipline has some core talents for which we show slots, but it does not have talent options.
     */
    #renderJourneymanTalents() { 
        const table = this.#presenter.getElement(".talents .edit .table-body", this.#section);
        table.replaceChildren();
        const circleHeaderTemplate = this.#presenter.getElement(".talents .circle-header-template", this.#section);
        
        // A Journeyman can never learn another discipline
        const slotsByCircle = Earthdawn.rules.advancement.getTalentCountPerCircle();
        for(let circle = 1; circle <= Earthdawn.rules.advancement.getMaximumCircle(); ++circle) {
            const circleHeader = /** @type {HTMLElement} */ (circleHeaderTemplate.cloneNode(true));
            circleHeader.textContent = `Circle ${circle}`;
            circleHeader.hidden = false;
            table.append(circleHeader);
            
            const learned = this.#discipline.getLearnedTalentsOf(circle).concat().sort(Talent.compare);
            this.#discipline.getData().getCoreTalents(circle)
                .map(talentId => Earthdawn.data.talents[talentId]).sort(TalentData.compare)
                .forEach(talentData => {
                    slotsByCircle[circle] -= 1;
                    const hasLearned = learned.findIndex(talent => talent.getId() === talentData.getId());
                    if(hasLearned !== -1) {  // this talent was learned by this discipline as a normal core talent of this circle
                        const talent = learned[hasLearned];
                        learned.splice(hasLearned, 1);
                        this.#renderLearnedTalent(talent, table);
                        return;
                    }
                    this.#renderOpenCoreSlot(talentData, circle, table);
            });
            while(slotsByCircle[circle]-- > 0) {
                if(learned.length > 0) {
                    const talent = learned.shift();
                    this.#renderLearnedTalent(talent, table);
                } else {
                    this.#renderOpenJourneymanSlot(circle, table, slotsByCircle[circle] === 0);
                }
            }
        }
    }
    /**
     * @param {Talent} talent - The talent to render
     * @param {HTMLElement} table - The container element into which detail rows can be added
     */
    #renderLearnedTalent(talent, table) {
        const karmaCss = {
            required: "fa-circle-dot",
            optional: "fa-circle-check",
            no: "fa-circle"
        };
        const row = /** @type {HTMLElement} */ (this.#presenter.getElement(".talents .talent-row-template", this.#section).cloneNode(true));
        const nameContainer = this.#presenter.getElement(".name", row);
        if(talent.getSource() === TalentSource.ABILITY || talent.wasMigrated()) {    // show extra icons if this talent was either added through an ability (in this case it may be displayed out-of-order) or was migrated from another discipline (in this case decreasing its rank may yield unexpected LP refunds and it may be more expensive to re-increase it)
            const name = this.#presenter.createElement("span");
            name.textContent = talent.getData().getName();
            nameContainer.append(name);
            const icon = this.#presenter.createElement("span");
            icon.className = "icon fa-solid";
            icon.classList.add(talent.wasMigrated() ? "fa-shuffle" : "fa-seedling");
            nameContainer.append(icon);
        } else {
            nameContainer.textContent = talent.getData().getName();
        }
        this.#presenter.addObserverTo(nameContainer, "click", () => this.#viewPresenter.showTalentDetails(talent));
        this.#presenter.getElement(".rank", row).textContent = talent.getTotalRank() + '';
        const attributeName = talent.hasStep() ? talent.getAttribute().getName().substring(0, 3) : "-";
        this.#presenter.getElement(".attribute", row).textContent = attributeName;
        this.#presenter.getElement(".step", row).textContent = talent.hasStep() ? (talent.getStep() + '') : "-";
        this.#presenter.getElement(".dice", row).textContent = talent.hasStep() ? talent.getDice() : "-";
        this.#presenter.getElement(".free-ranks", row).textContent = talent.getFreeRanks() + '';
        this.#presenter.getElement(".action > span", row).classList.add(talent.getData().needsAction() ? "fa-circle-dot" : "fa-circle");
        this.#presenter.getElement(".karma > span", row).classList.add(karmaCss[talent.getKarmaRequirement()]);
        this.#presenter.getElement(".strain", row).textContent = talent.getData().getStrain() + '';
        
        const minusButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.minus", row));
        minusButton.disabled = true;
        if(talent.mayDecreaseTotalRank() === TalentRankChange.POSSIBLE) {
            minusButton.disabled = false;
            this.#presenter.addObserverTo(minusButton, "click", () => {
                try {
                    talent.decreaseRank();
                    this.#storeChange();
                    this.#renderTalents();
                    this.#renderStatusBar();
                } catch(error) {    // we do a full check to see if this talent can be really decreased or if we would violate discipline circle advancement rules
                    this.#presenter.getNotifications().postError(`The rank of ${talent.getData().getName()} cannot be decreased further:\n${error.message}`);
                    minusButton.disabled = true;
                }
            }, {once: true});
        }
        const plusButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.plus", row));
        plusButton.disabled = true;
        if(talent.canIncreaseRank()) {
            plusButton.disabled = false;
            this.#presenter.addObserverTo(plusButton, "click", () => {
                talent.increaseRank();
                this.#storeChange();
                this.#renderTalents();
                this.#renderStatusBar();
            }, {once: true});
        }
        table.append(...row.children);
        if(talent.getSource() !== TalentSource.OPTION) {
            const separator = this.#presenter.createElement("div");
            separator.className = "row-separator";
            table.append(separator);
        }
    }
    /**
     * @param {TalentData} talentData - Game data of the talent that can still be learned for this slot
     * @param {number} circle - The circle that contains the open talent slot
     * @param {HTMLElement} table - Container element that can house details rows.
     */
    #renderOpenCoreSlot(talentData, circle, table) {
        const row = /** @type {HTMLElement} */ (this.#presenter.getElement(".talents .open-core-row-template", this.#section).cloneNode(true));
        const name = this.#presenter.getElement(".name", row);
        name.textContent = talentData.getName();
        this.#presenter.addObserverTo(name, "click", () => this.#showCoreTalentDataDetails(talentData));
        const attributeName = talentData.hasStep() ? AttributeIds.getName(talentData.getAttributeId()).substring(0, 3) : "-";
        this.#presenter.getElement(".attribute", row).textContent = attributeName;
        this.#presenter.getElement(".action > span", row).classList.add(talentData.needsAction() ? "fa-circle-dot" : "fa-circle");
        this.#presenter.getElement(".strain", row).textContent = talentData.getStrain() + '';
        const addButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.add", row));
        addButton.disabled = true;
        if(this.#discipline.canAffordNewTalent(circle)) {
            addButton.disabled = false;
            this.#presenter.addObserverTo(addButton, "click", () => {
                this.#discipline.learnNewTalent(talentData, TalentSource.CORE, circle);
                this.#storeChange();
                this.#renderTalents();
                this.#renderStatusBar();
            }, {once: true});
        }
        table.append(...row.children);
    }
    /**
     * @param {number} circle - Circle that contains the open talent option slot
     * @param {HTMLElement} table - Container element that houses details rows
     */
    #renderOpenTalentOptionSlot(circle, table) {
        const row = /** @type {HTMLElement} */ (this.#presenter.getElement(".talents .open-option-row-template", this.#section).cloneNode(true));
        const button = this.#presenter.getElement("button.add", row);
        if(this.#discipline.canAffordNewTalent(circle)) {
            this.#presenter.addObserverTo(button, "click", () => {
                const availableOptions = this.#discipline.getAvailableTalentOptions(circle).flat();
                this.#editPresenter.showNewTalents(availableOptions, "Add Talent Option", TalentSource.OPTION, (talentData, source) => {
                    this.#discipline.learnNewTalent(talentData, source, circle);
                    this.#storeChange();
                    this.#renderTalents();
                    this.#renderStatusBar();
                });
            });
        } else {
            button.classList.add('obfuscated');
            this.#presenter.addObserverTo(button, "click", () => {
                const availableOptions = this.#discipline.getAvailableTalentOptions(circle).flat();
                this.#editPresenter.showNewTalents(availableOptions, "Talent Options", TalentSource.OPTION);
            });
        }
        table.append(...row.children);
    }
    /**
     * @param {number} circle - Circle that contains the open talent slot
     * @param {HTMLElement} table - Container element that houses details rows 
     * @param {boolean} [skipRowSeparator=false] - Flag to signal if a row separator element should be rendered (false) or not (true). 
     */
    #renderOpenJourneymanSlot(circle, table, skipRowSeparator = false) {
        const row = /** @type {HTMLElement} */ (this.#presenter.getElement(".talents .open-option-row-template", this.#section).cloneNode(true));
        const button = this.#presenter.getElement("button.add", row);
        if(this.#discipline.canAffordNewTalent(circle)) {
            this.#presenter.addObserverTo(button, "click", () => {
                const availableOptions = this.#discipline.getAvailableVersatilityOptions(true, circle).map(info => info.data);
                this.#editPresenter.showNewTalents(availableOptions, "Add Foreign Talent", TalentSource.VERSATILITY, (talentData, source) => {
                    this.#discipline.learnNewTalent(talentData, source, circle);
                    this.#storeChange();
                    this.#renderTalents();
                    this.#renderStatusBar();
                });
            });
        } else {
            button.classList.add('obfuscated');
            this.#presenter.addObserverTo(button, "click", () => {
                const availableOptions = this.#discipline.getAvailableVersatilityOptions(false, circle).map(info => info.data);
                this.#editPresenter.showNewTalents(availableOptions, "Foreign Talents", TalentSource.VERSATILITY);
            });
        }
        table.append(...row.children);
        if(!skipRowSeparator) {
            const separator = this.#presenter.createElement("div");
            separator.className = "row-separator";
            table.append(separator);
        }
    }
    /**
     * The same talent may be learned by multiple disciplines. When rendering a core slot for a specific talent but that talent was acquired through a different discipline, we only show a reference in this slot but do not allow editing it.
     * @param {Talent} talent - A talent learned via a different discipline that blocks a core slot in this discipline because it is the same talent.
     * @param {Discipline} owner - The other discipline for which the talent was learned
     * @param {HTMLElement} table - Container element that houses details rows 
     */
    #renderTalentOwnedByOther(talent, owner, table) {
        const row = /** @type {HTMLElement} */ (this.#presenter.getElement(".talents .other-discipline-row-template", this.#section).cloneNode(true));
        const name = this.#presenter.getElement(".name", row);
        name.textContent = talent.getData().getName();
        this.#presenter.addObserverTo(name, "click", () => this.#viewPresenter.showTalentDetails(talent));
        this.#presenter.getElement(".rank", row).textContent = talent.getTotalRank() + '';
        this.#presenter.getElement(".discipline", row).textContent = `Learned by ${owner.getData().getName()}`;
        table.append(...row.children);
    }
    /**
     * @param {Talent} talent - Talent learned by versatility
     * @param {number} circle - The circle the talent was learned on
     * @param {HTMLElement} table - Container element that houses details rows
     */
    #renderLearnedByVersatility(talent, circle, table) {
        const row = /** @type {HTMLElement} */ (this.#presenter.getElement(".talents .versatility-taken-row-template", this.#section).cloneNode(true));
        const karmaCss = {
            required: "fa-circle-dot",
            optional: "fa-circle-check",
            no: "fa-circle"
        };
        const name = this.#presenter.getElement(".name", row);
        name.textContent = talent.getData().getName();
        this.#presenter.addObserverTo(name, "click", () => this.#viewPresenter.showTalentDetails(talent));
        this.#presenter.getElement(".rank", row).textContent = talent.getTotalRank() + '';
        const attributeName = talent.hasStep() ? talent.getAttribute().getName().substring(0, 3) : "-";
        this.#presenter.getElement(".attribute", row).textContent = attributeName;
        this.#presenter.getElement(".step", row).textContent = talent.hasStep() ? (talent.getStep() + '') : "-";
        this.#presenter.getElement(".dice", row).textContent = talent.hasStep() ? talent.getDice() : "-";
        this.#presenter.getElement(".free-ranks", row).textContent = talent.getFreeRanks() + '';
        this.#presenter.getElement(".action > span", row).classList.add(talent.getData().needsAction() ? "fa-circle-dot" : "fa-circle");
        this.#presenter.getElement(".karma > span", row).classList.add(karmaCss[talent.getKarmaRequirement()]);
        this.#presenter.getElement(".strain", row).textContent = talent.getData().getStrain() + '';
        const adoptButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.adopt", row));
        adoptButton.disabled = true;
        if(talent.canAdoptForeignTalentTo(this.#discipline, circle)) {
            adoptButton.disabled = false;
            this.#presenter.addObserverTo(adoptButton, "click", () => this.#askForTalentAdoptionAsCoreTalent(talent, circle));
        }
        table.append(...row.children);
    }
    /**
     * When a talent was previously learned via Versatility, but later the character learns a new discipline that makes this talent available as a core talent, the adept may adopt the existing talent into 
     * the new discipline by paying the difference in legend point costs. This frees up the used Versatility rank and may provide additional benefits (like free Karma use for core talents).
     * @param {Talent} talent - The existing talent that should be adopted by this discipline
     * @param {number} circle - The new circle on which the talent should be added to the new discipline
     */
    #askForTalentAdoptionAsCoreTalent(talent, circle) {
        const modal = this.#presenter.getModal();
        const cost = talent.calculateCostToAdoptForeignTalentTo(this.#discipline, circle);
        const costText = cost < 0 ? `refund ${-cost} ` : `cost ${cost} `;
        const content = `The talent ${talent.getData().getName()} was acquired as a Versatility option. You can now adopt it into this discipline as a normal core talent. This will free up the Versatility slot. The costs to increase this talent may be different afterwards.\n\nThis action will ${costText} Legend Points. Do you want to adopt this talent?`;
        modal.openConfirmation("Adopt Talent into Discipline?", content, "Cancel", "Adopt").then(() => {
            talent.adoptTo(this.#discipline, circle, TalentSource.CORE);
            this.#storeChange();
            this.#renderTalents();
            this.#renderStatusBar();
        }, () => {});
    }
    /**
     * @param {TalentData} talentData - Game data of a talent
     */
    #showCoreTalentDataDetails(talentData) {
        const modal = this.#presenter.getModal();
        modal.addTitle(talentData.getName());
        modal.addCssClass("talent-data");
        const content = /** @type {HTMLElement} */ (this.#presenter.getElement("#talent-data-details").children[0].cloneNode(true));
        this.#presenter.getElement("span[data-type=attribute]", content).textContent = talentData.hasStep() ? AttributeIds.getName(talentData.getAttributeId()) : 'n/a';
        this.#presenter.getElement("span[data-type=action]", content).textContent = talentData.needsAction() ? "yes" : "no";
        this.#presenter.getElement("span[data-type=karma]", content).textContent = talentData.hasStep() ? "optional" : "no";
        this.#presenter.getElement("span[data-type=strain]", content).textContent = talentData.getStrain() + '';
        this.#presenter.getElement(".description", content).textContent = talentData.getDescription();
        modal.addContent(content);
        modal.open();
    }
    #renderAbilities() {
        const container = this.#presenter.getElement(".abilities dl", this.#section);
        container.replaceChildren();
        const ownedAbilities = this.#discipline.getAbilitiesByCircle();
        const cache = {};
        const buildNodes = /** @type {(ability:AbilityData, owned:boolean) => void} */ (ability, owned) => {
            const css = owned ? '' : 'masked';
            let term = this.#presenter.createElement("dt");
            term.textContent = ability.getBaseData().getName();
            term.className = css;
            container.append(term);
            let definition = this.#presenter.createElement("dd");
            definition.textContent = ability.getBaseData().getDescription();
            definition.className = css;
            if(owned && ability.requiresChoice()) {
                this.#renderAbilitySelection(/** @type {Ability} */ (ability), ownedAbilities, definition, cache);
            }
            container.append(definition);
        };
        for(let circle = 1; circle <= Earthdawn.rules.advancement.getMaximumCircle(); ++circle) {
            const owned = circle <= this.#discipline.getCircle();
            const byCircle = owned ? ownedAbilities.get(circle) : this.#discipline.getData().getAllAbilities().get(circle);
            if(byCircle) {
                const subHeading = this.#presenter.createElement("div");
                subHeading.className = "sub-heading";
                subHeading.textContent = `Circle ${circle}`;
                container.append(subHeading);
                byCircle.forEach(abilityData => buildNodes(abilityData, owned));
            }
        }
    }
    /**
     * Some abilities are dynamic and provide several different options from which the user has to select one. This method renders the selection component for such an ability.
     * @param {Ability} ability - The ability that requires a choice by the user.
     * @param {Map.<number,Ability[]>} ownedAbilities - All abilities that the character already owns in this discipline indexed by circle.
     * @param {HTMLElement} parent - Parent container element where the selection show be rendered
     * @param {object} cache - A simple object map to temporarily cache information that might be reused
     */
    #renderAbilitySelection(ability, ownedAbilities, parent, cache) {
        let options = null;
        switch(ability.getBaseData().getType()) {
            case AbilityType.KARMA_USE:
                if(!cache.karmaUse) {
                    cache.karmaUse = Earthdawn.data.abilities.filter(data => data.getType() === AbilityType.KARMA_USE && data.getAffectedTraitId() >= AttributeIds.DEXTERITY && data.getAffectedTraitId() <= AttributeIds.CHARISMA);
                    cache.karmaUse.sort((a, b) => a.getName().localeCompare(b.getName()));
                }
                options = cache.karmaUse.concat();
                ownedAbilities.forEach(byCircle => {
                    byCircle.forEach(other => {
                        if(other !== ability && other.getType() === AbilityType.KARMA_USE && other.getAffectedTraitId() >= AttributeIds.DEXTERITY && other.getAffectedTraitId() <= AttributeIds.CHARISMA) {
                            options = options.filter(karmaUse => karmaUse.getAffectedTraitId() !== other.getAffectedTraitId());
                        }                        
                    });
                });
                break;
            case AbilityType.DEFENSE:
                if(!cache.defense) {
                    cache.defense = Earthdawn.data.abilities.filter(data => data.getType() === AbilityType.DEFENSE && data.getAffectedTraitId() > 0 && data.getBonusValue() === 1);
                }
                options = cache.defense.concat();
                const owned = new Map();
                ownedAbilities.forEach(byCircle => {
                    byCircle.forEach(other => {
                        if(other !== ability && other.getType() === AbilityType.DEFENSE && other.requiresChoice() && other.wasSelected()) {
                            owned.set(other.getAffectedTraitId(), (owned.get(other.getAffectedTraitId()) ?? 0) + 1);
                        }
                    });
                });
                options = options.filter(defense => (owned.get(defense.getAffectedTraitId()) ?? 0) < 4);
                break;
            case AbilityType.JOURNEYMAN_SELECTION:
                if(!cache.characteristics) {
                    cache.characteristics = Earthdawn.data.abilities.filter(data => (data.getType() === AbilityType.DURABILITY || data.getType() === AbilityType.INITIATIVE || data.getType() === AbilityType.RECOVERY_TESTS) && data.getBonusValue() === 1);
                    cache.characteristics.sort((a, b) => a.getName().localeCompare(b.getName()));
                }
                options = cache.characteristics.concat();
                ownedAbilities.forEach(byCircle => {
                    byCircle.some(other => {
                        if(other !== ability && other.getType() === AbilityType.DURABILITY && other.requiresChoice()) {
                            options = options.filter(characteristic => characteristic.getType() !== AbilityType.DURABILITY);
                            return true;
                        }
                        return false;
                    });
                });
                break;
            default:
                throw new Error(`Unsupported ability type for selection: ${ability.getBaseData().getType()}`);
        }
        const select = /** @type {HTMLSelectElement} */ (this.#presenter.createElement('select'));
        const choose = /** @type {HTMLOptionElement} */ (this.#presenter.createElement('option'));
        choose.text = "<choose an option>";
        choose.value = '0';
        choose.selected = !ability.wasSelected();
        select.add(choose);
        options.forEach(abilityData => {
            const option = /** @type {HTMLOptionElement} */ (this.#presenter.createElement('option'));
            option.text = abilityData.getName();
            option.value = abilityData.getId().toString();
            option.selected = ability.wasSelected() && ability.getId() === abilityData.getId();
            select.add(option);
        });
        parent.append(select);
        if(ability.wasSelected()) {
            const desc = this.#presenter.createElement('div');
            desc.className = 'option';
            desc.textContent = ability.getDescription();
            parent.append(desc);
        }
        this.#presenter.addObserverTo(select, 'change', event => {
            const selected = select.selectedOptions.length === 0 ? 0 : parseInt(select.selectedOptions.item(0).value, 10);
            let updated = null;
            if(selected === 0 && ability.wasSelected()) {
                updated = new Ability(ability.getBaseData());
            } else if(selected > 0 && ability.getId() !== selected) {
                updated = new Ability(Earthdawn.data.abilities[ability.getBaseData().getId()], Earthdawn.data.abilities[selected]);
            }
            if(updated) {
                this.#discipline.updateAbility(ability, updated);
                this.#storeChange();
                this.#renderAbilities();
            }            
        });
    }
    #storeChange() {
        this.#record.update(this.#character);
        return this.#presenter.getPersistence().updateCharacter(this.#record);
    }
    /**
     * @param {Presenter} presenter 
     * @param {HTMLElement} section 
     * @returns {DynamicNavbarScroller}
     */
    #getAndSetupNavbar(presenter, section) {
        const pageContainer = presenter.getElement(".page-container", section);
        const navbarContainer = presenter.getElement(".navbar-s", section);
        return new DynamicNavbarScroller(pageContainer, navbarContainer, presenter);
    }
    /**
     * @param {Presenter} presenter 
     * @param {HTMLElement} section 
     * @returns {{value:HTMLElement, minus:HTMLButtonElement, plus:HTMLButtonElement}} - References to UI elements for displaying the discipline circle and two buttons for modifying it.
     */
    #getAndSetupCircle(presenter, section) {
        const minusButton = /** @type {HTMLButtonElement} */ (presenter.getElement(".circle button.minus", section));
        presenter.addObserverTo(minusButton, "click", () => {
            if(!this.#discipline.canForgetCircle()) {
                this.#presenter.getNotifications().postWarning("Cannot decrease circle of this discipline!\nEnsure that you do not possess any talents from the current circle and have not used any attribute improvements gained by it.");
                return;
            }
            if(this.#discipline.getCircle() === 1) {    // only possible for disciplines after the first (see #renderCircle)
                this.#presenter.getModal().openConfirmation("Delete Discipline?", "Do you really want to unlearn this discipline and remove it from your character? All its remaining talents will get removed and the Legend Points will be refunded.", "Cancel", "Delete").then(() => {
                    this.#discipline.forgetCircle();
                    this.#storeChange().then(() => this.#presenter.getHistory().leaveSection());
                }, () => {});
                return;
            }
            this.#discipline.forgetCircle();
            this.#storeChange();
            this.#renderCircle();
            this.#renderTalents();
            this.#renderAbilities();
        });
        const plusButton = /** @type {HTMLButtonElement} */ (presenter.getElement(".circle button.plus", section));
        presenter.addObserverTo(plusButton, "click", () => {
            const newCircle = this.#discipline.getCircle() + 1;
            if(!this.#discipline.canAdvanceToCircle(newCircle)) {
                const countTalents = Earthdawn.rules.advancement.getMinimumNumberOfTalentsToAdvanceTo(newCircle);
                const minRank = Earthdawn.rules.advancement.getMinimumTalentRankToAdvanceTo(newCircle);
                this.#presenter.getNotifications().postWarning(`Cannot advance to next circle!\nIn order to do so you need at least ${countTalents} talents from this discipline at rank ${minRank} or higher. One of these talents has to be from the current circle.`);
                return;
            }
            this.#discipline.advanceToNextCircle();
            this.#storeChange();
            this.#renderCircle();
            this.#renderTalents();
            this.#renderAbilities();
        });
        return {value: presenter.getElement(".circle .value", section), minus: minusButton, plus: plusButton};
    }
}

/**
 * A section presenter for editing a {@link Character}. The editing of the character's disciplines is delegated further to the {@link DisciplineEditPresenter}.
 * @extends AbstractSectionPresenter
 */
class CharacterEditPresenter extends AbstractSectionPresenter {
    #presenter;
    #viewPresenter;
    /** @type {HTMLElement} - Root element of this presenter's HTML section */
    #section;
    /** @type {DynamicNavbarScroller} - Navigation component of this section */
    #navbar;
    /** @type {HTMLInputElement} - Input field for editing the character's name */
    #name;
    /** @type {HTMLInputElement} - Input field for editing the character's unspent legend point amount */
    #legendPoints;
    /** @type {{button:HTMLButtonElement, aborter?:AbortController}} - Button for adding a new skill and abort controller for cleaning up the event listeners */
    #skills;
    /** @type {{button:HTMLButtonElement, aborter?:AbortController}} - Button for adding a new spell and abort controller for cleaning up the event listeners */
    #spells;         
    /** @type {{button:HTMLButtonElement, aborter?:AbortController, has:boolean}} - Button for adding a new talent via Versatility, abort controller for cleaning up the event listeners, and a flag that signals if this character owns the Versatility talent */
    #versatility;
    /**
     * @typedef {object} EquipmentElements
     * @property {object} armor
     * @property {HTMLButtonElement} armor.add
     * @property {HTMLButtonElement} armor.remove
     * @property {?AbortController} armor.aborter
     * @property {object} melee
     * @property {?AbortController} melee.aborter
     * @property {object} throwing
     * @property {?AbortController} throwing.aborter
     * @property {object} missile
     * @property {?AbortController} missile.aborter
     */
    /** @type {EquipmentElements} - Buttons and abort controller (for cleaning up event listeners) related to various equipment  */
    #equipment;
    /** @type {Character} - The edited character */
    #character
    /** @type {CharacterRecord} - The db record of the edited character */
    #record;
    /**
     * @param {Presenter} presenter - Window presenter 
     * @param {CharacterViewPresenter} viewPresenter - Presenter that provides reusable rendering functions to display certain game information like talent, skill or spell details. 
     */
    constructor(presenter, viewPresenter) {
        super();
        this.#presenter = presenter;
        this.#viewPresenter = viewPresenter;
        this.#section = presenter.getElement("#edit-character");
        this.#navbar = this.#getAndSetupNavbar(presenter, this.#section);
        this.#name = this.#setupName(presenter, this.#section);
        this.#legendPoints = this.#setupLegendPoints(presenter, this.#section);
        this.#skills = this.#setupSkills(presenter, this.#section);
        this.#spells = this.#setupSpells(presenter, this.#section);
        this.#versatility = this.#setupVersatility(presenter, this.#section);
        this.#equipment = this.#setupEquipment(presenter, this.#section);

        this.#presenter.addObserverTo(this.#presenter.getElement(".header button.cancel", this.#section), "click", this.#presenter.getHistory().leaveSection);
        this.#presenter.addObserverTo(this.#presenter.getElement(".basics button.add", this.#section), "click", () => {
            this.#presenter.getNotifications().postInfo("Multi-discipline support is coming soon. Stay tuned!");
        });
    }
    static get sectionName() {
        return "edit-character";
    }
    /**
     * @override
     */
    hide() {
        this.#section.hidden = true;
        this.#record = null;
        this.#character = null;
        this.#versatility.has = false;
    }
    /**
     * @override
     * @param {CharacterRecord} characterRecord - Record of the character to edit
     */
    show(characterRecord) {
        this.#record = characterRecord;
        characterRecord.getCharacter().then(character => {
            this.#character = character;
            this.#adjustNavBar();
            this.#renderStatusBar();
            this.#renderPages();
            this.#renderCharacteristics();
            this.#section.hidden = false;
            this.#navbar.reset();
        });
    }
    #adjustNavBar() {
        this.#versatility.has = this.#character.getDisciplines().some(discipline => {
            return !discipline.isJourneyman() && discipline.getLearnedTalentsOf(1).some(talent => talent.is(KnownTalent.VERSATILITY));
        });
        if(this.#versatility.has) {
            this.#navbar.includePage(4);
        } else {
            this.#navbar.excludePage(4);
        }
        if(this.#canHaveSpells()) {
            this.#navbar.includePage(5);
        } else {
            this.#navbar.excludePage(5);
        }
    }
    #canHaveSpells() {
        for(const threadWeaving of this.#character.getAccessToSpells().values()) {
            if(threadWeaving != null && threadWeaving.getTotalRank() > 0) {
                return true;
            }
        }
        return false;
    }
    #renderStatusBar() {
        this.#presenter.getElement(".header .name [data-type=text]", this.#section).textContent = this.#character.getName();
        this.#presenter.getElement(".header .legend-points [data-type=available]", this.#section).textContent = this.#character.getLegendPoints().getUnspent() + '';
    }
    #renderPages() {
        this.#renderBasics();
        this.#renderAttributes();
        this.#renderSkills();
        this.#renderSpells();
        this.#renderVersatility();
        this.#renderEquipment(); 
    }
    #renderBasics() {
        const basics = this.#presenter.getElement(".basics", this.#section);
        this.#name.value = this.#character.getName();
        this.#presenter.getElement(".who [data-type=race]", basics).textContent = this.#character.getRace().getName();
        this.#legendPoints.value = this.#character.getLegendPoints().getUnspent() + '';
        this.#presenter.getElement(".legend-points [data-type=total]", basics).textContent = this.#character.getLegendPoints().getTotal() + '';
        
        const disciplineContainer = this.#presenter.getElement("div[data-type=disciplines] .content", basics);
        disciplineContainer.replaceChildren();
        this.#character.getDisciplines().forEach(discipline => {
            const row = /** @type {HTMLElement} */ (this.#presenter.getElement("div[data-type=disciplines] .row-template", basics).cloneNode(true));
            this.#presenter.getElement(".label", row).textContent = discipline.getData().getName();
            this.#presenter.getElement(".value", row).textContent = discipline.getCircle() + '';
            this.#presenter.addObserverTo(this.#presenter.getElement(".edit", row), "click", () => {
                this.#presenter.enterSection(DisciplineEditPresenter.sectionName, discipline, this.#record);
            }, {once: true});
            disciplineContainer.append(...row.children);
        });
    }
    #renderAttributes() {
        const here = this.#presenter.getElement(".characteristics .attributes", this.#section);
        const possibleImprovements = this.#character.getAvailableAttributeImprovements();
        this.#presenter.getElement(".attribute-improvements .points", here).textContent = possibleImprovements + '';

        const template = this.#presenter.getElement(".attribute-template", here);
        const table = this.#presenter.getElement(".table .content", here);
        table.replaceChildren();
        this.#character.getAttributes().forEach(attribute => {
            const row = /** @type {HTMLElement} */ (template.cloneNode(true));
            this.#presenter.getElement(".label", row).textContent = attribute.getName();
            this.#presenter.getElement(".value", row).textContent = attribute.getInitialValue() + '';
            this.#presenter.getElement(".improvements", row).textContent = attribute.getImprovements() + '';
            this.#presenter.getElement(".total", row).textContent = attribute.getTotalValue() + '';
            this.#presenter.getElement(".step", row).textContent = attribute.getStep() + '';
            this.#presenter.getElement(".dice", row).textContent = attribute.getDice();

            const canImprove = attribute.canImprove() && possibleImprovements > 0 && attribute.getCostOfNextImprovement() <= this.#character.getLegendPoints().getUnspent();
            const plusButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.plus", row));
            plusButton.disabled = !canImprove;
            if(canImprove) {
                this.#presenter.addObserverTo(plusButton, "click", () => {
                    this.#character.improveAttribute(attribute);
                    this.#storeChange();
                    this.#renderStatusBar();
                    this.#renderPages();
                    this.#renderCharacteristics();
                }, {once: true});
            }
            const minusButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.minus", row));
            minusButton.disabled = !attribute.canRemoveImprovement();
            if(attribute.canRemoveImprovement()) {
                this.#presenter.addObserverTo(minusButton, "click", () => {
                    this.#character.degradeAttribute(attribute);
                    this.#storeChange();
                    this.#renderStatusBar();
                    this.#renderPages();
                    this.#renderCharacteristics();
                }, {once: true});
            }
            table.append(...row.children);
        });
    }
    #renderCharacteristics() {
        const info = {
            defenses: {
                physical: this.#character.getDefense(DefenseType.PHYSICAL).getTotal(),
                spell: this.#character.getDefense(DefenseType.SPELL).getTotal(),
                social: this.#character.getDefense(DefenseType.SOCIAL).getTotal()
            },
            armor: {
                physical: this.#character.getArmorRating().getPhysical(),
                mystic: this.#character.getArmorRating().getMystic()
            },
            initiative: {
                step: this.#character.getInitiative().getStep(),
                dice: this.#character.getInitiative().getDice()
            },
            health: {
                unconsciousness: this.#character.getHealth().getUnconsciousnessRating(),
                death: this.#character.getHealth().getDeathRating(),
                woundThreshold: this.#character.getHealth().getWoundThreshold(),
                recoveryDays: this.#character.getHealth().getDaysForRecoveryTestFresh() === 1 ? "day" : `${this.#character.getHealth().getDaysForRecoveryTestFresh()} days`,
                recoveryCount: this.#character.getHealth().getRecoveryTestsPerRefreshPeriod(),
                recoveryStep: this.#character.getHealth().getRecoveryStep(),
                recoveryDice: this.#character.getHealth().getRecoveryDice()
            },
            karma: {
                maximum: this.#character.getKarma().getPossibleMaximum()
            },
            movement: {
                combat: this.#character.getMovement().map(move => `${move.getCombat()} (${move.getType()})`).join(" / "),
                full: this.#character.getMovement().map(move => `${move.getFull()} (${move.getType()})`).join(" / ")
            },
            encumberance: {
                carry: this.#character.getEncumberance().getCarry(),
                lift: this.#character.getEncumberance().getLift()
            }
        };
        const here = this.#presenter.getElement(".characteristics .calculated-values", this.#section);
        Object.keys(info).forEach(block => {
            Object.entries(info[block]).forEach(([type, value]) => {
                this.#presenter.getElement(`.${block} [data-type=${type}]`, here).textContent = value;
            });
        });
    }
    #renderSkills() {
        const template = this.#presenter.getElement(".skills .row-template", this.#section);
        const table = this.#presenter.getElement(".skills .edit .table-body", this.#section);
        table.replaceChildren();
        this.#character.getSkills().sort(Skill.compare).forEach(skill => {
            const row = /** @type {HTMLElement} */ (template.cloneNode(true));
            const name = this.#presenter.getElement(".name", row);
            name.textContent = skill.getData().getName();
            this.#presenter.addObserverTo(name, "click", () => this.#viewPresenter.showSkillDetails(skill));
            this.#presenter.getElement(".rank", row).textContent = skill.getTotalRank() + '';
            this.#presenter.getElement(".attribute", row).textContent = skill.getAttribute().getName().substring(0, 3);
            this.#presenter.getElement(".step", row).textContent = skill.getStep() + '';
            this.#presenter.getElement(".dice", row).textContent = skill.getDice();
            this.#presenter.getElement(".action > span", row).classList.add(skill.getData().needsAction() ? "fa-circle-dot" : "fa-circle");
            this.#presenter.getElement(".strain", row).textContent = skill.getData().getStrain() + '';
            if(skill.getFreeRanks() > 0) {
                this.#presenter.getElement(".free-ranks", row).textContent = skill.getFreeRanks() + '';
            }

            const canDecrease = skill.canDecreaseRank();
            const minusButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.minus", row));
            minusButton.disabled = !canDecrease;
            if(canDecrease) {
                this.#presenter.addObserverTo(minusButton, "click", () => {
                    skill.decreaseRank();
                    this.#storeChange();
                    this.#renderStatusBar();
                    this.#renderPages();
                }, {once: true});
            }
            const canIncrease = skill.canIncreaseRank();
            const plusButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.plus", row));
            plusButton.disabled = !canIncrease;
            if(canIncrease) {
                this.#presenter.addObserverTo(plusButton, "click", () => {
                    skill.increaseRank();
                    this.#storeChange();
                    this.#renderStatusBar();
                    this.#renderPages();
                }, {once: true});
            }
            table.append(...row.children);
        });
        
        if(this.#skills.aborter) {
            this.#skills.aborter.abort();   // remove previous event handler
        }
        this.#skills.aborter = new AbortController();
        const addButton = this.#skills.button;
        addButton.disabled = true;
        if(!this.#character.canAffordNewSkill()) {
            return;
        }
        const availableSkills = this.#character.getAvailableSkills();
        if(availableSkills.length > 0) {
            addButton.disabled = false;
            this.#presenter.addObserverTo(addButton, "click", () => this.showNewSkills(availableSkills, (skillData) => this.#addNewSkill(skillData)), {signal: this.#skills.aborter.signal});
        }
    }
    /**
     * @param {SkillData} skillData 
     */
    #addNewSkill(skillData) {
        this.#character.learnNewSkill(skillData);
        this.#storeChange();
        this.#renderStatusBar();
        this.#renderPages();
    }
    /**
     * Renders a selection of new skills that the character may learn in a modal dialog.
     * @param {SkillData[]} newSkills - All skills from which the character can choose
     * @param {(skillData:SkillData) => void} addCallback - A callback function to call in order to learn the given new skill
     * @param {string} [dialogTitle="Add Skill"] - Title of the modal dialog 
     */
    showNewSkills(newSkills, addCallback, dialogTitle = "Add Skill") {      // reuses CSS and templates from talents. Is reused by CreationPresenter.
        const modal = this.#presenter.getModal();
        modal.addCssClass("new-talents");
        modal.addTitle(dialogTitle);
        const table = /** @type {HTMLElement} */ (this.#presenter.getElement("#new-talents-dialog").cloneNode(true)).children[0];
        const expander = new GridExpandable(this.#presenter, (skillId) => {
            return Earthdawn.data.skills[skillId].getDescription();
        }, table, 3);
        const tableBody = this.#presenter.getElement(".table-body", table);
        const bodyChildren = [];
        newSkills.sort(SkillData.compare).forEach(skillData => {
            let row = /** @type {HTMLElement} */ (tableBody.cloneNode(true));
            const name = this.#presenter.getElement(".name", row);
            name.textContent = skillData.getName();
            expander.registerToggle(name, skillData.getId());
            this.#presenter.addObserverTo(this.#presenter.getElement(".add", row), "click", event => {
                expander.destroy();
                modal.close();
                addCallback(skillData);
            }, {once: true});
            this.#presenter.getElement(".attribute", row).textContent = AttributeIds.getName(skillData.getAttributeId()).substring(0, 3);
            this.#presenter.getElement(".action > span", row).classList.add(skillData.needsAction() ? "fa-circle-dot" : "fa-circle");
            this.#presenter.getElement(".strain", row).textContent = skillData.getStrain() + '';
            bodyChildren.push(...row.children);
        });
        tableBody.replaceChildren(...bodyChildren);
        modal.addContent(table);
        modal.open();
    }
    /**
     * Opens a modal dialog to select a new talent to be added to the discipline. Can be used also in read-only mode to only show the possibilities but without actually allowing to add any talents. 
     * @param {TalentData[]} newTalents - All talents from which the character can choose
     * @param {string} dialogTitle - Title of the modal dialog
     * @param {TalentSource} source - How the character can learn the new talent
     * @param {?(talentData:TalentData,source:TalentSource) => void} [addCallback=undefined] - Callback function that can be called to learn the given talent. May be undefined for read-only mode.
     */
    showNewTalents(newTalents, dialogTitle, source, addCallback = undefined) {      
        const modal = this.#presenter.getModal();
        modal.addCssClass("new-talents");
        modal.addTitle(dialogTitle);
        const content = /** @type {HTMLElement} */ (this.#presenter.getElement("#new-talents-dialog").cloneNode(true)).children[0];
        const expander = new GridExpandable(this.#presenter, (talentId) => {
            return Earthdawn.data.talents[talentId].getDescription();
        }, content, 3);
        const tableBody = this.#presenter.getElement(".table-body", content);
        const bodyChildren = [];
        newTalents.sort(TalentData.compare).forEach(talentData => {
            let row = /** @type {HTMLElement} */ (tableBody.cloneNode(true));
            const name = this.#presenter.getElement(".name", row);
            name.textContent = talentData.getName();
            expander.registerToggle(name, talentData.getId());
            const addButton = this.#presenter.getElement(".add", row);
            if(addCallback) {
                this.#presenter.addObserverTo(addButton, "click", event => { 
                    expander.destroy();
                    modal.close();
                    addCallback(talentData, source);
                }, {once: true});
            } else {
                addButton.classList.add('invisible');
            }
            this.#presenter.getElement(".attribute", row).textContent = talentData.hasStep() ? AttributeIds.getName(talentData.getAttributeId()).substring(0, 3) : "-";
            this.#presenter.getElement(".action > span", row).classList.add(talentData.needsAction() ? "fa-circle-dot" : "fa-circle");
            this.#presenter.getElement(".strain", row).textContent = talentData.getStrain() + '';
            bodyChildren.push(...row.children);
        });
        tableBody.replaceChildren(...bodyChildren);
        modal.addContent(content);
        modal.open();
    }
    #renderSpells() {
        if(!this.#canHaveSpells()) {
            return;
        }
        const freeSpellCircles = this.#character.getFreeSpellCircles();
        this.#presenter.getElement(".spells .edit .free-spell-points .points", this.#section).textContent = freeSpellCircles + '';
        const template = this.#presenter.getElement(".spells .row-template", this.#section);
        const tableBody =  this.#presenter.getElement(".spells .edit .table-body", this.#section);
        tableBody.replaceChildren();
        this.#character.getSpells().forEach(spell => {
            const row = /** @type {HTMLElement} */ (template.cloneNode(true));
            const name = this.#presenter.getElement(".name", row);
            name.textContent = spell.getData().getName();
            this.#presenter.addObserverTo(name, "click", () => {
                this.#viewPresenter.showSpellDetails(spell);
            });
            this.#presenter.getElement(".circle", row).textContent = spell.getData().getCircle() + '';
            this.#presenter.getElement(".threads", row).textContent = spell.getData().getThreads() + '';
            switch(spell.acquiredForFree()) {       // show icon if spell was somehow acquired for free
                case FreeSpell.NO:
                    this.#presenter.getElement(".free", row).hidden = true;
                    // fall through
                case FreeSpell.ADVANCEMENT:
                    this.#presenter.getElement(".creation", row).hidden = true;
                    break;
                case FreeSpell.CREATION:
                    this.#presenter.getElement(".free", row).hidden = true;
                    break;
            }
            const removeButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.remove", row));
            if(spell.acquiredForFree() === FreeSpell.CREATION) {
                removeButton.disabled = true;
            } else {
                this.#presenter.addObserverTo(removeButton, "click", () => {
                    spell.unlearn();
                    this.#storeChange();
                    this.#renderStatusBar();
                    this.#renderPages();
                }, {once: true});
            }
            tableBody.append(...row.children);
        });
        if(this.#spells.aborter) {
            this.#spells.aborter.abort();  // remove all previous event handlers
        }
        this.#spells.aborter = new AbortController();
        const addButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement(".spells .heading button.add", this.#section));
        addButton.disabled = true;
        let maxSpellCircleCanAffordWithLp = 0;
        for(let circle = 1; circle <= Earthdawn.rules.advancement.getMaximumCircle(); ++circle) {
            if(Earthdawn.rules.advancement.getSpellLegendPointCost(circle) > this.#character.getLegendPoints().getUnspent()) {
                maxSpellCircleCanAffordWithLp = circle - 1;
                break;
            }
        }
        const availableSpells = this.#character.getAvailableSpells(Math.max(maxSpellCircleCanAffordWithLp, freeSpellCircles));
        if(availableSpells.some(byCircle => byCircle.length > 0)) {
            addButton.disabled = false;
            this.#presenter.addObserverTo(addButton, "click", () => this.showNewSpells(availableSpells, (spellData) => this.#addNewSpell(spellData)), {signal: this.#spells.aborter.signal});
        }
    }
    /**
     * Shows a selection of new spells in a modal dialog that the character may learn.
     * @param {SpellData[][]} availableSpells - Array indexed by circle of array of game data for spells. The selection from which the character can choose to learn a new spell.
     * @param {(spellData:SpellData) => void} addCallback - Callback function that can be called to learn the given spell
     */
    showNewSpells(availableSpells, addCallback) {
        const modal = this.#presenter.getModal();
        modal.addTitle("Add Spell");
        modal.addCssClass("new-spells");
        const content = this.#presenter.createElement("div");
        availableSpells.forEach((byCircle, circle) => {
            if(byCircle.length > 0) {
                const block = /** @type {HTMLElement} */ (this.#presenter.getElement("#new-spells-dialog .circle-block").cloneNode(true));
                this.#presenter.getElement(".heading", block).textContent = `Circle ${circle}`;
                const tableBody = this.#presenter.getElement(".table-body", block);
                const expander = new GridExpandable(this.#presenter, (id) => {
                    return Earthdawn.data.spells[id].getDescription();
                }, this.#presenter.getElement(".table", block), 4);
                const bodyChildren = [];
                byCircle.sort(SpellData.compare).forEach(spellData => {
                    const row = /** @type {HTMLElement} */ (tableBody.cloneNode(true));
                    const name = this.#presenter.getElement(".name", row);
                    name.textContent = spellData.getName();
                    expander.registerToggle(name, spellData.getId());
                    this.#presenter.addObserverTo(this.#presenter.getElement(".add", row), "click", event => {
                        expander.destroy();
                        modal.close();
                        addCallback(spellData);
                    }, {once: true});
                    this.#presenter.getElement(".casting-diff", row).textContent = spellData.getCastingDifficulty();
                    this.#presenter.getElement(".threads", row).textContent = spellData.getThreads() + '';
                    this.#presenter.getElement(".weaving-diff", row).textContent = '' + (spellData.getWeavingDifficulty() ?? 'n/a');
                    this.#presenter.getElement(".reattune-diff", row).textContent = spellData.getReattunementDifficulty() + '';
                    bodyChildren.push(...row.children);
                });
                tableBody.replaceChildren(...bodyChildren);
                content.append(block);
            }
        });
        modal.addContent(content);
        modal.open();
    }
    /**
     * @param {SpellData} spellData 
     */
    #addNewSpell(spellData) {
        this.#character.learnNewSpell(spellData);
        this.#storeChange();
        this.#renderStatusBar();
        this.#renderPages();
    }
    #renderVersatility() {
        if(!this.#versatility.has) {
            return;
        }
        const table = this.#presenter.getElement(".versatility .edit .table-body", this.#section);
        table.replaceChildren();

        const versatility = this.#character.findTalent(KnownTalent.VERSATILITY.getId(), 1);
        this.#renderLearnedTalent(versatility, table);
        const separator = table.lastElementChild;
        separator.textContent = "Foreign Talents";
        separator.className = "header-separator";

        const firstDiscipline = this.#character.getDisciplines()[0];    // versatility and all foreign talents are always owned by the first discipline
        firstDiscipline.getLearnedTalents().flat().filter(talent => talent.getSource() === TalentSource.VERSATILITY).sort((a, b) => {
                let result = a.getBoughtOnCircle() === b.getBoughtOnCircle() ? 0 : (a.getBoughtOnCircle() > b.getBoughtOnCircle() ? 1 : -1);
                if(result === 0) {
                    result = Talent.compare(a, b);
                }
                return result;
            }).forEach(talent => {
                this.#renderLearnedTalent(talent, table);
        });

        if(this.#versatility.aborter) {
            this.#versatility.aborter.abort();   // remove previous event handler
        }
        this.#versatility.aborter = new AbortController();
        const addButton = this.#versatility.button;
        addButton.disabled = true;
        const availableOptions = firstDiscipline.getAvailableVersatilityOptions(true);
        if(availableOptions.length > 0) {
            addButton.disabled = false;
            this.#presenter.addObserverTo(addButton, "click", () => this.showNewTalents(availableOptions.map(info => info.data), "Add Foreign Talent", TalentSource.VERSATILITY, (talentData, source) => {
                let circle = 0;
                availableOptions.some(talentInfo => {
                    if(talentInfo.data === talentData) {
                        circle = talentInfo.circle;
                        return true;
                    }
                    return false;
                });
                firstDiscipline.learnNewTalent(talentData, source, circle);
                this.#storeChange();
                this.#renderStatusBar();
                this.#renderPages();
            }), {signal: this.#skills.aborter.signal});
        }
    }
    /**
     * Renders details about a learned talent. Used only for rendering versatility and foreign talents here. Core talents and talent options are normally handled by the {@link DisciplineEditPresenter}.
     * @param {Talent} talent - The learned talent
     * @param {HTMLElement} table - A container element that houses details rows
     */
    #renderLearnedTalent(talent, table) {
        const karmaCss = {
            required: "fa-circle-dot",
            optional: "fa-circle-check",
            no: "fa-circle"
        };
        const row = /** @type {HTMLElement} */ (this.#presenter.getElement(".versatility .talent-row-template", this.#section).cloneNode(true));
        this.#presenter.getElement(".circle", row).textContent = talent.getBoughtOnCircle() + '';
        const name = this.#presenter.getElement(".name", row);
        name.textContent = talent.getData().getName();
        this.#presenter.addObserverTo(name, "click", () => this.#viewPresenter.showTalentDetails(talent));
        this.#presenter.getElement(".rank", row).textContent = talent.getTotalRank() + '';
        const attributeName = talent.hasStep() ? talent.getAttribute().getName().substring(0, 3) : "-";
        this.#presenter.getElement(".attribute", row).textContent = attributeName;
        this.#presenter.getElement(".step", row).textContent = talent.hasStep() ? (talent.getStep() + '') : "-";
        this.#presenter.getElement(".dice", row).textContent = talent.hasStep() ? talent.getDice() : "-";
        this.#presenter.getElement(".free-ranks", row).textContent = talent.getFreeRanks() + '';
        this.#presenter.getElement(".action > span", row).classList.add(talent.getData().needsAction() ? "fa-circle-dot" : "fa-circle");
        this.#presenter.getElement(".karma > span", row).classList.add(karmaCss[talent.getKarmaRequirement()]);
        this.#presenter.getElement(".strain", row).textContent = talent.getData().getStrain() + '';
        
        const minusButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.minus", row));
        minusButton.disabled = true;
        if(talent.mayDecreaseTotalRank() === TalentRankChange.POSSIBLE) {
            minusButton.disabled = false;
            this.#presenter.addObserverTo(minusButton, "click", () => {
                try {
                    talent.decreaseRank();
                    this.#storeChange();
                    this.#renderStatusBar();
                    this.#renderPages();
                } catch(error) {    // we do a full check to see if this talent can be really decreased or if we would violate discipline circle advancement rules
                    this.#presenter.getNotifications().postError(`The rank of ${talent.getData().getName()} cannot be decreased further:\n${error.message}`);
                    minusButton.disabled = true;
                }
            }, {once: true});
        }
        const plusButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.plus", row));
        plusButton.disabled = true;
        if(talent.canIncreaseRank()) {
            plusButton.disabled = false;
            this.#presenter.addObserverTo(plusButton, "click", () => {
                talent.increaseRank();
                this.#storeChange();
                this.#renderStatusBar();
                this.#renderPages();
            }, {once: true});
        }
        table.append(...row.children);
    }
    #renderEquipment() {
        const equipment = this.#character.getEquipment();
        this.#renderArmor(equipment);
        this.#renderMeleeWeapons(equipment);
        this.#renderRangedWeapons(equipment, WeaponType.THROWING);
        this.#renderRangedWeapons(equipment, WeaponType.MISSILE);
    }
    /**
     * @param {Equipment} equipment 
     */
    #renderArmor(equipment) {
        if(this.#equipment.armor.aborter) {
            this.#equipment.armor.aborter.abort();
        }
        this.#equipment.armor.aborter = new AbortController();
        if(equipment.getArmor()) {
            const armor = equipment.getArmor().getData();
            this.#presenter.getElement(".equipment .armor .table", this.#section).hidden = false;
            this.#presenter.getElement(".equipment .armor [data-type=name]", this.#section).textContent = armor.getName();
            this.#presenter.getElement(".equipment .armor [data-type=physical]", this.#section).textContent = armor.getPhysicalRating() + '';
            this.#presenter.getElement(".equipment .armor [data-type=mystic]", this.#section).textContent = armor.getMysticalRating() + '';
            this.#presenter.getElement(".equipment .armor [data-type=initiative]", this.#section).textContent = -armor.getInitiativePenalty() + '';
            this.#presenter.getElement(".equipment .armor [data-type=damage]", this.#section).textContent = armor.usesBloodMagic() ? (armor.getImplantDamage() + '') : 'n/a';
            this.#equipment.armor.add.hidden = true;
            this.#presenter.addObserverTo(this.#equipment.armor.remove, "click", () => {
                equipment.unequipArmor();
                this.#storeChange();
                this.#renderEquipment();
                this.#renderCharacteristics();
            },{signal: this.#equipment.armor.aborter.signal});
        } else {
            this.#presenter.getElement(".equipment .armor .table", this.#section).hidden = true;
            this.#equipment.armor.add.hidden = false;
            this.#presenter.addObserverTo(this.#equipment.armor.add, "click", () => this.#showNewArmor(), {signal: this.#equipment.armor.aborter.signal});
        }
    }
    #showNewArmor() {
        const modal = this.#presenter.getModal();
        modal.addTitle("Add Armor");
        modal.addCssClass("new-armor");
        const content = /** @type {HTMLElement} */ (this.#presenter.getElement(".equipment .new-armor-dialog").cloneNode(true)).children[0];
        const tableBody = this.#presenter.getElement(".table-body", content);
        
        let armors = null;
        if(this.#character.getRace().getAbilities().some(abilityData => abilityData.getId() === 104)) {   // Blood Elf Armor Restriction: may wear only Fernweave armor
            armors = [Earthdawn.data.armors.find(armorData => armorData && armorData.getName() === 'Fernweave')];
        } else if(this.#character.getRace().getAbilities().some(abilityData => abilityData.getId() === 108)) {      // Obsidiman Natural Armor: may wear only living armor
            armors = Earthdawn.data.armors.filter(armorData => armorData.isLivingArmor());
        } else {
            armors = Earthdawn.data.armors.filter(armorData => true);
        }

        const bodyChildren = [];
        armors.sort(EquipmentData.compare).forEach(armorData => {
            const row = /** @type {HTMLElement} */ (tableBody.cloneNode(true));
            this.#presenter.getElement("[data-type=name]", row).textContent = armorData.getName();
            this.#presenter.getElement("[data-type=physical]", row).textContent = armorData.getPhysicalRating() + '';
            this.#presenter.getElement("[data-type=mystic]", row).textContent = armorData.getMysticalRating() + '';
            this.#presenter.getElement("[data-type=initiative]", row).textContent = -armorData.getInitiativePenalty() + '';
            this.#presenter.getElement("[data-type=damage]", row).textContent = armorData.usesBloodMagic() ? (armorData.getImplantDamage() + '') : 'n/a'; 
            this.#presenter.addObserverTo(this.#presenter.getElement(".add", row), "click", event => { 
                modal.close();
                this.#character.getEquipment().equipArmor(armorData);
                this.#storeChange();
                this.#renderEquipment();
                this.#renderCharacteristics();
            }, {once: true});
            bodyChildren.push(...row.children);
        });
        tableBody.replaceChildren(...bodyChildren);
        modal.addContent(content);
        modal.open();
    }
    /**
     * @param {Equipment} equipment 
     */
    #renderMeleeWeapons(equipment) {
        const weapons = equipment.getMeleeWeapons();
        const table = this.#presenter.getElement(".equipment .melee-weapons .table", this.#section);
        if(weapons.length === 0) {
            table.hidden = true;
            return;
        }
        if(this.#equipment.melee.aborter) {
            this.#equipment.melee.aborter.abort();
        }
        this.#equipment.melee.aborter = new AbortController();
        const charStrength = this.#character.getAttribute(AttributeIds.STRENGTH).getTotalValue();
        const charDexterity = this.#character.getAttribute(AttributeIds.DEXTERITY).getTotalValue();
        table.hidden = false;
        const template = this.#presenter.getElement(".equipment .melee-weapons .row-template", this.#section);
        const tableBody = this.#presenter.getElement(".table-body", table);
        tableBody.replaceChildren();
        weapons.forEach(weapon => {
            const row = /** @type {HTMLElement} */ (template.cloneNode(true));
            this.#presenter.getElement(".name", row).textContent = weapon.getData().getName();
            this.#presenter.getElement(".base-damage", row).textContent = weapon.getData().getDamageStep() + '';
            const dmgBonus = /** @type {HTMLInputElement} */ (this.#presenter.getElement(".damage-bonus", row));
            dmgBonus.min = -weapon.getData().getDamageStep() + '';
            dmgBonus.value = weapon.getDamageBonus() + '';
            this.#presenter.addObserverTo(dmgBonus, "change", event => {
                const val = Number(dmgBonus.value);
                if(isNaN(val) || weapon.getData().getDamageStep() + val < 0) {
                    dmgBonus.value = weapon.getDamageBonus() + '';
                } else {
                    weapon.setDamageBonus(val);
                    this.#storeChange();
                }
            }, {signal: this.#equipment.melee.aborter.signal});
            this.#presenter.getElement(".size-value", row).textContent = weapon.getData().getSize() + '';
            const sizeIcons = weapon.canCarryOneHanded() ? ["fa-solid", "fa-hand"] : ["fa-solid", "fa-hands"];
            this.#presenter.getElement(".size-hands", row).classList.add(...sizeIcons);
            const strength = this.#presenter.getElement(".strength", row);
            let penalty = Math.min(0, charStrength - weapon.getData().getMinimumStrength());
            if(penalty < 0) {
                strength.textContent = penalty + '';
                strength.classList.add("negative");
            } else {
                strength.innerHTML = '&#xB7';
            }
            const dexterity = this.#presenter.getElement(".dexterity", row);
            penalty = Math.min(0, charDexterity - weapon.getData().getMinimumDexterity());
            if(penalty < 0) {
                dexterity.textContent = penalty + '';
                dexterity.classList.add("negative");
            } else {
                dexterity.innerHTML = '&#xB7';
            }
            const removeButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.remove", row));
            this.#presenter.addObserverTo(removeButton, "click", () => {
                equipment.unequipWeapon(weapon);
                this.#storeChange();
                this.#renderEquipment();
            }, {once: true});
            tableBody.append(...row.children);
            if(weapon.getData().canEntangle()) {
                const entangling = this.#presenter.createElement("span");
                entangling.className = "entangling";
                entangling.innerText = `Entangling (difficulty ${weapon.getData().getEntanglingDifficulty()})`;
                tableBody.append(entangling);
            }
        });
    }
    #showNewMeleeWeapons() {
        const modal = this.#presenter.getModal();
        modal.addTitle("Add Melee Weapon");
        modal.addCssClass("new-melee-weapon");
        const content = /** @type {HTMLElement} */ (this.#presenter.getElement(".equipment .new-melee-weapon-dialog").cloneNode(true)).children[0];
        const tableBody = this.#presenter.getElement(".table-body", content);
        const charStrength = this.#character.getAttribute(AttributeIds.STRENGTH).getTotalValue();
        const charDexterity = this.#character.getAttribute(AttributeIds.DEXTERITY).getTotalValue();
        const weapons = Earthdawn.data.weapons.filter(weaponData => weaponData.getType() === WeaponType.MELEE && weaponData.handsNeeded(this.#character.getRace()) !== 0).sort(EquipmentData.compare);
        const bodyChildren = [];
        weapons.forEach(weaponData => {
            const row = /** @type {HTMLElement} */ (tableBody.cloneNode(true));
            this.#presenter.getElement("[data-type=name]", row).textContent = weaponData.getName();
            this.#presenter.getElement("[data-type=damage]", row).textContent = weaponData.getDamageStep() + '';
            this.#presenter.getElement("[data-type=size-value]", row).textContent = weaponData.getSize() + '';
            const sizeIcons = weaponData.handsNeeded(this.#character.getRace()) === 1 ? ["fa-solid", "fa-hand"] : ["fa-solid", "fa-hands"];
            this.#presenter.getElement("[data-type=size-hands]", row).classList.add(...sizeIcons);
            const strength = this.#presenter.getElement("[data-type=strength]", row);
            let penalty = Math.min(0, charStrength - weaponData.getMinimumStrength());
            if(penalty < 0) {
                strength.textContent = penalty + '';
                strength.classList.add("negative");
            } else {
                strength.innerHTML = '&#xB7';
            }
            const dexterity = this.#presenter.getElement("[data-type=dexterity]", row);
            penalty = Math.min(0, charDexterity -weaponData.getMinimumDexterity());
            if(penalty < 0) {
                dexterity.textContent = penalty + '';
                dexterity.classList.add("negative");
            } else {
                dexterity.innerHTML = '&#xB7';
            }
            this.#presenter.addObserverTo(this.#presenter.getElement(".add", row), "click", event => { 
                modal.close();
                this.#character.getEquipment().equipWeapon(weaponData);
                this.#storeChange();
                this.#renderEquipment();
            }, {once: true});
            bodyChildren.push(...row.children);
        });
        tableBody.replaceChildren(...bodyChildren);
        modal.addContent(content);
        modal.open();
    }
    /**
     * @param {Equipment} equipment
     * @param {WeaponType} type - The ranged weapon type to render
     */
    #renderRangedWeapons(equipment, type) {
        const typeName = type == WeaponType.MISSILE ? "missile" : "throwing";
        const table = this.#presenter.getElement(`.equipment .${typeName}-weapons .table`, this.#section);
        const weapons = equipment.getWeapons(type);
        if(weapons.length === 0) {
            table.hidden = true;
            return;
        }
        if(this.#equipment[typeName].aborter) {
            this.#equipment[typeName].aborter.abort();
        }
        this.#equipment[typeName].aborter = new AbortController();
        const charStrength = this.#character.getAttribute(AttributeIds.STRENGTH).getTotalValue();
        const charDexterity = this.#character.getAttribute(AttributeIds.DEXTERITY).getTotalValue();
        table.hidden = false;
        const template = this.#presenter.getElement(`.equipment .${typeName}-weapons .row-template`, this.#section);
        const tableBody = this.#presenter.getElement(".table-body", table);
        tableBody.replaceChildren();
        weapons.forEach(weapon => {
            const row = /** @type {HTMLElement} */ (template.cloneNode(true));
            this.#presenter.getElement(".name", row).textContent = weapon.getData().getName();
            this.#presenter.getElement(".base-damage", row).textContent = weapon.getData().getDamageStep() + '';
            const dmgBonus = /** @type {HTMLInputElement} */ (this.#presenter.getElement(".damage-bonus", row));
            dmgBonus.min = -weapon.getData().getDamageStep() + '';
            dmgBonus.value = weapon.getDamageBonus() + '';
            this.#presenter.addObserverTo(dmgBonus, "change", event => {
                const val = Number(dmgBonus.value);
                if(isNaN(val) || weapon.getData().getDamageStep() + val < 0) {
                    dmgBonus.value = weapon.getDamageBonus() + '';
                } else {
                    weapon.setDamageBonus(val);
                    this.#storeChange();
                }
            }, {signal: this.#equipment[typeName].aborter.signal});
            this.#presenter.getElement(".size-value", row).textContent = weapon.getData().getSize() + '';
            const sizeIcons = weapon.canCarryOneHanded() ? ["fa-solid", "fa-hand"] : ["fa-solid", "fa-hands"];
            this.#presenter.getElement(".size-hands", row).classList.add(...sizeIcons);
            const strength = this.#presenter.getElement(".strength", row);
            let penalty = Math.min(0, charStrength - weapon.getData().getMinimumStrength());
            if(penalty < 0) {
                strength.textContent = penalty + '';
                strength.classList.add("negative");
            } else {
                strength.innerHTML = '&#xB7';
            }
            const dexterity = this.#presenter.getElement(".dexterity", row);
            penalty = Math.min(0, charDexterity - weapon.getData().getMinimumDexterity());
            if(penalty < 0) {
                dexterity.textContent = penalty + '';
                dexterity.classList.add("negative");
            } else {
                dexterity.innerHTML = '&#xB7';
            }
            const removeButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.remove", row));
            this.#presenter.addObserverTo(removeButton, "click", () => {
                equipment.unequipWeapon(weapon);
                this.#storeChange();
                this.#renderEquipment();
            }, {once: true});
            tableBody.append(...row.children);
            const details = this.#presenter.createElement("span");
            details.className = "details";
            const ranges = /** @type {RangedWeaponData} */(weapon.getData()).getRange();
            details.textContent = `Range: ${ranges.short.min}-${ranges.short.max} (short), ${ranges.long.min}-${ranges.long.max} (long)`;
            if(weapon.getData().canEntangle()) {
                details.textContent += `, Entangling (${weapon.getData().getEntanglingDifficulty()})`;
            }
            tableBody.append(details);
        });
    }
    /**
     * @param {WeaponType} type 
     */
    #showNewRangedWeapons(type) {
        const modal = this.#presenter.getModal();
        modal.addTitle(`Add ${type === WeaponType.MISSILE ? 'Missile' : 'Throwing'} Weapon`);
        modal.addCssClass("new-ranged-weapon");
        const content = /** @type {HTMLElement} */ (this.#presenter.getElement(".equipment .new-ranged-weapon-dialog").cloneNode(true)).children[0];
        const tableBody = this.#presenter.getElement(".table-body", content);
        const charStrength = this.#character.getAttribute(AttributeIds.STRENGTH).getTotalValue();
        const charDexterity = this.#character.getAttribute(AttributeIds.DEXTERITY).getTotalValue();
        const weapons = Earthdawn.data.weapons.filter(weaponData => weaponData.getType() === type && weaponData.handsNeeded(this.#character.getRace()) !== 0).sort(EquipmentData.compare);
        const bodyChildren = [];
        weapons.forEach(/** @param {RangedWeaponData} weaponData */ weaponData => {
            const row = /** @type {HTMLElement} */ (tableBody.cloneNode(true));
            this.#presenter.getElement("[data-type=name]", row).textContent = weaponData.getName();
            this.#presenter.getElement("[data-type=damage]", row).textContent = weaponData.getDamageStep() + '';
            this.#presenter.getElement("[data-type=long-range]", row).textContent = `${weaponData.getRange().long.min}-${weaponData.getRange().long.max}`;
            this.#presenter.getElement("[data-type=size-value]", row).textContent = weaponData.getSize() + '';
            const sizeIcons = weaponData.handsNeeded(this.#character.getRace()) === 1 ? ["fa-solid", "fa-hand"] : ["fa-solid", "fa-hands"];
            this.#presenter.getElement("[data-type=size-hands]", row).classList.add(...sizeIcons);
            const strength = this.#presenter.getElement("[data-type=strength]", row);
            let penalty = Math.min(0, charStrength - weaponData.getMinimumStrength());
            if(penalty < 0) {
                strength.textContent = penalty + '';
                strength.classList.add("negative");
            } else {
                strength.innerHTML = '&#xB7';
            }
            const dexterity = this.#presenter.getElement("[data-type=dexterity]", row);
            penalty = Math.min(0, charDexterity -weaponData.getMinimumDexterity());
            if(penalty < 0) {
                dexterity.textContent = penalty + '';
                dexterity.classList.add("negative");
            } else {
                dexterity.innerHTML = '&#xB7';
            }
            this.#presenter.addObserverTo(this.#presenter.getElement(".add", row), "click", event => { 
                modal.close();
                this.#character.getEquipment().equipWeapon(weaponData);
                this.#storeChange();
                this.#renderEquipment();
            }, {once: true});
            bodyChildren.push(...row.children);
        });
        tableBody.replaceChildren(...bodyChildren);
        modal.addContent(content);
        modal.open();
    }
    #storeChange() {
        this.#record.update(this.#character);
        return this.#presenter.getPersistence().updateCharacter(this.#record);
    }
    /**
     * @param {Presenter} presenter 
     * @param {HTMLElement} section 
     * @returns {DynamicNavbarScroller}
     */
    #getAndSetupNavbar(presenter, section) {
        const pageContainer = presenter.getElement(".page-container", section);
        const navbarContainer = presenter.getElement(".navbar-s", section);
        return new DynamicNavbarScroller(pageContainer, navbarContainer, presenter);
    }
    /**
     * @param {Presenter} presenter 
     * @param {HTMLElement} section 
     * @returns {HTMLInputElement} - The text input field for editing the character name
     */
    #setupName(presenter, section) {
        const name = /** @type {HTMLInputElement} */ (presenter.getElement("#edit-character-name", section));
        presenter.addObserverTo(name, "change", event => {
            const newName = name.value.trim();
            if(newName.length > 0 && newName.length <= 50) {
                name.value = newName;
                this.#character.setName(newName);
                this.#storeChange();
                this.#renderStatusBar();
            } else {
                name.value = this.#character.getName();
            }
        });
        return name;
    }
    /**
     * @param {Presenter} presenter 
     * @param {HTMLElement} section 
     * @returns {HTMLInputElement} - The number input field for editing the amount of unspent legend points.
     */
    #setupLegendPoints(presenter, section) {
        const input = /** @type {HTMLInputElement} */ (presenter.getElement("#edit-character-legend-points", section));
        presenter.addObserverTo(input, "input", event => {
            if(input.checkValidity()) {
                this.#character.getLegendPoints().setUnspentAndAdjustTotal(Number(input.value));
                this.#presenter.getElement(".basics .legend-points [data-type=total]", this.#section).textContent = this.#character.getLegendPoints().getTotal() + '';
                this.#renderStatusBar();
            } else {
                event.preventDefault();
                input.value = this.#character.getLegendPoints().getUnspent() + '';
            }
        });
        presenter.addObserverTo(input, "change", event => {
            this.#storeChange();
            this.#renderPages();
        });
        return input;
    }
    /**
     * @param {Presenter} presenter 
     * @param {HTMLElement} section 
     * @returns {{button:HTMLButtonElement, aborter?:AbortController}}
     */
    #setupSkills(presenter, section) {
        const button = /** @type {HTMLButtonElement} */ (presenter.getElement(".skills .edit .heading button.add", section));
        return {button: button, aborter: null };
    }
    /**
     * @param {Presenter} presenter 
     * @param {HTMLElement} section 
     * @returns {{button:HTMLButtonElement, aborter?:AbortController}}
     */
    #setupSpells(presenter, section) {
        const button = /** @type {HTMLButtonElement} */ (presenter.getElement(".spells .heading button.add", section));
        return {button: button, aborter: null };
    }
    /**
     * @param {Presenter} presenter 
     * @param {HTMLElement} section 
     * @returns {{button:HTMLButtonElement, aborter?:AbortController, has:boolean}}
     */
    #setupVersatility(presenter, section) {
        const button = /** @type {HTMLButtonElement} */ (presenter.getElement(".versatility .heading button.add", section));
        return {button: button, aborter: null, has: false};
    }
    /**
     * @param {Presenter} presenter 
     * @param {HTMLElement} section 
     * @returns {EquipmentElements}
     */
    #setupEquipment(presenter, section) {
        presenter.addObserverTo(presenter.getElement(".equipment .melee-weapons .heading button.add", section), "click", () => this.#showNewMeleeWeapons());
        presenter.addObserverTo(presenter.getElement(".equipment .throwing-weapons .heading button.add", section), "click", () => this.#showNewRangedWeapons(WeaponType.THROWING));
        presenter.addObserverTo(presenter.getElement(".equipment .missile-weapons .heading button.add", section), "click", () => this.#showNewRangedWeapons(WeaponType.MISSILE));
        return {
            armor: {
                add: /** @type {HTMLButtonElement} */ (presenter.getElement(".equipment .armor button.add", section)), 
                remove: /** @type {HTMLButtonElement} */ (presenter.getElement(".equipment .armor button.remove", section)), 
                aborter: null
            },
            melee: {
                aborter: null
            },
            throwing: {
                aborter: null
            },
            missile: {
                aborter: null
            }
        };
    }
}

/**
 * Dialog for editing current Karma points
 */
class KarmaPointsDialog {
    #presenter;
    #character;
    /**
     * @param {Presenter} presenter - Window presenter
     * @param {Character} character - Owning character 
     */
    constructor(presenter /* Presenter */, character /* Character */) {
        this.#presenter = presenter;
        this.#character = character;
    }
    async render() {
        const modal = this.#presenter.getModal();
        modal.addCssClass("edit-current-karma");
        modal.addTitle("Current Karma");
        const content = new NumberField().initialize(0, this.#character.getKarma().getPossibleMaximum(), this.#character.getKarma().getAvailablePoints());
        modal.addContent(content);
        const done = this.#presenter.createElement('button');
        done.className = 'done';
        done.textContent = 'Set';
        modal.addContent(done);
        return new Promise((resolve, reject) => {
            this.#presenter.addObserverTo(done, 'click', () => {
                this.#character.getKarma().setAvailablePoints(content.getValue());
                modal.close().then(() => resolve());
            }, {once: true});
            modal.open();
        });
    }
}

/**
 * Dialog for editing current damage (taking damage or healing damage)
 */
class DamageAndHealingDialog {
    #presenter;
    #character;
    #section;
    /**
     * @typedef {object} DamageTab
     * @property {"Damage"} text - Tab title
     * @property {?HTMLElement} tab - Reference to the tab container
     * @property {?NumberField} damageTaken - Input for newly taken damage
     * @property {?HTMLInputElement} useArmor - Checkbox if armor should be taken into account when calculating final damage
     * @property {?HTMLInputElement} causeWounds - Checkbox if wounds can be caused by the damage taken
     * @property {?HTMLInputElement} isMystic - Checkbox if the damage type is mystic damage (instead of physical)
     * @property {?HTMLInputElement} isIllusionary - Checkbox if the damage was caused by an illusion
     * @property {?HTMLElement} finalDamage - Container to render the calculated final damage
     * @property {?HTMLElement} wounds - Container to render the calculated wounds
     * @property {?HTMLElement} knockdown - Container to render the knockdown difficulty
     * @property {?HTMLElement} status - Container to render the status (like e.g. dead)
     * @property {number} value - The damage taken after deducting armor
     */
    /**
     * @typedef {object} HealingTab
     * @property {"Healing"} text - Tab title
     * @property {?HTMLElement} tab - Reference to the tab container
     * @property {?HTMLInputElement} recovery - Radio button if a simple recovery test should be used
     * @property {?HTMLInputElement} boosterPotion - Radio button if a booster potion should be used
     * @property {?HTMLInputElement} healingPotion - Radio button if a healing potion should be used
     * @property {?HTMLInputElement} fireblood - Radio button if the Fireblood talent should be used
     * @property {?Talent} firebloodTalent - The character's Fireblood talent if owned
     * @property {?HTMLInputElement} free - Radio button if the healing should be free (no recovery test needed)
     * @property {?HTMLInputElement} useKarma - Checkbox if a Karma point should be spent on the test
     * @property {?NumberField} modifier - Input field for additional step modifier
     * @property {?HTMLElement} step - Container to render the final step used for the healing test
     * @property {?HTMLElement} dice - Container to render the final dice used for the healing test
     * @property {?NumberField} value - Input field for the final number of damage points healed
     */
    #content = {
        /**  @type {DamageTab} */
        damage: {
            text: "Damage",
            tab: null,
            damageTaken: null,
            useArmor: null,
            causeWounds: null,
            isMystic: null,
            isIllusionary: null,
            finalDamage: null,
            wounds: null,
            knockdown: null,
            status: null,
            value: 0
        },
        /** @type {HealingTab} */
        healing: {
            text: "Healing",
            tab: null,
            recovery: null,
            boosterPotion: null,
            healingPotion: null,
            fireblood: null,
            firebloodTalent: null,
            free: null,
            useKarma: null,
            modifier: null,
            step: null,
            dice: null,
            value: null
        },
        /** @type {(null|DamageTab|HealingTab)} - A reference to the selected tab or null if nothing is selected yet */
        selected: null
    };
    /**
     * @param {Presenter} presenter - Window presenter 
     * @param {HTMLElement} section - HTML parent container that houses the dialog template 
     * @param {Character} character - Owning character
     */
    constructor(presenter, section, character) {
        this.#presenter = presenter;
        this.#section = section;
        this.#character = character;
    }
    /**
     * Opens the dialog.
     * @returns {Promise} - Returns a Promise that is resolved if the character was changed. Is never resolved if the modal was cancelled. Never rejects.
     */
    async render() {
        const modal = this.#presenter.getModal();
        modal.addCssClass("edit-current-damage");
        const content = /** @type {HTMLElement} */ (this.#presenter.getElement("div.damage-modal-template", this.#section).cloneNode(true));
        this.#setupDamageTab(content);
        this.#setupHealingTab(content);
        const title = this.#presenter.createElement("span");
        modal.addTitle(title);
        this.#setupTabToggle(title, content);

        const applyButtons = this.#presenter.findElements("button.apply", content);
        modal.addContent(...content.children);
        return new Promise((resolve, reject) => {
            applyButtons.forEach(apply => {
                this.#presenter.addObserverTo(apply, "click", () => {
                    this.#applyChanges();
                    modal.close().then(() => resolve());
                }, {once: true});
            });
            modal.open();
        });
    }
    /**
     * Sets up the toggling elements. The dialog contains both the tab to take damage (title "Damage") as well as the tab to heal damage (title "Healing"). The user can switch between both by clicking the toggle label.
     * @param {HTMLElement} title - The title bar of the dialog
     * @param {HTMLElement} content - The content area of the dialog
     */
    #setupTabToggle(title, content) {
        const toggle = this.#presenter.getElement(".tab-switch", content);
        const switchLabel = this.#presenter.getElement("span[data-type=label]", toggle);
        this.#content.selected = this.#content.healing;
        this.#toggleTab(title, switchLabel);    // initialize: switch to Damage tab
        this.#presenter.addObserverTo(toggle, "click", () => this.#toggleTab(title, switchLabel));
    }
    /**
     * 
     * @param {HTMLElement} title - The title bar
     * @param {HTMLElement} switcher - The element that has to be clicked to switch between the tabs.
     */
    #toggleTab(title, switcher) {
        this.#content.selected.tab.hidden = true;
        switcher.innerText = this.#content.selected.text;
        this.#content.selected = (this.#content.selected === this.#content.damage) ? this.#content.healing : this.#content.damage;
        this.#content.selected.tab.hidden = false;
        title.innerText = this.#content.selected.text;
    }
    /**
     * @param {HTMLElement} content - The content container of this dialog that contains the two tab areas.
     */
    #setupDamageTab(content) {
        const tab = this.#content.damage.tab = this.#presenter.getElement(".tab.damage", content);
        this.#content.damage.damageTaken = /** @type {NumberField} */ (this.#presenter.getElement(".input [data-type=damage]", tab));
        this.#presenter.addObserverTo(this.#content.damage.damageTaken, "change", () => {
            this.#updateDamageTab();
        });
        this.#content.damage.useArmor = /** @type {HTMLInputElement} */ (this.#presenter.getElement("input[data-type=armor]", tab));
        this.#presenter.addObserverTo(this.#content.damage.useArmor, "change", () => {
            this.#updateDamageTab();
        });
        this.#content.damage.causeWounds = /** @type {HTMLInputElement} */ ( this.#presenter.getElement("input[data-type=wounds]", tab));
        this.#presenter.addObserverTo(this.#content.damage.causeWounds, "change", () => {
            this.#updateDamageTab();
        });
        this.#content.damage.isMystic = /** @type {HTMLInputElement} */ ( this.#presenter.getElement("input[data-type=mystic]", tab));
        this.#presenter.addObserverTo(this.#content.damage.isMystic, "change", () => {
            this.#updateDamageTab();
        });
        this.#content.damage.isIllusionary = /** @type {HTMLInputElement} */ ( this.#presenter.getElement("input[data-type=illusionary]", tab));
        this.#presenter.addObserverTo(this.#content.damage.isIllusionary, "change", () => {
            this.#updateDamageTab();
        });
        this.#content.damage.finalDamage = this.#presenter.getElement(".result span[data-type=final-damage]", tab);
        this.#content.damage.wounds = this.#presenter.getElement(".result span[data-type=wounds]", tab);
        this.#content.damage.knockdown = this.#presenter.getElement(".result span[data-type=knockdown]", tab);
        this.#content.damage.status = this.#presenter.getElement(".result span[data-type=status]", tab);
        this.#updateDamageTab();
    }
    #updateDamageTab() {
        let damage = this.#content.damage.damageTaken.getValue();
        if(this.#content.damage.useArmor.checked) {
            const armor = this.#character.getArmorRating();
            damage = Math.max(0, damage - (this.#content.damage.isMystic.checked ? armor.getMystic() : armor.getPhysical()));
        }
        this.#content.damage.value = damage;
        const health = this.#character.getHealth();
        const maxHealth = this.#content.damage.isIllusionary.checked ? health.getUnconsciousnessRating() : health.getDeathRating();      // illusionary damage never leads to death only to unconsciousness.
        const healthLeft = Math.max(0, maxHealth - health.getTotalDamage());        // Character cannot take more damage than max health (normally death rating) available.
        const finalDamage = Math.min(healthLeft, damage);
        this.#content.damage.finalDamage.innerText = finalDamage + '';
        let wounds = "-";
        let knockdown = "-";
        if(this.#content.damage.causeWounds.checked) {
            wounds = '0';
            if(damage >= health.getWoundThreshold()) {  // use original damage. Even if final damage is capped by max health, for wounds are not.
                wounds = '1';
                knockdown = (damage - health.getWoundThreshold() + 3) + '';
            }
        }
        this.#content.damage.wounds.innerText = wounds;
        this.#content.damage.knockdown.innerText = knockdown;
        let status = "conscious";
        if(finalDamage + health.getTotalDamage() >= health.getDeathRating()) {
            status = "dead";
        } else if(finalDamage + health.getTotalDamage() >= health.getUnconsciousnessRating()) {
            status = "unconscious";
        }
        this.#content.damage.status.innerText = status;
    }
    /**
     * @param {HTMLElement} content - The content container of this dialog that contains the two tab areas.
     */
    #setupHealingTab(content) {
        const tab = this.#content.healing.tab = this.#presenter.getElement(".tab.healing", content);
        const hasRecoveryTest = this.#character.getHealth().getAvailableRecoveryTests() > 0;
        this.#content.healing.recovery = /** @type {HTMLInputElement} */ (this.#presenter.getElement("input[data-type=recovery]", tab));
        this.#content.healing.recovery.disabled = !hasRecoveryTest;
        if(hasRecoveryTest) {
            this.#content.healing.recovery.checked = true;
            this.#presenter.addObserverTo(this.#content.healing.recovery, "change", () => {
                this.#updateHealingTab();
            });
        }
        this.#content.healing.boosterPotion = /** @type {HTMLInputElement} */ (this.#presenter.getElement("input[data-type=booster-potion]", tab));
        this.#content.healing.boosterPotion.disabled = !hasRecoveryTest;
        if(hasRecoveryTest) {
            this.#presenter.addObserverTo(this.#content.healing.boosterPotion, "change", () => {
                this.#updateHealingTab();
            });
        }
        this.#content.healing.healingPotion = /** @type {HTMLInputElement} */ (this.#presenter.getElement("input[data-type=healing-potion]", tab));
        this.#content.healing.healingPotion.checked = !hasRecoveryTest;
        this.#presenter.addObserverTo(this.#content.healing.healingPotion, "change", () => {
            this.#updateHealingTab();
        });
        this.#content.healing.fireblood = /** @type {HTMLInputElement} */ (this.#presenter.getElement("input[data-type=fireblood]", tab));
        this.#content.healing.firebloodTalent = this.#character.findTalent(KnownTalent.FIREBLOOD.getId());
        if(this.#content.healing.firebloodTalent) {
            this.#content.healing.fireblood.parentElement.hidden = false;
            this.#content.healing.fireblood.disabled = !hasRecoveryTest;
            if(hasRecoveryTest) {
                this.#presenter.addObserverTo(this.#content.healing.fireblood, "change", () => {
                    this.#updateHealingTab();
                });
            }
        }
        this.#content.healing.free = /** @type {HTMLInputElement} */ (this.#presenter.getElement("input[data-type=free]", tab));
        this.#presenter.addObserverTo(this.#content.healing.free, "change", () => {
            this.#updateHealingTab();
        });
        this.#content.healing.useKarma = /** @type {HTMLInputElement} */ (this.#presenter.getElement("input[data-type=karma]", tab));
        this.#presenter.addObserverTo(this.#content.healing.useKarma, "change", () => {
            this.#updateHealingTab();
        });
        this.#content.healing.modifier = /** @type {NumberField} */ (this.#presenter.getElement(".input [data-type=modifier]", tab));
        this.#presenter.addObserverTo(this.#content.healing.modifier, "change", () => {
            this.#updateHealingTab();
        });
        this.#content.healing.step = this.#presenter.getElement(".result span[data-type=step]", tab);
        this.#content.healing.dice = this.#presenter.getElement(".result span[data-type=dice]", tab);
        this.#content.healing.value = /** @type {NumberField} */ (this.#presenter.getElement(".input [data-type=health]", tab));
        this.#content.healing.value.initialize(0, Math.max(1, this.#character.getHealth().getTemporaryDamage()), 0);
        this.#updateHealingTab();
    }
    #updateHealingTab() {
        const hasRecoveryTest = this.#character.getHealth().getAvailableRecoveryTests() > 0;
        const hasKarma = this.#character.getKarma().getAvailablePoints() > 0;
        const karmaForRecovery = hasRecoveryTest && hasKarma && this.#character.getAllEffectiveAbilities().some(ability => ability.getType() === AbilityType.KARMA_USE && ability.getAffectedTraitId() === KarmaUse.RECOVERY_TEST);
        const toughnessStep = this.#character.getAttribute(AttributeIds.TOUGHNESS).getStep();
        let step = 0;
        let allowKarma = hasKarma;
        if(this.#content.healing.recovery.checked) {
            step = toughnessStep;
            allowKarma &&= karmaForRecovery;
        } else if(this.#content.healing.boosterPotion.checked) {
            step = toughnessStep + 8;
            allowKarma &&= karmaForRecovery;
        } else if(this.#content.healing.healingPotion.checked) {
            step = hasRecoveryTest ? (toughnessStep + 8) : 8;   // if character does not have any recovery tests left, then healing potion provides a free heal with step 8.
            allowKarma &&= karmaForRecovery;
        } else if(this.#content.healing.fireblood.checked) {
            step = this.#content.healing.firebloodTalent.getStep();
            allowKarma &&= this.#content.healing.firebloodTalent.getKarmaRequirement() !== KarmaRequirement.NO;
        } else if(this.#content.healing.free.checked) {
            step = toughnessStep;
            allowKarma &&= karmaForRecovery;
        }
        if(!allowKarma) {
            this.#content.healing.useKarma.checked = false;
            this.#content.healing.useKarma.disabled = true;
        } else {
            this.#content.healing.useKarma.disabled = false;
        }
        step = Math.max(1, step + this.#content.healing.modifier.getValue());
        this.#content.healing.step.innerText = step + '';
        let dice = Earthdawn.rules.getDiceForStep(step);
        if(this.#content.healing.useKarma.checked) {
            dice += "+" + this.#character.getKarma().getDice();
        }
        this.#content.healing.dice.innerText = dice;
    }
    #applyChanges() {
        if(this.#content.selected === this.#content.damage) {
            this.#character.getHealth().increaseTemporaryDamage(this.#content.damage.value, this.#content.damage.causeWounds.checked, this.#content.damage.isIllusionary.checked);
        } else {
            this.#character.getHealth().reduceTemporaryDamage(this.#content.healing.value.getValue());
            if(this.#content.healing.healingPotion.checked) {   // healing potions always also heal 1 wound for free
                this.#character.getHealth().decreaseWounds();
            }
            if(!this.#content.healing.free.checked) {   // all other options require spending of a recovery test if available
                this.#character.getHealth().useRecoveryTest();
            }
            if(this.#content.healing.useKarma.checked) {
                this.#character.getKarma().spendOnePoint();
            }
        }
    }
}

/**
 * Dialog for editing the current number of recovery tests the character has.
 */
class RecoveryTestsDialog {
    #presenter;
    #character;
    #section;
    /**
     * @param {Presenter} presenter - Window presenter 
     * @param {HTMLElement} section - Root element for the section that contains the HTML template for this dialog 
     * @param {Character} character - Edited character
     */
    constructor(presenter, section, character) {
        this.#presenter = presenter;
        this.#section = section;
        this.#character = character;
    }
    /**
     * Opens the dialog.
     * @returns {Promise} - Returns a Promise that is resolved if the character was changed. Is never resolved if the modal was cancelled. Never rejects.
     */
    async render() {
        const modal = this.#presenter.getModal();
        modal.addCssClass("edit-current-recovery");
        modal.addTitle("Recovery Tests");
        const content = /** @type {HTMLElement} */ (this.#presenter.getElement("div.recovery-tests-modal-template", this.#section).cloneNode(true));
        if(this.#character.getHealth().getDaysForRecoveryTestFresh() > 1) {
            this.#presenter.getElement("div[data-type=recoveryDays]", content).innerText = `${this.#character.getHealth().getDaysForRecoveryTestFresh()} days`;
        }
        const currentTests = /** @type {NumberField} */ (this.#presenter.getElement("[data-type=tests]", content));
        currentTests.initialize(0, this.#character.getHealth().getRecoveryTestsPerRefreshPeriod() + 10, this.#character.getHealth().getAvailableRecoveryTests());
        
        const restButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.rest", content));
        const setButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.set", content));
        modal.addContent(...content.children);
        return new Promise((resolve, reject) => {
            if(this.#character.getHealth().hasSpentRecoveryTests()) {
                this.#presenter.addObserverTo(restButton, "click", () => {
                    this.#character.getHealth().refreshRecoveryTests();
                    modal.close().then(() => resolve());
                }, {once: true});
            } else {
                restButton.disabled = true;
            }
            this.#presenter.addObserverTo(setButton, "click", () => {
                this.#character.getHealth().setAvailableRecoveryTestsTo(currentTests.getValue());
                modal.close().then(() => resolve());
            }, {once: true});
            modal.open();
        });
    }
}

/**
 * Dialog to edit the current wounds a character has.
 */
class WoundsDialog {
    #presenter;
    #character;
    #section;
    /**
     * @param {Presenter} presenter - Window presenter 
     * @param {HTMLElement} section - Root element for the section that contains the HTML template for this dialog 
     * @param {Character} character - Edited character
     */
    constructor(presenter, section, character) {
        this.#presenter = presenter;
        this.#section = section;
        this.#character = character;
    }
    /**
     * Opens the dialog.
     * @returns {Promise} - Returns a Promise that is resolved if the character was changed. Is never resolved if the modal was cancelled. Never rejects.
     */
    async render() {
        const modal = this.#presenter.getModal();
        modal.addCssClass("edit-current-wounds");
        modal.addTitle("Wounds");
        const content = /** @type {HTMLElement} */ (this.#presenter.getElement("div.wounds-modal-template", this.#section).cloneNode(true));
        const currentWounds = /** @type {NumberField} */ (this.#presenter.getElement("[data-type=wounds]", content));
        const health = this.#character.getHealth();
        const max = Math.max(15, health.getWounds());
        currentWounds.initialize(0, max, health.getWounds());
        
        const recoverButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.recover", content));
        const setButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.set", content));
        modal.addContent(...content.children);
        return new Promise((resolve, reject) => {
            if(health.getWounds() > 0 && health.getAvailableRecoveryTests() > 0 && health.getTemporaryDamage() === 0) {
                this.#presenter.addObserverTo(recoverButton, "click", () => {
                    health.decreaseWounds();
                    health.useRecoveryTest();
                    modal.close().then(() => resolve());
                }, {once: true});
            } else {
                recoverButton.disabled = true;
            }
            this.#presenter.addObserverTo(setButton, "click", () => {
                health.setWounds(currentWounds.getValue());
                modal.close().then(() => resolve());
            }, {once: true});
            modal.open();
        });
    }
}


/**
 * Dialog for modifying the step and dice to roll for a talent, skill, attribute or any other rollable trait test.
 * 
 * wound penalties (-x steps to all actions)
 * weapon attack penalties (-x steps based on weapon's attribute requirements)
 * 
 * aggressive attack (+3 close combat attack, +3 dmg, -3 phy und spell defense, 1 strain)
 * aiming (+2 range attack, 1 strain)
 * called shot (-3 attack, 1 strain)
 * defensive stance (-3 attack, +3 phys and spell def)
 * long shot (-2 attack step, -2 damage)
 * improvised weapon (-2 attack)
 * tail attack (-2 to all actions this round, second attack with tail for T'Skrank only)
 * split movement (-2 actions, -2 defenses, 1 strain.)
 * 
 * knocked down (-3 actions, -3 defenses)
 * stunned (only defensive actions, counts a harried)
 * darkness (partial: -2 all actions, full -4)
 * harried (-2 actions, -2 defenses)
 * blindsided (-2 phys and spell def)
 * surprised (no action, -3 phys and spell def)
 * cover (partial +2, full +4 phys and spell def)
 * changed/reserved action (+2 difficulty)
 * 
 * success levels (+2 steps per level)
 * additional modifier (-/+ x steps)
 * karma use (+extra karma die)
 * 
 */
class StepModDialog {
    #presenter;
    #character;
    #storeChange;
    /** @type {number} - The cumulated strain cost */
    #strain;
    /** @type {number} - The modified step number (may be less than 1), excluding any {@link Conditions} that were aleady active when opening the dialog */
    #step;
    /** @type {ActionType} - The type of talent/skill/trait used and in which capacity. Defines what options are shown and how some modifiers are added. */
    #action;
    /** @type {{attack: Object.<string,HTMLInputElement>, damage: Object.<string,HTMLInputElement>, general: Object.<string,HTMLInputElement>, fields: Object.<string,NumberField>, output: Object.<string,HTMLElement>}} */
    #elements;
    /**
     * @param {Presenter} presenter - Window presenter 
     * @param {Character} character - Owning character
     * @param {() => any} changeStorer - A function that updates the character's {@link CharacterRecord} and persists it. Will be called by the dialog whenever the user triggered a change of the character state.
     */
    constructor(presenter, character, changeStorer) {
        this.#presenter = presenter;
        this.#character = character;
        this.#storeChange = changeStorer;
        this.#strain = 0;
        this.#step = 0;
        this.#action = ActionType.GENERAL;
        this.#elements = {attack: null, damage: null, general: null, fields: null, output: null};
    }
    /**
     * Opens the dialog for any talent usage.
     * @param {Talent} talent - The used talent
     * @param {?EquippedWeapon} [weapon=null] - If this is an attack or damage test then this is the weapon used, else null. Also null for unarmed combat.
     * @returns {Promise} - Returns a Promise that is resolved when the dialog is closed. Never rejects.
     */
    async renderForTalent(talent, weapon = null) {
        const data = talent.getData();
        this.#action = data.getActionType();
        const step = this.#action === ActionType.INITIATIVE ? this.#character.getInitiative().getStep() + talent.getTotalRank() : talent.getStep();
        return this.#openDialog(data.getName(), step, data.getStrain(), talent.getKarmaRequirement(), weapon);
    }
    /**
     * Opens the dialog for any skill usage.
     * @param {Skill} skill - The used skill
     * @param {?EquippedWeapon} [weapon=null] - If this is an attack or a damage test then this is the weapon used, else null. Also null for unarmed combat.
     * @returns {Promise} - Returns a Promise that is resolved when the dialog is closed. Never rejects.
     */
    async renderForSkill(skill, weapon = null) {
        const data = skill.getData();
        this.#action = data.getActionType();
        return this.#openDialog(data.getName(), skill.getStep(), data.getStrain(), skill.canUseKarma() ? KarmaRequirement.OPTIONAL : KarmaRequirement.NO, weapon);
    }
    /**
     * Opens the dialog for a basic Initiative test (without the help of a talent or skill) based on dexterity.
     * @param {string} name - Name of the used trait
     * @param {number} step - Initiative step already reduced by armor penalty.
     * @param {number} [strain=0] - Strain damage for making this test
     * @param {KarmaRequirement} [karmaUse=KarmaRequirement.NO] - If the character can use Karma for initiative tests (through abilities) or because of a used talent.
     * @returns {Promise} - Returns a Promise that is resolved when the dialog is closed. Never rejects.
     */
    async renderInitiativeTest(name, step, strain = 0, karmaUse = KarmaRequirement.NO) {
        this.#action = ActionType.INITIATIVE;
        return this.#openDialog(name, step, strain, karmaUse);
    }
    /**
     * TODO: probably not needed if we treat the Wound Balance skill as a default and use that (even with rank 0) instead of Strength directly. 
     * 
     * Opens the dialog for a basic KnockDown test (without the help of a talent or skill) based on strength.
     * @param {number} step - Strength step.
     * @param {boolean} [canUseKarma=false] - If the character can use Karma for strength tests (through abilities).
     * @returns {Promise} - Returns a Promise that is resolved when the dialog is closed. Never rejects.
     */
    async renderKnockdownTest(step, canUseKarma = false) {
        this.#action = ActionType.KNOCKDOWN;
        return this.#openDialog("Knock Down Test", step, 0, canUseKarma ? KarmaRequirement.OPTIONAL : KarmaRequirement.NO);
    }
    /**
     * Opens the dialog for a basic attribute test that does not involve the usage of a talent or skill. Otherwise use one of the other methods.
     * @param {string} name - Name of the attribute, or if this is a basic damage test then name of the weapon being used or something like 'Unarmed Damage'. Is shown in the dialog title.
     * @param {number} step - The step of the attribute, or the sum of weapon step and attribute step for armed damage tests.
     * @param {boolean} [canUseKarma=false] - If the character can use Karma for the test (through abilities).
     * @param {ActionType} [action=ActionType.GENERAL] - Type of the action being performed, normally either DAMAGE (for basic damage tests with weapons) or GENERAL (for simple attribute tests, like spell effects) or KNOCKDOWN (for pure strength based knockdown tests).
     * @returns {Promise} - Returns a Promise that is resolved when the dialog is closed. Never rejects.
     */
    async renderAttributeTest(name, step, canUseKarma = false, action = ActionType.GENERAL) {
        this.#action = action;
        return this.#openDialog(name, step, 0, canUseKarma ? KarmaRequirement.OPTIONAL : KarmaRequirement.NO);
    }
    /**
     * @param {string} name - Name of the trait being used. Is shown in the dialog title.
     * @param {number} step - The step of the talent, skill or attribute that is being used.
     * @param {number} strain - The amount of strain the talent or skill use costs.
     * @param {KarmaRequirement} karmaUse - Information if karma use is possible or even required.
     * @param {?EquippedWeapon} [weapon=null] - If this is an attack with a weapon, the used weapon else null.
     * @returns {Promise} - Returns a Promise that is resolved when the dialog was closed. Never rejects.
     */
    async #openDialog(name, step, strain, karmaUse, weapon = null) {
        const modal = this.#presenter.getModal();
        modal.addTitle(name);
        modal.addCssClass("step-modding");
        const content = /** @type {HTMLElement} */ (this.#presenter.getElement("#global-templates #step-dice-modding").cloneNode(true));
        const applyButton = this.#prepareDialog(content, step, strain, karmaUse, weapon);
        modal.addContent(...content.children);
        this.#presenter.addObserverTo(applyButton, "click", () => {
            this.#applyChanges();
            modal.close();
        }, {once: true});
        return modal.open();
    }
    /**
     * @param {HTMLElement} container - The dialog template
     * @param {number} step - Step of the talent/skill/attribute/ini or any other trait
     * @param {number} strain - Strain required by the base trait
     * @param {KarmaRequirement} karmaUse - Is Karma use possible?
     * @param {?EquippedWeapon} [weapon=null] - If this an attack action or damage test with a weapon, else null. For damage tests the weapon's step is added to the given step (from e.g. an attribute or talent), for attacks the weapon's attack penalty is used.
     * @returns {HTMLButtonElement} - Returns the apply button element
     */
    #prepareDialog(container, step, strain, karmaUse, weapon = null) {
        this.#setupElements(container);
        if(this.#action === ActionType.DAMAGE && weapon) {
            step += weapon.getTotalDamageStep();
        }
        this.#step = step;
        this.#strain = strain;
        this.#presenter.getElement(".base [data-type=step]", container).textContent = step + '';
        this.#presenter.getElement(".base [data-type=strain]", container).textContent = strain + '';
        if(this.#character.getHealth().getStepPenaltyForWounds() !== 0) {
            this.#step += this.#character.getHealth().getStepPenaltyForWounds();
            this.#renderPredefinedPenalty("Wound penalty", this.#character.getHealth().getStepPenaltyForWounds(), container);
        }
        if(this.#action === ActionType.ATTACK && weapon && weapon.getAttackStepPenalty() !== 0) {
            this.#step += weapon.getAttackStepPenalty();
            this.#renderPredefinedPenalty("Weapon penalty", weapon.getAttackStepPenalty(), container);
        }
        const applyButton = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.apply", container));
        const karma = this.#elements.general.karma;
        switch(karmaUse) {
            case KarmaRequirement.NO: 
                karma.parentElement.hidden = true;
                break;
            case KarmaRequirement.OPTIONAL:
                karma.disabled = this.#character.getKarma().getAvailablePoints() === 0;
                break;
            case KarmaRequirement.REQUIRED:
                karma.checked = true;
                karma.disabled = true;
                if(this.#character.getKarma().getAvailablePoints() === 0) {
                    applyButton.disabled = true;
                    applyButton.textContent = "No Karma!";
                }
                break;
        }
        this.#renderConditions(container);
        this.#presenter.getElement(".attack", container).hidden = this.#action !== ActionType.ATTACK;
        this.#presenter.getElement(".damage", container).hidden = this.#action !== ActionType.DAMAGE;
        this.#elements.attack.tailAttack.parentElement.hidden = !this.#character.getRace().hasATail();
        this.#elements.damage.tailAttack.parentElement.hidden = !this.#character.getRace().hasATail();
        if(this.#action === ActionType.INITIATIVE) {
            this.#elements.general.resetRound.parentElement.hidden = false;
            this.#elements.general.resetRound.checked = true;
        }
        this.#toggleDependentInputs();
        this.#updateResult();
        return applyButton;
    }
    /**
     * @param {string} name 
     * @param {number} value 
     * @param {HTMLElement} container
     */
    #renderPredefinedPenalty(name, value, container) {
        const parent = this.#presenter.getElement(".base", container);
        const label = this.#presenter.createElement("span");
        label.className = "label";
        label.textContent = name;
        parent.append(label);
        parent.append(this.#presenter.createElement("span"));
        const penalty = this.#presenter.createElement("span");
        penalty.className = "value";
        penalty.textContent = value + '';
        parent.append(penalty);
        parent.append(this.#presenter.createElement("span"));
        parent.append(this.#presenter.createElement("span"));
    }
    /**
     * @param {HTMLElement} container
     */
    #renderConditions(container) {
        const parent = this.#presenter.getElement(".conditions", container);
        const conditions = this.#character.getConditions();
        this.#renderSingleCondition(conditions.getKnockedDown(), parent);
        this.#renderSingleCondition(conditions.getHarried(), parent);
        this.#renderSingleCondition(conditions.getStunned(), parent);
        this.#renderSingleCondition(conditions.getDarknessOrBlinded(), parent);
        this.#renderSingleCondition(conditions.getDefensiveStance(), parent);
        this.#renderSingleCondition(conditions.getSplitMovement(), parent);
        this.#renderSingleCondition(conditions.getTailAttack(), parent);
        this.#renderSingleCondition(conditions.getChangedOrReservedAction(), parent);
        parent.hidden = parent.children.length <= 1;
    }
    /**
     * @param {Condition} condition 
     * @param {HTMLElement} parent 
     */
    #renderSingleCondition(condition, parent) {
        const enabled = condition.isInEffect();
        if(enabled && condition.appliesTo(this.#action)) {
            const label = this.#presenter.createElement("span");
            label.textContent = condition.getName();
            const penalty = condition.getActionPenalty(this.#action);
            if(penalty !== 0) {
                label.textContent += ` (${penalty > 0 ? '+' : ''}${condition.getActionPenalty(this.#action)} steps)`;
            }
            const clear = this.#presenter.createElement("button");
            clear.className = "icon"
            const icon = this.#presenter.createElement("span");
            icon.className = "fa-solid fa-trash";
            clear.append(icon);
            parent.append(label, clear);
            this.#presenter.addObserverTo(clear, "click", () => {
                label.remove();
                clear.remove();
                if(parent.children.length <= 1) {
                    parent.hidden = true;
                }
                condition.clear();
                this.#toggleDependentInputs();
                this.#updateResult();
                this.#storeChange();    // immediately updates the character, even if the user later closes the dialog without clicking the apply button
            }, {once: true});
        }
    }
    #toggleDependentInputs() {
        if(this.#action === ActionType.DAMAGE || this.#action === ActionType.INITIATIVE || this.#action === ActionType.KNOCKDOWN) {
            this.#elements.general.defensiveStance.parentElement.parentElement.hidden = true;
        } else {
            this.#elements.general.defensiveStance.parentElement.hidden = this.#action === ActionType.ATTACK || this.#character.getConditions().getDefensiveStance().isInEffect();
            this.#elements.general.splitMovement.parentElement.hidden = this.#character.getConditions().getSplitMovement().isInEffect();
            this.#elements.general.reserveAction.parentElement.hidden = this.#character.getConditions().getChangedOrReservedAction().isInEffect();
        }
    }
    /** 
     * @param {HTMLElement} container - HTML container that contains the dialogs elements
     */
    #setupElements(container) {
        this.#elements.attack = {
            aggressiveAttack: this.#reactWith(/** @type {HTMLInputElement} */ (this.#presenter.getElement(".attack [data-type=aggressive-attack]", container)), 3, 1, ['attack', 'aiming', 'longRange']),
            calledShot: this.#reactWith(/** @type {HTMLInputElement} */ (this.#presenter.getElement(".attack [data-type=called-shot]", container)), -3, 1),
            aiming: this.#reactWith(/** @type {HTMLInputElement} */ (this.#presenter.getElement(".attack [data-type=aiming]", container)), 2, 1, ['attack', 'aggressiveAttack']),
            longRange: this.#reactWith(/** @type {HTMLInputElement} */ (this.#presenter.getElement(".attack [data-type=long-range]", container)), -2, 0, ['attack', 'aggressiveAttack']),
            improvised: this.#reactWith(/** @type {HTMLInputElement} */ (this.#presenter.getElement(".attack [data-type=improvised]", container)), -2),
            tailAttack: this.#reactWith(/** @type {HTMLInputElement} */ (this.#presenter.getElement(".attack [data-type=tail-attack]", container)), -2)
        };
        this.#elements.damage = {
            aggressiveAttack: this.#reactWith(/** @type {HTMLInputElement} */ (this.#presenter.getElement(".damage [data-type=aggressive-attack]", container)), 3, 0, ['damage', 'longRange']),
            tailAttack: this.#reactWith(/** @type {HTMLInputElement} */ (this.#presenter.getElement(".damage [data-type=tail-attack]", container)), 3, 0, ['damage', 'longRange']),
            longRange: this.#reactWith(/** @type {HTMLInputElement} */ (this.#presenter.getElement(".damage [data-type=long-range]", container)), -2, 0, ['damage', 'aggressiveAttack', 'tailAttack'])
        };
        this.#elements.general = {
            defensiveStance: /** @type {HTMLInputElement} */ (this.#presenter.getElement("input[data-type=defensive-stance]", container)),
            splitMovement: this.#reactWith(/** @type {HTMLInputElement} */ (this.#presenter.getElement("input[data-type=split-movement]", container)), -2, 1),
            reserveAction: /** @type {HTMLInputElement} */ (this.#presenter.getElement("input[data-type=reserve-action]", container)),
            karma: this.#reactWith(/** @type {HTMLInputElement} */ (this.#presenter.getElement("input[data-type=karma]", container)), 0),
            resetRound: /** @type {HTMLInputElement} */ (this.#presenter.getElement("input[data-type=reset-round]", container)),
        };
        this.#presenter.addObserverTo(this.#elements.general.defensiveStance, "change", () => {
            if(this.#action !== ActionType.DEFENSIVE) {
                this.#step += this.#elements.general.defensiveStance.checked ? -3 : 3;
                this.#updateResult();
            }
        });
        this.#elements.fields = {
            successLevels: /** @type {NumberField} */ (this.#presenter.getElement(".input [data-type=success]", container)),
            modifier: /** @type {NumberField} */ (this.#presenter.getElement(".input [data-type=modifier]", container))
        };
        ["successLevels", "modifier"].forEach(key => {
            this.#presenter.addObserverTo(this.#elements.fields[key], "change", () => {
                this.#updateResult();
            });
        });
        this.#elements.output = {
            strain: /** @type {HTMLElement} */ (this.#presenter.getElement(".calculated [data-type=strain]", container)),
            karma: /** @type {HTMLElement} */ (this.#presenter.getElement(".calculated [data-type=karma]", container)),
            step: /** @type {HTMLElement} */ (this.#presenter.getElement(".result [data-type=step]", container)),
            dice: /** @type {HTMLElement} */ (this.#presenter.getElement(".result [data-type=dice]", container))
        };
    }
    /**
     * Add change event listener to input element that adjusts accumulated step and strain depending on checked status
     * @param {HTMLInputElement} input - The input element to which we want to bind the event listener
     * @param {number} step - The step modifier to be applied when the input gets checked
     * @param {number} [strain=0] - The strain modifier to be applied when the input gets checked
     * @param {string[]} [exclusive=[]] - A list where the first entry is the overall this.#elements[category] and all further entries are keys in that category pointing to checkboxes that should be unchecked when the current input gets checked.
     * @returns {HTMLInputElement} - Returns the input
     */
    #reactWith(input, step, strain = 0, exclusive = []) {
        this.#presenter.addObserverTo(input, "change", () => {
            const invert = input.checked ? 1 : -1;
            this.#step += step * invert;
            this.#strain += strain * invert;
            this.#updateResult();
            if(input.checked && exclusive.length > 0) {
                const category = this.#elements[exclusive[0]];
                exclusive.slice(1).forEach(key => {
                    if(category[key].checked) {
                        category[key].checked = false;
                        category[key].dispatchEvent(new Event("change"));
                    }
                });
            }
        });
        return input;
    }
    #updateResult() {
        const finalStep = Math.max(1, this.#step + this.#elements.fields.successLevels.getValue() * 2 + this.#elements.fields.modifier.getValue() + this.#character.getConditions().getActionPenalty(this.#action));
        this.#elements.output.step.textContent = finalStep + '';
        let dice = Earthdawn.rules.getDiceForStep(finalStep);
        if(this.#elements.general.karma.checked) {
            dice += '+' + this.#character.getKarma().getDice();
            this.#elements.output.karma.textContent = '1';
        } else {
            this.#elements.output.karma.textContent = '0';
        }
        this.#elements.output.dice.textContent = dice;
        this.#elements.output.strain.textContent = this.#strain + '';
    }
    #applyChanges() {
        this.#character.getHealth().increaseTemporaryDamage(this.#strain, false);
        if(this.#elements.general.karma.checked) {
            this.#character.getKarma().spendOnePoint();
        }
        const conditions = this.#character.getConditions();
        if(this.#elements.general.resetRound.checked) {
            conditions.resetRoundConditions();
        }
        if(this.#elements.attack.aggressiveAttack.checked || this.#elements.damage.aggressiveAttack.checked) {
            conditions.getAggressiveAttack().setState(true);
        }
        if(this.#elements.attack.tailAttack.checked || this.#elements.damage.tailAttack.checked) {
            conditions.getTailAttack().setState(true);
        }
        if(this.#elements.general.defensiveStance.checked) {
            conditions.getDefensiveStance().setState(true);
        }
        if(this.#elements.general.splitMovement.checked) {
            conditions.getSplitMovement().setState(true);
        }
        if(this.#elements.general.reserveAction.checked) {
            conditions.getChangedOrReservedAction().setState(true);
        }
        this.#storeChange();
        // Afterwards opener of the dialog must re-render the view pages that are affected by the conditions
    }
}



/**
 * The presenter for the HTML section that displays an existing {@link Character}. This is the default view of a single character during play.
 * Most of its UI is read-only except for some regularly changing values like current damage or karma points. All other charateristics that are
 * quite static until changed via legend points can be edited only via the {@link CharacterEditPresenter}.
 * This class provides some reusable methods for displaying details of skills, talents and spells.
 * @extends AbstractSectionPresenter
 */
class CharacterViewPresenter extends AbstractSectionPresenter {
    #presenter;
    /** @type {HTMLElement} - Root element of this presenter's HTML section */
    #section;
    /** @type {DynamicNavbarScroller} - Navigation component of this section */
    #navbar;
    /** @type {Character} - Displayed character */
    #character;
    /** @type {CharacterRecord} - Storage record of the displayed character */
    #record;
    /** @type {boolean} - Flag to signal that this presenter should be opened in read-only mode. In this mode all modifications of the character are forbidden and the editing views cannot be opened from here. Used to display shared or conflicting characters from the cloud. */
    #readOnly;
    /**
     * @param {Presenter} presenter - Window presenter 
     */
    constructor(presenter) {
        super();
        this.#presenter = presenter;
        this.#section = presenter.getElement("#view-character");
        this.#navbar = this.#getAndSetupNavbar(presenter, this.#section);
        this.#readOnly = false;

        this.#presenter.addObserverTo(this.#presenter.getElement(".header button.cancel", this.#section), "click", this.#presenter.getHistory().leaveSection);
        this.#presenter.addObserverTo(this.#presenter.getElement(".header button.edit", this.#section), "click", () => this.#presenter.enterSection(CharacterEditPresenter.sectionName, this.#record));
        this.#renderStepTable();
        this.#setupKarma();
        this.#setupConditions();
        this.#setupDamageAndHealing();
        this.#setupRecoveryTests();
        this.#setupWoundsDialog();
        this.#setupSpellcasting();
    }
    static get sectionName() {
        return "view-character";
    }
    /**
     * @override
     */
    hide() {
        this.#section.hidden = true;
        this.#readOnly = false;
        this.#record = null;
        this.#character = null;
    }
    /**
     * @override
     * @param {CharacterRecord} characterRecord - Record of the character to display
     * @param {boolean} [readOnly=false] - Flag if this presenter should be opened in read-only mode. In this mode all modifications of the character are forbidden. This mode is used mainly to display remote characters that have not local representation.
     */
    show(characterRecord, readOnly = false) {
        this.#record = characterRecord;
        this.#readOnly = readOnly;
        this.#section.classList.toggle("readonly", readOnly);
        characterRecord.getCharacter().then((character) => {
            this.#character = character;
            /** @type {HTMLButtonElement} */ (this.#presenter.getElement(".header button.edit", this.#section)).disabled = this.#readOnly;
            this.#adjustNavBar();
            this.#renderPages(); 
            this.#section.hidden = false;
            this.#navbar.reset();
        });
    }
    #adjustNavBar() {
        if(this.#character.getSpells().length > 0) {
            this.#navbar.includePage(4);
        } else {
            this.#navbar.excludePage(4);
        }
    }
    #renderPages() {
        const effectiveAbilities = this.#character.getAllEffectiveAbilities();
        this.#renderBasics();
        this.#renderAttributes(effectiveAbilities);
        this.#renderCharacteristics();
        this.#renderConditions();
        this.#renderHealth();
        this.#renderTalents();
        this.#renderSkills();
        this.#renderSpells();
        this.#renderAbilities();
        this.#renderEquipment();
    }
    #renderStepTable() {
        const steps = [];
        for(let step = 1; step <= 40; ++step) {
            const row = this.#presenter.createElement("div");
            row.className = "row";
            const label = this.#presenter.createElement("span");
            label.textContent = step + '';
            row.append(label);
            const value = this.#presenter.createElement("span");
            value.textContent =Earthdawn.rules.getDiceForStep(step);
            row.append(value);
            steps.push(row);
        }
        this.#presenter.getElement(".basics .step-table .table-body", this.#section).replaceChildren(...steps);
    }
    #renderBasics() {
        const character = this.#character;
        this.#presenter.getElement(".header .name span[data-type=text]", this.#section).textContent = character.getName();
        this.#presenter.getElement(".basics span[data-type=name]", this.#section).textContent = character.getName();
        this.#presenter.getElement(".basics span[data-type=id]", this.#section).textContent = character.getId();
        this.#presenter.getElement(".basics span[data-type=race]", this.#section).textContent = character.getRace().getName();
        const disciplineContainer = this.#presenter.getElement(".basics div[data-type=disciplines] .content", this.#section);
        disciplineContainer.replaceChildren();
        character.getDisciplines().forEach(discipline => {
            const name = this.#presenter.createElement("span");
            name.className = "text";
            name.textContent = discipline.getData().getName();
            disciplineContainer.append(name);
            const circle = this.#presenter.createElement("span");
            circle.className = "value";
            circle.textContent = discipline.getCircle() + '';
            disciplineContainer.append(circle);
        });
        this.#presenter.getElement(".basics .karma span[data-type=step]", this.#section).textContent = character.getKarma().getStep() + '';
        this.#presenter.getElement(".basics .karma span[data-type=dice]", this.#section).textContent = character.getKarma().getDice();
        this.#presenter.getElement(".basics .karma output[data-type=current]", this.#section).textContent = character.getKarma().getAvailablePoints() + '';
        this.#presenter.getElement(".basics .karma span[data-type=maximum]", this.#section).textContent = character.getKarma().getPossibleMaximum() + '';
        this.#presenter.getElement(".basics .encumberance span[data-type=carry]", this.#section).textContent = character.getEncumberance().getCarry() + '';
        this.#presenter.getElement(".basics .encumberance span[data-type=lift]", this.#section).textContent = character.getEncumberance().getLift() + '';
        this.#presenter.getElement(".basics .movement span[data-type=combat]", this.#section).textContent = character.getMovement().map(move => `${move.getCombat()} (${move.getType()})`).join(" / ");
        this.#presenter.getElement(".basics .movement span[data-type=full]", this.#section).textContent = character.getMovement().map(move => `${move.getFull()} (${move.getType()})`).join(" / ");        
    }
    /**
     * @param {Ability[]} abilities - All effective abilities of this character
     */
    #renderAttributes(abilities) {
        const attributes = this.#presenter.getElement(".characteristics .attributes .content", this.#section);
        attributes.replaceChildren();
        const template = this.#presenter.getElement(".characteristics .attributes .attribute-template", this.#section);
        AttributeIds.getIds().forEach(id => {
            const attribute = this.#character.getAttribute(id);
            const canUseKarma = abilities.some(ability => ability.getType() === AbilityType.KARMA_USE && ability.getAffectedTraitId() === id);
            const row = /** @type {HTMLElement} */ (template.cloneNode(true));
            this.#presenter.getElement(".label", row).textContent = attribute.getName();
            this.#presenter.getElement("span[data-type=step]", row).textContent = attribute.getStep() + '';
            const dice = this.#presenter.getElement("span[data-type=dice]", row);
            dice.textContent = attribute.getDice();
            this.#presenter.addObserverTo(dice, "click", () => {
                new StepModDialog(this.#presenter, this.#character, () => this.#storeChange()).renderAttributeTest(attribute.getName(), attribute.getStep(), canUseKarma).then(() => {
                    this.#renderBasics();
                    this.#renderDefenses();
                    this.#renderConditions();
                });
            });
            const karmaUse = canUseKarma ? ["fa-regular", "fa-circle-check"] : ["fa-regular", "fa-circle"];
            this.#presenter.getElement("span[data-type=karma]", row).classList.add(...karmaUse);
            attributes.append(...row.children);
        });
    }
    #renderInitiative() {
        const character = this.#character;
        const template = this.#presenter.getElement(".characteristics .initiative-row-template", this.#section);
        const table = this.#presenter.getElement(".characteristics .initiative .table-body", this.#section);
        table.replaceChildren();
        let row = /** @type {HTMLElement} */ (template.cloneNode(true));
        const karmaRequirement = character.getInitiative().canUseKarma() ? KarmaRequirement.OPTIONAL : KarmaRequirement.NO;
        this.#presenter.getElement("span[data-type=step]", row).textContent = character.getInitiative().getStep() + '';
        let dice = this.#presenter.getElement("span[data-type=dice]", row);
        dice.textContent = character.getInitiative().getDice();
        this.#presenter.addObserverTo(dice, "click", () => {
            new StepModDialog(this.#presenter, character, () => this.#storeChange()).renderInitiativeTest("Initiative", character.getInitiative().getStep(), 0, karmaRequirement).then(() => {
                this.#renderBasics();
                this.#renderDefenses();
                this.#renderConditions();
            });
        });
        let karmaUse = ["fa-regular", "fa-circle"];
        if(character.getInitiative().canUseKarma()) {
            karmaUse = ["fa-regular", "fa-circle-check"];
        }
        this.#presenter.getElement("span[data-type=karma]", row).classList.add(...karmaUse);
        this.#presenter.getElement("span[data-type=strain]", row).textContent = '0';
        table.append(...row.children);
        const tigerSpring = character.findTalent(KnownTalent.TIGER_SPRING.getId());
        if(tigerSpring) {
            row = /** @type {HTMLElement} */ (template.cloneNode(true));
            const label = this.#presenter.getElement("span.label", row);
            label.innerHTML = ` &#x2022; ${tigerSpring.getData().getName()}`;
            this.#presenter.addObserverTo(label, "click", () => this.showTalentDetails(tigerSpring));
            const step = character.getInitiative().getStep() + tigerSpring.getTotalRank();
            this.#presenter.getElement("span[data-type=step]", row).textContent = step + '';
            dice = this.#presenter.getElement("span[data-type=dice]", row);
            dice.textContent = Earthdawn.rules.getDiceForStep(step);
            this.#presenter.addObserverTo(dice, "click", () => {
                new StepModDialog(this.#presenter, character, () => this.#storeChange()).renderInitiativeTest(tigerSpring.getData().getName(), step, tigerSpring.getData().getStrain(), karmaRequirement).then(() => {
                    this.#renderBasics();
                    this.#renderDefenses();
                    this.#renderConditions();
                    this.#renderHealth();
                });
            });
            this.#presenter.getElement("span[data-type=karma]", row).classList.add(...karmaUse);    // as this is a passive talent, apply karma rules of normal initiative
            this.#presenter.getElement("span[data-type=strain]", row).textContent = tigerSpring.getData().getStrain() + '';
            table.append(...row.children);
        }
        [KnownTalent.AIR_DANCE, KnownTalent.COBRA_STRIKE].forEach(knt => {
            const talent = character.findTalent(knt.getId());
            if(talent) {
                table.append(...this.#renderActiveIniTalent(talent, template, tigerSpring));
            }
        });
    }
    /**
     * @param {Talent} talent - The initiative talent to render
     * @param {HTMLElement} template - Row template
     * @param {?Talent} [tigerSpring=null] - Tiger Spring talent if the character possesses it else null
     * @returns {Element[]} - Collection of HTML elements to be added to the initiative table
     */
    #renderActiveIniTalent(talent, template, tigerSpring = null) {
        const result = [];
        let row = /** @type {HTMLElement} */ (template.cloneNode(true));
        const step = this.#character.getInitiative().getStep() + talent.getTotalRank();
        const karmaUse = ["fa-regular"];
        switch(talent.getKarmaRequirement()) {
            case KarmaRequirement.NO:
                karmaUse.push("fa-circle");
                break;
            case KarmaRequirement.OPTIONAL:
                karmaUse.push("fa-circle-check");
                break;
            case KarmaRequirement.REQUIRED:
                karmaUse.push("fa-circle-dot");
                break;
        }
        const label = this.#presenter.getElement("span.label", row);
        label.innerHTML = ` &#x2022; ${talent.getData().getName()}`;
        this.#presenter.addObserverTo(label, "click", () => this.showTalentDetails(talent));
        this.#presenter.getElement("span[data-type=step]", row).textContent = step + '';
        let dice = this.#presenter.getElement("span[data-type=dice]", row);
        dice.textContent = Earthdawn.rules.getDiceForStep(step);
        this.#presenter.addObserverTo(dice, "click", () => {
            new StepModDialog(this.#presenter, this.#character, () => this.#storeChange()).renderForTalent(talent).then(() => {
                this.#renderBasics();
                this.#renderDefenses();
                this.#renderConditions();
                this.#renderHealth();
            });
        });
        this.#presenter.getElement("span[data-type=karma]", row).classList.add(...karmaUse);
        this.#presenter.getElement("span[data-type=strain]", row).textContent = talent.getData().getStrain() + '';
        result.push(...row.children);
        if(tigerSpring) {
            row = /** @type {HTMLElement} */ (template.cloneNode(true));
            const step = this.#character.getInitiative().getStep() + talent.getTotalRank() + tigerSpring.getTotalRank();
            this.#presenter.getElement("span.label", row).innerHTML = ` &#x2022; ${tigerSpring.getData().getName()} + ${talent.getData().getName()}`;
            this.#presenter.getElement("span[data-type=step]", row).textContent = step + '';
            dice = this.#presenter.getElement("span[data-type=dice]", row);
            dice.textContent = Earthdawn.rules.getDiceForStep(step);
            this.#presenter.addObserverTo(dice, "click", () => {
                const name = `${tigerSpring.getData().getName()} + ${talent.getData().getName()}`;
                new StepModDialog(this.#presenter, this.#character, () => this.#storeChange()).renderInitiativeTest(name, step, talent.getData().getStrain() + tigerSpring.getData().getStrain(), talent.getKarmaRequirement()).then(() => {
                    this.#renderBasics();
                    this.#renderDefenses();
                    this.#renderConditions();
                    this.#renderHealth();
                });
            });
            this.#presenter.getElement("span[data-type=karma]", row).classList.add(...karmaUse);
            this.#presenter.getElement("span[data-type=strain]", row).textContent = talent.getData().getStrain() + tigerSpring.getData().getStrain() + '';
            result.push(...row.children);
        }
        return result;
    }
    #renderCharacteristics() {
        this.#renderInitiative();
        this.#renderDefenses();
        const character = this.#character;
        this.#presenter.getElement(".characteristics .armor span[data-type=physical]", this.#section).textContent = character.getArmorRating().getPhysical() + '';
        this.#presenter.getElement(".characteristics .armor span[data-type=mystic]", this.#section).textContent = character.getArmorRating().getMystic() + '';
    }
    #renderDefenses() {
        const character = this.#character;
        const mods = character.getConditions().getDefenseModification();
        ['physical', 'spell', 'social'].forEach(key => {
            const elem = this.#presenter.getElement(`.characteristics .defenses span[data-type=${key}]`, this.#section);
            /** @type {DefenseType} */
            const type = DefenseType[key.toUpperCase()];
            const unmodified = character.getDefense(type).getTotal();
            if(mods[type.id] !== 0) {
                const final = unmodified + mods[type.id];
                elem.textContent = `${unmodified} (${final})`;
            } else {
                elem.textContent = unmodified + '';
            }
        });
    }
    #renderConditions() {
        const conditions = this.#character.getConditions();
        const list = this.#presenter.getElement(".characteristics .conditions .list", this.#section);
        list.replaceChildren();
        const show = (condition, klass) => {
            if(condition === true || (condition && condition.isInEffect())) {
                const icon = this.#presenter.createElement("span");
                icon.className = klass;
                list.append(icon);
            }
        };
        show(conditions.getAggressiveAttack(), "fa-solid fa-hand-fist");
        show(conditions.getDefensiveStance(), "fa-solid fa-user-shield");
        show(conditions.getSplitMovement(), "fa-solid fa-arrows-split-up-and-left");
        show(conditions.getTailAttack(), "fa-brands fa-suse");
        show(conditions.getChangedOrReservedAction(), "fa-regular fa-clock");
        show(conditions.getKnockedDown(), "fa-solid fa-person-falling");
        show(conditions.getHarried(), "fa-solid fa-people-group");
        show(conditions.getStunned(), "fa-solid fa-dizzy");
        show(conditions.getBlindsided(), "fa-solid fa-ghost");
        show(conditions.getSurprised(), "fa-regular fa-surprise");
        show(conditions.getDarknessOrBlinded().getState() === VariableConditionState.PARTIAL, "fa-solid fa-eye-low-vision");
        show(conditions.getDarknessOrBlinded().getState() === VariableConditionState.FULL, "fa-solid fa-eye-slash");
        show(conditions.getCover().getState() === VariableConditionState.PARTIAL, "fa-solid fa-shield-halved");
        show(conditions.getCover().getState() === VariableConditionState.FULL, "fa-solid fa-shield");
        if(list.children.length === 0) {
            const none = this.#presenter.createElement("span");
            none.innerHTML = "&#x2015;&#x2015;";
            list.append(none);
        }
    }
    #renderConditionsModal() {
        const conditions = this.#character.getConditions();
        const modal = this.#presenter.getModal();
        modal.addTitle("Conditions");
        const content = /** @type {HTMLElement} */ (this.#presenter.getElement(".conditions-modal", this.#section).cloneNode(true));
        content.hidden = false;
        const setup = (condition, type) => {
            const elem = /** @type {HTMLInputElement} */ (this.#presenter.getElement(`input[data-type=${type}]`, content));
            elem.checked = condition.isInEffect();
            this.#presenter.addObserverTo(elem, "change", () => {
                condition.setState(elem.checked);
            });
        };
        setup(conditions.getAggressiveAttack(), "aggressive-attack");
        setup(conditions.getDefensiveStance(), "defensive-stance");
        setup(conditions.getSplitMovement(), "split-movement");
        setup(conditions.getChangedOrReservedAction(), "reserved-action");
        setup(conditions.getKnockedDown(), "knocked-down");
        setup(conditions.getHarried(), "harried");
        setup(conditions.getBlindsided(), "blindsided");
        setup(conditions.getStunned(), "stunned");
        setup(conditions.getSurprised(), "surprised");
        if(this.#character.getRace().hasATail()) {
            setup(conditions.getTailAttack(), "tail-attack");
        } else {
            const dt = this.#presenter.getElement("input[data-type=tail-attack]", content).parentElement.parentElement;
            dt.hidden = true;
            /** @type {HTMLElement} */ (dt.nextElementSibling).hidden = true;
        }
        const partialDarkness = /** @type {HTMLInputElement} */ (this.#presenter.getElement("input[data-type=partial-darkness]", content));
        const fullDarkness = /** @type {HTMLInputElement} */ (this.#presenter.getElement("input[data-type=full-darkness]", content));
        partialDarkness.checked = conditions.getDarknessOrBlinded().getState() === VariableConditionState.PARTIAL;
        this.#presenter.addObserverTo(partialDarkness, "change", () => {
            if(partialDarkness.checked) {
                conditions.getDarknessOrBlinded().setState(VariableConditionState.PARTIAL);
                fullDarkness.checked = false;
            } else {
                conditions.getDarknessOrBlinded().setState(VariableConditionState.NO);
            }
        });
        fullDarkness.checked = conditions.getDarknessOrBlinded().getState() === VariableConditionState.FULL;
        this.#presenter.addObserverTo(fullDarkness, "change", () => {
            if(fullDarkness.checked) {
                conditions.getDarknessOrBlinded().setState(VariableConditionState.FULL);
                partialDarkness.checked = false;
            } else {
                conditions.getDarknessOrBlinded().setState(VariableConditionState.NO);
            }
        });
        const partialCover = /** @type {HTMLInputElement} */ (this.#presenter.getElement("input[data-type=partial-cover]", content));
        const fullCover = /** @type {HTMLInputElement} */ (this.#presenter.getElement("input[data-type=full-cover]", content));
        partialCover.checked = conditions.getCover().getState() === VariableConditionState.PARTIAL;
        this.#presenter.addObserverTo(partialCover, "change", () => {
            if(partialCover.checked) {
                conditions.getCover().setState(VariableConditionState.PARTIAL);
                fullCover.checked = false;
            } else {
                conditions.getCover().setState(VariableConditionState.NO);
            }
        });
        fullCover.checked = conditions.getCover().getState() === VariableConditionState.FULL;
        this.#presenter.addObserverTo(fullCover, "change", () => {
            if(fullCover.checked) {
                conditions.getCover().setState(VariableConditionState.FULL);
                partialCover.checked = false;
            } else {
                conditions.getCover().setState(VariableConditionState.NO);
            }
        });
        this.#presenter.addObserverTo(this.#presenter.getElement("button.reset", content), "click", () => {
            ["aggressive-attack",  "defensive-stance", "split-movement", "tail-attack", "reserved-action", "blindsided", "surprised"].forEach(type => {
                const elem = /** @type {HTMLInputElement} */ (this.#presenter.getElement(`input[data-type=${type}]`, content));
                elem.checked = false;
                elem.dispatchEvent(new Event("change"));
            });
        });
        this.#presenter.addObserverTo(this.#presenter.getElement("button.clear", content), "click", () => {
            this.#presenter.findElements("input[type=checkbox]", content).forEach(/** @param {HTMLInputElement} elem */ elem => {
                elem.checked = false;
                elem.dispatchEvent(new Event("change"));
            });
        });
        modal.addContent(content);
        modal.open().then(() => {
            this.#renderConditions();
            this.#renderDefenses();
            this.#storeChange();
        });
    }
    #renderHealth() {
        const character = this.#character;
        const health = this.#presenter.getElement(".characteristics .health", this.#section);
        this.#presenter.getElement("[data-type=unconsciousness]", health).textContent = character.getHealth().getUnconsciousnessRating() + '';
        this.#presenter.getElement("[data-type=currentDamage]", health).textContent = character.getHealth().getTemporaryDamage() + '';
        this.#presenter.getElement("[data-type=death]", health).textContent = character.getHealth().getDeathRating() + '';
        this.#presenter.getElement("[data-type=totalDamage]", health).textContent = character.getHealth().getTotalDamage() + '';
        this.#presenter.getElement("[data-type=woundThreshold]", health).textContent = character.getHealth().getWoundThreshold() + '';
        this.#presenter.getElement("[data-type=wounds]", health).textContent = character.getHealth().getWounds() + '';
        this.#presenter.getElement("[data-type=recoveryStep]", health).textContent = character.getHealth().getRecoveryStep() + '';
        this.#presenter.getElement("[data-type=recoveryDice]", health).textContent = character.getHealth().getRecoveryDice();
        this.#presenter.getElement("[data-type=recoveryDays]", health).textContent = character.getHealth().getDaysForRecoveryTestFresh() === 1 ? "Day" : `${character.getHealth().getDaysForRecoveryTestFresh()} Days`;
        this.#presenter.getElement("[data-type=availableTests]", health).textContent = character.getHealth().getAvailableRecoveryTests() + '';
        this.#presenter.getElement("[data-type=recoveryMax]", health).textContent = character.getHealth().getRecoveryTestsPerRefreshPeriod() + '';
    }
    #renderTalents() {
        const allTalents = [];
        this.#character.getDisciplines().forEach(discipline => {
            discipline.getLearnedTalents().forEach(byCircle => {
                byCircle.forEach(talent => {
                    allTalents.push(talent);
                });
            });
        });
        allTalents.sort(Talent.compare);
        const actionCss = [["fa-regular", "fa-circle"], ["fa-regular", "fa-circle-dot"]];
        const karmaCss = {
            required: ["fa-regular", "fa-circle-dot"],
            optional: ["fa-regular", "fa-circle-check"],
            no: ["fa-regular", "fa-circle"]
        };
        const container = this.#presenter.getElement(".talents .table-body", this.#section);
        container.replaceChildren();
        const template = this.#presenter.getElement(".talents .row-template", this.#section);
        allTalents.forEach(talent => {
            const row = /** @type {HTMLElement} */ (template.cloneNode(true));
            const name = this.#presenter.getElement("[data-type=name]", row);
            name.textContent = talent.getData().getName();
            this.#presenter.addObserverTo(name, "click", () => this.showTalentDetails(talent));
            this.#presenter.getElement("[data-type=action]", row).classList.add(...actionCss[talent.getData().needsAction() - 0]);
            this.#presenter.getElement("[data-type=karma]", row).classList.add(...karmaCss[talent.getKarmaRequirement()]);
            this.#presenter.getElement("[data-type=strain]", row).textContent = talent.getData().getStrain();
            this.#presenter.getElement("[data-type=rank]", row).textContent = talent.getTotalRank();
            if(talent.hasStep()) {
                this.#presenter.getElement("[data-type=attribute]", row).textContent = talent.getAttribute().getName();
                this.#presenter.getElement("[data-type=step]", row).textContent = talent.getStep();
                const dice = this.#presenter.getElement("[data-type=dice]", row);
                dice.classList.add("modable");
                dice.textContent = talent.getDice();
                this.#presenter.addObserverTo(dice, "click", () => {
                    new StepModDialog(this.#presenter, this.#character, () => this.#storeChange()).renderForTalent(talent).then(() => {
                        this.#renderBasics();
                        this.#renderDefenses();
                        this.#renderConditions();
                        this.#renderHealth();
                    });
                });
            } else {
                this.#presenter.getElement("[data-type=attribute]", row).textContent = "-";
                this.#presenter.getElement("[data-type=step]", row).textContent = "-";
                this.#presenter.getElement("[data-type=dice]", row).textContent = "-";
            }
            container.append(...row.children);
        });
    }
    /**
     * Shows details of a learned talent in a modal dialog. Reusable by other presenters.
     * @param {Talent} talent - Talent to render
     */
    showTalentDetails(talent) {
        let sourceText = "";
        switch(talent.getSource()) {
            case TalentSource.CORE:
                sourceText = "Core Talent";
                break;
            case TalentSource.OPTION:
                sourceText = "Talent Option";
                break;
            case TalentSource.VERSATILITY:
                sourceText = "Versatility";
                break;
            case TalentSource.ABILITY:
                sourceText = "Ability";
                break;
        }
        const modal = this.#presenter.getModal();
        modal.addTitle(talent.getData().getName());
        modal.addCssClass("talent");
        const content = /** @type {HTMLElement} */ (this.#presenter.getElement("#talent-details").children[0].cloneNode(true));
        this.#presenter.getElement("span[data-type=freeRank]", content).textContent = talent.getFreeRanks() + '';
        this.#presenter.getElement("span[data-type=rank]", content).textContent = talent.getTotalRank() + '';
        this.#presenter.getElement("span[data-type=step]", content).textContent = talent.hasStep() ? (talent.getStep() + '') : 'n/a';
        this.#presenter.getElement("span[data-type=dice]", content).textContent = talent.hasStep() ? talent.getDice() : 'n/a';
        this.#presenter.getElement("span[data-type=attribute]", content).textContent = talent.hasStep() ? talent.getAttribute().getName() : 'n/a';
        this.#presenter.getElement("span[data-type=action]", content).textContent = talent.getData().needsAction() ? "yes" : "no";
        this.#presenter.getElement("span[data-type=karma]", content).textContent = talent.getKarmaRequirement();
        this.#presenter.getElement("span[data-type=strain]", content).textContent = talent.getData().getStrain() + '';
        this.#presenter.getElement("span[data-type=source]", content).textContent = sourceText;
        this.#presenter.getElement("span[data-type=circle]", content).textContent = talent.getBoughtOnCircle() + '';
        this.#presenter.getElement(".description", content).textContent = talent.getData().getDescription();
        modal.addContent(content);
        modal.open();
    }
    #renderSkills() {
        const allSkills = this.#character.getSkills(true).concat();    // create shallow copy
        allSkills.sort(Skill.compare);
        const actionCss = [["fa-regular", "fa-circle"], ["fa-regular", "fa-circle-dot"]];
        const karmaCss = [["fa-regular", "fa-circle"], ["fa-regular", "fa-circle-check"]];
        const container = this.#presenter.getElement(".skills .table-body", this.#section);
        container.replaceChildren();
        const template = this.#presenter.getElement(".skills .row-template", this.#section);
        allSkills.forEach(skill => {
            const row = /** @type {HTMLElement} */ (template.cloneNode(true));
            const name = this.#presenter.getElement("[data-type=name]", row);
            name.textContent = skill.getData().getName();
            if(skill.getTotalRank() === 0) {
                name.innerHTML = `${skill.getData().getName()} <span class="fa-regular fa-star-half annotation"></span>`;
            }
            this.#presenter.addObserverTo(name, "click", () => this.showSkillDetails(skill));
            this.#presenter.getElement("[data-type=action]", row).classList.add(...actionCss[Number(skill.getData().needsAction())]);
            this.#presenter.getElement("[data-type=karma]", row).classList.add(...karmaCss[Number(skill.canUseKarma())]);
            this.#presenter.getElement("[data-type=strain]", row).textContent = skill.getData().getStrain()+ '';
            this.#presenter.getElement("[data-type=rank]", row).textContent = skill.getTotalRank() + '';
            this.#presenter.getElement("[data-type=attribute]", row).textContent = skill.getAttribute().getName();
            this.#presenter.getElement("[data-type=step]", row).textContent = skill.getStep() + '';
            const dice = this.#presenter.getElement("[data-type=dice]", row);
            dice.classList.add("modable");
            dice.textContent = skill.getDice();
            this.#presenter.addObserverTo(dice, "click", () => {
                new StepModDialog(this.#presenter, this.#character, () => this.#storeChange()).renderForSkill(skill).then(() => {
                    this.#renderBasics();
                    this.#renderDefenses();
                    this.#renderConditions();
                    this.#renderHealth();
                });
            });
            container.append(...row.children);
        });
    }
    /**
     * Show details of a learned skill in a modal dialog. Reusable by other presenters.
     * @param {Skill} skill - The skill to render 
     */
    showSkillDetails(skill) {      
        const modal = this.#presenter.getModal();
        modal.addTitle(skill.getData().getName());
        modal.addCssClass("skill");
        const content = /** @type {HTMLElement} */ (this.#presenter.getElement("#skill-details").children[0].cloneNode(true));
        this.#presenter.getElement("span[data-type=freeRank]", content).textContent = skill.getFreeRanks() + '';
        this.#presenter.getElement("span[data-type=rank]", content).textContent = skill.getTotalRank() + '';
        this.#presenter.getElement("span[data-type=step]", content).textContent = skill.getStep() + '';
        this.#presenter.getElement("span[data-type=dice]", content).textContent = skill.getDice();
        this.#presenter.getElement("span[data-type=attribute]", content).textContent = skill.getAttribute().getName();
        this.#presenter.getElement("span[data-type=action]", content).textContent = skill.getData().needsAction() ? "yes" : "no";
        this.#presenter.getElement("span[data-type=karma]", content).textContent = skill.canUseKarma() ? "optional" : "no";
        this.#presenter.getElement("span[data-type=strain]", content).textContent = skill.getData().getStrain() + '';
        this.#presenter.getElement(".description", content).textContent = skill.getData().getDescription();
        modal.addContent(content);
        modal.open();
    }
    #renderSpellcastingTalents() {
        const spellCasting = this.#character.findTalent(KnownTalent.SPELLCASTING.getId(), 1);
        this.#presenter.getElement(".spells .casting-talents", this.#section).hidden = !spellCasting;   // unlikely but possible that someone has Thread Weaving (to learn spells) but no spellcasting to actually use them.
        if(!spellCasting) {
            return;
        }
        const casting = this.#presenter.getElement(".spells .spellcasting", this.#section);
        this.#presenter.getElement(".label", casting).textContent = spellCasting.getData().getName();
        this.#presenter.getElement("[data-type=rank]", casting).textContent = spellCasting.getTotalRank() + '';
        this.#presenter.getElement("[data-type=dice]", casting).textContent = spellCasting.getDice();

        const weaving = this.#presenter.getElement(".spells .thread-weaving", this.#section);
        weaving.replaceChildren();
        const template = this.#presenter.getElement(".spells .thread-weaving-template", this.#section);
        for(const [id, threadWeaving] of this.#character.getAccessToSpells()) {
            if(!threadWeaving) {
                continue;
            }
            const copy = /** @type {HTMLElement} */ (template.cloneNode(true));
            this.#presenter.getElement(".label", copy).textContent = threadWeaving.getData().getName();
            this.#presenter.getElement("[data-type=step]", copy).textContent = threadWeaving.getStep() + '';
            const dice = this.#presenter.getElement("[data-type=dice]", copy);
            dice.textContent = threadWeaving.getDice();
            this.#presenter.addObserverTo(dice, "click", () => {
                new StepModDialog(this.#presenter, this.#character, () => this.#storeChange()).renderForTalent(threadWeaving).then(() => {
                    this.#renderBasics();
                    this.#renderDefenses();
                    this.#renderConditions();
                    this.#renderHealth();
                });
            });
            weaving.append(...copy.children);
        }

        const will = this.#presenter.getElement(".spells .will", this.#section);
        const willforce = this.#character.findTalent(KnownTalent.WILLFORCE.getId());
        if(willforce) {
            this.#presenter.getElement(".label", will).textContent = willforce.getData().getName();
            this.#presenter.getElement("[data-type=step]", will).textContent = willforce.getStep() + '';
            const dice = this.#presenter.getElement("[data-type=dice]", will);
            dice.textContent = willforce.getDice();
            this.#presenter.addObserverTo(dice, "click", () => {
                new StepModDialog(this.#presenter, this.#character, () => this.#storeChange()).renderForTalent(willforce).then(() => {
                    this.#renderBasics();
                    this.#renderDefenses();
                    this.#renderConditions();
                    this.#renderHealth();
                });
            });
        } else {
            const willpower = this.#character.getAttribute(AttributeIds.WILLPOWER);
            const canUseKarma = this.#character.getDisciplineAbilities().some(ability => ability.getType() === AbilityType.KARMA_USE && ability.getAffectedTraitId() === willpower.getId());
            this.#presenter.getElement(".label", will).textContent = willpower.getName();
            this.#presenter.getElement("[data-type=step]", will).textContent = willpower.getStep() + '';
            const dice = this.#presenter.getElement("[data-type=dice]", will);
            dice.textContent = willpower.getDice();
            this.#presenter.addObserverTo(dice, "click", () => {
                new StepModDialog(this.#presenter, this.#character, () => this.#storeChange()).renderAttributeTest(willpower.getName(), willpower.getStep(), canUseKarma).then(() => {
                    this.#renderBasics();
                    this.#renderDefenses();
                    this.#renderConditions();
                });
            });
        }
    }
    #renderSpells() {
        if(this.#character.getSpells().length === 0) {
            return;
        }
        this.#renderSpellcastingTalents();
        const highestMatrix = this.#character.getSpellMatrices().reduce((accu, talent) => Math.max(accu, talent.getTotalRank()), 0);
        const circleSafeCastable = Math.min(highestMatrix, this.#character.getHighestAchievedCircle());

        const rowTemplate = this.#presenter.getElement(".spells .row-template", this.#section);
        const tableBody =  this.#presenter.getElement(".spells .table-body", this.#section);
        tableBody.replaceChildren();
        this.#character.getSpells().forEach(spell => {
            const row = /** @type {HTMLElement} */ (rowTemplate.cloneNode(true));
            this.#presenter.getElement("[data-type=circle]", row).textContent = spell.getData().getCircle() + '';
            const name = this.#presenter.getElement("[data-type=name]", row);
            name.textContent = spell.getData().getName();
            this.#presenter.addObserverTo(name, "click", () => {
                this.showSpellDetails(spell);
            });
            this.#presenter.getElement("[data-type=threads]", row).textContent = spell.getData().getThreads() + '';
            this.#presenter.getElement("[data-type=weaving]", row).textContent = '' + (spell.getData().getWeavingDifficulty() ?? 'n/a');
            this.#presenter.getElement("[data-type=casting]", row).textContent = spell.getData().getCastingDifficulty();
            this.#presenter.getElement("[data-type=effect]", row).textContent = spell.getData().getEffect();
            this.#presenter.getElement("[data-type=range]", row).textContent = spell.getData().getRange();
            this.#presenter.getElement("[data-type=duration]", row).textContent = spell.getData().getDuration();
            this.#presenter.getElement("[data-type=school]", row).textContent = spell.getThreadWeavingSpecialityNames().join(" | ");
            if(spell.getData().getCircle() <= circleSafeCastable) {
                this.#presenter.getElement(".grimoire", row).hidden = true;
            }
            tableBody.append(...row.children);
        });
    }
    /**
     * Show details of a learned spell in a modal dialog. Reusable by other presenters.
     * @param {Spell} spell - The spell to render
     */
    showSpellDetails(spell) {    
        const modal = this.#presenter.getModal();
        modal.addTitle(spell.getData().getName());
        modal.addCssClass("spell");
        const content = /** @type {HTMLElement} */ (this.#presenter.getElement("#spell-details").children[0].cloneNode(true));
        this.#presenter.getElement("span[data-type=casting]", content).textContent = spell.getData().getCastingDifficulty();
        this.#presenter.getElement("span[data-type=threads]", content).textContent = spell.getData().getThreads() + '';
        this.#presenter.getElement("span[data-type=weaving]", content).textContent = '' + (spell.getData().getWeavingDifficulty() ?? 'n/a');
        this.#presenter.getElement("span[data-type=reattune]", content).textContent = spell.getData().getReattunementDifficulty() + '';
        this.#presenter.getElement("span[data-type=range]", content).textContent = spell.getData().getRange();
        this.#presenter.getElement("span[data-type=duration]", content).textContent = spell.getData().getDuration();
        this.#presenter.getElement("span[data-type=effect]", content).textContent = spell.getData().getEffect();
        this.#presenter.getElement("span[data-type=circle]", content).textContent = spell.getData().getCircle() + '';
        if(spell.getData().isIllusion()) {
            this.#presenter.getElement("span[data-type=disbelief]", content).textContent = spell.getSensingDifficulty() + '';
        } else {
            this.#presenter.getElement("span[data-type=disbelief]", content).hidden = true;
            this.#presenter.getElement(".disbelief", content).hidden = true;
        }
        this.#presenter.getElement("span[data-type=school]", content).textContent = spell.getThreadWeavingSpecialityNames().join(", ");
        this.#presenter.getElement(".description", content).textContent = spell.getData().getDescription();
        modal.addContent(content);
        modal.open();
    }
    #renderAbilities() {
        const buildNodes = /** @type {(ability:Ability, container:HTMLElement) => void} */ (ability, container) => {
            let term = this.#presenter.createElement("dt");
            term.textContent = ability.getName();
            container.append(term);
            let definition = this.#presenter.createElement("dd");
            definition.textContent = ability.getDescription();
            container.append(definition);
        };
        const racial = this.#presenter.getElement(".abilities .racial dl", this.#section);
        racial.replaceChildren();
        this.#character.getRace().getAbilities().forEach(ability => buildNodes(ability, racial));
        const discipline = this.#presenter.getElement(".abilities .disciplines dl", this.#section);
        discipline.replaceChildren();
        this.#character.getDisciplineAbilities().forEach(ability => buildNodes(ability, discipline));
        this.#presenter.getElement(".abilities .disciplines", this.#section).hidden = discipline.children.length === 0;
    }
    #renderEquipment() {
        this.#renderArmor();
        this.#renderMeleeWeapons();
        this.#renderRangedWeapons(WeaponType.THROWING);
        this.#renderRangedWeapons(WeaponType.MISSILE);
    }
    #renderArmor() {
        const equipment = this.#character.getEquipment();
        const armorSection = this.#presenter.getElement(".equipment .armor", this.#section);
        if(equipment.getArmor()) {
            armorSection.hidden = false;
            const armor = equipment.getArmor().getData();
            this.#presenter.getElement("[data-type=name]", armorSection).textContent = armor.getName();
            this.#presenter.getElement("[data-type=physical]", armorSection).textContent = armor.getPhysicalRating() + '';
            this.#presenter.getElement("[data-type=mystic]", armorSection).textContent = armor.getMysticalRating() + '';
            this.#presenter.getElement("[data-type=initiative]", armorSection).textContent = -armor.getInitiativePenalty() + '';
            const livingCss = armor.isLivingArmor() ? 'fa-regular fa-circle-dot' : 'fa-regular fa-circle';
            this.#presenter.getElement("[data-type=living]", this.#section).className = livingCss;
        } else {
            armorSection.hidden = true;
        }
    }
    #renderMeleeWeapons() {
        const weaponSection = this.#presenter.getElement(".equipment .melee-weapons", this.#section);
        const weapons = this.#character.getEquipment().getMeleeWeapons();
        if(weapons.length === 0) {
            weaponSection.hidden = true;
            return;
        }
        weaponSection.hidden = false;
        const canUseKarma = this.#character.getEquipment().canUseKarmaForDamageTestsWithWeaponsOf(WeaponType.MELEE);
        const container = this.#presenter.getElement(".table-body", weaponSection);
        container.replaceChildren();
        const template = this.#presenter.getElement(".row-template", weaponSection);
        weapons.forEach(weapon => {
            const row = /** @type {HTMLElement} */ (template.cloneNode(true));
            this.#presenter.getElement("[data-type=name]", row).textContent = weapon.getData().getName();
            const totalStep = this.#character.getAttribute(AttributeIds.STRENGTH).getStep() + weapon.getTotalDamageStep();
            this.#presenter.getElement("[data-type=step]", row).textContent = `${totalStep} (${weapon.getTotalDamageStep()})`;
            const dice = this.#presenter.getElement("[data-type=dice]", row);
            dice.textContent = Earthdawn.rules.getDiceForStep(totalStep);
            this.#presenter.addObserverTo(dice, "click", () => {
                new StepModDialog(this.#presenter, this.#character, () => this.#storeChange()).renderAttributeTest(weapon.getData().getName(), totalStep, canUseKarma, ActionType.DAMAGE).then(() => {
                    this.#renderBasics();
                    this.#renderDefenses();
                    this.#renderConditions();
                });
            });
            const sizeIcons = weapon.canCarryOneHanded() ? ["fa-solid", "fa-hand"] : ["fa-solid", "fa-hands"];
            this.#presenter.getElement("[data-type=size-hands]", row).classList.add(...sizeIcons);
            this.#presenter.getElement("[data-type=penalty]", row).textContent = weapon.getAttackStepPenalty() + '';
            if(weapon.getData().canEntangle()) {
                this.#presenter.getElement("[data-type=entangling]", row).textContent = `Entangling (${weapon.getData().getEntanglingDifficulty()})`;
            }
            container.append(...row.children);
        });
        this.#presenter.getElement(".heading .karma-use", weaponSection).hidden = !canUseKarma;
    }
    /**
     * @param {WeaponType} type 
     */
    #renderRangedWeapons(type) {
        const typeCss = type === WeaponType.MISSILE ? "missile" : "throwing";
        const weaponSection = this.#presenter.getElement(`.equipment .${typeCss}-weapons`, this.#section);
        const weapons = this.#character.getEquipment().getWeapons(type);
        if(weapons.length === 0) {
            weaponSection.hidden = true;
            return;
        }
        weaponSection.hidden = false;
        const canUseKarma = this.#character.getEquipment().canUseKarmaForDamageTestsWithWeaponsOf(type);
        const container = this.#presenter.getElement(".table-body", weaponSection);
        container.replaceChildren();
        const template = this.#presenter.getElement(".row-template", weaponSection);
        weapons.forEach(weapon => {
            const row = /** @type {HTMLElement} */ (template.cloneNode(true));
            this.#presenter.getElement("[data-type=name]", row).textContent = weapon.getData().getName();
            const totalStep = this.#character.getAttribute(AttributeIds.STRENGTH).getStep() + weapon.getTotalDamageStep();
            this.#presenter.getElement("[data-type=step]", row).textContent = `${totalStep} (${weapon.getTotalDamageStep()})`;
            const dice = this.#presenter.getElement("[data-type=dice]", row);
            dice.textContent = Earthdawn.rules.getDiceForStep(totalStep);
            this.#presenter.addObserverTo(dice, "click", () => {
                new StepModDialog(this.#presenter, this.#character, () => this.#storeChange()).renderAttributeTest(weapon.getData().getName(), totalStep, canUseKarma, ActionType.DAMAGE).then(() => {
                    this.#renderBasics();
                    this.#renderDefenses();
                    this.#renderConditions();
                });
            });
            const sizeIcons = weapon.canCarryOneHanded() ? ["fa-solid", "fa-hand"] : ["fa-solid", "fa-hands"];
            this.#presenter.getElement("[data-type=size-hands]", row).classList.add(...sizeIcons);
            const range = /** @type {RangedWeaponData} */ (weapon.getData()).getRange();
            this.#presenter.getElement("[data-type=long-range]", row).textContent = `${range.long.min}-${range.long.max}`;
            this.#presenter.getElement("[data-type=penalty]", row).textContent = weapon.getAttackStepPenalty() + '';
            if(weapon.getData().canEntangle()) {
                this.#presenter.getElement("[data-type=entangling]", row).textContent = `Entangling (${weapon.getData().getEntanglingDifficulty()})`;
            }
            container.append(...row.children);
        });
        this.#presenter.getElement(".heading .karma-use", weaponSection).hidden = !canUseKarma;
    }
    /**
     * @param {Presenter} presenter 
     * @param {HTMLElement} section 
     * @returns {DynamicNavbarScroller}
     */
    #getAndSetupNavbar(presenter, section) {
        const pageContainer = presenter.getElement(".page-container", section);
        const navbarContainer = presenter.getElement(".navbar-s", section);
        return new DynamicNavbarScroller(pageContainer, navbarContainer, presenter);
    }
    #setupKarma() {
        const karma = this.#presenter.getElement(".basics .karma output[data-type=current]", this.#section);
        this.#presenter.addObserverTo(karma, "click", () => {
            if(this.#readOnly) {
                return;
            }
            const modal = new KarmaPointsDialog(this.#presenter, this.#character);
            modal.render().then(() => {
                this.#storeChange();
                this.#renderBasics();
            });
        });
    }
    #setupConditions() {
        if(!this.#readOnly) {
            this.#presenter.addObserverTo(this.#presenter.getElement(".characteristics .conditions", this.#section), "click", () => {
                this.#renderConditionsModal();
            });
        }
    }
    #setupDamageAndHealing() {
        const currentDamage = this.#presenter.getElement(".characteristics .health output[data-type=currentDamage]", this.#section);
        this.#presenter.addObserverTo(currentDamage, "click", () => {
            if(this.#readOnly) {
                return;
            }
            const modal = new DamageAndHealingDialog(this.#presenter, this.#section, this.#character);
            modal.render().then(() => {
                this.#storeChange();
                this.#renderHealth();
                this.#renderBasics();
            });
        });
    }
    #setupRecoveryTests() {
        const recovery = this.#presenter.getElement(".characteristics .health .recovery output[data-type=availableTests]");
        this.#presenter.addObserverTo(recovery, "click", () => {
            if(this.#readOnly) {
                return;
            }
            const modal = new RecoveryTestsDialog(this.#presenter, this.#section, this.#character);
            modal.render().then(() => {
                this.#storeChange();
                this.#renderHealth();
            });
        });
    }
    #setupWoundsDialog() {
        const wounds = this.#presenter.getElement(".characteristics .health output[data-type=wounds]");
        this.#presenter.addObserverTo(wounds, "click", () => {
            if(this.#readOnly) {
                return;
            }
            const modal = new WoundsDialog(this.#presenter, this.#section, this.#character);
            modal.render().then(() => {
                this.#storeChange();
                this.#renderHealth();
            });
        });
    }
    #setupSpellcasting() {
        const dice = this.#presenter.getElement(".spells .spellcasting [data-type=dice]", this.#section);
        this.#presenter.addObserverTo(dice, "click", () => {
            const spellCasting = this.#character.findTalent(KnownTalent.SPELLCASTING.getId(), 1);
            if(spellCasting) {
                new StepModDialog(this.#presenter, this.#character, () => this.#storeChange()).renderForTalent(spellCasting).then(() => {
                    this.#renderBasics();
                    this.#renderDefenses();
                    this.#renderConditions();
                    this.#renderHealth();
                });
            }
        });
    }
    #storeChange() {
        if(!this.#readOnly) {
            this.#record.update(this.#character);
            this.#presenter.getPersistence().updateCharacter(this.#record);
        }
    }
}

/**
 * Presenter for the settings section of the UI. Allows configuration of the application.
 * @extends AbstractSectionPresenter
 */
class SettingsPresenter extends AbstractSectionPresenter {
    #presenter;  
    #cloud;      
    #appUpdater;   
    /** @type {HTMLElement} - Root element of the HTML section this presenter controls */
    #section;
    /** @type {DynamicNavbarScroller} - Navigation component of this section */
    #navbar;
    #interactive;
    /**
     * @param {Presenter} presenter - Window presenter
     * @param {CloudSynchronizer} cloud - Cloud synchronizer instance of the app
     * @param {AppUpdater} appUpdater - App updater instance of the app
     */
    constructor(presenter, cloud, appUpdater) {
        super();
        this.#presenter = presenter;
        this.#cloud = cloud;
        this.#appUpdater = appUpdater;
        this.#section = presenter.getElement("#settings");
        this.#navbar = this.#getAndSetupNavbar(presenter, this.#section);
        this.#interactive = this.#getAndSetupElements(presenter, this.#section);

        this.#presenter.addObserverTo(this.#presenter.getElement(".header button.cancel", this.#section), "click", this.#presenter.getHistory().leaveSection);
    }
    static get sectionName() {
        return "settings";
    }
    /**
     * @override
     */
    hide() {
        this.#section.hidden = true;
    }
    /**
     * @override
     */
    show() {
        this.#initialize();
        this.#section.hidden = false;
        this.#navbar.reset();
    }
    async #requestApiToken() {
        this.#interactive.email.disabled = true;
        this.#interactive.token.disabled = true;
        this.#interactive.request.disabled = true;
        try {
            if(this.#cloud.getApiToken()) {
                const response = await this.#cloud.checkApiToken();
                if(response.ok) {
                    this.#presenter.getNotifications().postWarning("Your existing token is still valid! You will not receive a new token.");
                    return;
                }
            }
            this.#interactive.token.value = "";
            this.#updateToken("");
            const result = await this.#cloud.requestApiToken();
            switch(result) {
                case "ok":
                    this.#presenter.getNotifications().postInfo("A new token has been issued. Please wait a bit and check your email (including your spam folder)!");
                    return;
                case "invalid-email":
                    this.#presenter.getNotifications().postError("The email you've entered is not valid. Please correct your address and try again.");
                    return;
                case "too-many-requests":
                    this.#presenter.getNotifications().postWarning("Your request was already handled. Please stop clicking this button!");
                    return;
                default:
                    // fall through
                    break;
            }
        } catch {
            // fall through
        } finally {
            this.#interactive.email.disabled = false;
            this.#interactive.token.disabled = false;
            this.#interactive.request.disabled = false;
        }
        this.#presenter.getNotifications().postError("There was a connection error. Please check your signal and try again in a bit.");
    }
    /**
     * Sets a new API auth token and checks if it works. If it doesn't, it clears it again.
     * @param {string} token - The new API auth token. May be empty (but not null) to reset.
     */
    async #updateToken(token) {
        token = token.trim();
        await this.#cloud.setApiToken(token);
        if(token) {
            try {
                const response = await this.#cloud.checkApiToken();
                if(!response.ok) {
                    token = "";
                    this.#cloud.setApiToken(token);
                    this.#interactive.token.value = token;
                    this.#presenter.getNotifications().postWarning("The entered token is not valid! Please make sure that you copy all characters from the email.");
                }
            } catch {
                this.#presenter.getNotifications().postError("Unable to verify token due to network issue. Please try again later.");
            }
        }
    }
    #updateEmail() {
        this.#cloud.setEmail(this.#interactive.email.value.trim());
    }
    #toggleRequestButton() {
        this.#interactive.request.disabled = this.#interactive.email.value.length === 0 || !this.#interactive.email.checkValidity();
    }
    #renderAppVersion() {
        const icons = this.#interactive.update.children[0].classList;
        this.#interactive.version.innerText = this.#appUpdater.getCurrentVersion();
        if(this.#appUpdater.hasWaitingUpdate()) {
            this.#interactive.status.innerText = `new version ${this.#appUpdater.getAvailableUpdate()} available`;
            icons.remove("fa-satellite-dish");
            icons.add("fa-gear");
            /** @type {HTMLElement} */ (this.#interactive.update.children[1]).innerText = " Install Update";
        } else {
            if(this.#appUpdater.getLastCheckTime()) {
                this.#interactive.status.innerText = `up to date (${this.#appUpdater.getLastCheckTime().toLocaleString()})`;
            } else {
                this.#interactive.status.innerText = "check failed";
            }
            icons.remove("fa-gear");
            icons.add("fa-satellite-dish");
            /** @type {HTMLElement} */ (this.#interactive.update.children[1]).innerText = " Check for Update";
        }
    }
    #checkOrUpdateApp() {
        if(this.#appUpdater.hasWaitingUpdate()) {
            this.#interactive.update.children[0].classList.add("fa-spin");
            this.#interactive.update.disabled = true;
            this.#appUpdater.applyUpdate().catch((e) => {
                console.log("Applying app udpate failed", e);
                this.#interactive.update.children[0].classList.remove("fa-spin");
                this.#interactive.update.disabled = false;
                this.#presenter.getNotifications().postError("An error occurred during the app update. Please close the app and re-open it again.");
            });
        } else {
            this.#interactive.update.children[0].classList.add("fa-beat");
            this.#interactive.update.disabled = true;
            this.#appUpdater.pollServer().catch(() => {
                this.#presenter.getNotifications().postError("Unable to contact server. Please try again later.");
            }).finally(() => {
                this.#renderAppVersion();
                this.#interactive.update.children[0].classList.remove("fa-beat");
                this.#interactive.update.disabled = false;
            });
        }
    }
    #initialize() {
        this.#interactive.email.value = this.#cloud.getEmail();
        this.#interactive.token.value = this.#cloud.getApiToken();
        this.#toggleRequestButton();
        this.#renderAppVersion();
    }
    /**
     * @param {Presenter} presenter 
     * @param {HTMLElement} section 
     * @returns {DynamicNavbarScroller}
     */
    #getAndSetupNavbar(presenter, section) {
        const pageContainer = presenter.getElement(".page-container", section);
        const navbarContainer = presenter.getElement(".navbar-s", section);
        return new DynamicNavbarScroller(pageContainer, navbarContainer, presenter);
    }
    /**
     * @param {Presenter} presenter - Window presenter
     * @param {HTMLElement} section - Root element of this section
     * @returns {{show:HTMLButtonElement, request:HTMLButtonElement, email:HTMLInputElement, token:HTMLInputElement, version:HTMLElement, status:HTMLElement, update:HTMLButtonElement}} - References to all interactive elements of this section plus containers for rendering the app version and update status.
     */
    #getAndSetupElements(presenter, section) {
        const interactive = {
            show: /** @type {HTMLButtonElement} */ (presenter.getElement("fieldset button.show", section)),
            request: /** @type {HTMLButtonElement} */ (presenter.getElement("fieldset button.request", section)),
            email: /** @type {HTMLInputElement} */ (presenter.getElement("#settings-email", section)),
            token: /** @type {HTMLInputElement} */ (presenter.getElement("#settings-api-token", section)),
            version: presenter.getElement(".version .info span[data-type=version]", section),
            status: presenter.getElement(".version .info span[data-type=status]", section),
            update: /** @type {HTMLButtonElement} */ (presenter.getElement(".version button.update", section))
        };
        presenter.addObserverTo(interactive.show, "click", () => {
            const icon = this.#presenter.getElement("span", interactive.show);
            if(interactive.token.type === "password") {
                interactive.token.type = "text";
                icon.classList.remove("fa-eye");
                icon.classList.add("fa-eye-slash");
            } else {
                interactive.token.type = "password";
                icon.classList.remove("fa-eye-slash");
                icon.classList.add("fa-eye");
            }
        });
        presenter.addObserverTo(interactive.request, "click", () => this.#requestApiToken());
        presenter.addObserverTo(interactive.email, "input", () => this.#toggleRequestButton());
        presenter.addObserverTo(interactive.email, "change", () => this.#updateEmail());
        presenter.addObserverTo(interactive.token, "change", () => this.#updateToken(this.#interactive.token.value));
        presenter.addObserverTo(interactive.update, "click", () => this.#checkOrUpdateApp());
        presenter.addObserverTo(presenter.getElement(".rules-link", section), "click", () => this.#navbar.jumpToPage(3));
        return interactive;
    }
}


/**
 * Presenter of the start screen that renders a list of all existing {@link Character}s and allows to create new ones. Also contains buttons to enter the {@link SettingsPresenter}'s section
 * and to synchronize characters with the cloud. Because this presenter represents the top-level view, its section cannot be left. Browser back will close the app instead.
 * @extends AbstractSectionPresenter
 */
class OverviewPresenter extends AbstractSectionPresenter {
    #presenter;  
    #cloud;  
    #appUpdater;
    /** @type {HTMLElement} - Root element of this presenter's HTML section */
    #section;
    /** @type {Map.<CharacterId,CharacterRecord>} - Map of all loaded characters. The key is the {@link Character#getId}, value is the record describing the character. This map includes soft-deleted records until synchronized with the cloud.  */
    #characters;
    /** @type {boolean} - Flag if this presenter was already initialized once (when the app is opened the first time) in order to skip some processes when coming back to this presenter from another. */
    #initialized;
    /** @type {{last:number,syncing:?Promise,button:HTMLButtonElement}} - State object to remember when the last cloud synchronization was triggered. Last contains a timestamp (epoch millis), syncing is a Promise of a running sync or null, button is a reference to the sync button shown in the UI */
    #sync;
    /**
     * @param {Presenter} presenter - Window presenter
     * @param {CloudSynchronizer} cloud - Cloud synchronizer instance of the app
     * @param {AppUpdater} appUpdater - App updater instance of the app 
     */
    constructor(presenter, cloud, appUpdater) {
        super();
        this.#presenter = presenter;
        this.#cloud = cloud;
        this.#appUpdater = appUpdater;
        this.#section = presenter.getElement("#overview");
        this.#characters = new Map();
        this.#initialized = false;
        this.#sync = {
            last: 0,
            syncing: null,
            button: /** @type {HTMLButtonElement} */ (presenter.getElement(".header .sync", this.#section))
        };
        this.#setupButtons(presenter);
    }
    static get sectionName() {
        return "overview";
    }
    /**
     * @override
     */
    hide() {
        this.#section.hidden = true;
    }
    /**
     * @override
     */
    show() {
        this.#section.hidden = false;
        if(!this.#initialized) {
            this.#toggleSyncButton(false);
            Promise.all([this.#loadCharacters(), this.#loadSettings(), this.#checkAppUpdate()]).then(() => {
                this.#initialized = true;
                this.#toggleSettingsButtonAttention(this.#appUpdater.hasWaitingUpdate());
                return this.#synchronizeAllCharacters().then(success => {
                    if(success) {
                        this.#renderCharacters();
                    }
                });
            }, (e) => console.log("Unable to initialize OverviewPresenter", e));
        } else {
            this.#toggleSettingsButtonAttention(this.#appUpdater.hasWaitingUpdate());
            const canSync = this.#cloud.getApiToken() != "";
            this.#toggleSyncButton(canSync);
            this.#synchronizeAllCharacters().then(() => this.#renderCharacters());
        }
    }
    /**
     * @override
     * @param {LeaverFunction} leave
     * @returns {boolean} - This method throws an exception if called because this presenter controls the root screen. There is no regular leaving from this section.
     */
    close(leave) {
        throw new Error(`Section ${OverviewPresenter.sectionName} does not support the close action because it is the root section.`);
    }
    /**
     * Inserts a new character into the local persistence and the list of loaded characters. Intended ofr internal usage within this presenter and by {@link CreationPresenter}.
     * @param {Character} character - The new character to persist
     * @returns {Promise.<CharacterRecord>} - The newly created record of the inserted character
     */
    async insertNewCharacter(character) {
        if(!(character instanceof Character)) {
            throw new Error(`Argument has the wrong type ${typeof character}`);
        }
        const record = await this.#presenter.getPersistence().insertNewCharacter(character);
        this.#characters.set(character.getId(), record);
        return record;
    }
    /**
     * Updates an existing CharacterRecord in both local persistence and in the map of loaded characters, and returns it. Does not trigger rendering or syncing.
     * @param {Character} character - The changed character. Its corresponding record will be updated with its new state.
     * @returns {Promise.<CharacterRecord>} - Returns the updated character record after it was persisted.
     */
    async #updateCharacter(character) { 
        if(!(character instanceof Character)) {
            throw new Error(`Argument has the wrong type ${typeof character}`);
        }
        const existing = this.#characters.get(character.getId());
        if(!existing) {
            throw new Error(`No existing character with id ${character.getId()} found for update`);
        }
        existing.update(character);
        return this.#presenter.getPersistence().updateCharacter(existing);
    }
    #renderCharacters() {
        const sorted = Array.from(this.#characters.values()).filter(record => !record.isDeleted() || record.hasConflict()).sort(CharacterRecord.compare);   // show (soft-)deleted characters only if they have a conflict with their remote version and need attention by the user
        const container = this.#presenter.getElement("#overview .characters");
        const template = this.#presenter.getElement("#overview .character-template");
        const children = sorted.map(record => {
            const item = /** @type {HTMLElement} */ (template.cloneNode(true));
            item.className = "character";
            // show basic details even for soft-deleted records if there is a sync conflict
            this.#presenter.getElement(".name", item).textContent = record.getName();
            this.#presenter.getElement(".id", item).textContent = record.getId();
            let lastSync = record.getLastSyncTime();
            const lastUpdate = record.getLastUpdateTime();
            if(record.hasConflict()) {
                lastSync = new Date(record.getConflictDetails().synced);    
            }
            if(lastSync == null) {
                this.#presenter.getElement(".synced .icon", item).classList.add("never");
            } else if(lastSync < lastUpdate) {
                this.#presenter.getElement(".synced .icon", item).classList.add("outdated");
            }
            if(lastSync != null) {
                this.#presenter.getElement(".synced .time", item).textContent = lastSync.toLocaleString();
            }
            if(!record.isDeleted()) {           // show more character details if Character was not soft-deleted
                record.getCharacter().then(character => {
                    const typeInfo = this.#presenter.getElement(".type", item);
                    const race = this.#presenter.createElement("span");
                    race.textContent = character.getRace().getName();
                    typeInfo.append(race);
                    character.getDisciplines().forEach(discipline => {
                        const info = this.#presenter.createElement("span");
                        info.textContent = `${discipline.getData().getName()} ${discipline.getCircle()}`;
                        typeInfo.append(info);
                    });
                });
            }
            let showLocalCharacter = true;
            if(record.hasConflict()) {
                item.classList.add("conflicted");
                let showRemoteCharacter = true;
                if(record.isDeleted()) {    // locally deleted but updated in cloud
                    showLocalCharacter = false;
                    this.#presenter.addObserverTo(this.#presenter.getElement(".conflict", item), "click", () => this.#resolveLocalDeleteConflict(record));
                } else if(!record.getConflictDetails().serialized) {    // remote deleted but locally updated
                    showRemoteCharacter = false;
                    this.#presenter.addObserverTo(this.#presenter.getElement(".conflict", item), "click", () => this.#resolveRemoteDeleteConflict(record));
                } else {    // updated both locally and remote
                    this.#presenter.addObserverTo(this.#presenter.getElement(".conflict", item), "click", () => this.#resolveUpdateConflict(record));
                }
                if(showRemoteCharacter) {
                    this.#presenter.getElement(".view-conflict", item).hidden = false;
                    this.#presenter.addObserverTo(this.#presenter.getElement(".view-conflict > span", item), "click", () => this.#presenter.enterSection(CharacterViewPresenter.sectionName, record.getConflictCharacterRecord(), true));
                }            
            }
            if(showLocalCharacter) {
                this.#presenter.addObserverTo(this.#presenter.getElement(".details", item), "click", () => this.#presenter.enterSection(CharacterViewPresenter.sectionName, record));
                this.#presenter.addObserverTo(this.#presenter.getElement(".buttons .export", item), "click", () => record.export(this.#presenter.getFileHandler()));
                this.#presenter.addObserverTo(this.#presenter.getElement(".buttons .delete", item), "click", () => this.#askForCharacterDeletion(record));
            } else {
                this.#presenter.findElements(".buttons > span", item).forEach(/** @param {HTMLElement} icon */ (icon) => icon.hidden = true);
            }
            item.hidden = false;
            return item;
        });
        container.replaceChildren(...children);
    }
    /**
     * Ask the user if they really want to delete the given character. On confirmation, synced characters (that also exist remotely) are marked as soft-deleted while pure-local characters are immediately destroyed.
     * @param {CharacterRecord} record 
     */
    async #askForCharacterDeletion(record) {
        const character = await record.getCharacter();
        const modal = this.#presenter.getModal();
        modal.addCssClass("delete-character");
        modal.openConfirmation("Delete Character?", `Do you want to permanently delete ${character.getName()}?`, "Cancel", "Delete").then(async () => {
            if(record.getLastSyncTime() == null) {
                await this.destroyCharacter(record);
                this.#renderCharacters();
            } else {
                await this.#markCharacterDeleted(record);
            }
            this.#presenter.getNotifications().postInfo(`Character ${character.getName()} with ID ${character.getId()} was successfully deleted.`);
        }, () => {});
    }
    /**
     * Loads a character from the given state object and presists it. This method can be used to import a character from an external file.
     * @param {object} characterState - The saved state deserialized into an object via JSON.parse.
     */
    async #importCharacter(characterState) {
        const character = await Character.load(characterState);
        const existing = this.#characters.get(character.getId());
        if(existing) {
            const old = this.#presenter.escapeHtml(existing.getName());
            const imported = this.#presenter.escapeHtml(character.getName());
            const modal = this.#presenter.getModal();
            modal.addTitle("Overwrite Existing Character?");
            const content = /** @type {HTMLElement} */ (this.#presenter.getElement(".duplicate-import-template", this.#section).cloneNode(true));
            content.hidden = false;
            this.#presenter.getElement(".message", content).innerHTML = `There is already an existing character <b>${old}</b> with the same ID.<br />Do you want to replace and overwrite it with the uploaded <i>${imported}</i>, or do you want to import a copy with a new ID?`;
            this.#presenter.addObserverTo(this.#presenter.getElement("button.cancel", content), "click", () => modal.close(), {once: true});
            this.#presenter.addObserverTo(this.#presenter.getElement("button.copy", content), "click", () => modal.close().then(() => this.#importNewCopy(character), error => this.#handleImportError(error)), {once: true});
            this.#presenter.addObserverTo(this.#presenter.getElement("button.replace", content), "click", () => modal.close().then(() => this.#replaceCharacter(character), error => this.#handleImportError(error)), {once: true});
            modal.addContent(content);
            modal.open();
            return;
        }
        await this.insertNewCharacter(character).then(record => this.#synchronizeSingleCharacter(record));
        this.#renderCharacters();
        const name = this.#presenter.escapeHtml(character.getName());
        this.#presenter.getNotifications().postInfo(`Character '${name}' with ID ${character.getId()} was succesfully imported.`);
    }
    /**
     * Persists a given character copy as a new entry by assigning a new {@link CharacterId}.
     * @param {Character} character - The new character to persist. This has to be the copy already. It is modified by this method.
     */
    #importNewCopy(character) {
        character.setId(crypto.randomUUID());
        if(!character.getName().endsWith("(copy)")) {
            character.setName(character.getName() + " (copy)");
        }
        const name = this.#presenter.escapeHtml(character.getName());
        this.insertNewCharacter(character).then(record => this.#synchronizeSingleCharacter(record)).then(() => {
            this.#renderCharacters();
            this.#presenter.getNotifications().postInfo(`Character '${name}' was succesfully imported as a new copy with ID ${character.getId()}.`);
        }, error => this.#presenter.getNotifications().postError("Unable to save new character! " + error.message));
    }
    /**
     * Replaces an existing character with a new one having the same {@link CharacterId}.
     * @param {Character} newCharacter - The new character instance that should replace the existing character with the same id.
     */
    #replaceCharacter(newCharacter) {
        const existing = this.#characters.get(newCharacter.getId());
        if(!existing) {
            throw new Error(`No existing character found for id ${newCharacter.getId()}`);
        }
        existing.getCharacter().then(async oldChar => {
            const oldName = this.#presenter.escapeHtml(oldChar.getName());
            const newName = this.#presenter.escapeHtml(newCharacter.getName());
            await this.#updateCharacter(newCharacter).then(record => this.#synchronizeSingleCharacter(record));
            this.#renderCharacters();
            this.#presenter.getNotifications().postInfo(`Character '${oldName}' was succesfully replaced with imported character '${newName}'.`);
        }).catch(error => this.#presenter.getNotifications().postError("Unable to save new character! " + error.message));
    }
    /**
     * @param {object} error 
     */
    #handleImportError(error) {
        let message = "Import failed! ";
        switch(error.reason) {
            case "empty":
                message += "The file is empty.";
                break;
            case "malformed":
            case "encoding":
            default:
                message += "The file does not contain a valid Earthdawn character supported by this application.";
                break;
            
        }
        console.log(error);
        this.#presenter.getNotifications().postError(message);
    }
    /**
     * @param {CharacterRecord} record 
     */
    async #markCharacterDeleted(record) {
        await this.#presenter.getPersistence().softDeleteCharacter(record);
        await this.#synchronizeSingleCharacter(record);
        this.#renderCharacters();
    }
    /**
     * @param {CharacterRecord} record 
     */
    async destroyCharacter(record) {
        if(!(record instanceof CharacterRecord)) {
            throw new Error(`Argument has the wrong type ${typeof record}`);
        }
        await this.#presenter.getPersistence().destroyCharacter(record);
        this.#characters.delete(record.getId());
    }
    async #loadCharacters() {
        const loaded = await this.#presenter.getPersistence().loadAllCharacters();
        loaded.forEach(record => {
            this.#characters.set(record.getId(), record);
        });
        this.#renderCharacters();
    }
    /**
     * The character was updated both locally and remote. This method asks the user to chose a way to resolve the conflict.
     * They can either replace the local version with the one from the cloud, or create a copy from the local version while keeping the original cloud version, or keep the local character and replacing the cloud version.
     * @param {CharacterRecord} record 
     */
    #resolveUpdateConflict(record) {
        const modal = this.#presenter.getModal();
        modal.addTitle("Synchronization Conflict");
        const content = /** @type {HTMLElement} */ (this.#presenter.getElement(".update-conflict-template", this.#section).cloneNode(true));
        content.hidden = false;
        const button = /** @type {HTMLButtonElement} */ (this.#presenter.getElement("button.resolve", content));
        button.disabled = true;
        /** @type {(event:Event) => void} */
        const buttonUpdater = (event) => {
            const checkbox = /** @type {HTMLInputElement} */ (event.target);
            if(checkbox.checked) {
                button.disabled = false;
                button.setAttribute("data-action", checkbox.value);
            }
        };
        this.#presenter.addObserverTo(this.#presenter.getElement("input[value=replace]", content), "change", buttonUpdater);
        this.#presenter.addObserverTo(this.#presenter.getElement("input[value=copy]", content), "change", buttonUpdater);
        this.#presenter.addObserverTo(this.#presenter.getElement("input[value=keep]", content), "change", buttonUpdater);
        this.#presenter.addObserverTo(button, "click", () => {
                const action = button.getAttribute("data-action");
                modal.close().then(() => {
                    switch(action) {
                        case "replace":
                            record.resolveUsingCloud();
                            this.#presenter.getPersistence().updateCharacter(record, false).then(() => {
                                this.#renderCharacters();
                                this.#presenter.getNotifications().postInfo(`Character was overwritten with cloud version`);
                            });
                            break;
                        case "copy":
                            record.resolveCreateCopy().then(async (newCharacter) => {
                                const newRecord = await this.insertNewCharacter(newCharacter);
                                await this.#presenter.getPersistence().updateCharacter(record, false);
                                this.#renderCharacters();
                                this.#presenter.getNotifications().postInfo(`New copy of local character was created with ID ${newCharacter.getId()}`);
                                return this.#synchronizeSingleCharacter(newRecord).then(() => this.#renderCharacters());
                            });
                            break;
                        case "keep":
                            record.resolveUsingLocal();
                            this.#presenter.getPersistence().updateCharacter(record, false).then(() => {
                                this.#renderCharacters();
                                this.#synchronizeSingleCharacter(record).then(() => this.#renderCharacters());
                                this.#presenter.getNotifications().postInfo(`Cloud version was discarded and replaced with local character.`);
                            });
                            break;
                    }
                });
            }, {once: true});
        modal.addContent(content);
        modal.open();
    }
    /**
     * The character was deleted in the cloud while it was updated locally. Ask the user how to resolve this. They can either keep the local version and push it back into the cloud, or delete the local character too.
     * @param {CharacterRecord} record 
     */
    #resolveRemoteDeleteConflict(record) {
        const modal = this.#presenter.getModal();
        modal.addTitle("Synchronization Conflict");
        const content = /** @type {HTMLElement} */ (this.#presenter.getElement(".remote-delete-conflict-template", this.#section).cloneNode(true));
        content.hidden = false;
        this.#presenter.addObserverTo(this.#presenter.getElement("button.keep", content), "click", () => {
            modal.close().then(() => {
                record.resolveUsingLocal();
                return this.#presenter.getPersistence().updateCharacter(record, false).then((record) => {
                    this.#renderCharacters();
                    return this.#synchronizeSingleCharacter(record);
                }).then(() => {
                    this.#presenter.getNotifications().postInfo(`Character was not deleted but pushed back into the cloud.`);
                    this.#renderCharacters();
                });
            });
        }, {once: true});
        this.#presenter.addObserverTo(this.#presenter.getElement("button.delete", content), "click", () => {
            modal.close().then(() => this.destroyCharacter(record)).then(() => {
                this.#renderCharacters()
                this.#presenter.getNotifications().postInfo(`Character was deleted on your device, too`);
            });
        }, {once: true});
        modal.addContent(content);
        modal.open();
    }
    /**
     * The character was deleted locally while it was updated in the cloud. Ask the user how to resolve this conflict. They can either restore the character from the cloud or delete it also remotely.
     * @param {CharacterRecord} record 
     */
    #resolveLocalDeleteConflict(record) {
        const modal = this.#presenter.getModal();
        modal.addTitle("Synchronization Conflict");
        const content = /** @type {HTMLElement} */ (this.#presenter.getElement(".local-delete-conflict-template", this.#section).cloneNode(true));
        content.hidden = false;
        this.#presenter.addObserverTo(this.#presenter.getElement("button.keep", content), "click", () => {
            modal.close().then(() => {
                record.resolveUsingCloud();
                return this.#presenter.getPersistence().updateCharacter(record, false).then((record) => {
                    this.#renderCharacters();
                    this.#presenter.getNotifications().postInfo(`Character was restored from the cloud.`);
                });
            });
        }, {once: true});
        this.#presenter.addObserverTo(this.#presenter.getElement("button.delete", content), "click", () => {
            modal.close().then(() => {
                record.resolveUsingLocal();     // character still marked as soft-deleted but now update its change time again and try another sync to get it deleted everywhere.
                return this.#presenter.getPersistence().updateCharacter(record, true).then((record) => {
                    this.#renderCharacters();
                    this.#synchronizeSingleCharacter(record).then(() => this.#presenter.getNotifications().postInfo(`Character was deleted in the cloud.`));
                });
            });
        }, {once: true});
        modal.addContent(content);
        modal.open();
    }
    /**
     * Triggers the synchronization of all characters with cloud storage. Shows a success toast if the action was triggered by the user explicitly.
     * @param {boolean} userTriggered - Flag to signal if this synchronization was triggered explicitly by the user via button click or implicitly by the presenter e.g. by user navigation.
     * @returns {Promise.<boolean>} - A boolean Promise that is resolved to true if the synchronization was successful else false.
     */
    #synchronizeAllCharacters(userTriggered = false) {
        if(this.#sync.syncing) {
            return this.#sync.syncing.then(() => this.#synchronizeAllCharacters(userTriggered));
        }
        this.#sync.syncing = new Promise(async (resolve) => {
            let result = false;
            try {
                result = await this.#syncWithCloud(userTriggered);
                if(userTriggered && result) {
                    this.#presenter.getNotifications().postInfo("All characters synchronized!");
                }
            } finally {
                this.#sync.syncing = null;
                resolve(result);
            }
        });
        return this.#sync.syncing;
    }
    /**
     * Synchronizes the given character with the cloud.
     * @param {CharacterRecord} record 
     * @returns {Promise.<boolean>} - A boolean Promise that is resolved to true if the synchronization was successful else false.
     */
    #synchronizeSingleCharacter(record) {   
        if(this.#sync.syncing) {
            return this.#sync.syncing.then(() => this.#synchronizeSingleCharacter(record));
        }
        this.#sync.syncing = new Promise(async (resolve) => {
            let result = false;
            try {
                result = await this.#syncWithCloud(false, record);
            } finally {
                this.#sync.syncing = null;
                resolve(result);
            }
        });
        return this.#sync.syncing;
    }
    /**
     * Synchronizes either all characters or only one given character with the cloud storage.
     * @param {boolean} userTriggered - Flag to indicate if this sync was explicitly triggered by the user or implicitly by the code as a reaction to e.g. an update of a character.
     * @param {?CharacterRecord} [singleRecord=null] - If this is null all characters are synced, else only the given one.
     * @returns {Promise.<boolean>} - Returns true if successfully synced. Returns false if no sync was executed (e.g. because not needed or no API token available) or the sync failed (e.g. due to connectivity problems).
     */
    async #syncWithCloud(userTriggered, singleRecord = null) {
        if(!singleRecord && !userTriggered && this.#sync.last && (this.#sync.last + 15*60*1000) > Date.now()) {  
            let syncNeeded = false;
            for(let [key, record] of this.#characters) {
                if(record.needsSync()) {
                    syncNeeded = true;
                    break;
                }
            }
            if(!syncNeeded) {
                return false;   // we already synced within the last 15 minutes and this sync was not triggered explicitly by user interaction and there are no new changes, therefore skip.
            }
        }
        if(!this.#cloud.getApiToken()) {    
            return false;   // we can't sync because we don't have an API auth token
        }
        this.#toggleSyncButtonBusy(true);
        let syncDisabled = false;
        try {
            const syncTime = await this.#cloud.synchronizeWithCloud(this.#characters, singleRecord);
            if(!singleRecord) {
                this.#sync.last = syncTime;
            }
            return true;
        } catch(error) {
            if(error instanceof SyncDisabled) {
                syncDisabled = true;
                if(!singleRecord) {     // for internal syncs of single records we ignore the cloud sync and wait until a full sync is executed later.
                    this.#cloud.requestApiToken(false).then(result => {
                        if(result === "ok") {
                            this.#presenter.getNotifications().postWarning("Cloud synchronization is disabled because your authentication token is expired. Please check your emails for a new token!");
                        } else if(userTriggered) {
                            this.#presenter.getNotifications().postError("Cloud synchronization is disabled because your authentication token is expired. Please update your settings!");
                        }
                    });
                }
            } else if(userTriggered) {
                this.#presenter.getNotifications().postError("Cloud synchronization failed. Please check your connectivity and try again later.");
            }
            return false;
        } finally {
            this.#toggleSyncButtonBusy(false);
            this.#toggleSyncButton(!syncDisabled);
        }
    }
    /**
     * @param {boolean} busy - Pass true if the sync button should show as busy (spinning), else false.
     */
    #toggleSyncButtonBusy(busy) {
        this.#sync.button.classList.toggle("busy", busy);
        this.#sync.button.children[0].classList.toggle("fa-spin", busy);
    }
    /**
     * @param {boolean} enabled - Pass true to enable the sync button, else false.
     */
    #toggleSyncButton(enabled) {
        this.#sync.button.classList.toggle("disabled", !enabled);
    }
    /**
     * @param {boolean} active - Pass true to let the settings button bounce to get the user's attention, else false.
     */
    #toggleSettingsButtonAttention(active) {
        this.#presenter.getElement(".header .settings span", this.#section).classList.toggle("fa-bounce", active);
    }
    async #loadSettings() {
        return this.#cloud.loadSettings().then(() => {
            // If we find an API auth token in the URL fragment of the PWA, we try to set it in the settings. This allows a user to just click a link in the email to set the API token without having to manually copy & paste it into the app.
            if(window.location.hash !== "") {
                const token = window.location.hash.substring(1);
                if(token.length > 10 && token.length <= 20) {
                    this.#cloud.setApiToken(token)
                    .then(() => this.#cloud.checkApiToken())
                    .then(response => {
                        if(!response.ok) {
                            throw new SyncDisabled('Api token invalid');
                        }
                        return true;
                    })
                    .then(() => this.#presenter.getNotifications().postInfo("Authentication token was successfully updated!"))
                    .catch(() => {
                        this.#cloud.setApiToken("");
                        this.#presenter.getNotifications().postError("Invalid authentication token found in URL. Please try again.");
                    });
                }
            }
        });
    }
    async #checkAppUpdate() {
        return this.#appUpdater.pollServer().catch(() => {});
    }
    /**
     * @param {Presenter} presenter 
     */
    #setupButtons(presenter) {
        this.#sync.button = /** @type {HTMLButtonElement} */ (presenter.getElement(".header .sync", this.#section));
        presenter.addObserverTo(this.#sync.button, "click", event => this.#synchronizeAllCharacters(true).then(success => {
            if(success) {
                this.#renderCharacters();
            }
        }));
        presenter.addObserverTo(presenter.getElement(".header .cancel", this.#section), "click", event => presenter.getHistory().leaveSection());
        presenter.addObserverTo(presenter.getElement(".header .settings", this.#section), "click", event => presenter.enterSection(SettingsPresenter.sectionName));
        presenter.addObserverTo(presenter.getElement(".content .floating button.add", this.#section), "click", event => presenter.enterSection(CreationPresenter.sectionName, this));
        presenter.addObserverTo(presenter.getElement(".content .floating button.import", this.#section), "click", event => {
            presenter.getFileHandler().openUploadDialog().then(file => this.#importCharacter(file)).catch(error => this.#handleImportError(error));
        });
    }
}

/**
 * This class manages the browser's history state to allow the browser back button to be used on mobile devices to close dialogs and go back to previous UI sections without closing the app.
 */
class BackManager {
    /** @type {History} - Browser history (window.history) */
    #history;
    /**
     * @typedef {() => void} LeaverFunction - A callback that pops the current section frame and thus leaves the current UI section without calling the section's {@link SectionLeaveCallback} again.
     */
    /**
     * @callback SectionLeaveCallback - Callback function given when entering a new UI section via {@link BackManager#enterSection} and thus creating a new section frame. This function is called when the section should be left again. This callback is normally implemented by {@link AbstractSectionPresenter#close}.
     * @param {LeaverFunction} leave - A function provided by the {@link BackManager} to signal that the current section should be left now. It will leave the current section (pop the section frame) without calling this callback again.
     * @returns {boolean} - Should return true if the current's section presenter allows an immediate exit. Else false to prevent the back navigation. In the latter case the method can call the provided leave function to trigger the delayed the exit later.
     */
    /**
     * @typedef {object} SectionStackFrame - A single section frame.
     * @property {SectionLeaveCallback} callback - Callback function provided when entering the section via {@link BackManager#enterSection}.
     * @property {(value:any) => void} resolve - A Promise's resolve method that is called when this frame's section is finally left.
     */
    /** @type {SectionStackFrame[]} - A stack of section frames. Every time the UI navigates forward into a new section, a frame is pushed onto this stack. Then the user leaves a section and navigates back to the previous section, a frame is popped from the stack. */
    #stack;
    /** @type {boolean} - True if someone has already called history.forward but the event was not yet executed/received. */
    #forwardQueued = false;
    /** @type {boolean} - Set to true while we handle a browser back event in order to prevent another such event from being fired by our code in the meantime. */
    #handlingBack = false;
    /** @type {?(value:any) => void} - The Promise.resolve method of the last section frame popped if we had to call history.forward first to keep the history chain under control. */
    #leavingSection = null; 
    /**
     * @param {Window} window 
     * @param {Presenter} presenter 
     */
    constructor(window, presenter) {
        this.#history = window.history;
        this.#stack = [];
        this.leaveSection = this.leaveSection.bind(this);
        this.#initialze(window, presenter);
    }
    /**
     * Call this method to signal that a new UI section was entered (see {@link Presenter#enterSection}). It adds a new frame to the section stack. A callback can be registered that is called when
     * this section frame is popped again, i.e. when the user uses the browser's back button or the code calls {@link BackManager#leaveSection}.
     * @param {?SectionLeaveCallback} [callback=null] - Called when the user wants to leave this section again. The callback has to return true to immediately allow the back navigation (which leaves the section and resolves the returned Promise), or false to prevent it. The callback receives a leave function that can be used to exit the section (pop the section frame and resolve the Promise) without getting another call to the callback function. Do not call this leave function if you have returned true from your callback in the first place.
     * @returns {Promise.<void>} - Returns a Promise that is resolved when the entered section is left again (never rejects). Is is resolved earliest after the callback was fully executed. Subscribe to it in order to e.g. clean up resources or switch the UI again.
     */
    enterSection(callback = null) {
        if(!callback) {
            callback = this.#noop;
        }
        return new Promise((resolve) => {
            this.#stack.push({callback: callback, resolve: resolve});
            this.#ensureHistoryState();
        });
    }
    /**
     * Has to be called if code wants to leave a section without a browser back navigation having been triggered by the user. It triggers the callback from {@link BackManager#enterSection}. 
     * Do NOT call this from within such a callback, use the provided callback argument ({@link LeaverFunction}) instead (which triggers #leaveWithoutAnotherCallback).
     * This function is bound to this BackManager and thus can be used as event listener.
     */
    leaveSection() {
        if(this.#handlingBack) {
            throw new Error(`Must not call leaveSection from within a section callback!`);
        }
        this.#history.back();   // triggers a normal browser back event that is then handled by the #stateChanged method.
    }
    /**
     * Triggered when the session history state is popped by the browser. This happens when browser back is called.
     * @param {PopStateEvent} event - The event that holds a reference to the history state to which the browser navigated to if it exists.
     */
    #stateChanged(event) {
        if(event.state === this.#appState) {    // this is a forward nagivation that was triggered by #ensureHistoryState (or any other shenanigans the user did with the browser) to stay on top of the history entry we control.
            this.#forwardQueued = false;
            if(this.#leavingSection) {
                const resolve = this.#leavingSection;
                this.#leavingSection = null;
                resolve();
            }
            if(this.#stack.length === 0) {      // if the section stack is empty and we come here (e.g. because the user clicked browser forward button), reset the history chain back to the start so that browser.back will always close the app while we are in the root section.
                this.#history.back();
            }
            return;
        }
        if(this.#stack.length === 0) {  // back button clicked on top-level (start section of the app), allow browser to navigate away. On mobile this will close the app.
            return;
        }
        this.#handlingBack = true;      // set a safe-guard to prevent another browser.back call while we are still handling the last event.
        const position = this.#stack.length -1;
        const frame = this.#stack[position];
        if(frame.callback === this.#noop || frame.callback(() => this.#leaveWithoutAnotherCallback(position, frame))) {     // ask the owning presenter of the frame if we are allowed to leave its section (callback returns true). Otherwise do not pop the stack and adjust the histroy state forward to counter the back event.
            this.#stack.pop();      // leave the current section
            if(this.#needToSetHistoryForwardAgain()) {
                this.#leavingSection = frame.resolve;       // delay the fulfilling of the enterSection.Promise after we have set the browser history back to the top of the list because the listener of the Promise will most likely trigger another browser event that may not play well with our forward navigation if it comes in-between.
            } else {
                this.#handlingBack = false;
                frame.resolve();
                return;
            }
        }
        this.#handlingBack = false;
        this.#ensureHistoryState();
    }
    /**
     * Implementation of the {@link LeaverFunction} that is returned by the {@link BackManager}. It can be called by the section presenter to trigger a delayed exit.
     * It replaces the registered {@link SectionLeaveCallback} with a NOOP dummy so that the presenter's callback is not called again, and then triggers a browser.back so that the our stateChanged handling will pop the corresponding frame.
     * @param {number} stackPosition - The position in the section stack that we want to pop.
     * @param {SectionStackFrame} frame - The section frame we want to pop. Use for a safety check.
     */
    #leaveWithoutAnotherCallback(stackPosition, frame) {
        if(this.#stack[stackPosition] === frame) {   // safety check
            this.#stack[stackPosition].callback = this.#noop;    // removes the original callback (preventing an infinite loop)
        }
        this.leaveSection();
    }
    /**
     * As long as we have something on the section stack (and thus want to control what browser.back does), keep the history state on the second entry (top of the history).
     */
    #ensureHistoryState() {
        if(this.#needToSetHistoryForwardAgain()) {
            this.#forwardQueued = true;     // It can happen that someone calls enterSection (to e.g. open a new modal dialog) while we are already waiting to navigate forward. In this case do not navigate twice.
            this.#history.forward();
        }
    }
    /**
     * As long as there are frames on the section stack (i.e. the user is not in the root section but deeper down in the UI) we want to stay on top of the browser history so that we can intercept any back navigation events. Without our artifical history entry, browser.back would just close the app on mobile devices.
     * @returns {boolean} - Returns true if we are currently at the bottom of the browser history but we have still section frames on our stack and should therefore be on top of the history.
     */
    #needToSetHistoryForwardAgain() {
        return !this.#forwardQueued && this.#stack.length > 0 && this.#history.state !== this.#appState;
    }
    /**
     * @param {Window} window - Browser window
     * @param {Presenter} presenter - Window presenter 
     */
    #initialze(window, presenter) {
        this.#history.pushState(this.#appState, this.#unused);      // we basically switch between two history states: the first one was created by the browser when opening the app, the second (top) one we append here so that we can react to the browser back event (popstate) and block the back navigation if we want.
        this.#history.back();   // we always start in the first (bottom) position (so that a back will close the app, at least on mobile) and only switch to the second (top) position when new sections were entered (are pushed to the section stack).
        presenter.addObserverTo(window, "popstate", /** @param {PopStateEvent} event */ (event) => this.#stateChanged(event));    // is called when the history entries change because user clicked the browser/device buttons or code called back/forward/go (but not with pushState/replaceState)
    }
    #noop() {
        return true;
    }
    get #appState() {
        return "earthdawn app";     // artifical history state that we push onto the top of the browser history
    }
    get #unused() {
        return "";  // constant required by browser History API
    }
}

/**
 * A reusable modal dialog. Automatically clears its content when it is closed so that it can be filled again the next time before it is opened.
 * As the name suggests only one modal dialog can be open at the same time. The modal also creates a backdrop element. Clicking the backdrop or triggering browser.back has the same effect as clicking the close (x) button
 * of the dialog or calling the close method.
 */
class ModalDialog {
    #presenter;
    /** @type {HTMLElement} - The backdrop element that sits directly below the dialog but above the rest of the UI. Clicking it will also close the dialog. */
    #backdrop;
    /** @type {HTMLElement} - Container element for the dialog */
    #dialog;
    /** @type {HTMLElement} - Container element for the title bar within the dialog */
    #title; 
    /** @type {HTMLElement} - Container element for the content area within the dialog */
    #content;
    /** @type {?(value:any) => void} - A Promise.resolve callback while a dialog is currently closing, else null */
    #closing = null;
    /**
     * @param {Presenter} presenter - The window presenter
     */
    constructor(presenter) {
        this.#presenter = presenter;
        this.#backdrop = presenter.getElement("#modal-backdrop");
        this.#dialog = presenter.getElement("#modal-dialog");
        this.#title = presenter.getElement(".title", this.#dialog);
        this.#content = presenter.getElement(".content", this.#dialog);
        this.#setupClosers();
    }
    /**
     * Appends content to the content area of the dialog. Ideally call this before opening the dialog.
     * @param  {...(string|Node)} elementOrText - Contents to add. Can be either strings that should be added as a text node or any HTML nodes.
     * @returns {ModalDialog} - Returns this.
     */
    addContent(...elementOrText) {
        this.#content.append(...elementOrText);
        return this;
    }
    /**
     * Appends content to the title bar of the dialog. Ideally call this before opening the dialog.
     * @param {(string|Node)} elementOrText - Title to add. Can be either a string that should be added as a text node or any HTML node.
     * @returns {ModalDialog} - Returns this
     */
    addTitle(elementOrText) {
        this.#title.append(elementOrText);
        return this;
    }
    /**
     * Adds CSS classes to the dialog container element. Use this to override the style of the whole dialog.
     * @param  {...string} klasses - The customer CSS classes to add
     * @returns {ModalDialog} - Returns this
     */
    addCssClass(...klasses) {
        this.#dialog.classList.add(...klasses);
        return this;
    }
    /**
     * Convenience method to open a modal dialog that asks for user confirmation. Styles this modal as a confirmation dialog with a title (question), content text, a cancel button (with given label) 
     * and a confirm button (with given label).
     * @param {string} title - The title of the dialog. Should be a short question.
     * @param {string} text - The content of the dialog. Should explain what choice the user has to make.
     * @param {string} cancelLabel - Label of the cancel button.
     * @param {string} confirmLabel - Label of the confirm button.
     * @returns {Promise.<void>} - Returns a Promise that is resolved when the confirm button is clicked, or rejected when the cancel button is clicked (or the dialog is closed in any other way like browser.back).
     */
    async openConfirmation(title, text, cancelLabel, confirmLabel) {  
        this.addCssClass("confirmation");
        this.addTitle(title);
        const question = this.#presenter.createElement("p");
        question.textContent = text;
        this.addContent(question);
        const buttonContainer = this.#presenter.createElement("div");
        buttonContainer.className = "buttons";
        this.addContent(buttonContainer);
        const cancel = /** @type {HTMLButtonElement} */ (this.#presenter.createElement("button"));
        cancel.type = "button";
        cancel.className = "cancel";
        cancel.textContent = cancelLabel;
        buttonContainer.append(cancel);
        const confirm = /** @type {HTMLButtonElement} */ (this.#presenter.createElement("button"));
        confirm.type = "button";
        confirm.className = "confirm";
        confirm.textContent = confirmLabel;
        buttonContainer.append(confirm);
        return new Promise((resolve, reject) => {
            this.#presenter.addObserverTo(confirm, "click", () => {
                this.close().then(() => resolve());
            });
            this.#presenter.addObserverTo(cancel, "click", () => {
                this.close().then(() => reject());
            });
            this.open();
        });
    }
    /**
     * Closes this dialog.
     * @returns {Promise.<void>} - Returns a Promise that is resolved as soon as the modal dialog was fully closed. Never rejects.
     */
    async close() {
        return new Promise((resolve) => {
            this.#closing = resolve;
            this.#presenter.getHistory().leaveSection();    // will eventually result in #hideAndCleanup to be called
        });
    }
    /**
     * Opens the modal dialog. Adds a new section frame to the {@link BackManager} so that the user can close the dialog also via browser back.
     * @returns {Promise.<void>} - Returns a Promise that is resolved when the modal dialog was closed in any way, never rejects. It is resolved after the promise returned by {@link ModalDialog#close}.
     * @throws {Error} - Throws if there is already a modal dialog open at the moment.
     */
    open() {
        if(this.#closing) {
            throw new Error(`Cannot open another modal dialog while there is still one open`);
        }
        this.#backdrop.hidden = false;
        this.#dialog.hidden = false;
        this.#content.scrollTop = 0;
        return new Promise((resolve) => {
            this.#presenter.getHistory().enterSection().then(() => this.#hideAndCleanup()).then(() => resolve());
        });
    }
    #hideAndCleanup() {
        this.#dialog.hidden = true;
        this.#dialog.className = "";
        this.#backdrop.hidden = true;
        this.#content.replaceChildren();
        this.#title.replaceChildren();
        if(this.#closing) {
            const resolve = this.#closing;
            this.#closing = null;
            resolve();
        }
    }
    #setupClosers() {
        this.#presenter.addObserverTo(this.#backdrop, "click", () => this.close());
        this.#presenter.addObserverTo(this.#presenter.getElement("button.close", this.#dialog), "click", () => this.close());
    }
}

/**
 * A helper class that can show temporary toasts at the top of the screen that vanish automatically after some time.
 * Multiple toasts can exist at the same time. They are stacked above each other, oldest at the top.
 */
class Notifications {
    #presenter;
    /** @type {HTMLElement} - Container element that overlay the normal UI at the top of the screen */
    #container;
    /**
     * @param {Presenter} presenter - Window presenter
     */
    constructor(presenter) {
        this.#presenter = presenter;
        this.#container = presenter.getElement("#notifications");
    }
    /**
     * Shows a blue information toast.
     * @param {string} text - The text content
     */
    postInfo(text) {
        this.#showNotification(text, "info");
    }
    /**
     * Shows a yellow warning toast.
     * @param {string} text - The text content.
     */
    postWarning(text) {
        this.#showNotification(text, "warning");
    }
    /**
     * Shows a red error toast.
     * @param {string} text - The text content.
     */
    postError(text) {
        this.#showNotification(text, "error");
    }
    /**
     * @param {string} text - Content of the toast. The longer the text the longer the toast stays visible.
     * @param {string} type - Type constant that identifies both the icon as well as the CSS class (for the background color) of the toast.
     */
    #showNotification(text, type) {
        const iconCss = {
            info: "fa-regular fa-comment",
            warning: "fa-regular fa-bell",
            error: "fa-solid fa-bomb"
        };
        const note = this.#presenter.createElement("div");
        note.className = type;
        const icon = this.#presenter.createElement("span");
        icon.className = iconCss[type];
        note.append(icon);
        const content = this.#presenter.createElement("span");
        content.textContent = text;
        note.append(content);
        this.#container.prepend(note);
        const delay = 1000 + text.length * 60;
        const timer = setTimeout(() => note.remove(), delay);
        this.#presenter.addObserverTo(note, "click", () => {
            clearTimeout(timer);
            note.remove();
        }, {once: true});
    }
}

/**
 * Helper class for uploading file to and downloading files from the app.
 */
class FileHandler {
    #presenter;
    /**
     * @param {Presenter} presenter - Window presenter
     */
    constructor(presenter) {
        this.#presenter = presenter;
    }
    /**
     * Presents a save dialog to the user to save the given content as a file.
     * @param {(string|object|Array)} content - The content that should be downloaded as a file. Must either be a string or a simple object/array that can be stringified to JSON. Must not be a {@link Character} or {@link CharacterRecord}.
     * @param {string} filename - The filename that should be proposed by the save dialog. The method automatically adds a file extension of '.ed'
     * @param {string} [mimeType="application/json"] - Mime type of the file. If an object/array was provided as content, this will be overriden with 'application/json'.
     */
    openDownloadDialog(content, filename, mimeType = "application/json") {
        if(content instanceof Character || content instanceof CharacterRecord) {
            throw new Error(`Character or CharacterRecord cannot be exported directly. Serialize to object first!`);
        }
        if(typeof content === "object") {   // Javascript object or array (do not use full Character class here but save() it first)
            content = JSON.stringify(content);
            mimeType = "application/json";
        }
        const blob = new Blob([content], {type: mimeType});
        const url = URL.createObjectURL(blob);
        const link = /** @type {HTMLAnchorElement} */ (this.#presenter.createElement("a"));
        link.download = this.#toFileName(filename);
        link.href = url;
        link.click();
        setTimeout(() => URL.revokeObjectURL(url), 250);
    }
    /**
     * Opens a file selection dialog in the browser that allows uploading a file into the app. Expects the file content to be parsable as JSON.
     * @returns {Promise.<any>} - Returns a Promise that is either resolved with the parsed JSON content of the uploaded file, or is rejected with a simple object that contains the filename as 'file' property and an error string as 'reason'.
     */
    openUploadDialog() {        // upload characters and returns the deserialized JSON object
        const input = /** @type {HTMLInputElement} */ (this.#presenter.createElement("input"));
        input.type = "file";
        input.accept = this.#characterFileExtension;
        return new Promise((resolve, reject) => {
            this.#presenter.addObserverTo(input, "change", event => {
                const file = input.files[0];
                if(file.size > 0) {
                    file.text().then(content => {
                        try {
                            resolve(JSON.parse(content));
                        } catch {
                            reject({file: file.name, reason: "malformed"});
                        }
                    }, () => reject({file: file.name, reason: "encoding"}));
                } else {
                    reject({file: file.name, reason: "empty"});
                }
            }, {once: true});
            input.click();
        });
    }
    /**
     * @param {string} name - A raw filename
     * @returns {string} - Returns an cleaned filename where all whitespace, commas and dots are replaced with underscores and the extension '.ed' is added. 
     */
    #toFileName(name) {
        return name.trim().replaceAll(/[\s.,]+/g, "_").substring(0, 30) + this.#characterFileExtension;
    }
    get #characterFileExtension() {
        return ".ed";
    }
}


/**
 * Error object used for problems in {@link Persistence} transactions.
 */
class PersistenceError extends Error {
    /**
     * @param {string} action - During which action did the error occur. 
     * @param {string} name - Error code/name. 
     * @param {?string} message - Human-readable error message
     */
    constructor(action, name, message) {
        super(message);
        this.action = action;
        this.name = name;
    }
}

/**
 * A record object that wraps the database entry of a {@link Character} and allows lazy-loading of said character. Also contains additional information used by {@link CloudSynchronizer}.
 */
class CharacterRecord {
    #dbRecord;
    #character;
    /**
     * @typedef {object} ConflictDetails - Details about a remote character when the local version is in conflict with the cloud version.
     * @property {?object} serialized - A saved character as it exists in cloud storage. Can be undefined if the remote character was deleted.
     * @property {?number} updated - Timestamp (epoch millis UTC) when the remote version was last updated, or undefined if the remote character was deleted.
     * @property {number} synced - Timestamp (epoch millis UTC) when we have downloaded (synced) this character from the cloud
     */
    /** @type {?ConflictDetails} - Conflicting remote data that matches this character. The latest serialized character data from the cloud with same id as a local character but with different content. */
    #conflict;
    /**
     * @param {CharacterDatabaseRecord} dbRecord - The database entry of a character
     * @param {Character} [character=null] - The character parsed/loaded from the saved state of the database record. Can be null to be lazy-loaded later.
     */
    constructor(dbRecord, character = null) {
        this.#dbRecord = dbRecord;
        this.#character = character;
        this.#conflict = null;
    }
    /**
     * @returns {CharacterId} - Unique id of the character.
     */
    getId() {
        return this.#dbRecord.character.id;
    }
    /**
     * @returns {CharacterDatabaseRecord} - The internal database entry of this record.
     */
    getDbRecord() {
        return this.#dbRecord;
    }
    /**
     * Lazy-loads the characer from its saved state loaded from persistence.
     * @throws {Error} - Throws if the character is soft-deleted. In this case the saved state is truncated and only the id and name are available.
     * @returns {Promise.<Character>}
     */
    async getCharacter() {
        if(this.isDeleted()) {
            throw new Error(`Cannot return deleted character`);
        }
        if(!this.#character) {
            this.#character = await Character.load(this.#dbRecord.character);
        }
        return this.#character;
    }
    /**
     * @returns {Date} - Returns the datetime the character record was last updated (or created). This value is updated whenever the character is stored in {@link Persistence}.
     */
    getLastUpdateTime() {
        return new Date(this.#dbRecord.updated);
    }
    /**
     * @returns {?Date} - Returns the datetime when this character was successfully synchronized the last time, or null. If synchronization resulted in a conflict, this value is not updated until the conflict is resolved.
     */
    getLastSyncTime() {
        return this.#dbRecord.synced ? new Date(this.#dbRecord.synced) : null;
    }
    /**
     * @returns {boolean} - Returns true if this character was soft-deleted. {@link CharacterRecord#getCharacter} is not available if this returns true.
     */
    isDeleted() {
        return this.#dbRecord.deleted;
    }
    /**
     * @returns {boolean} - Returns true if this record was never synced or the characters was locally updated since the last sync.
     */
    needsSync() {
        return !this.#dbRecord.synced || this.#dbRecord.synced < this.#dbRecord.updated;
    }
    /**
     * Updates this record with the content of the passed in character but does not call {@link Persistence}.
     * @param {Character} character - The modified character that belongs to this record.
     * @returns {CharacterRecord} - Returns this
     */
    update(character) {
        if(character.getId() !== this.getId()) {
            throw new Error(`Cannot update character record ${this.getId()} with data from non-matching character ${character.getId()}`);
        }
        this.#character = character;
        this.#dbRecord.character = character.save();
        return this;
    }
    /**
     * Marks this character as soft-deleted. Truncates all character data down to its id and name.
     * @returns {CharacterRecord} - Returns this
     */
    delete() { 
        this.#character = null;
        this.#dbRecord.deleted = true;
        this.#dbRecord.character = {id: this.#dbRecord.character.id, name: this.#dbRecord.character.name};  // remove all data except for the important parts
        return this;
    }
    /**
     * Updates this record's synchronization time after a successful synchronization with cloud storage. If the remote character is newer, also the character and its last modification time are updated.
     * @param {number} syncTime - Timestamp (epoch millis UTC) when the sync was executed by the app.
     * @param {?object} [serialized=null] - Saved character state retrieved from the cloud or null if the local version won.
     * @param {?number} [changeTime=null] - Timestamp (epoch millis UTC) when the given serialized character was last updated or null if the local version won.
     */
    synced(syncTime, serialized = null, changeTime = null) {
        this.#dbRecord.synced = syncTime;
        if(serialized && changeTime) {
            this.#dbRecord.updated = changeTime;
            this.#dbRecord.character = serialized;
            this.#character = null;
        }
    }
    /**
     * Marks this record as conflicted when the last synchronization reported a conflict between the local character and its remote version. This method does not update the local
     * character handled by this record. Use any of the resolve methods of this record to clear the conflict.
     * @param {number} syncTime - Timestamp (epoch millis UTC) when the sync was executed by the app.
     * @param {?object} serialized - Saved character state retrieved from the cloud, or undefined if the character was deleted in the cloud.
     * @param {?number} changed - Timestamp (epoch millis UTC) when the serialized remote character was last updated in the cloud, or undefined if the remote character has been deleted.
     */
    conflict(syncTime, serialized, changed) {
        // Do not update #dbRecord.synced because we have not resolved the conflict yet and thus nothing was updated locally
        this.#conflict = {
            serialized: serialized,     // may be undefined if there is a deletion conflict
            updated: changed,           // may be undefined if there is a deletion conflict
            synced: syncTime
        };
    }
    /**
     * @returns {boolean} - Returns true if the local character handled by this record is conflicting with its remote version as detected by the last cloud sync.
     */
    hasConflict() {
        return this.#conflict != null;
    }
    /**
     * @returns {?ConflictDetails} - Returns an object with details about the remote version of this character if {@link CharacterRecord#hasConflict} is true.
     */
    getConflictDetails() {
        return this.#conflict;
    }
    /**
     * Returns a dummy character record representing a remote character from cloud storage. This method can be used only if {@link CharacterRecord#hasConflict} is true and the conflict does not represent a remote deletion.
     * The returned record can be used to display this character via {@link CharacterViewPresenter} but it must never be stored in local {@link Persistence}!
     * @returns {CharacterRecord} - A dummy record that represents a conflicting remote version of this character. This record must not be stored as is in the local database!
     */
    getConflictCharacterRecord() {
        if(!this.hasConflict()) {
            throw new Error(`No conflicting character record available`);
        }
        if(!this.#conflict.serialized) {
            throw new Error(`No character data provided for delete conflict`);
        }
        return new CharacterRecord({
            type: CharacterRecordType.remote,
            updated: this.#conflict.updated,
            synced: this.#conflict.synced,
            deleted: false,
            character: this.#conflict.serialized
        });
    }
    /**
     * Overwrites the local record with the cloud data. May restore a previously soft-deleted character. Needs a db update afterwards.
     */
    resolveUsingCloud() {  
        if(!this.#conflict.serialized) {
            throw new Error(`Can resolve using cloud only for update conflicts`);
        }
        this.#dbRecord.updated = this.#conflict.updated;
        this.#dbRecord.synced = this.#conflict.synced;
        this.#dbRecord.character = this.#conflict.serialized;
        this.#dbRecord.deleted = false;
        this.#character = null;
        this.#conflict = null;
    }
    /**
     * Discards the cloud changes and keeps the local version. Needs a db update of this record and a sync to push the new state into the cloud to overwrite the version there.
     */
    resolveUsingLocal() {     
        if(!this.#conflict.serialized) {    // cloud version was deleted, therefore treat this local character as never synced to trigger a new upload.
            this.#dbRecord.synced = null;
        } else {
            this.#dbRecord.synced = this.#conflict.synced;      // character exists in both cloud and local, therefore mark local as up-to-date to force an update on remote.
        }
        this.#conflict = null;
    }
    /**
     * Creates a copy of the local character and assigns a new id and name to it. Then updates this record with the character data from the cloud to resolve the conflict. Requires a db update of this record afterwards.
     * @returns {Promise.<Character>} - Returns the newly created character copy. This copy was not stored locally and requires a db insert to persist it.
     */
    async resolveCreateCopy() {  
        const copy = await this.getCharacter();
        copy.setId(crypto.randomUUID());
        if(!copy.getName().endsWith("(copy)")) {
            copy.setName(copy.getName() + " (copy)");
        }
        this.resolveUsingCloud();   // resets this character record to the remote version
        return copy;
    }
    /**
     * Opens a download dialog to export this character as a save file.
     * @param {FileHandler} fileHandler
     */
    export(fileHandler) {
        fileHandler.openDownloadDialog(this.#dbRecord.character, this.getName());
    }
    /**
     * @returns {string} - Returns the name of this character. Works also if the character was soft-deleted, in which case {@link CharacterRecord#getCharacter} will not be available.
     */
    getName() {
        if(this.#character) {
            return this.#character.getName();
        }
        return this.#dbRecord.character.name;
    }
    /**
     * Sorts two instances by their localized name and id.
     * @param {CharacterRecord} a 
     * @param {CharacterRecord} b 
     * @returns {number} Returns 0 if a and b are equal, 1 if a is greater than b, -1 if a is less than b.
     */
    static compare(a, b) {      // compares two CharacterRecords
        let result = a.getName().localeCompare(b.getName());
        if(result === 0) {
            result = a.getId().localeCompare(b.getId());
        }
        return result;
    }
}

/**
 * @enum {string} - Type of a {@link CharacterRecord}.
 */
const CharacterRecordType = Object.freeze({
    /** @constant final - Used for characters that have completed their character creation and can be stored in the local database */
    final: "final",
    /** @constant remote - Marks a record as representing a remote version of character as retrieved from the cloud. Such a record must never be stored in the local database as is. */
    remote: "remote"
});

/**
 * @typedef {object} CharacterDatabaseRecord - Db record of a single {@link Character}. Wrapped by {@link CharacterRecord}.
 * @property {CharacterRecordType} type - Type of this record. Final is used for local db entries, remote for records retrieved from the cloud but not locally saved.
 * @property {number} updated - Timestamp (epoch millis in UTC) when this record was last updated/created.
 * @property {?number} synced - Timestamp (epoch millis in UTC) when this record was last synchronized into the cloud. May be null if it was never synced with the cloud yet.
 * @property {boolean} deleted - A flag if this character was soft-deleted. Soft-deleted characters are kept in local storage until their state was synced to the cloud.
 * @property {object} character - A character saved as a simple Javascript object that can be easily converted to JSON. Created via {@link Character#save}.
 */
/**
 * @typedef {object} AuthSettingsDatabaseRecord - Db record for the authentication settings used by {@link CloudSynchronizer}.
 * @property {"auth"} type - Type of this record. Used to distinguish different records in the settings store.
 * @property {string} email - Email address of the app user under which the characters are stored in the cloud. May be empty.
 * @property {string} token - Auth token (apikey) used to make API tokens as the user identified by the email. May be empty.
 */

/**
 * Repository to persist {@link Character}s and settings in the IndexedDB of the browser.
 */
class Persistence {
    /** @type {IDBFactory} - from window.indexedDB */
    #storage;
    /** @type {IDBDatabase} - Caches an instance of the open database. Don't use this directly, use this.#getDb() instead! */
    #db;
    /** @type {Object.<string,IDBTransactionMode>}} - Constants used by the IndexedDB API */
    #constants = {
        readonly: "readonly",
        readwrite: "readwrite"
    };
    /**
     * @param {Window} window - Browser window 
     */
    constructor(window) {
        this.#storage = window.indexedDB;
        this.#db = null;
    }
    /**
     * @param {object} serialized - A character saved as a simple Javascript object that can be easily converted to JSON for storage in IndexedDB. Created via {@link Character#save}.
     * @returns {CharacterDatabaseRecord} - Db record that is stored in the 'characters' object store.
     */
    #newCharacterDbRecord(serialized) {
        return {
            type: CharacterRecordType.final,
            updated: Date.now(),
            synced: null,
            deleted: false,
            character: serialized
        };
    }
    /**
     * Inserts a new local character into the database.
     * @param {Character} character - The character to insert into the db. Has to have a unique id.
     * @returns {Promise.<CharacterRecord,PersistenceError>} - Resolves to the newly created record for the given character or rejects with a {@link PersistenceError}.
     */
    async insertNewCharacter(character) {
        if(!(character instanceof Character)) {
            throw new Error(`Argument is not a Character`);
        }
        const serialized = character.save();
        if(!serialized.id) {
            throw new Error(`Id property missing in serialized character. This is used a the primary db key and thus needed.\n${serialized}`);
        }
        const record = this.#newCharacterDbRecord(serialized);
        const db = await this.#getDb();
        const transaction = db.transaction([this.#characterStoreName], this.#constants.readwrite);
        const store = transaction.objectStore(this.#characterStoreName);
        this.#insertInto(store, record).catch(error => {
            if(error.name === "DuplicateKey") {
                record.character.id = crypto.randomUUID();
                return this.#insertInto(store, record);
            }
            throw error;
        });
        return this.#transactionResult(transaction).then(() => new CharacterRecord(record, character));
    }
    /**
     * Inserts a new character into storage that was received from remote via {@link CloudSynchronizer}.
     * @param {object} serialized - A character saved as a simple Javascript object that can be easily converted to JSON for storage in IndexedDB. Created via {@link Character#save}.
     * @param {number} updated - Timestamp (epoch millis in UTC) when this record was last updated/created.
     * @param {number} synced - Timestamp (epoch millis in UTC) when this record was last synchronized into the cloud.
     * @returns {Promise.<CharacterRecord,PersistenceError>} - Resolves to a new record for the stored character, or rejects with an error.
     */
    async insertCloudRecord(serialized, updated, synced) {
        if(!serialized.id) {
            throw new Error(`Id property missing in serialized character. This is used a the primary db key and thus needed.\n${serialized}`);
        }
        const record = this.#newCharacterDbRecord(serialized);
        record.updated = updated;
        record.synced = synced;
        const db = await this.#getDb();
        const transaction = db.transaction([this.#characterStoreName], this.#constants.readwrite);
        const store = transaction.objectStore(this.#characterStoreName);
        store.add(record);
        return this.#transactionResult(transaction).then(() => new CharacterRecord(record));
    }
    /**
     * Updates the database entry of the given character record.
     * @param {CharacterRecord} characterRecord - The existing record to persist.
     * @param {boolean} [refreshUpdateTime=true] - If set to true, will update the record's 'updated' timestamp to now. Else leaves it as it was.
     * @returns {Promise.<CharacterRecord,PersistenceError>} - Resolves to the updated record after it was stored, or rejects with an error.
     */
    async updateCharacter(characterRecord, refreshUpdateTime = true) {
        if(!(characterRecord instanceof CharacterRecord)) {
            throw new Error(`Argument is not a CharacterRecord`);
        }
        const record = characterRecord.getDbRecord();
        if(record.type === CharacterRecordType.remote) {
            throw new Error(`Database entry cannot be updated from remote CharacterRecord`);
        }
        if(refreshUpdateTime) {
            record.updated = Date.now();
        }
        const db = await this.#getDb();
        const transaction = db.transaction([this.#characterStoreName], this.#constants.readwrite);
        const store = transaction.objectStore(this.#characterStoreName);
        store.put(record);
        return this.#transactionResult(transaction, characterRecord);
    }
    /**
     * Marks the character as soft-deleted (instead of destroying it directly) so that we can sync this information later to the cloud and updates its db entry.
     * @param {CharacterRecord} characterRecord - The existing record to update.
     * @returns {Promise.<CharacterRecord,PersistenceError>} - Resolves to the updated record after it was stored, or rejects with an error.
     */
    async softDeleteCharacter(characterRecord) {
        if(!(characterRecord instanceof CharacterRecord)) {
            throw new Error(`Argument is not a CharacterRecord`);
        }
        return this.updateCharacter(characterRecord.delete());
    }
    /**
     * Permanently deletes a local character from the database. Use this only for characters that were never stored remotely or after synchronizing the deletion into the cloud.
     * @param {CharacterRecord} characterRecord - The existing record to permanently delete from the db.
     * @returns {Promise.<void,PersistenceError>} - Resolves the promise on success or rejects with an error.
     */
    async destroyCharacter(characterRecord) { 
        if(!(characterRecord instanceof CharacterRecord)) {
            throw new Error(`Argument is not a CharacterRecord`);
        }
        if(characterRecord.getDbRecord().type === CharacterRecordType.remote) {
            throw new Error(`Database entry cannot be deleted by remote CharacterRecord`);
        }
        const db = await this.#getDb();
        const transaction = db.transaction([this.#characterStoreName], this.#constants.readwrite);
        const store = transaction.objectStore(this.#characterStoreName);
        store.delete(characterRecord.getId());
        return this.#transactionResult(transaction);
    }
    /**
     * Loads all existing characters from the database.
     * @returns {Promise.<CharacterRecord[],PersistenceError>} - All loaded characters, including soft-deleted ones. Rejects with an error if the query failed.
     */
    async loadAllCharacters() { 
        const db = await this.#getDb();
        const transaction = db.transaction([this.#characterStoreName], this.#constants.readonly);
        const store = transaction.objectStore(this.#characterStoreName);
        return this.#wrapDbRequest(() => store.getAll(), "get-all", true).then(result => result.map(record => new CharacterRecord(record)));
    }
    /**
     * Load the existing auth settings from the database or creates and inserts a new auth settings record.
     * @returns {Promise.<AuthSettingsDatabaseRecord,PersistenceError>} - Resolves to the existing or new settings record or rejects with an error.
     */
    async loadSettings() {
        const db = await this.#getDb();
        const transaction = db.transaction([this.#settingsStoreName], this.#constants.readonly);
        const store = transaction.objectStore(this.#settingsStoreName);
        const record = await this.#wrapDbRequest(() => store.get("auth"), "get", true);
        if(record) {
           return record;
        }
        return this.#createSettings();
    }
    /**
     * Updates the db entry of the given auth settings.
     * @param {AuthSettingsDatabaseRecord} record - The auth settings record to persist.
     * @returns {Promise.<void,PersistenceError>} - Resolves on success or rejects with an error.
     */
    async updateSettings(record) {
        const db = await this.#getDb();
        const transaction = db.transaction([this.#settingsStoreName], this.#constants.readwrite);
        const store = transaction.objectStore(this.#settingsStoreName);
        store.put(record);
        return this.#transactionResult(transaction);
    }
    /**
     * @returns {AuthSettingsDatabaseRecord} - Returns a new auth settings record.
     */
    #newAuthSettingsRecord() {
        return {
            type: "auth",
            email: "",
            token: ""
        };
    }
    /**
     * Creates new auth settings and inserts them into the database.
     * @returns {Promise.<AuthSettingsDatabaseRecord,PersistenceError>} - Resolves to the new settings record or rejects with an error.
     */
    async #createSettings() {
        const record = this.#newAuthSettingsRecord();
        const db = await this.#getDb();
        const transaction = db.transaction([this.#settingsStoreName], this.#constants.readwrite);
        const store = transaction.objectStore(this.#settingsStoreName);
        store.add(record);
        return this.#transactionResult(transaction, record);
    }
    /**
     * Inserts a new record into the given object store.
     * @param {IDBObjectStore} store - The object store holding the corresponding record type
     * @param {object} record - The db record to store. Has to include a property that acts as inline-key as defined by the createXstore methods.
     * @returns {Promise.<IDBValidKey,PersistenceError>} - Resolves with the key of the inserted record, or rejects with an error.
     */
    async #insertInto(store, record) {
        return new Promise((resolve, reject) => {
            const adding = store.add(record);
            adding.onsuccess = event => {
                resolve(/** @type {IDBRequest} */ (event.target).result);
            };
            adding.onerror = event => {
                const e = /** @type {IDBRequest} */ (event.target).error;
                const error = new PersistenceError("insert-record", e.name, e.message);
                if(e.name === "ConstraintError") {
                    event.stopPropagation();
                    error.name = "DuplicateKey";
                }
                reject(error);
            };
        });
    }
    /**
     * Executes a given db query and wraps the result in a promise. Mainly used for read queries where we do not have to wait for the transaction to complete but can directly use the result of the IDBRequest success event.
     * Readwrite operations should listen for the complete event on the IDBTransaction instead because the IDBRequest.onsuccess is fired before the transaction was actually committed.
     * @param {() => IDBRequest} action - Callback that produces and executes the db request to listen on.
     * @param {string} actionName - Human-readable name of the action/query that gets executed. Used for error reporting only.
     * @param {boolean} [stopError=false] - For readonly requests this can be set to true to stop any error events from bubbling up to the transaction and then the db. Must not be set to true for readwrite requests.
     * @returns {Promise.<any,PersistenceError>} - Resolves with the requested data (db record), or rejects with an error.
     */
    #wrapDbRequest(action, actionName, stopError = false) {
        return new Promise((resolve, reject) => {
            const request = action();
            request.onsuccess = event => {
                resolve(/** @type {IDBRequest} */ (event.target).result);
            }
            request.onerror = event => {
                if(stopError) {
                    event.stopPropagation();
                }
                const target = /** @type {IDBRequest} */ (event.target);
                reject(new PersistenceError(actionName, target.error.name, target.error.message));
            }
        });
    }
    /**
     * Waits for the current transaction to finish: either completing after committing which is done automatically when all outstanding IBDRequests created on this transaction has been satisfied,
     * or rolling back with an error if there were any issues during the commit or already earlier during the query. All error events on the IDBRequest that are not stopped from propagating will bubble up to the transaction
     * and result in a rollback here. Therefore readwrite events should be handled on the transaction to make sure everything was committed. Readonly requests can instead handle the events directly on the IDBRequest.
     * @param {IDBTransaction} transaction - The db transaction on which to listen for events.
     * @param {any} [result=null] - Optional result object that should be returned by the Promise.resolve. May be null. This could be the content of IDBRequest.result for read queries.
     * @returns {Promise.<any,PersistenceError>} - Resolves when the transaction was sucessfully committed and returns the object passed in the result parameter. Rejects on rollback if there was an error.
     */
    #transactionResult(transaction, result = null) {
        return new Promise((resolve, reject) => {
            transaction.oncomplete = event => resolve(result);
            transaction.onerror = event => {
                event.stopPropagation();    // prevent this error to bubble up to the IDBDatabase instance where we also have an error handler registered.
                reject(new PersistenceError("commit", transaction.error.name, transaction.error.message));
            };
        });
    }
    /**
     * Get a cached open db connection or opens a new connection if necessary.
     * @returns {Promise.<IDBDatabase>} - Resolves to an open database connection
     */
    async #getDb() {
        if(!this.#db) {
            this.#db = await this.#openDb();
            this.#db.onclose = () => {
                this.#db = null;
            };
        }
        return this.#db;
    }
    /**
     * Opens a new db connection. Do not call this directly, use this.#getDb() instead.
     * @returns {Promise.<IDBDatabase,PersistenceError>} - Resolves to an open database connection
     */
    async #openDb() { 
        return new Promise((resolve, reject) => {
            const opening = this.#storage.open(this.#dbName, this.#dbVersion);
            opening.onerror = event => {
                console.log("Unable to open database", event);
                const target = /** @type {IDBOpenDBRequest} */ (event.target);
                reject(new PersistenceError("db-open", target.error.name, target.error.message));
            };
            opening.onsuccess = event => {
                const target = /** @type {IDBOpenDBRequest} */ (event.target);
                const db = target.result;
                db.onerror = event => console.log("Database error", target.error);    // unexpected errors not handled by the transaction or request.
                resolve(db);
            };
            opening.onupgradeneeded = event => {    // called when the db has to be created for the first time or if opened with a higher version. Automatically calls opening.onsuccess afterwards.
                const target = /** @type {IDBOpenDBRequest} */ (event.target);
                const db = target.result;
                db.onerror = event => console.log("Database error", target.error);    // unexpected errors not handled by the transaction or request.
                this.#createCharacterStore(db);
                this.#createSettingsStore(db);
            };
        });
    }
    /**
     * @param {IDBDatabase} db 
     */
    #createCharacterStore(db) {
        db.createObjectStore(this.#characterStoreName, {
            keyPath: "character.id"
        });
    }
    /**
     * @param {IDBDatabase} db 
     */
    #createSettingsStore(db) {
        db.createObjectStore(this.#settingsStoreName, {
            keyPath: "type"
        });
    }
    get #characterStoreName() {
        return "characters";
    }
    get #settingsStoreName() {
        return "settings";
    }
    get #dbName() {
        return "earthdawn-pwa";
    }
    get #dbVersion() {
        return 1;
    }
}

/**
 * Error thrown by {@link CloudSynchronizer} if synchronization is not possible because there isn't a valid auth token configured for the API.
 */
class SyncDisabled extends Error {
    constructor(...args) {
        super(...args);
        this.name = "SyncDisabled";
    }
}

/**
 * Helper class to synchronize {@link Character}s with remote cloud storage in order to make them available across devices. Interacts with a remote
 * server API for which the user has to request and enter an auth token in the app.
 */
class CloudSynchronizer {
    #presenter;
    /** @type {?AuthSettingsDatabaseRecord} - Db record of the authentication settings that contain the user identifier (email) and API auth token. */
    #dbRecord;
    /** @type {boolean} - Flag set to true if the app auto-requested a new auth token from the server to prevent another request in the meantime */
    #tokenRequested;
    /**
     * @param {Presenter} presenter - Window presenter 
     */
    constructor(presenter /* Presenter */) {
        this.#presenter = presenter;
        this.#dbRecord = null;
        this.#tokenRequested = false;
    }
    /**
     * Synchronizes the local characters with the cloud storage. Can sync either all loaded characters or only a single one.
     * @param {Map.<CharacterId,CharacterRecord>} allCharacters - Reference to the live map of all loaded characters (including soft-deleted) managed by the app. This map is owned by {@link OverviewPresenter} but may be modified by this method.
     * @param {CharacterRecord} [singleRecord=null] - A single record from the map of allCharacters that is the only record that should be synced. Can be null if all characters should be synced. 
     * @returns {Promise.<number,(SyncDisabled|Error)>} - Resolves to the timestamp (epoch millis UTC) when the sync was executed, or rejects with an error. If syncing failed because no valid auth token is currently configured, {@link SyncDisabled} is returned to signal that a retry is useless.
     */
    async synchronizeWithCloud(allCharacters, singleRecord = null) {
        if(!(allCharacters instanceof Map)) {
            throw new Error(`AllCharacters parameter has to be a Map but was ${typeof allCharacters}`);
        }
        /** @type {Map.<CharacterId,CharacterRecord>} - Either a shallow copy of allCharacters or a new map containing only singleRecord. Loop over this map because we may modify the live map during the loop. */
        let localRecords = null;
        if(singleRecord) {
            if(!(singleRecord instanceof CharacterRecord)) {
                throw new Error(`SingleRecord has to be a CharacterRecord but was ${typeof singleRecord}`);
            }
            const asMap = new Map();
            asMap.set(singleRecord.getId(), singleRecord);
            localRecords = asMap;
        } else {
            localRecords = new Map(allCharacters);   // localRecords will be the Map of records we want to sync with the cloud. We make a shallow-copy here in case someone adds/removes entries while we are talking to the server.
        }
        /** @type {Object.<CharacterId,object>} - a simple object map containing state information for each character that describes when this character was last updated and synced and if it was soft-deleted. */
        const payload = {};
        localRecords.forEach(record => {
            const db = record.getDbRecord();
            const state = {
                changed: db.updated
            };
            if(db.synced) {
                state.synced = db.synced;
            }
            if(db.deleted) {
                state.deleted = true;
            }
            payload[record.getId()] = state;
        });
        const body = {};
        body[singleRecord ? "check" : "check-all"] = payload;
        const syncTime = Date.now();
        let response = await this.#authenticatedFetch("/check_sync", JSON.stringify(body));
        if(!response.ok) {
            if(response.status === 401) {
                this.setApiToken("");
                throw new SyncDisabled("Token invalid");
            }
            throw new Error("Bad Request");
        }
        /** @type {Object.<CharacterId,object>} - A simple object map indexed by record id with cloud state (see Synchronizer.php) */
        const checkResult = (await response.json()).state;
        /** @type {CharacterRecord[]} - list of characters that need to be uploaded to the cloud */
        const uploads = [];
        const runParallel = [];
        for(const id in checkResult) {
            const result = checkResult[id];
            const record = localRecords.get(id);     // Assumes that this is a live reference to the original record. Is undefined if result.action is "create". 
            switch(result.action) {     // the action reported by the server: this is what the local app should do with the affected character.
                case "create":      // The character exists on remote only because it was created on another device -> create locally. Can never happen for singleUpdates because the character definitely existed locally already (was != null)
                    runParallel.push(this.#presenter.getPersistence().insertCloudRecord(result.character, result.changed, syncTime).then(newRecord => allCharacters.set(newRecord.getId(), newRecord)));
                    break;
                case "delete":      // The character does not exist on remote anymore -> delete locally. Will also be reported for characters that are already marked as soft-deleted locally.
                    // TODO: should we only delete it if it had already the deleted flag locally and else ask the user if they want to still keep a copy?
                    runParallel.push(this.#presenter.getPersistence().destroyCharacter(record).then(() => allCharacters.delete(record.getId())));
                    break;
                case "upload":      // The character never existed remotely -> upload local version to cloud.
                    uploads.push(record);       // we upload first and then update local storage in case something goes wrong
                    break;
                case "update":      // The character was edited on another device -> update local version with cloud data.
                    record.synced(syncTime, result.character, result.changed);
                    runParallel.push(this.#presenter.getPersistence().updateCharacter(record, false));
                    break;
                case "conflict":    // The character was either edited both locally and remotely, or deleted on one side -> ask user to resolve the conflict.
                    record.conflict(syncTime, result.character, result.changed);    // nothing to store on the db here
                    break;
                case "keep":        // Remote and local version are identical -> just update local sync time, nothing else.
                    record.synced(syncTime);
                    runParallel.push(this.#presenter.getPersistence().updateCharacter(record, false));
                    break;
            }
        }
        await Promise.all(runParallel);
        if(uploads.length > 0) {
            await this.#uploadToCloudAndUpdateSyncTime(uploads);
        }
        // in case of a conflict show conflicting file under existing and ask user what to do: if user decides to overwrite local, nothing has to be uploaded. If user decides to keep local as newest, set its change to now and try to upload (send original change time to server in case someone has uploaded another version in the meantime). If user decides to keep both versions, then keep the remote version as it is and change the id of the local version to something new.
        return syncTime;
    }
    /**
     * @param {CharacterRecord[]} uploads - Collection of new characters to be uploaded to the cloud.
     * @returns {Promise.<CharacterRecord[],(SyncDisabled|Error)>} - Resolves when all characters were uploaded and their local db entries updated, or rejects with an error.
     */
    async #uploadToCloudAndUpdateSyncTime(uploads) {
        const payload = {};
        uploads.forEach(record => {
            payload[record.getId()] = {
                changed: record.getDbRecord().updated,
                character: record.getDbRecord().character
            };
        });
        const body = {
            update: payload
        };
        const response = await this.#authenticatedFetch("/update_sync", JSON.stringify(body));
        if(!response.ok) {
            if(response.status === 401) {
                throw new SyncDisabled("Token invalid");
            }
            throw new Error("Upload to cloud failed");
        }
        return Promise.all(uploads.map(record => {
            record.synced(record.getDbRecord().updated);    // set the sync time to the last update on this character, not to the current time (syncTime) so that we have a chance later to figure out on server-side if there is a delete conflict.
            return this.#presenter.getPersistence().updateCharacter(record, false);
        }));
    }
    /**
     * @returns {string} - Returns the user's email if configured or an empty string.
     */
    getEmail() {
        return this.#dbRecord.email;
    }
    /**
     * @param {string} email - The app user's email if they want to use cloud sync.
     * @returns {Promise.<void,PersistenceError>} - Resolves if the email was successfully persisted in local db, else rejects with error.
     */
    async setEmail(email) {
        if(this.#dbRecord.email === email) {
            return;
        }
        this.#dbRecord.email = email;
        return this.#storeSettings();
    }
    /**
     * @returns {string} - Returns the configured auth token used for synchronization API calls, or an empty string.
     */
    getApiToken() {
        return this.#dbRecord.token;
    }
    /**
     * @param {string} token - The auth token needed for API call. This is normally requested from the API and send to the user's email address.
     * @returns {Promise.<void,PersistenceError>} - Resolves if the token was successfully persisted in local db, else rejects with error.
     */
    async setApiToken(token) {
        if(this.#dbRecord.token === token) {
            return;
        }
        if(token) {
            this.#tokenRequested = false;
        }
        this.#dbRecord.token = token;
        return this.#storeSettings();
    }
    /**
     * Checks if the configured API auth token is valid by doing a ping.
     * @returns {Promise.<Response>} - Resolves with the fetch response. The response is ok (204) if the token is valid and the server could be contacted, with not-ok (401) if the token is invalid, or any other not-ok result if the server cannot be reached.
     */
    checkApiToken() {
        return this.#authenticatedFetch("/ping");
    }
    /**
     * Requests a new API auth token for the given user. Requires the email to be configured.
     * @param {boolean} userTriggered - Flag to indicate if this method was triggered explicitly by the user (e.g. via an UI interaction) or implicitly by code.
     * @returns {Promise.<("ok"|"invalid-email"|"too-many-requests"|"bad-connection")>} - Returns a status string defining if the request was successful. Never rejects.
     */
    async requestApiToken(userTriggered = true) {
        if(!this.#dbRecord.email) {
            return "invalid-email";
        }
        if(this.#tokenRequested && !userTriggered) {
            return "too-many-requests";
        }
        this.#tokenRequested = true;
        const body = new URLSearchParams();
        body.set("email", this.#dbRecord.email);
        const aborter = new AbortController();
        const timeout = setTimeout(() => aborter.abort(), 30000);
        try {
            const response = await this.#presenter.fetch(this.#apiBaseUrl + "/request_token", {
                method: "POST",
                headers: {
                    'Content-Type': "application/x-www-form-urlencoded"
                },
                body: body,
                cache :"no-store",
                signal: aborter.signal
            });
            clearTimeout(timeout);
            if(response.ok) {
                return "ok";
            }
            return response.status === 429 ? "too-many-requests" : "invalid-email";
        } catch(error) {    // AbortError (timeout) or other network issues
            this.#tokenRequested = false;
            return "bad-connection";
        }
    }
    /**
     * Loads the settings from the local database. Call this method once at app startup before using any other methods of this class!
     * @returns {Promise.<true>}
     */
    async loadSettings() {
        this.#dbRecord = await this.#presenter.getPersistence().loadSettings();
        return true;
    }
    async #storeSettings() {
        return this.#presenter.getPersistence().updateSettings(this.#dbRecord);
    }
    /**
     * Fetch request to an API endpoint.
     * @param {string} endpoint - API endpoint (path) to call. Will be appended to the base server URL.
     * @param {?object} [json=null] - Optional JSON object that is posted as request body. If missing a get request is executed.
     * @returns {Promise.<Response>} - The fetch response. Will be non-ok (401) if no email or API auth token is currently configured.
     */
    #authenticatedFetch(endpoint, json = null) {
        if(!this.#dbRecord.email || !this.#dbRecord.token) {
            return Promise.resolve(new Response(null, {status: 401}));
        }
        const headers = {
            'Authorization': btoa(this.#dbRecord.email) + ":" + btoa(this.#dbRecord.token)      // base64 encoded email:token
        }
        if(json) {
            headers['Content-Type'] = "application/json";
        }
        return this.#presenter.fetch(this.#apiBaseUrl + endpoint, {
            method: json ? "POST" : "GET",
            headers: headers,
            body: json,
            cache :"no-store"
        });
    }
    get #apiBaseUrl() {
        return "https://earthdawn.behindthemirrors.de/pwa/api";
    }
}

/**
 * Helper class to check if a newer app version is available on the server and force its the installation without having to manually close the app or relying on the browser to check for updates.
 * Normally a browser will check for updates of the {@link ServiceWorker} or app manifest only every couple of hours without consulting local caches. This can have the effect that a newer version
 * is not found immediately. And even if an update was downloaded, the service worker is normally not replaced until the app is closed and re-opened again.
 * The AppUpdater tries to mitigate all this.
 */
class AppUpdater {
    #presenter;
    /** @type {?string} - Current app version string as received from the {@link ServiceWorker}. Null initially until polled. */
    #version = null;
    /** @type {?string} - The new version available on the server or null if the current version is up-to-date. Also null initially until polled. */
    #updateAvailable = null; 
    /** @type {?Date} - Time of the last poll to check for updates. Also set when the server could not be contacted but the current app version was received. */
    #lastCheckTime = null;
    /**
     * @param {Presenter} presenter - Window presenter 
     */
    constructor(presenter) {
        this.#presenter = presenter;
    }
    /**
     * Asks the {@link ServiceWorker} for the current app version and if there is a newer version available at the server. 
     * We store the app version in the service worker and also use it to name the cache. The service worker is able to fetch the information directly from the server circumventing any caches.
     * @returns {Promise.<boolean,string>} - Resolves to true if there is a new update available else false. Rejects on error.
     */
    async pollServer() {
        return new Promise((resolve, reject) => {
            this.#presenter.registerMessageListener("appVersion", (type, data) => {
                this.#version = data.activeVersion;
                if(data.error) {
                    return reject(data.error);
                }
                this.#updateAvailable = (data.activeVersion !== data.serverVersion) ? data.serverVersion : null;
                this.#lastCheckTime = new Date();
                resolve(this.hasWaitingUpdate());
            }).then(() => this.#presenter.postMessage("checkForUpdate")).catch(e => reject(e));   // ServiceWorkerRegistration.update() may use cached data and thus would not see new server versions. We therefore fetch from remote explicitly, thus updating the browser cache, before we later trigger the update.
        });
    }
    /**
     * @returns {?string} - Returns the current app version. Call this after {@link AppUpdater#pollServer}.
     */
    getCurrentVersion() {
        return this.#version;
    }
    /**
     * @returns {?Date} - Returns the last time we checked for updates. May be null if the poll failed. Call this after {@link AppUpdater#pollServer}.
     */
    getLastCheckTime() {
        return this.#lastCheckTime;
    }
    /**
     * @returns {?string} - Returns the new app version if there is one available on the server. May be null if the poll failed. Call this after {@link AppUpdater#pollServer}.
     */
    getAvailableUpdate() {
        return this.#updateAvailable;
    }
    /**
     * @returns {boolean} - Returns true if there is a new app version.  Call this after {@link AppUpdater#pollServer}.
     */
    hasWaitingUpdate() { 
        return this.getAvailableUpdate() != null;
    }
    /**
     * Updates the app immediatly if there is an update available. After triggering the installation it posts a message to the new {@link ServiceWorker} to activate itself immediately. This in turn
     * will trigger an 'controllerchanged' event on the {@link ServiceWorkerContainer} for which we are listening in the {@link Presenter} and then reload the app.
     * @returns {Promise.<void,string>} - Resolves when the update was triggered, else rejects. This method resolves before the update goes live.
     */
    async applyUpdate() {
        if(this.hasWaitingUpdate()) {
            return this.#presenter.updateServiceWorker().then((reg) => {        // make sure that a new ServiceWorker is installed if available (may be a no-op if the browser already did this during the app start). Afterwards the new ServiceWorker should be in status installed (registration.waiting).
                const newWorker = reg.waiting ?? reg.installing;
                newWorker.postMessage({type: "activate"});    // force new ServiceWorker to become active (waiting -> activating -> active) immediately instead of waiting for an app restart (because a window.reload does not free old worker)
            });
        }
        return Promise.reject("No update available");
    }
}


/**
 * Root presenter managing Window/Document references and globally shared objects.
 */
class Presenter {
    #win;
    #doc;
    /** @type {ModalDialog} - Reference to a reusable modal dialog */
    #modal;
    /** @type {Notifications} - Helper to show temporary notifications (toasts) at the top of the screen which will vanish automatically again */
    #notifications;
    /** @type {BackManager} - Helper that manages the browser's history state. Used to trigger back navigation between different sections. */
    #history;
    /** @type {Persistence} - Database used for storing characters and settings */
    #persistence;
    /** @type {CloudSynchronizer} - Helper to store and synchronizes characters in the cloud */
    #cloud;
    /** @type {FileHandler} - Helper to up- and download files from within the app (client-side) */
    #file;
    /**
     * @typedef {object} SectionInfo
     * @property {Object.<string,AbstractSectionPresenter>} presenters - Map of {@link AbstractSectionPresenter}s keyed by their section name.
     * @property {object} current - Information about the currently active section presenter (the own that is showing its UI)
     * @property {?string} current.name - Section name of the active presenter
     * @property {any[]} current.args - Array of optionsl arguments that were passed to {@link AbstractSectionPresenter#show} when it was opened. Used to re-open a previous presenter when the user navigates back to it from another downstream section.
     */
    /** @type {SectionInfo} - Object that contains references to all section presenters */
    #sections;  // object of sections
    /** @type {Map.<string,(type:string, data:object) => void>} - Map of event listeners for messages received from {@link ServiceWorker}. Keyed by type of message. */
    #messageListeners = new Map();
    /**
     * @param {Window} win - The browser window in which the app runs
     */
    constructor(win) {
        this.#win = win;
        this.#doc = win.document;
        this.deferredInstall = null;
    }
    /**
     * Creates a new HTML element owned by the current document but not added to the DOM yet.
     * @param {string} type - Type (tag name) of the element to create
     * @returns {(HTMLElement|HTMLButtonElement|HTMLInputElement|HTMLSelectElement|HTMLOptionElement)} - Returns the newly created, unattached HTML element
     */
    createElement(type) {
        return this.#doc.createElement(type);
    }
    /**
     * Search for a single element in the DOM by CSS selector
     * @param {string} selector - A CSS selector defining which single element to get
     * @param {(Document|Element)} [ancestor=null] - The ancestor element defining the sub-tree in the DOM in which to search for the element. If omitted will use the current document as the tree root.
     * @returns {(HTMLElement|HTMLButtonElement|HTMLInputElement|HTMLSelectElement|HTMLOptionElement|NumberField)} - The first found HTML element or null
     */
    getElement(selector, ancestor = null) {
        ancestor = ancestor ?? this.#doc;
        return ancestor.querySelector(selector);
    }
    /**
     * Search for any number of elements matching the given CSS selector.
     * @param {string} selector - A CSS selector defining which elements to get
     * @param {(Document|Element)} [ancestor=null] -  The ancestor element defining the sub-tree in the DOM in which to search for the element. If omitted will use the current document as the tree root.
     * @returns {NodeList} - A list of found elements. Can be empty, never null.
     */
    findElements(selector, ancestor = null) {
        ancestor = ancestor ?? this.#doc;
        return ancestor.querySelectorAll(selector);
    }
    /**
     * Adds an event listener to the given element for the given event type.
     * @param {EventTarget} element - The target element to which an event listener should be attached.
     * @param {string} eventType - Name of the event to listen for.
     * @param {EventListener} listener - The callback function that is called when the event is fired on the target element.
     * @param {AddEventListenerOptions|boolean} [options=null] - An optional object with additional control settings for the event listener. See {@link EventTarget#addEventListener}
     * @returns {() => void} - Returns a function that removes the attached event listener again when called. Use this for cleanup or set the {@link AbortSignal} in the options argument.
     */
    addObserverTo(element, eventType, listener, options = null) {
        options = options ?? false;
        element.addEventListener(eventType, listener, options);
        return () => { element.removeEventListener(eventType, listener, options); };
    }
    /**
     * Returns the given text with all HTML entities escaped.
     * @param {string} text - Unsafe text to escape
     * @returns {string} - The escaped text
     */
    escapeHtml(text) {
        const dummy = this.createElement("div");
        dummy.textContent = text;
        return dummy.innerHTML;
    }
    /**
     * Make a web request using the browser window's Fetch API
     * @param {RequestInfo | URL} input - The url to call
     * @param {RequestInit} [init=null] - Object to modify the request
     * @returns {Promise.<Response>} - The web response
     */
    fetch(input, init = null) {
        return this.#win.fetch(input, init);
    }
    getModal() {
        return this.#modal;
    }
    getNotifications() {
        return this.#notifications;
    }
    getHistory() {
        return this.#history;
    }
    getPersistence() {
        return this.#persistence;
    }
    getFileHandler() {
        return this.#file;
    }
    /**
     * Enter a new UI section of the app by switching the view to the new section element and putting a frame onto the history stack. Registers a callback that is triggered when the history frame is popped 
     * in order to switch back to the previous section that was active before we entered the new section.
     * @param {string} sectionName - Name of the new {@link AbstractSectionPresenter} that should be shown.
     * @param  {...any} args - Optional arguments to be passed to {@link AbstractSectionPresenter#show} when the new presenter is actived.
     */
    enterSection(sectionName, ...args) {
        const previous = this.#switchTo(sectionName, ...args);
        this.getHistory().enterSection((leave) => this.#sections.presenters[sectionName].close(leave)).then(() => this.#switchTo(previous.section, ...previous.args));
    }
    /**
     * Switches the UI to a new section. First calls {@link AbstractSectionPresenter#hide} on the currently active section (if any), then sets the new section as the active one and calls {@link AbstractSectionPresenter#show}
     * on the presenter of the matching name, passing along the optional arguments. The method returns the details of the previous (now hidden) section to the caller.
     * @param {string} sectionName - Name of the new {@link AbstractSectionPresenter} that should be shown. That's the key used by {@link Presenter#sections#presenters}.
     * @param  {...any} args - Optional arguments to be passed to {@link AbstractSectionPresenter#show} when the new presenter is actived.
     * @returns {{section:string, args:any[]}} - Returns an object that contains the name and optional arguments that were used when the previous section was activated. Can be used to switch back to this old section again.
     */
    #switchTo(sectionName, ...args) {    // returns the name and arguments of the previous section or null/empty.
        const {name: oldSection, args: oldArgs} = this.#sections.current;
        if(oldSection) {
            this.#sections.presenters[oldSection].hide();
        }
        this.#sections.current.name = sectionName;
        this.#sections.current.args = args;
        this.#sections.presenters[sectionName].show(...args);
        return {section: oldSection, args: oldArgs};
    }
    isMobileDevice() {
        return this.#win.matchMedia("only screen and (hover: none), only screen and (pointer: coarse), only screen and (display-mode: standalone)").matches;    // if device does not support hovering or the primary pointer it not very accurate (not a mouse) or the app runs in standalone mode without browser UI, then treat this as a mobile device.
    }
    async #getServiceWorkerRegistration() {     // aborts if served from file-system. for local testing where service worker is never installed
        if(this.#win.location.protocol === "file:") {
            console.warn("Service worker is not supported locally when served from file system");
            return Promise.reject("Service worker not supported");
        }
        return navigator.serviceWorker.ready;
    }
    /**
     * Posts a new message to the active {@link ServiceWorker}.
     * @param {string} type - Type of the message. This value is automatically added to the data object before it is posted.
     * @param {object} [data={}] - Optional additional data to be passed along in the message. Must not contain a type property because that is added to it by this method.
     */
    async postMessage(type, data = {}) {
        if(!(data instanceof Object)) {
            throw new Error(`Data must an object but was ${data}`);
        }
        if(data.type) {
            throw new Error(`Data object must not contain a type property but had ${data.type}`);
        }
        if(!type || typeof type !== "string") {
            throw new Error(`Type must be non-empty String but was ${type}`);
        }
        data.type = type;
        const worker = await this.#getServiceWorkerRegistration().then(registration => registration.active);
        worker.postMessage(data);
    }
    /**
     * Registers a listener that receives messages posted by the active {@link ServiceWorker} back to this application. Only a single listener per type can be registered.
     * Registering a new listener will overwrite an existing one.
     * @param {string} type - The type of the message this listener should listen to.
     * @param {(type:string, data?:object) => void} listener - The callback that receives the message. Is called with the type for which it was registered and an optional data object with additional arguments.
     */
    async registerMessageListener(type, listener) {     // listens for messages send from the ServiceWorker. Listener has to be a function (type:string, data:object)
        if(!type || typeof type !== "string") {
            throw new Error(`Type must be non-empty String but was ${type}`);
        }
        if(!(listener instanceof Function)) {
            throw new Error(`Listener must be a callback function`);
        }
        this.#messageListeners.set(type, listener);
    }
    /**
     * Asks the browser to install a new ServiceWorker if available. The new worker is not directly activated but after installing stays in status installed (reg.waiting) until the client (app) is restarted. 
     * May not download the latest version automatically if cached, therefore we have to poll this ourselves first. Reload the app afterwards (else we would have to re-register the message handler).
     * @returns {Promise.<ServiceWorkerRegistration>} - Resolves after updating
     */
    async updateServiceWorker() {
        return this.#getServiceWorkerRegistration().then(registration => registration.update().then(() => registration));
    }
    /**
     * Receives messages posted by a {@link ServiceWorker}. Assumes that the data object contains a type string. Looks up a matching event listener that was registered for that type and calls it with the given event data.
     * @param {MessageEvent} event - The message received from the worker.
     */
    async #handleMessage(event) {
        const data = event.data;
        const type = data.type;
        if(!type || typeof type !== "string") {
            throw new Error(`Type must be non-empty String but was ${type}`);
        }
        delete data.type;
        const listener = this.#messageListeners.get(type);
        if(listener) {
            listener(type, data);
        }
    }
    #load() {
        navigator.serviceWorker.addEventListener("message", (event) => this.#handleMessage(event));     // receive messages from active ServiceWorker (controller).
        navigator.serviceWorker.addEventListener("controllerchange", (event) => {       // reload app after a new ServiceWorker was installed and activated. See AppUpdater#applyUpdate
            this.#win.location.reload();
        });
        this.#history = new BackManager(this.#win, this);
        this.#modal = new ModalDialog(this);
        this.#notifications = new Notifications(this);
        this.#persistence = new Persistence(this.#win);
        this.#cloud = new CloudSynchronizer(this);
        this.#file = new FileHandler(this);
        this.#sections = {
            presenters: {},
            current: {
                name: null,
                args: []
            }
        };
        const appUpdater = new AppUpdater(this);
        const viewPresenter = new CharacterViewPresenter(this);
        const editCharacterPresenter = new CharacterEditPresenter(this, viewPresenter);
        this.#sections.presenters[OverviewPresenter.sectionName] = new OverviewPresenter(this, this.#cloud, appUpdater);
        this.#sections.presenters[SettingsPresenter.sectionName] = new SettingsPresenter(this, this.#cloud, appUpdater);
        this.#sections.presenters[CreationPresenter.sectionName] = new CreationPresenter(this, viewPresenter, editCharacterPresenter);
        this.#sections.presenters[CharacterViewPresenter.sectionName] = viewPresenter;
        this.#sections.presenters[CharacterEditPresenter.sectionName] = editCharacterPresenter;
        this.#sections.presenters[DisciplineEditPresenter.sectionName] = new DisciplineEditPresenter(this, viewPresenter, editCharacterPresenter);
        this.#switchTo(OverviewPresenter.sectionName);
    }
    /**
     * Handles the BeforeInstallPrompt window event by storing the prompt event for later. Then shows an install button that asks the user to install this progressive web app
     * to their home screen. If the user decides to install the app, we trigger the browser's prompt from the original event in order to show the official install dialog.
     * @param {Event} event - The BeforeInstallPromptEvent
     * @param {Presenter} presenter - The app's main presenter
     */
    static #manageInstallPrompt(event, presenter) {
        event.preventDefault();
        presenter.deferredInstall = event;

        const installBanner = presenter.getElement("#install");
        installBanner.hidden = false;

        const aborter = new AbortController();
        const installButton = presenter.getElement("#overview .header .install");
        presenter.addObserverTo(installButton, "click", () => {
            const text = "Do you want to install this progressive web app to your mobile home screen or computer desktop for easier access and better user experience?";
            presenter.getModal().openConfirmation("Install to Home Screen", text, "No, thanks", "Yes, install").then(() => {
                presenter.deferredInstall.prompt();
                presenter.deferredInstall.userChoice.then((choice) => {
                    if(choice.outcome === 'accepted') {
                        presenter.deferredInstall = null;
                        aborter.abort();
                        installButton.hidden = true;
                    }
                });
            }, () => {
                presenter.deferredInstall = null;
                aborter.abort();
                installButton.hidden = true;
            });
        },{signal: aborter.signal});
        presenter.addObserverTo(installBanner, "click", () => {
            presenter.getModal().openConfirmation("Install to Home Screen", "You can install this progressive web app to your mobile home screen or computer desktop for easier access and better user experience.\nDo you want to do this now?", "Maybe later", "Yes, install").then(() => {
                presenter.deferredInstall.prompt();
                presenter.deferredInstall.userChoice.then((choice) => {
                    if(choice.outcome === 'accepted') {
                        presenter.deferredInstall = null;
                    } else {
                        installButton.hidden = false;
                    }
                });
                installBanner.hidden = true;
            }, () => {
                installButton.hidden = false;
                installBanner.hidden = true;
            });
        }, {once: true});

        presenter.addObserverTo(window, 'appinstalled', () => {
            aborter.abort();
            installButton.hidden = true;
            installBanner.hidden = true;
            presenter.deferredInstall = null;
        });
    }
    /**
     * Creates the initial instance of this presenter, which in turn loads all other presenters to control the HTML UI.
     * @param {Window} window - The current browser window in which this app runs
     */
    static create(window) {
        const presenter = new Presenter(window);
        if (window.document.readyState === 'loading') {
            window.document.addEventListener('DOMContentLoaded', () => presenter.#load());
        } else {
            presenter.#load();
        }
        window.addEventListener('beforeinstallprompt', (e) => {
            Presenter.#manageInstallPrompt(e, presenter);
        });
    }
}

if('serviceWorker' in navigator) {
    navigator.serviceWorker.register('./service-worker.js');
};
  
Presenter.create(window);