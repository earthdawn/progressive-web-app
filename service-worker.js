// @ts-check
/// <reference no-default-lib="false"/>
/// <reference lib="es2022" />
/// <reference lib="webworker" />

const version = "1.2.10";   // make sure to minify this file to remove the comments above so that the version is in the first line, else our regex below does not work.
const cacheName = "earthdawn-pwa-v" + version;   // increase version to force fetching the new content

/**
 * @typedef {ServiceWorkerGlobalScope& typeof globalThis} Self
 */

/**
 * Installing Service Worker
 */
/** @type {Self} */ (self).addEventListener('install', (event) => {
    event.waitUntil((async () => {
        const cache = await caches.open(cacheName);
        const contentToCache = [
            './app.js',
            './data.js',
            './fontawesome/webfonts/fa-brands-400.ttf',
            './fontawesome/webfonts/fa-brands-400.woff2',
            './fontawesome/webfonts/fa-regular-400.ttf',
            './fontawesome/webfonts/fa-regular-400.woff2',
            './fontawesome/webfonts/fa-solid-900.ttf',
            './fontawesome/webfonts/fa-solid-900.woff2',
            './fontawesome/css/all.min.css',
            './styles.css',
            './images/icon-192.png',
            './images/icon-512.png',
            './images/icon-512-maskable.png',
            './index.html',
            './'
        ];
        await cache.addAll(contentToCache);
    })());
});

/**
 * Clean up old caches after we introduce a new service worker (with new cache version)
 */
/** @type {Self} */ (self).addEventListener('activate', (event) => {
    event.waitUntil(caches.keys().then((keyList) => {
        return Promise.all(keyList.map((key) => {
            if (key === cacheName) { 
                return Promise.resolve(true); 
            }
            return caches.delete(key);
        }))
    }));
});
  
/** 
 * Fetching content using Service Worker. Will retrieve responses from {@link Cache} first except for requests with cache mode 'no-store' or 'no-cache' and for files with extension '.webmanifest'.
 * If the cache does not contain a match, it fetches the file from the server and then stores it into the cache before returning. For requests with mode 'no-store' or 'no-cache' we always ask
 * the server first. 'no-store' requests are never put into the cache. This mode should be used for {@link CloudSynchronizer} API requests.
 */
/** @type {Self} */ (self).addEventListener('fetch', (event) => {
    event.respondWith((async () => {
        const remoteOnly = event.request.cache === "no-store";  // do not cache API calls that always have to return fresh data.
        const remoteElseCache = event.request.cache === "no-cache" || event.request.url.endsWith(".webmanifest");   // prefer remote (network) results and fallback to cache only on error.
        if(!remoteOnly && !remoteElseCache) {   // default: serve from cache first if available, else from remote.
            const found = await caches.match(event.request);
            if (found) {
                return found;
            }
        }
        try {
            const response = await fetch(event.request.clone());
            if(response.ok) { 
                if(!remoteOnly) {
                    const toCache = response.clone();
                    event.waitUntil(caches.open(cacheName).then(cache => cache.put(event.request, toCache)));
                }      
            } else if(remoteElseCache) {
                const found = await caches.match(event.request);
                if (found) {
                    return found;
                }
            }
            return response;
        } catch(error) {
            if(remoteElseCache) {
                const found = await caches.match(event.request);
                if (found) {
                    return found;
                }
            }
            throw error;
        }
    })());
});

/**
 * Handle messages sent from the client (app.js) and then calls an internal function here on this/self with a name matching the message type.
 */
/** @type {Self} */ (self).addEventListener('message', (event) => {
    const data = event.data;
    const type = data.type;
    const client = event.source;    // can be used to post messages back to the client
    if(this[type] instanceof Function) {
        event.waitUntil(this[type](client, data));
    }
});

/**
 * Called by client (via messaging) to get information about currently installed version and possible update on server
 * @param {Client} client 
 */
async function checkForUpdate(client) {     
    const result = {
        type: "appVersion",
        activeVersion: version,
        serverVersion: version
    }
    await fetch(self.location.href, {method: "GET", cache: "no-cache"}).then(response => {      // we fetch the latest service-worker.js file from the server because it contains the version number as a constant.
        if(response.ok) {
            return response.text();
        }
        throw new Error(response.statusText);
    }).then(body => {
        const matches = body.match(/^\s*const\s+version\s*=\s*"(.+?)"/);    // has to match first line of this service-worker file. Be careful: minifying may change whitespace and replace trailing semi-colon with comma.
        if(!matches || matches.length !== 2) {
            throw new Error("No version found in response");
        }
        result.serverVersion = matches[1];
    }).catch(error => {
        result.error = error;
    });
    client.postMessage(result);     // always returns at least the current version
}

/**
 * Called by client (via messaging) to force an immediate activation of a newly updated ServiceWorker (new version). Afterwards the client has to reload itself.
 * @returns {Promise.<void>}
 */
async function activate() {
    return /** @type {Self} */ (self).skipWaiting().then(() => /** @type {Self} */ (self).clients.claim());
}