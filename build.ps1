# Build a release of the PWA by minifying apps.js, service-worker.js and styles.css and copying everything else into /release folder. Deploy from there!
# We do not minify data.js because it is not worth it, the file is already very compact.

# Usage: place cursor onto last line of this script and select Run To Cursor from right-click context menu. Running it directly from powershell may otherwise result in permission error.

function Minify($uri, $paths) {
    foreach($file in $paths) {
        $body = @{input = Get-Content -Path $file -Raw}
        Invoke-WebRequest -Uri $uri -Method "POST" -Body $body -OutFile "$($targetFolder)$($file)" -ContentType "application/x-www-form-urlencoded"
        Write-Output "Minified file $($file)"
    }
}


Write-Progress -Activity "Releasing PWA" -CurrentOperation "Creating output folder" -PercentComplete 0

$targetFolder = "release\"
if(Test-Path $targetFolder) {
    Remove-Item -Path $targetFolder -Recurse
    Write-Output "Deleted existing folder $($targetFolder)"
}
New-Item -Path $targetFolder -ItemType Directory > $null

Write-Progress -Activity "Releasing PWA" -CurrentOperation "Minifying Javascript" -PercentComplete 10

Minify "https://www.toptal.com/developers/javascript-minifier/api/raw" @("app.js", "service-worker.js")

Write-Progress -Activity "Releasing PWA" -CurrentOperation "Minifying CSS" -PercentComplete 70

Minify "https://www.toptal.com/developers/cssminifier/api/raw" @("styles.css")

Write-Progress -Activity "Releasing PWA" -CurrentOperation "Copying all other files" -PercentComplete 90

Copy-Item -Path "data.js" -Destination $targetFolder
Copy-Item -Path "index.html" -Destination $targetFolder
Copy-Item -Path "manifest.webmanifest" -Destination $targetFolder
Copy-Item -Path "favicon.ico" -Destination $targetFolder
Copy-Item -Path "images" -Destination "$($targetFolder)images" -Recurse
Copy-Item -Path "fontawesome" -Destination "$($targetFolder)fontawesome" -Recurse

Write-Progress -Activity "Releasing PWA" -PercentComplete 100
Write-Output "Release built successfully at $($targetFolder)"
