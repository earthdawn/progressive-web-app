# Intro
This is a self-contained progressive web app (PWA) with a responsive UI for managing characters for the Earthdawn RPG.

At the moment it is optimized for mobile usage only. On larger screens a mobile-sized overlay is used. This may change in the future, the code should be flexible enough to create a responsive design also for tablets or desktop resolutions.

You can see and use the app in action here: https://earthdawn.behindthemirrors.de/pwa/  
It was tested on latest Chrome and Firefox browsers.

# Disclaimer
This is an unofficial fan project. It is not approved or endorsed by the official Earthdawn license holder/copyright owner.
Earthdawn is a registered trademark of [FASA Corporation](https://fasagames.com/). Copyright of Earthdawn 1993-now FASA Corporation.
